from ocr_ctc.preprocessings.BatchPreprocessing import BatchPreprocessing

import numpy as np
from PIL import Image, ImageDraw
from ocr_ctc.preprocessings.batch_preprocessing.ImagesFromBaseline import ImagesFromBaseline
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence


class To2DSequenceFromBaseline(BatchPreprocessing):

    def __init__(self, baselines, page_images, height=0, points_under=None, nb_cols=6, stride=1, pad=False,\
                 win_structure='vertical', keep_ratio=True ,rate_under=0.25):
        # window structure:
        #   'vertical' = follow the baseline but vertical per frame
        #   'horizontal' = the window is construct according to an horizontal baseline at the middle point
        #   'incline' = incline according to the baseline

        self.nb_cols = nb_cols if nb_cols>0 else 1
        self.stride = stride if stride>0 else 1
        self.pad = pad
        self.height = height
        if not points_under:
            self.points_under = int(self.height * rate_under)
        self.rate_under = rate_under
        self.sup_points = 2
        self.points_upper = self.height - self.points_under
        self.baselines = baselines
        self.page_images = page_images
        self.win_structure = win_structure
        self.counter = 0
        self.keep_ratio = keep_ratio


    def __get_vertical_frame(self, image, pos, pt_baseline, padding):

        try:
            new_frame = [image[pos, l] \
                         for l in range(pt_baseline - self.points_under, pt_baseline, 1)] \
                        + [image[pos, l] for l in range(pt_baseline, pt_baseline + self.points_upper, 1)]
        except:
            print("The image built is outside the original image. Padding is performed.")
            _, iheight = image.size
            new_frame = []
            for l in range(pt_baseline - self.points_under, pt_baseline, 1):
                if l < 0 or l > iheight:
                    new_frame.append(padding)
                else:
                    new_frame.append(image[pos,l])
            for l in range(pt_baseline, pt_baseline + self.points_upper, 1):
                if l < 0 or l > iheight:
                    new_frame.append(padding)
                else:
                    new_frame.append(image[pos,l])

        return new_frame


    def __get_baseline_middle_point(self, pos, sorted_baseline, pos_k=0):

        middle_pt = (2 * pos + self.nb_cols) / 2
        k = pos_k
        while k < len(sorted_baseline) and middle_pt < sorted_baseline[k][0]:
            k += 1
        if k == len(sorted_baseline):
            k=k-1

        if k == 0:
            a, b = get_straight_line(sorted_baseline[0][0], sorted_baseline[0][1], sorted_baseline[1][0],
                                     sorted_baseline[1][1])
        else:
            a, b = get_straight_line(sorted_baseline[k - 1][0], sorted_baseline[k - 1][1], sorted_baseline[k][0],
                                     sorted_baseline[k][1])
        y_baseline_mpt = a * middle_pt + b

        return y_baseline_mpt

    def __get_baseline_middle_point2(self, pos, sorted_baseline):

        middle_pt = (sorted_baseline[pos][0] + sorted_baseline[pos+1][0]) / 2

        a, b = get_straight_line(sorted_baseline[pos][0], sorted_baseline[pos][1], sorted_baseline[pos+1][0],
                                     sorted_baseline[pos+1][1])
        y_baseline_mpt = a * middle_pt + b

        return y_baseline_mpt


    def get_vertical(self, x, y, seq_len, label_len, padding):

        ImFB = ImagesFromBaseline(self.baselines, self.page_images, height=self.height, points_under=self.points_under, rate_under=0.25)
        x_ret, y, seq_len_ret, label_len, padding = ImFB.__call__(x, y, seq_len, label_len, padding)

        t2Ds = To2DSequence(nb_cols=self.nb_cols, stride=self.stride, pad=self.pad)
        x_out, _, seq_len_out, _, _ = t2Ds.__call__([np.asarray(x_ret[0].convert('L'))], y, seq_len_ret, label_len, padding)

        return x_out, y, seq_len_out, label_len, padding


    def get_horizontal(self, x, y, seq_len, label_len, padding):

        new_x = []
        new_seq_len = []

        for idx, image in enumerate(x):

            # check baseline validity
            sorted_baseline = sorted(self.baselines[self.counter])
            page_im_file = self.page_images[self.counter]
            page_im = Image.open(page_im_file)
            page_im = np.asarray(page_im.convert('L'))

            self.counter += 1

            if len(sorted_baseline) == 0:
                print(
                    "Image from baseline preprocessing: there is no baseline. Can\'t do it.\nStandard To2DSequence is performed instead.")
                t2Ds = To2DSequence(nb_cols=self.nb_cols, stride=self.stride, pad=self.pad)
                x_out, _, seq_len_out, _, _ = t2Ds.__call__([image], [y[idx]], [seq_len[idx]],
                                                            [label_len[idx]], padding)
                new_x.append(image)
                new_seq_len.append(seq_len[idx])
                continue
            elif len(sorted_baseline) == 1:
                print(
                    "Image from baseline preprocessing: there is only one point in the baseline. Can\'t do it.\nStandard To2DSequence is performed instead.")
                t2Ds = To2DSequence(nb_cols=self.nb_cols, stride=self.stride, pad=self.pad)
                x_out, _, seq_len_out, _, _ = t2Ds.__call__([image], [y[idx]], [seq_len[idx]],
                                                            [label_len[idx]], padding)
                new_x.append(image)
                new_seq_len.append(seq_len[idx])
                continue


            w_lim, h_lim = image.size#shape
            w_pim, h_pim = page_im.shape


            # # Get new sequence length (according to the window siz, stride and padding
            # sh = image.shape
            # index_start_windows = [idx_start_window for idx_start_window in range(0, sh[0], self.stride)]
            # selected_start_windows = [start_window for start_window in index_start_windows if
            #                           start_window <= sh[0] - self.nb_cols + 1]
            # nb_windows = len(selected_start_windows)
            # new_len = selected_start_windows[-1]
            #
            # if self.pad:  #  padding less than the half of the window
            #     pad_elmt = int((max([start_window for start_window in index_start_windows if
            #                          start_window < sh[0] - self.nb_cols / 2]) - new_len) / self.stride)
            #     total_len = nb_windows + 2 * pad_elmt
            # else:
            #     pad_elmt = 0
            #     total_len = nb_windows
            #
            # # DEFINE WINDOWS
            # new_arr = np.zeros((total_len, self.nb_cols, sh[1], 1), "float32")
            # index_window = 0
            #
            # # padding at the beginning of the sequence
            # pos_k = 0
            # if pad_elmt > 0:
            #
            #     for i in range(-pad_elmt, 0, self.stride):
            #
            #         y_baseline_mpt = self.__get_baseline_middle_point(i, sorted_baseline, pos_k=pos_k)
            #
            #         for j in range(self.nb_cols):
            #             if i + j >= 0:
            #                 vect_window = self.__get_vertical_frame(image, i+j, y_baseline_mpt, padding)
            #             else:
            #                 vect_window = [padding for k in range(sh[1])]
            #
            #             new_arr[index_window][j] = np.reshape(vect_window, (sh[1], 1))
            #
            #         index_window += 1
            #
            # #  build sequence of windows for complete sub-images (without padding)
            # for i in range(0, new_len, self.stride):
            #     y_baseline_mpt = self.__get_baseline_middle_point(i, sorted_baseline, pos_k=pos_k)
            #     for j in range(self.nb_cols):
            #         vect_window = self.__get_vertical_frame(image, i+j, y_baseline_mpt, padding)
            #         new_arr[index_window][j] = np.reshape(vect_window, (sh[1], 1))
            #
            #     index_window += 1
            #
            # # padding at the end of the sequence
            # if pad_elmt > 0:
            #     for i in range(pad_elmt):
            #         y_baseline_mpt = self.__get_baseline_middle_point(new_len + i, sorted_baseline, pos_k=pos_k)
            #         for j in range(self.nb_cols):
            #             if new_len + i + j < sh[0]:
            #                 vect_window = self.__get_vertical_frame(image, new_len + i + j, y_baseline_mpt, padding)
            #             else:
            #                 vect_window = [padding for k in range(sh[1])]
            #
            #             new_arr[index_window][j] = np.reshape(vect_window, (sh[1], 1))
            #
            #         index_window += 1
            #
            # x_ret.append(new_arr)
            # seq_len_ret.append(total_len)



            #############
            # new_im = Image.fromarray(image)
            # new_im.save("../../../data/viz_tmp/img" + str(self.counter) + "_line.png", "PNG")
            # new_im.show()
            # new_im = Image.fromarray(page_im)
            # new_im.show()
            #
            new_im = Image.fromarray(page_im)
            drawtxt = ImageDraw.Draw(new_im)
            drawtxt.line(sorted_baseline, width=2)
            new_im.save("../../../../../data/viz_tmp2/img" + str(self.counter) + "_page.png", "PNG")
            # new_im.show()
            # toto = page_im[27:1695][950:1030]
            # new_im = Image.fromarray(toto)
            # new_im.show()

            points_under = int(h_lim * self.rate_under) + self.sup_points
            nb_points = h_lim + 2 * self.sup_points

            new_image = np.zeros((nb_points, int(sorted_baseline[-1][0] - sorted_baseline[0][0])))
            for i in range(len(sorted_baseline) - 1):
                # a, b = get_straight_line(sorted_baseline[i][0], sorted_baseline[i][1], sorted_baseline[i + 1][0],
                #                          sorted_baseline[i + 1][1])
                current_x = sorted_baseline[i][0]
                y_baseline = int(self.__get_baseline_middle_point2(i, sorted_baseline))
                for j in range(int(current_x), int(sorted_baseline[i + 1][0]), 1):
                    #y_baseline = int(a * j + b)
                    try:
                        for k in range(nb_points):
                            val = page_im[y_baseline + points_under - k][j]
                            new_image[nb_points - k - 1, j - int(sorted_baseline[0][0])] = val
                    except:
                        #print("The image built is outside the original image. Padding is performed.")
                        for k in range(nb_points):
                            h_k = y_baseline + points_under - k
                            if h_k >= h_pim or h_k < 0:
                                new_image[nb_points - k - 1, j - int(sorted_baseline[0][0])] = padding
                            else:
                                val = page_im[h_k][j]
                                new_image[nb_points - k - 1, j - int(sorted_baseline[0][0])] = val

            new_image = np.asarray(new_image)
            new_im = Image.fromarray(new_image)
            # new_im.show()
            # new_im2 = Image.fromarray(new_image2)
            # new_im2.show()
            # new_im2 = new_im2.convert('L')
            # new_im2.save("../../../data/viz_tmp/img" + str(self.counter) + "_baseline.png", "PNG")
            # with open("../../../data/viz_tmp/img" + str(self.counter) + ".txt", 'w') as tmptxt:
            #     tmptxt.write(str(y[idx]))

            # resize image
            # if self.keep_ratio:
            #     ratio = self.height / h_lim
            #     new_im = new_im.resize((int(w_lim * ratio), self.height), Image.ANTIALIAS)
            # elif self.height > 0:
            #     new_im = new_im.resize((w_lim, self.height), Image.ANTIALIAS)
            #new_im.save("../../../data/viz_tmp/img2" + str(self.counter) + ".png", "PNG")

            iseq_len = new_image.shape[1]
            new_seq_len.append(iseq_len)
            new_x.append(Image.fromarray(new_image))
            #new_x.append(Image.fromarray(np.transpose(new_image)))

        return new_x, y, new_seq_len, label_len, padding



    def __call__(self, x, y, seq_len, label_len, padding):


        if self.win_structure == 'vertical':
            return self.get_vertical(x, y, seq_len, label_len, padding)
        elif self.win_structure == 'horizontal':
            return self.get_horizontal(x, y, seq_len, label_len, padding)
        elif self.win_structure == 'incline':
            print("not done")

        return x, y, seq_len, label_len, padding


    def __str__(self):
        return "To 2D Sequence"



def get_straight_line(x1, y1, x2, y2):

    a = (y2 - y1)/(x2 - x1)
    b = y2 - a*x2
    return a, b
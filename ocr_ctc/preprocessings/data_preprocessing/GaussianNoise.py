import numpy as np

from ocr_ctc.preprocessings.batch_preprocessing.ImagePreprocessing import ImagePreprocessing
from numpy.random import normal
from numpy import reshape


class GaussianNoise(ImagePreprocessing):
    """
    Normalise dans [-0.5;0.5] les séquences dont les valeurs sont dans [0;255] 
    """
    def __init__(self, **kwargs):

        std = kwargs.pop('std', 0.01)
        self.std = std

    def preprocessing(self, image):

        width, height = image.shape
        noise = reshape(normal(0, self.std, height*width), (width, height))

        return image + noise

    def __str__(self):
        return "Gaussian Noise :"+str(self.std)

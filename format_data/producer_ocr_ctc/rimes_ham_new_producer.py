from ocr_ctc.dataset_manager import Producer
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose

if __name__ == '__main__':
    nb_cols = 2
    stride = 1
    padding = -1.
    padding = -1.
    norm = 100
    keep_ratio = True

    input_path = "../datasets/RIMES_ham/"
    output_path = input_path + "RIMES_ham_" + str(norm) + "_" + str(nb_cols) + "/"

    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
    preprocessing_batch = []

    producer = Producer(input_path, output_path,
                        norm=norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=True,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch
                        ).produce_dataset()

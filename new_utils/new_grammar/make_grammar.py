import sys, os, shutil
sys.path.append('./')
from new_utils.settings import FINAL_CHARSET

def check_text_with_charset(text, flag):
    res = ''
    for char in text:
        if char == ' ':
            res += '|'
        elif char == '|':
            res += '|'
        elif char == '\\':
            res += '\\'
        elif char in FINAL_CHARSET:
            res += char
        # elif flag:
        #     print('flag:',flag)
        #     print('\nthe phrase:',text)
        #     print('contains a letter out of the charset:', char)
        #     decision = input('press Enter to ignore this one, write "yes" to ignore all: ')
        #     if decision == "yes":
        #         print(decision)
        #         flag = False
    return res, flag

def make_string_from_list(list_names):
    res = " "
    flag = True
    for line in list_names:
        line = line.replace('\n','').strip().replace("\"","\\\"")
        print(line)
        if line == '':
            continue
        line, flag = check_text_with_charset(line, flag)
        res += "\""+line+"\".utf8"+' | '
    res = res[:-2]
    return res

def make_list_from_file(file_path):
    items = []
    with open(file_path) as f:
        for line in f.readlines():
            temp = line.replace('\n','').strip()
            if temp not in items:
                items.append(temp)
    return items


def make_grammar(grammar_path, list_names=[]):
    """
    This function takes 2 arguments:
    - The grammar file that wants to be compiled,
    - A list of strings as the lexical list. (optional)
    """
    grammar_name = grammar_path.split('/')[-1].split('.')[0]
    str_list_names = make_string_from_list(list_names)
    with open(grammar_path) as original_f:
        with open('new_utils/new_grammar/make_fst/lexicon.grm', 'w') as new_f:
            for line in original_f.readlines():
                tmp = line
                if 'LIST_TO_REPLACE' in tmp:
                    tmp = tmp.replace('LIST_TO_REPLACE', str_list_names)
                new_f.write(tmp)

    hbdex_root_folder = os.getcwd()
    os.chdir('new_utils/new_grammar/make_fst')
    output_neutral_name = os.path.join("../output", grammar_name)
    commando = 'python3 grammar2fst.py '+output_neutral_name
    os.system(commando)
    os.chdir(hbdex_root_folder)

def make_lexique(list_file_path):
    """
    this function takes as an entry:
    - a .list file with a list of the elements to make a closed lexique
    """
    grammar_name = list_file_path.split('/')[-1].split('.')[0]
    items_list = make_list_from_file(list_file_path)
    str_list_names = make_string_from_list(items_list)
    with open("new_utils/new_grammar/grm_files/1899/only_lexique.grm") as original_f:
        with open('new_utils/new_grammar/make_fst/lexicon.grm', 'w') as new_f:
            for line in original_f.readlines():
                tmp = line
                if 'LIST_TO_REPLACE' in tmp:
                    tmp = tmp.replace('LIST_TO_REPLACE', str_list_names)
                new_f.write(tmp)

    hbdex_root_folder = os.getcwd()
    os.chdir('new_utils/new_grammar/make_fst')
    output_neutral_name = os.path.join("../output", grammar_name)
    commando = 'python3 grammar2fst.py '+output_neutral_name
    os.system(commando)
    os.chdir(hbdex_root_folder)

def main_grammar(lexique_path):
    grammar_path = 'new_utils/new_grammar/grm_files/1899_with_begin_and_end_char/designation_des_valeurs.grm'
    # lexique_path = 'new_utils/new_grammar/designation_des_valeurs.list'
    make_grammar(grammar_path, make_list_from_file(lexique_path))
    name = grammar_path.split('/')[-1].split('.')[0]+"TLG_lexicon.fst"
    grammar_output_folder = 'new_utils/new_grammar/output/'
    grammar_folder_in_use = 'new_utils/thrax/fst_folder/'
    shutil.move(os.path.join(grammar_output_folder, name),\
            os.path.join(grammar_folder_in_use, name))

if __name__ == '__main__':
    # main_grammar(lexique_path)
    make_grammar("new_utils/new_grammar/grm_files/parquet_with_begin_and_end_char/titles.grm", make_list_from_file("new_utils/new_grammar/grm_files/parquet_with_begin_and_end_char/titles.list"))

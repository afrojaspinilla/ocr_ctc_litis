FROM conda/miniconda3:latest as build

RUN conda install -y -c conda-forge thrax

FROM kaldiasr/kaldi:2020-09 as kaldi

COPY --from=build /usr/local/lib/libthrax.so.135.0.0 /lib/libthrax.so.135.0
COPY --from=build /usr/local/lib/libfstfar.so.23.0.0 /lib/libfstfar.so.23.0
COPY --from=build /usr/local/lib/libfst.so.23.0.0 /lib/libfst.so.23.0
COPY --from=build /usr/local/bin/thraxmakedep /usr/local/bin
COPY --from=build /usr/local/bin/thraxcompiler /usr/local/bin

RUN apt update && apt install -y --no-install-recommends \
	time \
	software-properties-common \
	graphviz \
	libreadline-gplv2-dev \
	libncursesw5-dev \
	libssl-dev \
	libsqlite3-dev \
	tk-dev \
	libgdbm-dev \
	libc6-dev \
	libbz2-dev \
	locales-all

ENV LANG=en_IN.utf8
ENV PATH=$PATH:/opt/kaldi/tools/openfst/bin/
ENV PATH=$PATH:/opt/kaldi/src/bin/
ENV PATH=$PATH:/opt/kaldi/src/fstbin

WORKDIR /opt

RUN wget https://www.python.org/ftp/python/3.5.5/Python-3.5.5.tar.xz && \
	tar xvf Python-3.5.5.tar.xz

WORKDIR Python-3.5.5

RUN ./configure && make -j $(nproc) && make install && rm ../Python-3.5.5.tar.xz

RUN pip3 install --upgrade pip
RUN pip3 install tensorflow==1.10.0 keras==2.2.3 editdistance matplotlib pillow unidecode tqdm pytomlpp python-Levenshtein \
	&& apt update

RUN echo 'alias python2="python2.7"' >> ~/.bashrc

WORKDIR /

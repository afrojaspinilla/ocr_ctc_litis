# encoding=utf-8
import getopt
import sys
import os

import numpy as np
from lxml import etree
import shutil

from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.ImagesFromBaseline import ImagesFromBaseline
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose


#import cv2


def HoGFeatures(image, normF):#normF[0] = largeur colonne normF[1] = hauteur de normalisation
     largeurCol = normF[0] / 2
     imageMat = image
     m, n = imageMat.shape
#     hog = cv2.HOGDescriptor((normF[0],normF[1]), (normF[0],16), (normF[0],8), (normF[0],8), 9)# Tradi : (8,64), (8,16), (8,8), (8,8), 9 # LAPA : ((8,32), (8,16), (8,8), (8,8), 9)
#     for i in range(n):
#         colImage = imageMat[:, int(max(i - largeurCol, 0)) : int(min(i + largeurCol, n))]
#         if(i - largeurCol < 0):
#             zeroPadding = np.ones((m, int(abs(i - largeurCol))), 'uint8') * 255
#             colImage = np.concatenate((zeroPadding, colImage), axis=1)
#         if(i + largeurCol > n):
#             zeroPadding = np.ones((m, int(i + largeurCol - n)), 'uint8') * 255
#             colImage = np.concatenate((colImage, zeroPadding), axis=1)
#         tmp = hog.compute(colImage).T
#         if i == 0:
#             hogf = np.zeros((n, int(tmp.shape[1])),'f')
#             hogf[i][:] = tmp
#         else:
#             hogf[i][:] = tmp
#     return hogf.T


def extract_baselines_FromXML(xml_path, input_path):#, ns = {"ns": "http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15"}):
    """ """

    print("Not done")
    baselines = []

    with open(input_path + 'baselines.txt', "w") as file_baselines:
        for name_files in ["train.txt", "valid.txt", "test.txt"]:
            with open(input_path + name_files, "r") as my_file:
                for line in my_file.readlines():
                    if line is "\n":
                        continue

                    file_name = line.replace("\n", "").split(" ")[0]
                    page_name = file_name.split("_")[0]

                    tree = etree.parse(xml_path + page_name + ".xml")

                    lines = []
                    for line in tree.findall(".//TextLine"):#".//ns:TextLine", namespaces=ns):

                        mbaseline = line.find(".//Baseline")#".//ns:Baseline", namespaces=ns)
                        if mbaseline is None:
                            lines.append(None)
                            continue
                        mbaseline = mbaseline.get("points")
                        mbaseline = mbaseline.replace(",", " ").replace(";", " ").split(" ")
                        mbaseline = list(map(lambda x: int(x), mbaseline))

                        baseline = []

                        for i in range(0, len(mbaseline) - 1, 2):
                            baseline.append((mbaseline[i], mbaseline[i + 1]))

                    baselines.append(baseline)
                    str_baseline = ""
                    for pt in baseline:
                        str_baseline += " " + str(pt)
                    file_baselines.write(file_name + str_baseline)


    return baselines



def extract_baselines_FromTXT(txt_path, input_path):
    baselines = []

    with open(input_path + 'baselines.txt', "w") as file_baselines:
        for name_files in ["train.txt", "valid.txt", "test.txt"]:
            with open(input_path + name_files, "r") as my_file: # read files pred with the name of reference line
                for line in my_file.readlines():
                    if line is "\n":
                        continue

                    file_name = line.replace("\n", "").split(" ")[0]
                    page_name = file_name.split("_")[0]
                    num_line = int(file_name.split("_")[-1])

                    with open(txt_path + page_name + '.txt') as file_b:
                        all_b = file_b.readlines()

                    if len(all_b) <= num_line:
                        print("Problem with baselines : index segmented line is ", num_line, " and there is ", len(all_b), " segmented lines.")
                        print(txt_path + page_name + '.txt', file_name, page_name)
                    file_baselines.write(file_name + ' ' + all_b[num_line])
                    #print(file_name, page_name, all_b[num_line])


def create_subsets(input_path, data_dir, nb_subsets=[]):

    str_subset = '-'.join([str(elmt) for elmt in nb_subsets])

    if not os.path.exists(input_path + 'TRAIN_' + str_subset + '/'):
        os.mkdir(input_path + 'TRAIN_' + str_subset + '/')
    if not os.path.exists(input_path + 'TST_' + str_subset + '/'):
        os.mkdir(input_path + 'TST_' + str_subset + '/')
    if not os.path.exists(input_path + 'VAL_' + str_subset + '/'):
        os.mkdir(input_path + 'VAL_' + str_subset + '/')


    with open(data_dir + 'trackB-GT-ite1.txt') as file_lab:
        all_lines = file_lab.readlines()


    for line in all_lines:

        split_line = line.split(' ')
        file_line = split_line[0]
        pred = ' '.join(split_line[1:])

        num_file = int(file_line.split('_')[0]) - 10000

        if len(nb_subsets)==3:
            if num_file < nb_subsets[0]:
                with open(input_path + 'train.txt', 'a') as my_file:
                    my_file.write(line)
                shutil.copy(data_dir + 'img/' + file_line + '.tif', input_path + 'TRAIN_' + str_subset + '/'+ file_line + '.tif')
            elif num_file < np.sum(nb_subsets[:2]):
                with open(input_path + 'valid.txt', 'a') as my_file:
                    my_file.write(line)
                shutil.copy(data_dir + 'img/' + file_line + '.tif', input_path + 'VAL_' + str_subset + '/'+ file_line + '.tif')
            else:
                with open(input_path + 'test.txt', 'a') as my_file:
                    my_file.write(line)
                shutil.copy(data_dir + 'img/' + file_line + '.tif', input_path + 'TST_' + str_subset + '/'+ file_line + '.tif')


if __name__ == '__main__':

    opts, _ = getopt.getopt(sys.argv[1:], '', ["input=", "out=", "norm="])
    input_path = output_path = None
    input_path = "../../datasets/READ/" #os.path.join(os.path.dirname(__file__), "datasets/READ/")
    baseline_txt_path = "../../../../../code/segmenteurFCN/xml/READ/s0.65/"
    #os.path.join(os.path.dirname(__file__), "datasets/READ/batch/")
    nb_col = 1
    stride = 1
    padding = -1.
    keep_ratio = True
    norm = 64
    hog = False
    seqIm = False
    reduce_charset = True
    ImageFrombaseline=True
    nb_subsets = [5000, 2000, 3000]

    for opt, arg in opts:
        if '--input' in opt:
            input_path = arg
        elif '--out' in opt:
            output_path = arg
        elif '--norm' in opt:
            norm = int(arg)

    if not os.path.exists(input_path + 'TRAIN' + '-'.join([str(elmt) for elmt in nb_subsets]) + '/') or not os.path.exists(input_path + 'train.txt'):
        create_subsets(input_path, input_path + 'Train-B/', nb_subsets=nb_subsets)

    if ImageFrombaseline:
        if os.path.exists(input_path + 'baselines.txt'):
            with open(input_path + 'baselines.txt', "r") as file_baselines:
                baselines = []
                for line in file_baselines.readlines():
                    if line is "\n":
                        continue
                    ls = line.split(";")
                    baselines.append([])
                    for elmt in ls[1:]:
                        x, y = elmt.split(",")
                        baselines[-1].append((float(x), float(y)))
        else:
            baselines = extract_baselines_FromTXT(baseline_txt_path, input_path)


    str_reduce_charset = '_reduce' if reduce_charset else ''
    str_hog = '_hog' if hog else '_pix'
    str_norm = '_norm' + str(norm)
    str_ncol = '_ncol' + str(nb_col)
    str_struct = '_seqImages-stride' + str(stride) if seqIm else '_seqVect'

    output_path = input_path + "READ_" + str_struct + str_ncol + str_norm # + str_hog + str_reduce_charset

    if not input_path or not output_path:
        exit()

    if not input_path or not output_path:
        exit()

    if seqIm:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        if ImageFrombaseline:
            preprocessing_batch.append(ImagesFromBaseline(baselines, norm))
            norm = 0
        preprocessing_batch.append(To2DSequence(nb_cols=nb_col, stride=stride))
    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_col, padding=padding)]
        preprocessing_batch = []#ImagesFromBaseline(baselines, height, points_under=pt_under)]
        if ImageFrombaseline:
            preprocessing_batch.append(ImagesFromBaseline(baselines, norm))
            norm = 0

    producer = Producer(input_path, output_path,
                        norm=norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_col, standardize=True,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch
                        ).produce_dataset(sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TST/"},
                                          ext_images=".tif")


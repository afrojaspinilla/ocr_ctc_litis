import sys
import re

list_file = sys.argv[1]

with open(list_file) as f:
    items = f.read().splitlines()

print(items)
canonical_forms = []
for item in items:
    item = ''.join([ '~' if char.isdigit() else char for char in item])
    if item not in canonical_forms:
        canonical_forms.append(item)

print('---------------------------')
print(canonical_forms)

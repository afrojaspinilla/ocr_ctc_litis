import sys

from ocr_ctc.CTC_model.loggers.Logger import Logger


class SummaryLogger(Logger):
    def __init__(self, log_path, name="summary.txt", **kwargs):
        self.log_path = log_path
        self.name = name
        self.model = kwargs.get("model")

    def log(self):

        orig_stdout = sys.stdout
        with open(self.log_path+'/' + self.name, 'a') as f:
            sys.stdout = f
            self.model.summary()
        sys.stdout = orig_stdout


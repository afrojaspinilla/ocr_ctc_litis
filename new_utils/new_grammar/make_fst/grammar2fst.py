#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import time
import codecs
import shutil
import collections
import sys


def writeToFile(filename, string):
    with open(filename, 'w') as f:
        f.write("\n".join(map(str, string)))

def create_Lists(units):
    tok = []
    lab = []
    lex = []
    cnt = 0
    with open(units) as f1:
        unitsList = f1.read().splitlines()
    tok.append("<epsilon> "+str(cnt))
    lab.append("<epsilon> "+str(cnt))
    cnt += 1
    tok.append("<blk> "+str(cnt))
    cnt += 1
    for i in unitsList:
        tok.append(i+" "+str(cnt))
        lab.append(i+" "+str(cnt-1))
        cnt +=1
        lex.append(i+" 1.0 "+i)
    writeToFile("Tokens.txt", tok)
    writeToFile("Labels.txt", lab)
    writeToFile("lexiconprop.txt", lex)

    return '{}'.format("Tokens.txt"), '{}'.format("Labels.txt"), '{}'.format("lexiconprop.txt")

def create_T_FST(tokens):
    os.system("./ctc_token_fst.py " + tokens )
    os.system("./ctc_token_fst.py "+tokens+" | fstcompile --isymbols="+tokens+" --osymbols="+tokens+" --keep_isymbols=false --keep_osymbols=false | fstarcsort --sort_type=olabel > T.fst")
    return '{}'.format("T.fst")


def create_L_FST(tokens,labels, lexicon):
    os.system("./make_lexicon_fst.pl --pron-probs "+lexicon+" | fstcompile --isymbols="+tokens+" --osymbols="+labels+" --keep_isymbols=false --keep_osymbols=false | fstarcsort --sort_type=olabel > L.fst")
    return '{}'.format("L.fst")

def create_G_FST(grammar, fst, labels, tokens):
    os.system("python /usr/local/bin/thraxmakedep --save_symbols "+grammar)
    #print("thraxmakedep --save_symbols "+grammar)
    os.system("make")
    ffar = grammar[0:grammar.index(".grm")]
    os.system("farextract "+ffar+".far")
    #os.system("./grammarCompiler.sh "+grammar+" "+ffar)
    os.system("fstprint "+fst+" > G_"+ffar+".syntax")
    os.system("fstcompile --isymbols="+labels+" --osymbols="+labels+" --keep_isymbols=false --keep_osymbols=false G_"+ffar+".syntax | fstarcsort --sort_type=ilabel > G_"+ffar+".fst")
    return '{}'.format("G_"+ffar+".fst")

def create_TLG_FST(T, L, G, specific_name, tokens, labels):

    fst_path ="/mnt/d/vm_shared/gits/kaldi/src/fstbin/"
    fst_path = ""
    os.system(fst_path+"fsttablecompose "+L+" "+G+"  | "+fst_path+"fstminimizeencoded | fstarcsort --sort_type=ilabel > L"+G)
    os.system(fst_path+"fsttablecompose "+T+" L"+G+"  > "+specific_name+"TL"+G)
    os.system("fstprint --isymbols="+tokens+" --osymbols="+labels+" "+specific_name+"TL"+G+" > TLG.syntax")
    return specific_name+"TL"+G
    return '{}'.format("L"+G)

def draw_FST(isymbols, osymbols, fst):
    print("fstdraw --isymbols="+isymbols+" --osymbols="+osymbols+" -portrait "+fst+" | dot -Tpdf >"+fst[0:fst.index(".fst")]+".pdf")
    os.system("fstdraw --isymbols="+isymbols+" --osymbols="+osymbols+" -portrait "+fst+" | dot -Tpdf >"+fst[0:fst.index(".fst")]+".pdf")

def main():
    (tokens, labels, lexicon) =  create_Lists('units.list')
    print(tokens, labels, lexicon)
    (T_fst) = create_T_FST(tokens)
    print(T_fst)
    # draw_FST(tokens, tokens, T_fst)
    (L_fst) = create_L_FST(tokens, labels, lexicon)
    print("+++++++",L_fst)
    # draw_FST(tokens, labels, L_fst)
    (G_fst) = create_G_FST("lexicon.grm", "Test_Lexicon_grm", labels, tokens)
    print('--------',G_fst)
    # draw_FST(labels, labels, G_fst)
    specific_name = sys.argv[1]
    (TLG_fst) = create_TLG_FST(T_fst, L_fst, G_fst, specific_name, tokens, labels)
    # draw_FST(tokens, labels, TLG_fst)
    print(TLG_fst)
if __name__ == "__main__":
        main()

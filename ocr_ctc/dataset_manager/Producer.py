import os
from PIL import Image
from keras.preprocessing import sequence
from random import shuffle

from ocr_ctc.dataset_manager.new_batch.DatasetBatch import DatasetBatch
import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.Standardize import Standardize


class Producer:
    def __init__(self, input_path, output_path,
                 norm, keep_ratio=True, padding=-1,
                 nb_cols=1, window_size=1,
                 standardize=True, label_array=None,
                 preprocessing_data=[], preprocessing_batch=[], early_preprocessing_batch=[], shuff=False):

        self.standardize = standardize
        self.keep_ratio = keep_ratio
        self.preprocessing_batch = preprocessing_batch
        self.preprocessing_data = preprocessing_data
        self.early_preprocessing_batch = early_preprocessing_batch
        self.window_size = window_size
        self.nb_cols = nb_cols
        self.padding = padding
        self.norm = norm
        self.input_path = input_path
        self.output_path = output_path

        self.train_files = []
        self.test_files = []
        self.valid_files = []

        self.label_dict = {}
        self.label_array = []
        self.cur_label = 0
        if label_array is not None:
            print("Given charset = ", label_array)
            self.label_array = label_array
            self.cur_label = len(label_array)
            for i,l in enumerate(label_array):
                self.label_dict[l]=i
        self.shuff = shuff

    def load_img(self, it_data):
        output_path, label = self.train_files[it_data]
        image = Image.open(output_path)
        image = image.convert('L')
        width, height = image.size
        if self.keep_ratio:
            ratio = self.norm / height
            image = image.resize((int(width * ratio), self.norm), Image.ANTIALIAS)
        else:
            image = image.resize((width, self.norm), Image.ANTIALIAS)
        return np.array(image)


    def __read_from_file(self, file, files, path, splitter_label="", ext_images="", sup_dir="", no_new_label=False):
        for line in file.readlines():
            if line is "\n":
                continue
            new_line = line.replace("\n", "")
            split_line = new_line.split(" ")
            filename = split_line[0]
            # label = (" ".join(split_line[1:]))

            if splitter_label == "":
                label = list((" ".join(split_line[1:])))
            else:
                label = (" ".join(split_line[1:])).split(splitter_label)
            # label = (''.join(line.split(" ")[1:])).replace("\n", "")
            # print("LABEL LENGTHS:", len(label), len(tmp))

            # files can be in a sub directory
            fn = path + sup_dir + filename + ext_images
            if not os.path.exists(fn):
                subdir = check_file_in_subDirectory(path + sup_dir, filename + ext_images)
                #print("Not found ", filename, "\n", os.path.isdir(path), os.path.isdir(path + sup_dir), os.path.exists(fn), os.path.exists(path + sup_dir + filename))
                fn = path + sup_dir + subdir + '/' + filename + ext_images
            #else:
            #    print("found ", fn, "!!!")

            label_to_remove = []
            for i in label:
                if i not in self.label_dict:
                    if no_new_label:
                        print("Producer: label ", i, " is removed.")
                        label_to_remove.append(i)
                    else:
                        print("Producer: add label ", i)
                        self.label_dict[i] = self.cur_label
                        self.label_array.append(i)
                        self.cur_label += 1

            if len(label_to_remove)>0:
                for k in label_to_remove:
                    if isinstance(label, list):
                        label = list(''.join(label).replace(k, ''))
                    else:
                        label.replace(k, '')
            files.append((fn, label))

    def produce_dataset(self, splitter_label="", ext_images="", sup_dir={"train": "", "valid": "", "test": ""},
                        files_info_lab={"train": "train.txt", "valid": "valid.txt", "test": "test.txt"},
                        removed_empty=True, mean_stand=None, std_stand=None, padding_stand=300, encoding='utf-8', \
                        no_new_label=False):
        """
        Producer of datasets
        splitter_label = delimiter between labels (by default we have a string and one considers each character so this is ""
        ext_images = extension of line image file name (if not given in the txt file)
        sup_dir = additional directory where are the train, validation, and test line images
        files_info_lab = txt for the training, validation and test sets containing the name of image files and the labeling
        removed_empty : if True, data for which there is no label are removed

        mean_stand = mean for standardize (if pre-computed on a training set), otherwise this is computed on the given training set
        str_stand = standard deviation. Used in a similar manner.
        padding_stand = padding given by the standardize preprocessing. If mean_stand and std_stand are given (not None), padding_stand is used.

        """

        print(self.input_path + files_info_lab["train"])
        if os.path.exists(self.input_path + files_info_lab["train"]):
            with open(self.input_path + files_info_lab["train"], "r", encoding=encoding) as train:
                self.__read_from_file(train, self.train_files, self.input_path, no_new_label=no_new_label,
                                      splitter_label=splitter_label, ext_images=ext_images, sup_dir=sup_dir["train"])
        else:
            print("There is no data for training.")

        if os.path.exists(self.input_path + files_info_lab["valid"]):
            with open(self.input_path + files_info_lab["valid"], "r", encoding=encoding) as valid:
                self.__read_from_file(valid, self.valid_files, self.input_path, no_new_label=no_new_label,
                                      splitter_label=splitter_label, ext_images=ext_images, sup_dir=sup_dir["valid"])
        else:
            print("There is no data for validating.")

        if os.path.exists(self.input_path + files_info_lab["test"]):
            with open(self.input_path + files_info_lab["test"], "r", encoding=encoding) as test:
                self.__read_from_file(test, self.test_files, self.input_path, no_new_label=no_new_label,
                                      splitter_label=splitter_label, ext_images=ext_images, sup_dir=sup_dir["test"])
        else:
            print("There is no data for testing.")

        if self.shuff:
            shuffle(self.train_files)
            shuffle(self.test_files)
            shuffle(self.valid_files)

        if self.standardize:
            if mean_stand is None or std_stand is None:
                my_norm_std = Standardize(**{'func_load_img': self.load_img, 'nb_data': len(self.train_files)})
                mean, std = my_norm_std.get_mean_std()
                self.padding = my_norm_std.padding()
            else:
                mean = mean_stand
                std = std_stand
                self.padding = padding_stand
            self.preprocessing_data.append(Standardize(**{'mean': mean, 'std': std}))

        dataset = DatasetBatch(self.output_path)
        nb_features = self.norm  # norm*nb_cols if nb_cols else norm
        dataset.new_dataset(label_array=self.label_array, label_dict=self.label_dict, padding=self.padding,
                            nb_features=nb_features,
                            window_size=self.window_size, nb_cols=self.nb_cols,
                            train_files=self.train_files, valid_files=self.valid_files, test_files=self.test_files)

        def __prepare_set(file_list, index, padding, norm, removed_empty=True):
            x = []
            y = []
            x_length = []
            y_length = []

            try:
                true_label = []
                output_path, label = file_list[index]
                for i in label:
                    true_label.append(self.label_dict[i])
                image = Image.open(output_path)
                image = image.convert('L')
                width, _ = image.size
                original_length = image.size
                original_label = len(true_label)

                for early_preproc_batch in self.early_preprocessing_batch:
                    [image], [_], [width], _, padding = early_preproc_batch([image], [label], [width],
                                                                            [len(true_label)], padding)
                    # true_label

                _, height = image.size
                if self.keep_ratio:
                    ratio = norm / height
                    image = image.resize((int(width * ratio), norm), Image.ANTIALIAS)
                elif norm > 0:
                    image = image.resize((width, norm), Image.ANTIALIAS)
                image = np.array(image)

                for preproc in self.preprocessing_data:
                    image, true_label = preproc(image, true_label)
                seq_len = image.shape[0]
                label_len = len(true_label)

                # Warning: non-correct lengths
                if seq_len < 2 * label_len + 1:
                    print("Input dimensions: ", original_length, original_label, seq_len, label_len, output_path, label,
                          "\nWarning in Producer: sequence length is lower than the label length. Can have problems with CTC.")
                    #print("Data removed.")
                    if removed_empty:
                        print("Data removed.")
                    else:
                        x.append(image)
                        y.append(true_label)
                        x_length.append(seq_len)
                        y_length.append(label_len)
                elif label_len == 0:
                    print("Input dimensions: ", original_length, original_label, seq_len, label_len, output_path, label,
                          "\nWarning in Producer: label length is equal to 0.")
                    if removed_empty:
                        print("Data removed (no label)..")
                    else:
                        x.append(image)
                        y.append(true_label)
                        x_length.append(seq_len)
                        y_length.append(label_len)

                else:
                    x.append(image)
                    y.append(true_label)
                    x_length.append(seq_len)
                    y_length.append(label_len)

            except IndexError:
                print("Last batch is smaller...")

            if len(x) > 0:
                for preproc_batch in self.preprocessing_batch:
                    x, y, x_length, y_length, padding = preproc_batch(x, y, x_length, y_length, padding)

                    for idx_tmp in range(len(x)): # reverse images to get height x width for conv2D layers
                        if len(x[idx_tmp])>0:
                            x[idx_tmp] = np.swapaxes(x[idx_tmp], 1, 2)

                elmt_to_remove = []
                for i in range(len(x)):
                    if x_length[i] == 0:
                        print("Lengths x : ", x_length[i])
                        print("Data removed...")
                        elmt_to_remove.append(i)
                    elif x_length[i] < 2 * y_length[i] + 1:
                        print("Lengths x and y (seq to short) : ", x_length[i], '--', y_length[i])
                        if removed_empty:
                            print("Data removed...")
                            elmt_to_remove.append(i)
                    elif y_length[i] == 0:
                        if removed_empty:
                            print("Data removed (no label)...")
                            elmt_to_remove.append(i)


                if len(elmt_to_remove) > 0:
                    for k in sorted(elmt_to_remove, reverse=True):
                        del x[k]
                        del y[k]
                        del x_length[k]
                        del y_length[k]

                if len(x) > 0:
                    x = sequence.pad_sequences(x, value=float(padding), dtype='float32', padding="post")
                    y = sequence.pad_sequences(y, value=float(len(self.label_array)), dtype='float32', padding="post")

            return x, y, np.asarray(x_length), np.asarray(y_length)

        print("save train")
        data_to_remove = []
        for i in range(0, len(self.train_files)):
            x, y, x_length, y_length = __prepare_set(self.train_files, i, self.padding, self.norm,
                                                     removed_empty=True)
            dataset.save_train_batch((x, y, np.asarray(x_length), np.asarray(y_length)))
            if len(x) == 0:
                data_to_remove.append(i)
        for j in range(len(data_to_remove) - 1, -1, -1):
            del self.train_files[data_to_remove[j]]

        print("save valid")
        data_to_remove = []
        for i in range(0, len(self.valid_files)):
            x, y, x_length, y_length = __prepare_set(self.valid_files, i, self.padding, self.norm,
                                                     removed_empty=True)
            dataset.save_valid_batch((x, y, np.asarray(x_length), np.asarray(y_length)))

            if len(x) == 0:
                data_to_remove.append(i)
        for j in range(len(data_to_remove) - 1, -1, -1):
            del self.valid_files[data_to_remove[j]]

        print("save test")
        data_to_remove = []
        for i in range(0, len(self.test_files)):
            x, y, x_length, y_length = __prepare_set(self.test_files, i, self.padding, self.norm,
                                                     removed_empty=removed_empty)
            dataset.save_test_batch((x, y, np.asarray(x_length), np.asarray(y_length)))

            if len(x) == 0:
                data_to_remove.append(i)
        for j in range(len(data_to_remove) - 1, -1, -1):
            del self.test_files[data_to_remove[j]]

        dataset.save()

        with open(self.output_path + "/parameters.txt", "w") as f:

            log = "Hauteur normalisée : " + str(self.norm) + "\n"
            log += "Taille des fenêtres : " + str(self.window_size) + "\n"
            log += "Nombre de colonnes groupées : " + str(self.nb_cols) + "\n"
            log += "Valeur du padding : " + str(self.padding) + "\n"
            log += "Labels : " + str(self.label_array) + "\n"
            log += "Ratio conservé : " + str(self.keep_ratio) + "\n"

            log += "Preprocessings : \n"
            for prep in self.preprocessing_batch:
                log += " - " + str(prep) + "\n"
            for prep in self.preprocessing_data:
                log += " - " + str(prep) + "\n"

            f.write(log)



def check_file_in_subDirectory(dir_file, name_file):

    k=0
    subdir = os.listdir(dir_file)
    nb_subdir = len(subdir)
    notseen = True
    sd = ''

    while k<nb_subdir and notseen:
        sd = subdir[k]
        if os.path.exists(dir_file + '/' + sd + '/' + name_file):
            notseen = False
        k += 1

    return sd

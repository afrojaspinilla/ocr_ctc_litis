import sys
sys.path.append('./')

import ocr_ctc
from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import random



def main(input_path, dir_origin="", \
         dir_line_images='', str_info='', \
         dir_origin_images='../../../data/RIMES_paragraph/all_im/', \
         dir_baselines="", files_info_lab=None, charset=None, \
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, ext_im='.png',\
         window_structure='vertical', shuff=True, padding=-1, standardize=True, ext_origin='xml', \
         mean_stand=None, std_stand=None, no_new_label=False):



    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + input_path.split('/')[-2] + "conv"  + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []

        output_path = input_path + input_path.split('/')[-2] + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)


    if files_info_lab is None:
        files_info_lab = {"train": "train.txt", "valid": "valid.txt", "test": "test.txt"}


    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding, label_array=charset,
                        nb_cols=nb_cols, standardize=standardize,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        early_preprocessing_batch=early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(splitter_label="",\
                                          sup_dir={"train": "imagettes/",\
                                                   "valid": "imagettes/",\
                                                   "test": ""}, \
                                          files_info_lab=files_info_lab, mean_stand=mean_stand, std_stand=std_stand,\
                                          ext_images="", removed_empty=removed_empty, no_new_label=no_new_label)

    # sup_dir = {"train": "out558_predict0_0.5/extracted_lines/", \
    #            "valid": "out558_predict0_0.5/extracted_lines/", \
    #            "test": "out558_predict0_0.5/extracted_lines/"}, \

def several_datasets(input_path):

    nb_cols = 16  # number of time frames that are considered at each time step
    stride = 2
    padding = -1.
    norm = 32  # height dimension
    keep_ratio = True  # resize the image by keeping the ratio
    conv = True  #  format data for a conv LSTM network
    conv_pad = True  #  do padding in the extremities of the images built for the conv LSTM network
    ImageFrombaseline = False  #  image is build according to the baseline (not available here)
    window_structure = 'vertical'  # parameter for ImageFromBaseline

    #input_path = "/mnt/d/vm_shared/data/coulisse_1924_page_2/"
    #input_path = "/mnt/d/vm_shared/coulisse/coulisse_ftp_omnipage/data_for_ocr/"

    shuff = False  # mix the data before saved them


    str_info = 'coulisse_one_page_per_year' #'Art30866_' + elmt  # _30893_1' _test_ArtPoly0p35015

    files_info_lab = {"train": "train_file.txt", \
                      "valid": "valid_file.txt", \
                      "test": "no"}

    mean_stand = 178.46429447857915 # 169.14015652234303 #None #169.13909315586866 #None #127.15439987266659 # 237.20395680296994  #210.09484374142548 #210.08713638936308  # 209.55817933603757 #19.120696372144852# 209.55817933603757 # None = default, this is computed on the training set
    std_stand = 33.56414641258921 # 42.83750628205289 #None #42.887909774371195 #None #37.12547797780864# 45.972764583180705  #34.925641777103046 #34.275111586890084  # 36.98662735037311 # 58.82913064998043 # 36.98662735037311 # None = default, this is computed on the training set

    #charset = ['J', 'e', ' ', 'v', 'o', 'u', 's', 'a', 'd', 'r', 'c', 'i', 'f', 'n', 'l', 't', '.', 'V', 'z', 'm', 'q', 'é', 'à', 'h', 'è', 'D', ',', 'j', 'p', 'M', 'P', 'b', "'", 'g', 'x', 'E', 'S', 'L', 'C', ':', '0', '3', '4', '2', '9', '7', '8', '5', 'A', 'y', '-', 'R', 'G', 'N', '6', ';', 'î', '1', 'I', 'B', '°', '(', 'Y', 'Z', 'W', ')', 'ç', 'ê', 'ô', '?', 'H', 'â', '/', 'U', 'Q', '"', 'O', 'F', 'T', 'É', '*', 'ë', 'X', 'K', '%', '€', 'û', 'k', 'w', '+', 'ù', '¤', '!', '=', '{', 'À', '}', '²', 'œ', '_']
    # with open('/'.join(input_path.split('/')[:-2]) + '/READ18/' + 'charset.txt') as f:
    #     for line in f.readlines():
    #         charset.append(line.rstrip('\n'))
    # for file_ch in ['charset_1158.txt', 'charset_558.txt', 'charset_431.txt', 'charset_BibDroitNormand.txt']:
    #     print(file_ch)
    #     ch_path = '../datasets/PlaIR/'
    #     with open(ch_path + file_ch, 'r') as fr:
    #         for line in fr.readlines():
    #             ch = line.replace('\n','')
    #             if ch not in charset:
    #                 print("add elmt from PlaIR: ", ch)
    #                 charset.append(ch)

    charset = [' ', '"', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', \
               '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '@', \
               'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', \
               'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', \
               'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', \
               'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', \
               '£']
    print(len(charset))
    removed_empty = False
    no_new_label = True # do not add new label if there is some

    #return
    main(input_path, \
         h_norm=norm, nb_cols=nb_cols, stride=stride, str_info=str_info, charset=charset, \
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
         window_structure=window_structure, shuff=shuff, padding=padding, files_info_lab=files_info_lab, \
         mean_stand=mean_stand, std_stand=std_stand, removed_empty=removed_empty, no_new_label=no_new_label)



if __name__ == '__main__':

    input_path = sys.argv[1]
    several_datasets(input_path)

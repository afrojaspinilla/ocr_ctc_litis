import numpy as np
from sklearn import preprocessing

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class Scale(ImagePreprocessing):
    """
    Scale the image using Scikit-learn scale
    """

    def __init__(self, max_width=0):
        self.max_width = max_width

    def preprocessing(self, image):
        img = preprocessing.scale(image)

        return img.astype('float32')

    def __str__(self):
        return "Scale the image using Scikit-learn scale"
import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class InvertPixelValues(ImagePreprocessing):
    """
    Revert the pixel values (px = 255-px)
    """

    def __init__(self, **kwargs):
        pass

    def preprocessing(self, image):
        img = image.astype(float)
        img *= -1
        img += 255

        return img.astype('float32')

    def __str__(self):
        return "Revert the pixel values"
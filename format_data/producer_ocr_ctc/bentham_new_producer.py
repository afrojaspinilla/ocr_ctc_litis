from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import shutil


def create_train_valid_test(input_path):

    os.mkdir(input_path)

    data_dir = '../../../../../data/' #'./datasets/' # '../../../../../data/'
    lines_dir = data_dir + 'Bentham/BenthamDatasetR0-GT/Images/Lines/'
    trans_dir = data_dir + 'Bentham/BenthamDatasetR0-GT/Transcriptions/'



    for file_trans in os.listdir(trans_dir):

        ref_doc = int(file_trans.split('_')[0])
        ref_pages = int(file_trans.split('_')[1])

        image_line = file_trans[:-3] + 'png'

        with open(trans_dir + file_trans, "r") as trans:
            line = trans.readline()

        if ref_doc<116 or (ref_doc==116 and ref_pages<200):
            with open(input_path + "/train.txt", "a") as train:
                train.write(image_line + ' ' + line)
        elif ref_pages<400:
            with open(input_path + "/valid.txt", "a") as valid:
                valid.write(image_line + ' ' + line)
        else:
            with open(input_path + "/test.txt", "a") as test:
                test.write(image_line + ' ' + line)

        shutil.copy(lines_dir + image_line, input_path)


if __name__ == '__main__':
    nb_cols = 1
    stride = 1
    padding = -1.
    padding = -1.
    norm = 100
    keep_ratio = True
    splitter_label=""

    input_path = "../../datasets/Bentham/Bentham_origin"
    output_path = input_path + "_" + str(norm) + "_" + str(nb_cols)

    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
    preprocessing_batch = []


    if not os.path.exists(input_path):
        create_train_valid_test(input_path)

    input_path = input_path + '/'

    producer = Producer(input_path, output_path,
                        norm=norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=True,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch
                        ).produce_dataset(splitter_label=splitter_label)

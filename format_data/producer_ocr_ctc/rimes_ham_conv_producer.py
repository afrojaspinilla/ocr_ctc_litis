from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose

if __name__ == '__main__':
    nb_cols = 16
    stride = 4
    padding = -1.
    padding = -1.
    norm = 64
    keep_ratio = True

    input_path = "../datasets/RIMES_ham/"
    output_path = input_path + "RIMES_ham_conv_" + str(norm) + "_" + str(nb_cols) + "/"

    preprocessing_data = [Transpose()]
    preprocessing_batch = [To2DSequence(nb_cols=nb_cols, stride=stride)]

    producer = Producer(input_path, output_path,
                        norm=norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=False,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        window_size=nb_cols
                        ).produce_dataset()

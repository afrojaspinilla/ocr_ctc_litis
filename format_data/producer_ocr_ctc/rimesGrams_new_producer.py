from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose


def main(input_path, dir_origin="", \
         dir_line_images='', str_info='', \
         dir_origin_images='../../../data/RIMES_paragraph/all_im/', \
         dir_baselines="", files_info_lab=None, \
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, ext_im='.png',\
         window_structure='vertical', shuff=True, padding=-1, standardize=True, ext_origin='xml'):

    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + "RIMESconv_"  + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []

        output_path = input_path + "RIMES_" + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)


    if files_info_lab is None:
        files_info_lab = {"train": "train.txt", "valid": "valid.txt", "test": "test.txt"}


    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=standardize,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        early_preprocessing_batch=early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(splitter_label=" ",\
                                          sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TST/"}, \
                                          files_info_lab=files_info_lab, \
                                          ext_images=".Gray.png", removed_empty=removed_empty)


if __name__ == '__main__':

    nb_cols = 16 # number of time frames that are considered at each time step
    stride = 4
    padding = -1.
    norm = 64 # height dimension
    keep_ratio = True # resize the image by keeping the ratio
    conv = True # format data for a conv LSTM network
    conv_pad = True # do padding in the extremities of the images built for the conv LSTM network
    ImageFrombaseline = False # image is build according to the baseline (not available here)
    window_structure = 'vertical' # parameter for ImageFromBaseline

    input_path = "../datasets/RIMES/"

    shuff=False # mix the data before saved them

    str_info = 'm2g400'

    files_info_lab = {"train": "train_" + str_info + ".txt", \
                      "valid": "valid_" + str_info + ".txt", \
                      "test": "test_" + str_info + ".txt"}

    main(input_path, \
         h_norm=norm, nb_cols=nb_cols, stride=stride, str_info=str_info,\
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
         window_structure=window_structure, shuff=shuff, padding=padding, files_info_lab=files_info_lab)


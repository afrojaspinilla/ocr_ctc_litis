import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class NormalizeGray(ImagePreprocessing):
    """
    Normalization of the gray levels
    """

    def __init__(self, **kwargs):
        pass

    def preprocessing(self, image_gray):
        q5 = np.percentile(image_gray, 5)
        q30 = np.percentile(image_gray, 30)
        img_norm0 = image_gray.astype('float32')
        img_norm0 *= 255. / (q30 - q5)
        img_norm0 -= 255. * q5 / (q30 - q5)
        img_norm1 = np.clip(img_norm0, 0, 255)
        return img_norm1.astype('float32')

    def __str__(self):
        return "Normalization of the gray levels"
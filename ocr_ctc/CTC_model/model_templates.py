from keras.layers import TimeDistributed, Activation, Dense, Input, Bidirectional, Conv2D, Flatten, MaxPooling2D, \
    GaussianNoise, Masking, LSTM, Dropout, concatenate, Conv1D, Reshape, LeakyReLU, BatchNormalization, Add
from keras.optimizers import Adam
from ocr_ctc.CTC_model.CTCModel import CTCModel
from ocr_ctc.CTC_model.weightnorm import AdamWithWeightnorm
#from ocr_ctc.CTC_model.layers.gatedConv2D import GatedConv2D, gatedconv2d, GatedConv2D_v2

def string_to_model(string):
    models = { "conv1" : conv1,
               "conv4" : conv4,
               "conv7" : conv7,
               "conv7drop_128" : conv7drop_128,
               "conv7drop_256" : conv7drop_256,
               "conv_10_residual_block" : conv_10_residual_block,
               "conv8drop_256_residual_block_3" : conv8drop_256_residual_block_3,
               "conv8drop_256_con1Ddilated" : conv8drop_256_con1Ddilated,
               "conv5Dbltsm5" : conv5Dbltsm5,
               "conv5bltsm5" : conv5bltsm5,
               "conv7inv" : conv7inv,
               "conv4inv" : conv4inv,
               "conv7dense" : conv7dense,
               "conv7_128" : conv7_128,
               "convCausalOnly7drop_256" : convCausalOnly7drop_256,
               "convCausalInceptOnly7drop_256" : convCausalInceptOnly7drop_256,
               "conv11drop_256" : conv11drop_256,
               "convOnly11drop_256" : convOnly11drop_256,
               "convOnly7drop_256" : convOnly7drop_256,
               "conv8drop_256" : conv8drop_256,
              }
    try:
        return models[string]
    except KeyError:
        print("Bad template name provided")
               
def conv1(dataset, dropout=0.1):
    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv0)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv0)
    dense = TimeDistributed(Dense(128), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    # lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    # lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(masking2)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv4(dataset, dropout=0.1):
    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv0)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(pool0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(pool1)
    pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(conv2)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(pool2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(pool3)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv4)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def conv7(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(pool1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(pool3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(pool5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv7drop_128(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(128), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(128, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(128, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv7drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv6)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv8drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def convOnly7drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    # masking2 = Masking(mask_value=dataset.padding)(dense)
    #
    # lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    # lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(dense)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def convOnly11drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    # masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv0 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)
    pool7 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool7")(conv7)
    drop7 = Dropout(dropout)(pool7)
    conv8 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv8")(drop7)
    conv9 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv9")(conv8)
    pool9 = TimeDistributed(MaxPooling2D(pool_size=(2, 1), data_format="channels_last"), name="Pool9")(conv9)
    drop9 = Dropout(dropout)(pool9)
    conv10 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv10")(drop9)
    conv11 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv11")(conv10)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv11)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    # masking2 = Masking(mask_value=dataset.padding)(dense)
    #
    # lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    # lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(dense)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def conv11drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)
    pool7 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool7")(conv7)
    drop7 = Dropout(dropout)(pool7)
    conv8 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv8")(drop7)
    conv9 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv9")(conv8)
    pool9 = TimeDistributed(MaxPooling2D(pool_size=(2, 1), data_format="channels_last"), name="Pool9")(conv9)
    drop9 = Dropout(dropout)(pool9)
    conv10 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv10")(drop9)
    conv11 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv11")(conv10)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv11)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def convCausalInceptOnly7drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv00 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=1), name="Conv00")(noise)
    conv10 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=1), name="Conv10")(conv00)
    conv01 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=2),
                            name="Conv01")(noise)
    conv11 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=2),
                            name="Conv11")(conv01)
    conv02 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=4),
                             name="Conv02")(noise)
    conv12 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=4),
                             name="Conv12")(conv02)
    concat1 = concatenate([conv10, conv11, conv12], axis=-1)
    reshape0 = TimeDistributed(Reshape((dataset.nb_features*dataset.nb_cols*64*3,)))(concat1)
    conv1d0 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=1)(reshape0)
    #conv1d00 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=1)(conv1d0)
    conv1d1 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=2)(conv1d0)
    #conv1d10 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=2)(conv1d1)
    conv1d2 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=4)(conv1d1)
    #conv1d20 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=4)(conv1d2)

    # conv000 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=1),
    #                          name="Conv000")(conv1d0)
    # conv100 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=1),
    #                          name="Conv100")(conv000)
    # conv010 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=2),
    #                          name="Conv010")(conv1d0)
    # conv110 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=2),
    #                          name="Conv110")(conv010)
    # conv020 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=4),
    #                          name="Conv010")(conv1d0)
    # conv120 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last", dilation_rate=4),
    #                          name="Conv110")(conv020)
    # concat2 = concatenate([conv100, conv110, conv120], axis=-1)
    # conv1d1 = TimeDistributed(Conv1D(64, 3, strides=1, padding='causal', dilation_rate=2),
    #                           name="Conv1D1")(concat2)
    #
    # conv0000 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last", dilation_rate=1),
    #                          name="Conv0000")(conv1d1)
    # conv1000 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last", dilation_rate=1),
    #                          name="Conv1000")(conv0000)
    # conv0100 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last", dilation_rate=2),
    #                          name="Conv0100")(conv1d1)
    # conv1100 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last", dilation_rate=2),
    #                          name="Conv1100")(conv0100)
    # conv0200 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last", dilation_rate=4),
    #                          name="Conv0100")(conv1d1)
    # conv1200 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last", dilation_rate=4),
    #                          name="Conv1100")(conv0200)
    # concat3 = concatenate([conv1000, conv1100, conv1200], axis=-1)
    # conv1d2 = TimeDistributed(Conv1D(128, 3, strides=1, padding='causal', dilation_rate=4),
    #                           name="Conv1D2")(concat3)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv1d2)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    # masking2 = Masking(mask_value=dataset.padding)(dense)
    #
    # lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    # lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(dense)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def convCausalOnly7drop_256(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv00 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv00")(noise)
    conv10 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv10")(conv00)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv10)
    drop1 = Dropout(dropout)(pool1)

    conv01 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"),
                            name="Conv01")(drop1)
    conv11 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"),
                            name="Conv11")(conv01)
    pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv11)
    drop2 = Dropout(dropout)(pool2)

    conv02 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"),
                             name="Conv02")(drop2)
    conv12 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"),
                             name="Conv12")(conv02)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(conv12)
    drop3 = Dropout(dropout)(pool3)

    reshape0 = TimeDistributed(Reshape((int(dataset.nb_features*dataset.nb_cols/64)*256,)))(drop3)
    conv1d0 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=1)(reshape0)
    #conv1d00 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=1)(conv1d0)
    conv1d1 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=2)(conv1d0)
    #conv1d10 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=2)(conv1d1)
    conv1d2 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=4)(conv1d1)
    #conv1d20 = Conv1D(256, 3, strides=1, padding='causal', dilation_rate=4)(conv1d2)


    flatten = TimeDistributed(Flatten(), name="Flatten")(conv1d2)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    # masking2 = Masking(mask_value=dataset.padding)(dense)
    #
    # lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    # lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(dense)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv7_128(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    conv2 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(pool1)
    conv3 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    conv4 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(pool3)
    conv5 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    conv6 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(pool5)
    conv7 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(128), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(128, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(128, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv7dense(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(pool1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(pool3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(pool5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    dense0 = TimeDistributed(Dense(dataset.nb_labels + 1, activation='softmax'), name="DenseSoftmax0")(lstm0)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(dense0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def conv4inv(dataset, dropout=0.1):
    input_data = Input(name='input',
                       shape=(None, dataset.nb_cols, dataset.nb_features, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv0)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(pool0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(pool1)
    pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(conv2)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(pool2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(pool3)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv4)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv7inv(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_cols, dataset.nb_features, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(pool1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(pool3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(pool5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



# def gatedConv4inv(dataset, dropout=0.1):
#
#     i=0
#
#     input_data = Input(name='input',
#                        shape=(None, dataset.nb_cols, dataset.nb_features, 1))  #  window_size instead of nb_cols
#
#     masking = Masking(mask_value=dataset.padding)(input_data)
#     noise = GaussianNoise(0.01)(masking)
#
#     conv0 = TimeDistributed(Conv2D(32, (3, 3), activation='relu', padding="same", data_format="channels_last"), name="Conv0")(noise)
#     #pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv0)
#     conv1 = TimeDistributed(Conv2D(32, (3, 3), activation='relu', padding="same", data_format="channels_last"), name="Conv1")(conv0)
#     gc1 = TimeDistributed(GatedConv2D_v2(32), name='gcnn{}'.format(i + 1))(conv1)
#     i+=1
#     conv2 = TimeDistributed(Conv2D(64, (3, 3), activation='relu', padding="same", data_format="channels_last"), name="Conv2")(gc1)
#     #pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(conv2)
#     conv3 = TimeDistributed(Conv2D(64, (3, 3), activation='relu', padding="same", data_format="channels_last"), name="Conv3")(conv2)
#     gc3 = TimeDistributed(GatedConv2D_v2(32),
#                           name='gcnn{}'.format(i + 1))(conv3)
#     i += 1
#     #pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
#     conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(gc3)
#     conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
#
#     flatten = TimeDistributed(Flatten(), name="Flatten")(conv4)
#     dense = TimeDistributed(Dense(256), name="Dense")(flatten)
#
#     masking2 = Masking(mask_value=dataset.padding)(dense)
#
#     lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
#     lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1")(lstm0)
#
#     out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
#     output = Activation('softmax', name='Softmax')(out)
#
#     return CTCModel([input_data], [output], charset=dataset.all_labels)


# def gatedConvBluche(dataset, dropout=0.1):
#
#     i=0
#
#     input_data = Input(name='input',
#                        shape=(None, dataset.nb_cols, dataset.nb_features, 1))  #  window_size instead of nb_cols
#
#     masking = Masking(mask_value=dataset.padding)(input_data)
#     noise = GaussianNoise(0.01)(masking)
#
#     conv0 = TimeDistributed(Conv2D(8, (3, 3), activation='tanh', padding="same", data_format="channels_last"), name="Conv0")(noise)
#     #pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv0)
#     conv1 = TimeDistributed(Conv2D(16, (2, 4), activation='tanh', padding="same", data_format="channels_last"), name="Conv1")(conv0)
#     gc1 = TimeDistributed(GatedConv2D_v2(16), name='gcnn{}'.format(i + 1))(conv1)
#     i+=1
#     conv2 = TimeDistributed(Conv2D(32, (3, 3), activation='tanh', padding="same", data_format="channels_last"), name="Conv2")(gc1)
#     gc3 = TimeDistributed(GatedConv2D_v2(32),
#                           name='gcnn{}'.format(i + 1))(conv2)
#     i += 1
#     conv3 = TimeDistributed(Conv2D(64, (2, 4), activation='tanh', padding="same", data_format="channels_last"), name="Conv3")(gc3)
#     gc4 = TimeDistributed(GatedConv2D_v2(64),
#                           name='gcnn{}'.format(i + 1))(conv3)
#     i += 1
#     conv4 = TimeDistributed(Conv2D(128, (3, 3), activation='tanh', padding="same", data_format="channels_last"), name="Conv4")(gc4)
#
#     pool0 = TimeDistributed(MaxPooling2D(pool_size=(1, dataset.nb_features), data_format="channels_last"), name="Pool0")(conv4)
#
#     flatten = TimeDistributed(Flatten(), name="Flatten")(pool0)
#     # dense = TimeDistributed(Dense(128), name="Dense")(flatten)
#
#     masking2 = Masking(mask_value=dataset.padding)(flatten)
#
#     lstm0 = Bidirectional(LSTM(128, return_sequences=True, dropout=dropout), name="LSTM0")(masking2)
#     dense0 = TimeDistributed(Dense(128, activation='tanh'), name="Dense")(lstm0)
#     lstm1 = Bidirectional(LSTM(128, return_sequences=True, dropout=dropout), name="LSTM1")(dense0)
#
#     out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm1)
#     output = Activation('softmax', name='Softmax')(out)
#
#     return CTCModel([input_data], [output], charset=dataset.all_labels)



def conv5bltsm5(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    act0 = TimeDistributed(LeakyReLU())(conv0)
    pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(act0)
    bn0 = TimeDistributed(BatchNormalization())(pool0)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(bn0)
    act1 = TimeDistributed(LeakyReLU())(conv1)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(act1)
    bn1 = TimeDistributed(BatchNormalization())(pool1)
    conv2 = TimeDistributed(Conv2D(48, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(bn1)
    act2 = TimeDistributed(LeakyReLU())(conv2)
    pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(act2)
    bn2 = TimeDistributed(BatchNormalization())(pool2)
    drop2 = TimeDistributed(Dropout(0.2))(bn2)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(drop2)
    act3 = TimeDistributed(LeakyReLU())(conv3)
    bn3 = TimeDistributed(BatchNormalization())(act3)
    drop3 = TimeDistributed(Dropout(0.2))(bn3)
    conv4 = TimeDistributed(Conv2D(80, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    act4 = TimeDistributed(LeakyReLU())(conv4)
    bn4 = TimeDistributed(BatchNormalization())(act4)
    drop4 = TimeDistributed(Dropout(0.2))(bn4)

    flatten = TimeDistributed(Flatten(), name="Flatten")(drop4)
    #dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(flatten)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM1")(lstm0)
    lstm2 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM2")(lstm1)
    lstm3 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM3")(lstm2)
    lstm4 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM4")(lstm3)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm4)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def conv5Dbltsm5(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(Conv2D(16, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    act0 = TimeDistributed(LeakyReLU())(conv0)
    pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(act0)
    bn0 = TimeDistributed(BatchNormalization())(pool0)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(bn0)
    act1 = TimeDistributed(LeakyReLU())(conv1)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(act1)
    bn1 = TimeDistributed(BatchNormalization())(pool1)
    conv2 = TimeDistributed(Conv2D(48, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(bn1)
    act2 = TimeDistributed(LeakyReLU())(conv2)
    pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(act2)
    bn2 = TimeDistributed(BatchNormalization())(pool2)
    drop2 = TimeDistributed(Dropout(0.2))(bn2)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(drop2)
    act3 = TimeDistributed(LeakyReLU())(conv3)
    bn3 = TimeDistributed(BatchNormalization())(act3)
    drop3 = TimeDistributed(Dropout(0.2))(bn3)
    conv4 = TimeDistributed(Conv2D(80, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    act4 = TimeDistributed(LeakyReLU())(conv4)
    bn4 = TimeDistributed(BatchNormalization())(act4)
    drop4 = TimeDistributed(Dropout(0.2))(bn4)

    flatten = TimeDistributed(Flatten(), name="Flatten")(drop4)
    dense = TimeDistributed(Dense(1024), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM0")(masking2)
    lstm1 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM1")(lstm0)
    lstm2 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM2")(lstm1)
    lstm3 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM3")(lstm2)
    lstm4 = Bidirectional(LSTM(256, return_sequences=True, dropout=0.5), name="LSTM4")(lstm3)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(lstm4)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)

def conv8drop_256_con1Ddilated(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    reshape0 = TimeDistributed(Reshape((256,1)), name="Reshape_0")(dense)
    #ResidualBlock1
    conv1d_1a = TimeDistributed(Conv1D(256, 3, dilation_rate=1, padding="causal", activation="relu"), name="Block1_Conv1")(reshape0)
    dropout_1a = Dropout(0.05)(conv1d_1a)
    conv1d_2a = TimeDistributed(Conv1D(256, 3, dilation_rate=1, padding="causal", activation="relu"), name="Block1_Conv2")(dropout_1a)
    dropout_2a = Dropout(0.05)(conv1d_2a)

    #ResidualBlock2
    conv1d_1b = TimeDistributed(Conv1D(256, 3, dilation_rate=2, padding="causal", activation="relu"), name="Block2_Conv1")(dropout_2a)
    dropout_1b = Dropout(0.05)(conv1d_1b)
    conv1d_2b = TimeDistributed(Conv1D(256, 3, dilation_rate=2, padding="causal", activation="relu"), name="Block2_Conv2")(dropout_1b)
    dropout_2b = Dropout(0.05)(conv1d_2b)
    #ResidualBlock3
    conv1d_1c = TimeDistributed(Conv1D(256, 3, dilation_rate=4, padding="causal", activation="relu"), name="Block3_Conv1")(dropout_2b)
    dropout_1c = Dropout(0.05)(conv1d_1c)
    conv1d_2c = TimeDistributed(Conv1D(256, 3, dilation_rate=4, padding="causal", activation="relu"), name="Block3_Conv2")(dropout_1c)
    dropout_2c = Dropout(0.05)(conv1d_2c)

    flatten1 = TimeDistributed(Flatten(), name="Flatten1")(dropout_2c)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(flatten1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def conv8drop_256_residual_block_3(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)

    conv0 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0")(noise)
    conv1 = TimeDistributed(Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1")(conv0)
    pool1 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1")(conv1)
    drop1 = Dropout(dropout)(pool1)
    conv2 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2")(drop1)
    conv3 = TimeDistributed(Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3")(conv2)
    pool3 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3")(conv3)
    drop3 = Dropout(dropout)(pool3)
    conv4 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4")(drop3)
    conv5 = TimeDistributed(Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5")(conv4)
    pool5 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5")(conv5)
    drop5 = Dropout(dropout)(pool5)
    conv6 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6")(drop5)
    conv7 = TimeDistributed(Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7")(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv7)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    reshape0 = TimeDistributed(Reshape((256,1)), name="Reshape_0")(dense)
    #ResidualBlock1
    conv1d_1a = TimeDistributed(Conv1D(256, 3, dilation_rate=1, padding="causal", activation="relu"), name="Block1_Conv1")(reshape0)

    dropout_1a = Dropout(0.05)(conv1d_1a)
    conv1d_2a = TimeDistributed(Conv1D(256, 3, dilation_rate=1, padding="causal", activation="relu"), name="Block1_Conv2")(dropout_1a)
    dropout_2a = Dropout(0.05)(conv1d_2a)
    residual1 = Add()([reshape0, dropout_2a])
    out_res1 = Activation('relu')(residual1)

    #ResidualBlock2
    conv1d_1b = TimeDistributed(Conv1D(256, 3, dilation_rate=2, padding="causal", activation="relu"), name="Block2_Conv1")(out_res1)
    dropout_1b = Dropout(0.05)(conv1d_1b)
    conv1d_2b = TimeDistributed(Conv1D(256, 3, dilation_rate=2, padding="causal", activation="relu"), name="Block2_Conv2")(dropout_1b)
    dropout_2b = Dropout(0.05)(conv1d_2b)
    residual2 = Add()([out_res1, dropout_2b])
    out_res2 = Activation('relu')(residual2)

    #ResidualBlock3
    conv1d_1c = TimeDistributed(Conv1D(256, 3, dilation_rate=4, padding="causal", activation="relu"), name="Block3_Conv1")(out_res2)
    dropout_1c = Dropout(0.05)(conv1d_1c)
    conv1d_2c = TimeDistributed(Conv1D(256, 3, dilation_rate=4, padding="causal", activation="relu"), name="Block3_Conv2")(dropout_1c)
    dropout_2c = Dropout(0.05)(conv1d_2c)
    residual3 = Add()([out_res2, dropout_2c])
    out_res3 = Activation('relu')(residual3)

    flatten1 = TimeDistributed(Flatten(), name="Flatten1")(out_res3)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(flatten1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)

def conv_10_residual_block(dataset, dropout=0.1):

    input_data = Input(name='input',
                       shape=(None, dataset.nb_features, dataset.nb_cols, 1))  #  window_size instead of nb_cols

    #masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(input_data)
    reshape0 = TimeDistributed(Reshape((64,32)), name="Reshape_0")(noise)

    #ResidualBlock1
    conv1d_1a = TimeDistributed(Conv1D(32, 3, dilation_rate=1, padding="causal"), name="Block1_Conv1")(reshape0)
    norm_1a = TimeDistributed(BatchNormalization())(conv1d_1a)
    activation_1a = Activation('relu')(norm_1a)
    dropout_1a = Dropout(0.05)(activation_1a)
    conv1d_2a = TimeDistributed(Conv1D(32, 3, dilation_rate=1, padding="causal"), name="Block1_Conv2")(dropout_1a)
    norm_2a = TimeDistributed(BatchNormalization())(conv1d_2a)
    activation_2a = Activation('relu')(norm_2a)
    dropout_2a = Dropout(0.05)(activation_2a)
    residual1 = Add()([reshape0, dropout_2a])
    out_res1 = Activation('relu')(residual1)

    #ResidualBlock2
    conv1d_1b = TimeDistributed(Conv1D(32, 3, dilation_rate=2, padding="causal"), name="Block2_Conv1")(out_res1)
    norm_1b = TimeDistributed(BatchNormalization())(conv1d_1b)
    activation_1b = Activation('relu')(norm_1b)
    dropout_1b = Dropout(0.05)(activation_1b)
    conv1d_2b = TimeDistributed(Conv1D(32, 3, dilation_rate=2, padding="causal"), name="Block2_Conv2")(dropout_1b)
    norm_2b = TimeDistributed(BatchNormalization())(conv1d_2b)
    activation_2b = Activation('relu')(norm_2b)
    dropout_2b = Dropout(0.05)(activation_2b)
    residual2 = Add()([out_res1, dropout_2b])
    out_res2 = Activation('relu')(residual2)

    #ResidualBlock3
    conv1d_1c = TimeDistributed(Conv1D(32, 3, dilation_rate=4, padding="causal"), name="Block3_Conv1")(out_res2)
    norm_1c = TimeDistributed(BatchNormalization())(conv1d_1c)
    activation_1c = Activation('relu')(norm_1c)
    dropout_1c = Dropout(0.05)(activation_1c)
    conv1d_2c = TimeDistributed(Conv1D(32, 3, dilation_rate=4, padding="causal"), name="Block3_Conv2")(dropout_1c)
    norm_2c = TimeDistributed(BatchNormalization())(conv1d_2c)
    activation_2c = Activation('relu')(norm_2c)
    dropout_2c = Dropout(0.05)(activation_2c)
    residual3 = Add()([out_res2, dropout_2c])
    out_res3 = Activation('relu')(residual3)

    #ResidualBlock4
    conv1d_1d = TimeDistributed(Conv1D(32, 3, dilation_rate=8, padding="causal"), name="Block4_Conv1")(out_res3)
    norm_1d = TimeDistributed(BatchNormalization())(conv1d_1d)
    activation_1d = Activation('relu')(norm_1d)
    dropout_1d = Dropout(0.05)(activation_1d)
    conv1d_2d = TimeDistributed(Conv1D(32, 3, dilation_rate=8, padding="causal"), name="Block4_Conv2")(dropout_1d)
    norm_2d = TimeDistributed(BatchNormalization())(conv1d_2d)
    activation_2d = Activation('relu')(norm_2d)
    dropout_2d = Dropout(0.05)(activation_2d)
    residual4 = Add()([out_res3, dropout_2d])
    out_res4 = Activation('relu')(residual4)

    #ResidualBlock5
    conv1d_1e = TimeDistributed(Conv1D(32, 3, dilation_rate=16, padding="causal"), name="Block5_Conv1")(out_res4)
    norm_1e = TimeDistributed(BatchNormalization())(conv1d_1e)
    activation_1e = Activation('relu')(norm_1e)
    dropout_1e = Dropout(0.05)(activation_1e)
    conv1d_2e = TimeDistributed(Conv1D(32, 3, dilation_rate=16, padding="causal"), name="Block5_Conv2")(dropout_1e)
    norm_2e = TimeDistributed(BatchNormalization())(conv1d_2e)
    activation_2e = Activation('relu')(norm_2e)
    dropout_2e = Dropout(0.05)(activation_2e)
    residual5 = Add()([out_res4, dropout_2e])
    out_res5 = Activation('relu')(residual5)

    #ResidualBlock6
    conv1d_1f = TimeDistributed(Conv1D(32, 3, dilation_rate=32, padding="causal"), name="Block6_Conv1")(out_res5)
    norm_1f = TimeDistributed(BatchNormalization())(conv1d_1f)
    activation_1f = Activation('relu')(norm_1f)
    dropout_1f = Dropout(0.05)(activation_1f)
    conv1d_2f = TimeDistributed(Conv1D(32, 3, dilation_rate=32, padding="causal"), name="Block6_Conv2")(dropout_1f)
    norm_2f = TimeDistributed(BatchNormalization())(conv1d_2f)
    activation_2f = Activation('relu')(norm_2f)
    dropout_2f = Dropout(0.05)(activation_2f)
    residual6 = Add()([out_res5, dropout_2f])
    out_res6 = Activation('relu')(residual6)

    #ResidualBlock7
    conv1d_1g = TimeDistributed(Conv1D(32, 3, dilation_rate=64, padding="causal"), name="Block7_Conv1")(out_res6)
    norm_1g = TimeDistributed(BatchNormalization())(conv1d_1g)
    activation_1g = Activation('relu')(norm_1g)
    dropout_1g = Dropout(0.05)(activation_1g)
    conv1d_2g = TimeDistributed(Conv1D(32, 3, dilation_rate=64, padding="causal"), name="Block7_Conv2")(dropout_1g)
    norm_2g = TimeDistributed(BatchNormalization())(conv1d_2g)
    activation_2g = Activation('relu')(norm_2g)
    dropout_2g = Dropout(0.05)(activation_2g)
    residual7 = Add()([out_res6, dropout_2g])
    out_res7 = Activation('relu')(residual7)

    #ResidualBlock8
    conv1d_1h = TimeDistributed(Conv1D(32, 3, dilation_rate=128, padding="causal"), name="Block8_Conv1")(out_res7)
    norm_1h = TimeDistributed(BatchNormalization())(conv1d_1h)
    activation_1h = Activation('relu')(norm_1h)
    dropout_1h = Dropout(0.05)(activation_1h)
    conv1d_2h = TimeDistributed(Conv1D(32, 3, dilation_rate=128, padding="causal"), name="Block8_Conv2")(dropout_1h)
    norm_2h = TimeDistributed(BatchNormalization())(conv1d_2h)
    activation_2h = Activation('relu')(norm_2h)
    dropout_2h = Dropout(0.05)(activation_2h)
    residual8 = Add()([out_res7, dropout_2h])
    out_res8 = Activation('relu')(residual8)

    flatten1 = TimeDistributed(Flatten(), name="Flatten1")(out_res8)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(flatten1)
    output = Activation('softmax', name='Softmax')(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)



def manage_model_template(model, dataset, dropout):
    network = model(dataset, dropout)
    try:
        network = model(dataset, dropout)
        return network
    except:
        print('can not read model. Return default one')
        return conv4(dataset, dropout=0.1)

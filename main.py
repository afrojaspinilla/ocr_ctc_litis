#!/bin/python
"""
A python script used to launch the various tasks for this OCR.
"""

import shutil
import argparse
import pytomlpp
import format_data.producer_ocr_ctc.general_producer as producer
import ocr_ctc.main_convRNN as train
import ocr_ctc.main_evalRNN as evaluation
import hbdex_ocr as run
import new_utils.new_grammar.make_grammar as grammar
from keras.optimizers import Adam


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "config_file",
        type=argparse.FileType("r"),
        help="Path to the configuration file of the OCR",
    )
    parser.add_argument(
        "-f",
        "--format_data",
        action="store_true",
        help="Formats the data before running any other steps",
    )
    parser.add_argument("-t", "--train", action="store_true", help="Trains the OCR")
    parser.add_argument(
        "-e", "--eval", action="store_true", help="Runs the eval on the OCR"
    )
    parser.add_argument("-r", "--run", action="store_true", help="Runs the OCR")
    parser.add_argument("-l", "--lexicon", action='store_true', help="compile the lexicon declared in the config")
    args = parser.parse_args()

    config = pytomlpp.loads(args.config_file.read())
    try:
        ocr_config = config["ocr"]

        if args.format_data:
            format_config = config["ocr"]["format_data"]
            keys = ["train", "valid", "test"]

            producer.main(
                input_path=ocr_config["dataset_path"],
                files_info_lab={key: format_config[key]["label_file"] for key in keys},
                sup_dir={key: format_config[key]["folder_name"] for key in keys},
                h_norm=format_config["normalization"],
                nb_cols=format_config["nb_cols"],
                stride=format_config["stride"],
                str_info=format_config["name"],
                charset=format_config["charset"],
                keep_ratio=format_config["keep_ratio"],
                conv=True,
                conv_pad=True,
                ImageFrombaseline=False,
                window_structure="vertical",
                shuff=format_config["shuffle"],
                padding=format_config["padding"],
                mean_stand=None,
                std_stand=None,
                removed_empty=False,
                no_new_label=True,
            )

        if args.train:
            train_config = config["ocr"]["train"]

            train.main(
                batch_size=train_config["batch_size"],
                nb_batchs_train=-1,
                nb_batchs_valid=-1,
                setPath=ocr_config["formatted_dataset_path"],
                name=ocr_config["folder_name"],
                defined_model=train_config["model"],
                initial_epoch=train_config["initial_epoch"],
                transfer_learning=None
                if train_config["transfer_learning"] == ""
                else train_config["transfer_learning"],
                init_archi=True,
                learning_rate=train_config["learning_rate"],
            )

        if args.eval:
            # NAME DIRECTORY RESULTS AND CHECKPOINTS
            eval_config = config["ocr"]["evaluation"]
            name_model = ocr_config["folder_name"]
            sup_dir = "ocr_ctc/"
            list_log_path = ["./" + sup_dir + "outputs/results/" + name_model + "/"]
            list_dir_weights = [
                "./" + sup_dir + "outputs/checkpoints/" + name_model + "/"
            ]
            save_dir = "./" + sup_dir + "outputs/results/" + name_model + "/"
            evaluation.main_eval(
                list_log_path,
                list_dir_weights,
                [ocr_config["formatted_dataset_path"]],
                set_to_test=eval_config["data_to_test"],
                batch_size=eval_config["batch_size"],
                save_dir=save_dir,
                list_of_it=None
                if len(eval_config["list_of_iterations"]) == 0
                else eval_config["list_of_iterations"],
                file_to_eval=None,
                get_pred=eval_config["get_predictions"],
                get_eval=eval_config["get_eval"],
                get_prob=eval_config["get_proba"],
                get_loss=eval_config["get_loss"],
                rnn_size=[100, 100, 100, 100],
                save_gt=False,
                global_save=False,
                dir_dataset_train=None,
                greedy_decode=eval_config["greedy_decode"],
                top_paths_decode=1,
                beam_width_decode=100,
                optimizer=Adam(lr=0.0001),
                dropout=0.1,
                noise_stddev=0.01,
                name_expe=eval_config["experiment_name"],
                verbose=eval_config["verbose"],
                save_output=True,
                plot_output=False,
                duplicate_labels=None,
            )

        if args.run:
            run_config = config["ocr"]["run"]
            run.run_all(
                run_config["folder_with_jsons"],
                run_config["folder_with_images"],
                run_config["json_output_folder"],
                run_config["images_format"],
                run_config["log_path"],
                run_config["model_weights"],
                fst_folder=run_config['fst_folder'],
                use_title_grammar=run_config['use_title_grammar'],
                use_grammar=run_config['use_grammar']
            )

        if args.lexicon:
            lexicon_file = config["ocr"]["lexicon"]['file_with_lexic']
            grammar.main_grammar(lexicon_file)

    except KeyError:
        print("Please check your configuration file")

# encoding=utf-8
from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class Yell(ImagePreprocessing):
    """
    Hello world!
    """
    def __init__(self, **kwargs):
        self.text = kwargs.get("text")

    def preprocessing(self, image):
        print(self.text)
        return image

    def __str__(self):
        return "Yell"
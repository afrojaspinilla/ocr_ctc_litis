import cv2
import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class ExtractHoGFeatures(ImagePreprocessing):
    """
    Transforme une image en séquence de HoG
    """
    def preprocessing(self, img):
        normF = self.normF
        largeurCol = int(normF[0] / 2)
        m, n = img.shape
        hog = cv2.HOGDescriptor((normF[0], normF[1]), (normF[0], 16), (normF[0], 8), (normF[0], 8), 9)

        for i in range(n):
            colImage = img[:, max(i - largeurCol, 0): min(i + largeurCol, n)]

            if i - largeurCol < 0:
                zeroPadding = np.zeros((m, abs(i - largeurCol)), "uint32")
                colImage = np.concatenate((zeroPadding, colImage), axis=1)

            if i + largeurCol > n:
                zeroPadding = np.zeros((m, i + largeurCol - n), "uint32")
                colImage = np.concatenate((colImage, zeroPadding), axis=1)
            print(colImage.dtype)
            tmp = hog.compute(colImage).L

            if i == 0:
                hogf = np.zeros((n, int(tmp.shape[1])), dtype="uint32")
                hogf[i][:] = tmp
            else:
                hogf[i][:] = tmp

        return hogf

    def __init__(self, **kwargs):
        self.normF = kwargs.get("normF")

    def __str__(self):
        return "HoG Features"

import keras.backend as K




def Kreshape(my_tensor, type_reshape='1D'):
    """ Perform a K.reshape with dimensions not specified (None) """

    if type_reshape=='1D': # Tensor with N dimensions becomes 1D
        return Kreshape_To1D(my_tensor)



def Kreshape_To1D(my_tensor):
    """ Reshape to a 1D Tensor using K.reshape"""

    sum_shape = K.sum(K.shape(my_tensor))
    return K.reshape(my_tensor, (sum_shape,))


def Kreshape_3DTo2Dsumlast2(my_tensor):
    """ Reshape a 3D tensor to a 2D Tensor using K.reshape 
    The last two dimensions are merged 
    DONT WORK
    """

    tensor_shapes = K.shape(my_tensor)
    batch_size = tensor_shapes.get_shape()[0]
    prodlast2 = K.prod(K.shape(my_tensor)[-2:])
    # K.shape(my_tensor)[0]

    #tensor_shapes = K.shape(my_tensor)
    return K.reshape(my_tensor, (batch_size, prodlast2))


def Kreshape_4DTo3Dsumlast2(my_tensor, new_dim=None):
    """ Reshape a 4D tensor to a 3D Tensor using K.reshape 
    The last two dimensions are merged 
    DONT WORK
    """
    new_dim = 32 * 20
    batch_size = K.cast(K.shape(my_tensor)[0], dtype='int32')
    if not new_dim:
        new_dim = K.prod(K.shape(my_tensor)[-2:])
    # K.shape(my_tensor)[0]

    prod_shape = K.prod(K.shape(my_tensor))
    dim1 = K.cast(prod_shape / (batch_size * new_dim), dtype='int32')

    # tensor_shapes = K.shape(my_tensor)
    return K.reshape(my_tensor, (batch_size, dim1, new_dim))


def Kreshape_3DTo4D_lastOne(my_tensor):
    """ Reshape a 3D tensor to a 2D Tensor using K.reshape 
    The last two dimensions are merged 
    DONT WORK
    """
    return K.expand_dims(my_tensor)

    # tensor_shapes = K.shape(my_tensor)
    # batch_size = tensor_shapes.get_shape()[0]
    # # K.shape(my_tensor)[0]
    #
    # # tensor_shapes = K.shape(my_tensor)
    # return K.reshape(my_tensor, (K.shape(my_tensor)[0], K.shape(my_tensor)[1], 128, 1))#K.shape(my_tensor)[2], 1))
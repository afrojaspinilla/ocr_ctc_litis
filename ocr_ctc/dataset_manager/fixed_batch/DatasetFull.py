import pickle

import h5py
import numpy as np
import os
import sys
import random

from ocr_ctc.dataset_manager.Dataset import Dataset
from ocr_ctc.dataset_manager.DatasetInfo import DatasetInfo


class DatasetFull(Dataset):
    def __init__(self, path, train_size=-1, valid_size=-1, test_size=-1):
        super().__init__(path, train_size, test_size, valid_size)

    def new_dataset(self, train, test, valid, label_array, label_dict, padding, sep=' ', all_labels=None, all_label_dict=None, nb_features=None, nb_cols=None):

        self._train_data, self._train_labels, self._train_seqlen, self._train_labellen = train
        self._test_data, self._test_labels, self._test_seqlen, self._test_labellen = test
        self._valid_data, self._valid_labels, self._valid_seqlen, self._valid_labellen = valid

        if not nb_features:
            nb_features = self._train_data[0].shape[2]

        self.dataset_info = DatasetInfo(len(self._train_data), len(self._test_data.shape),
                                        len(self._valid_data), nb_features, padding,
                                        label_array, label_dict, sep=sep, all_labels=all_labels, all_label_dict=all_label_dict, nb_cols=nb_cols)

        self._loaded = True

    def save(self):

        if os.path.exists(self._path):
            print("PATH", self.path, "already exists. The dataset_manager can not be created.")
            sys.exit(0)
        else:
            os.mkdir(self._path)
            os.mkdir(self._path + "/TRAIN")
            os.mkdir(self._path + "/TEST")
            os.mkdir(self._path + "/VALID")

        with open(self._path + "/dataset_manager.pickle", 'wb') as f:
            pickle.dump(self.dataset_info, f)

        for i in range(len(self._train_data)):
            h5f = h5py.File(self._path + "/TRAIN/" + str(i) + ".h5", 'w')
            h5f.create_dataset("data", data=self._train_data[i])
            h5f.create_dataset("labels", data=self._train_labels[i])
            h5f.create_dataset("seqlen", data=self._train_seqlen[i])
            h5f.create_dataset("labellen", data=self._train_labellen[i])
            h5f.close()

        for i in range(len(self._test_data)):
            h5f = h5py.File(self._path + "/TEST/" + str(i) + ".h5", 'w')
            h5f.create_dataset("data", data=self._test_data[i])
            h5f.create_dataset("labels", data=self._test_labels[i])
            h5f.create_dataset("seqlen", data=self._test_seqlen[i])
            h5f.create_dataset("labellen", data=self._test_labellen[i])
            h5f.close()

        for i in range(len(self._valid_data)):
            h5f = h5py.File(self._path + "/VALID/" + str(i) + ".h5", 'w')
            h5f.create_dataset("data", data=self._valid_data[i])
            h5f.create_dataset("labels", data=self._valid_labels[i])
            h5f.create_dataset("seqlen", data=self._valid_seqlen[i])
            h5f.create_dataset("labellen", data=self._valid_labellen[i])
            h5f.close()

    def load(self):
        with open(self._path + "/dataset_manager.pickle", 'rb') as f:
            self.dataset_info = pickle.load(f)

        for batch in sorted(os.listdir(self._path + "/TRAIN/")):
            h5f = h5py.File(self._path + "/TRAIN/" + batch, 'r')
            self._train_data.append(h5f["data"][:])
            self._train_labels.append(h5f["labels"][:])
            self._train_seqlen.append(h5f["seqlen"][:])
            self._train_labellen.append(h5f["labellen"][:])

        for batch in sorted(os.listdir(self._path + "/TEST/")):
            h5f = h5py.File(self._path + "/TEST/" + batch, 'r')
            self._test_data.append(h5f["data"][:])
            self._test_labels.append(h5f["labels"][:])
            self._test_seqlen.append(h5f["seqlen"][:])
            self._test_labellen.append(h5f["labellen"][:])

        for batch in sorted(os.listdir(self._path + "/VALID/")):
            h5f = h5py.File(self._path + "/VALID/" + batch, 'r')
            self._valid_data.append(h5f["data"][:])
            self._valid_labels.append(h5f["labels"][:])
            self._valid_seqlen.append(h5f["seqlen"][:])
            self._valid_labellen.append(h5f["labellen"][:])

    @staticmethod
    def __sort_file_list(files):

        lengths = []
        for file in files:
            lengths.append(file[0].shape[0])

        files, _ = zip(*sorted(zip(files, lengths), key=lambda x: x[1]))
        return list(files)

    def get_train_generator(self, nb_batch=None, order="shuffle"):

        if nb_batch:
            train_data = self._train_data[:nb_batch]
            train_labels = self._train_labels[:nb_batch]
            train_seqlen = self._train_seqlen[:nb_batch]
            train_labellen = self._train_labellen[:nb_batch]
        else:
            train_data = self._train_data
            train_labels = self._train_labels
            train_seqlen = self._train_seqlen
            train_labellen = self._train_labellen

        if order == "shuffle":
            zip_list = list(zip(train_data, train_labels, train_seqlen, train_labellen))
            random.shuffle(zip_list, random.random)
            train_data, train_labels, train_seqlen, train_labellen = [list(tup) for tup in zip(*zip_list)] #zip(*zip_list)

        if order == "curriculum":
            train_data, train_labels, train_seqlen, train_labellen = self.__sort_file_list((train_data, train_labels, train_seqlen, train_labellen))

        while True:
            for ret in zip(train_data, train_labels, train_seqlen,
                           train_labellen):
                yield (list(ret), np.zeros(ret[0].shape[0]))

    def get_test_generator(self):
        while True:
            for ret in zip(self._test_data, self._test_labels, self._test_seqlen,
                           self._test_labellen):
                yield (list(ret), np.zeros(ret[0].shape[0]))

    def get_valid_generator(self):
        while True:
            for ret in zip(self._valid_data, self._valid_labels, self._valid_seqlen,
                           self._valid_labellen):
                yield (list(ret), np.zeros(ret[0].shape[0]))

    @property
    def nb_features(self):
        return self.dataset_info.nb_features

    @property
    def nbbatch_train(self):
        return self.dataset_info.nbbatch_train

    @property
    def nbbatch_test(self):
        return self.dataset_info.nbbatch_test

    @property
    def nbbatch_valid(self):
        return self.dataset_info.nbbatch_valid

    @property
    def padding(self):
        return self.dataset_info.padding

    @property
    def label_array(self):
        return self.dataset_info.label_array

    @property
    def label_dict(self):
        return self.dataset_info.label_dict

    @property
    def nb_labels(self):
        return len(self.dataset_info.label_array)

    @property
    def sep(self):
        try:
            return self.dataset_info.sep
        except AttributeError:
            return ' '

    @property
    def all_labels(self):
        try:
            return self.dataset_info.all_labels if self.dataset_info.all_labels else self.label_array
        except AttributeError:
            return self.label_array

    @property
    def all_label_dict(self):
        try:
            return self.dataset_info.all_labels_dict if self.dataset_info.all_labels_dict else self.label_dict
        except AttributeError:
            return self.label_dict


    @property
    def window_size(self):
        try:
            return self.dataset_info.window_size
        except AttributeError:
            return None

    @property
    def nb_cols(self):
        try:
            return self.dataset_info.nb_cols
        except AttributeError:
            return None
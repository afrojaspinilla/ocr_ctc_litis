import numpy as np
from PIL import Image, ImageDraw
from ocr_ctc.preprocessings.BatchPreprocessing import BatchPreprocessing


class ImagesFromBaseline(BatchPreprocessing):
    """
    Normalization in an interval [min_int, max_int]
    """
    def __init__(self, baselines, page_images, height=0, points_under=None, keep_ratio=True, rate_under=0.25):
        """  
        baselines = list in the same order than train.txt, valid.txt and test.txt with a list of points refering to baselines.
        height = height of the output image
        points_under = number of points that are considered under the baseline interpolation. 
        """
        self.height = height
        if not points_under:
            self.points_under = int(self.height * 0.25)

        self.rate_under = rate_under
        self.sup_points = 2
        #self.points_upper = self.height - self.points_under
        self.baselines = baselines
        self.page_images = page_images
        self.counter = 0

        self.keep_ratio = keep_ratio


    def __call__(self, x, y, seq_len, label_len, padding):


        new_x = []
        new_seq_len = []

        for idx,image in enumerate(x):

            sorted_baseline = sorted(self.baselines[self.counter])
            page_im_file = self.page_images[self.counter]
            page_im = Image.open(page_im_file)
            page_im = np.asarray(page_im.convert('L'))
            image = np.asarray(image.convert('L'))

            self.counter += 1

            if len(sorted_baseline)==0:
                print("Image from baseline preprocessing: there is no baseline. Can\'t do it.")
                new_x.append(image)
                new_seq_len.append(seq_len[idx])
                continue
            elif len(sorted_baseline)==1:
                print("Image from baseline preprocessing: there is only one point in the baseline. Can\'t do it.")
                new_x.append(image)
                new_seq_len.append(seq_len[idx])
                continue

            h_lim, w_lim = image.shape
            h_pim, w_pim = page_im.shape
            #print(image.shape, page_im.shape)
            new_im = Image.fromarray(image)
            new_im.save("../../../../../data/viz_tmp/img" + str(self.counter) + "_line.png", "PNG")
            #new_im.show()
            # new_im = Image.fromarray(page_im)
            # new_im.show()
            #
            # new_im = Image.fromarray(page_im)
            # drawtxt = ImageDraw.Draw(new_im)
            # drawtxt.line(sorted_baseline, width=2)
            # new_im.save("../../../../../data/viz_tmp/img" + str(self.counter) + "_page.png", "PNG")
            # new_im.show()

            points_under = int(h_lim * self.rate_under) + self.sup_points
            nb_points = h_lim + 2 * self.sup_points

            new_image2 = np.zeros((nb_points, int(sorted_baseline[-1][0]-sorted_baseline[0][0])))
            for i in range(len(sorted_baseline)-1):
                a, b = get_straight_line(sorted_baseline[i][0], sorted_baseline[i][1], sorted_baseline[i+1][0],
                                         sorted_baseline[i+1][1])
                current_x = sorted_baseline[i][0]
                for j in range(int(current_x), int(sorted_baseline[i+1][0]), 1):
                    y_baseline = int(a * j + b)
                    try:
                        # be careful that increasing y provides a lower position in the image
                        # new_frame = [page_im[l][j] for l in range(y_baseline + self.points_under, y_baseline, -1)] + \
                        #             [page_im[l][j] for l in range(y_baseline, y_baseline - self.points_upper, -1)]

                        for k in range(nb_points):
                            val = page_im[y_baseline + points_under- k][j]
                            new_image2[nb_points -k -1, j-int(sorted_baseline[0][0])] = val
                    except:
                        #print("The image built is outside the original image. Padding is performed.")
                        # new_frame = []
                        # for l in range(y_baseline + self.points_under, y_baseline, -1):
                        #     if l<0 or l>=h_pim:
                        #         new_frame.append(padding)
                        #     else:
                        #         #new_frame.append(page_im[l,j])
                        #         new_frame.append(page_im[l][j])
                        # for l in range(y_baseline, y_baseline - self.points_upper, -1):
                        #     if l<0 or l>=h_pim:
                        #         new_frame.append(padding)
                        #     else:
                        #         #new_frame.append(page_im[l,j])
                        #         new_frame.append(page_im[l][j])
                        for k in range(nb_points):
                            h_k = y_baseline + self.points_under - k
                            if h_k >= h_pim or h_k<0:
                                new_image2[nb_points -k -1, j - int(sorted_baseline[0][0])] = padding
                            else:
                                val = page_im[h_k][j]
                                new_image2[nb_points -k -1,j-int(sorted_baseline[0][0])] = val

            # new_image = np.asarray(new_image)
            # new_im = Image.fromarray(new_image)
            # new_im.show()
            # new_im2 = Image.fromarray(new_image2)
            # #new_im2.show()
            # new_im2 = new_im2.convert('L')
            # new_im2.save("../../../data/viz_tmp/img" + str(self.counter) + "_baseline.png", "PNG")
            # with open("../../../data/viz_tmp/img" + str(self.counter) + ".txt", 'w') as tmptxt:
            #     tmptxt.write(str(y[idx]))

            # # resize image
            # if self.keep_ratio:
            #     ratio = self.height / h_lim
            #     new_im = new_im.resize((int(w_lim * ratio), self.height), Image.ANTIALIAS)
            # elif self.height > 0:
            #     new_im = new_im.resize((w_lim, self.height), Image.ANTIALIAS)

            #new_im = Image.fromarray(new_image)
            #new_im.save("../../../data/viz_tmp/img2" + str(self.counter) + ".png", "PNG")


            iseq_len = new_image2.shape[1]
            new_seq_len.append(iseq_len)
            #print(new_image2.shape, iseq_len)
            new_x.append(Image.fromarray(new_image2))
            #new_x.append(Image.fromarray(np.transpose(new_image2)))


        return new_x, y, new_seq_len, label_len, padding


    def __str__(self):
        return "Image build according to the baseline. at each image frame, the same length is under and over the baseline."



def get_straight_line(x1, y1, x2, y2):

    a = (y2 - y1)/(x2 - x1)
    b = y2 - a*x2
    return a, b
from ocr_ctc.CTC_model.loggers.Logger import Logger


class HPLogger(Logger):
    def __init__(self, log_path, name="hyperparameters.txt", **kwargs):
        self.name = name
        self.log_path = log_path
        self.parameters = kwargs.get("parameters")

    def log(self, **kwargs):
        with open(self.log_path+"/"+self.name, "a") as f:
            for key, value in self.parameters.items():
                f.write(key+" : "+str(value)+"\n")


from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
from random import shuffle
import shutil

def create_trainValidTest(input_path, dir_origin, writing='all', rates={'train':0.5, 'valid':0.1}):
    """ Create a train, valid and test set. 
    writing = 'neat' if one want only the neat handwriting words, 'quick' for the quick ones, otherwise all"""

    neat = True
    quick = True
    str_info = ''

    if writing == 'neat':
        quick = False
    elif writing == 'quick':
        neat = False

    words = {}
    if neat:
        str_info+='_Neat'
        for dir_word in os.listdir(dir_origin + 'NEAT/'):
            words[str(dir_word)] = []
            for file_im in os.listdir(dir_origin + 'NEAT/' + dir_word + '/'):
                words[str(dir_word)].append(('NEAT',file_im))

    if quick:
        str_info +='_Quick'
        for dir_word in os.listdir(dir_origin + 'QUICK/'):
            if not str(dir_word) in words:
                words[str(dir_word)] = []
            for file_im in os.listdir(dir_origin + 'QUICK/' + dir_word + '/'):
                words[str(dir_word)].append(('QUICK',file_im))

    str_info += '_Tr' + str(rates['train']) + 'Val' + str(rates['valid']) + 'Te' + str(1. - rates['train'] - rates['valid'])

    if not os.path.exists(input_path):
        os.mkdir(input_path)

    # with open(input_path + "/words.txt", "w") as f:
    #     for key in words:
    #         f.write(key + '\n')

    if not os.path.exists(input_path + '/TRAIN/'):
        os.mkdir(input_path + '/TRAIN/')
        os.mkdir(input_path + '/VAL/')
        os.mkdir(input_path + '/TST/')
    for w in words:
        shuffle(words[w])
        nb_train = max(int(rates['train']*len(words[w])),1)
        nb_val = int(rates['valid'] * len(words[w]))
        for i in range(nb_train):
            shutil.copyfile(dir_origin + words[w][i][0] + '/' + w + '/' + words[w][i][1], input_path + '/TRAIN/' + words[w][i][1])
            if 0:#i == 0:
                with open(input_path + "/train.txt", "w") as f:
                    f.write(words[w][i][1] + ' ' + w + '\n')
            else:
                with open(input_path + "/train.txt", "a") as f:
                    f.write(words[w][i][1] + ' ' + w + '\n')

        for i in range(nb_train, nb_train+nb_val,1):
            shutil.copyfile(dir_origin + words[w][i][0] + '/' + w + '/' + words[w][i][1],
                            input_path + '/VAL/' + words[w][i][1])
            if 0:#i == nb_train:
                with open(input_path + "/valid.txt", "w") as f:
                    f.write(words[w][i][1] + ' ' + w + '\n')
            else:
                with open(input_path + "/valid.txt", "a") as f:
                    f.write(words[w][i][1] + ' ' + w + '\n')

        for i in range(nb_train+nb_val, len(words[w]),1):
            shutil.copyfile(dir_origin + words[w][i][0] + '/' + w + '/' + words[w][i][1],
                            input_path + '/TST/' + words[w][i][1])
            if 0:#i == nb_train+nb_val:
                with open(input_path + "/test.txt", "w") as f:
                    f.write(words[w][i][1] + ' ' + w + '\n')
            else:
                with open(input_path + "/test.txt", "a") as f:
                    f.write(words[w][i][1] + ' ' + w + '\n')

    return str_info



def main(input_path, output_path, writing='all', dir_origin='/home/soullard/data/WORDS_HAM_V2/WORDS/',\
         rates={'train':0.5, 'valid':0.1}, \
        h_norm = 32, removed_empty = True, dict_dir = None, nb_cols = 1, stride = 1, \
        keep_ratio = True, conv = False, conv_pad = False, ImageFrombaseline = False, \
        window_structure = 'vertical', shuff = True, padding = -1, standardize = False, ext_origin = ''):

    str_info = ''

    if not os.path.exists(input_path):
        info = create_trainValidTest(input_path, dir_origin, writing=writing, rates=rates)
        str_info += info
    else:
        str_info += '_' + writing
        str_info += '_Tr' + str(rates['train']) + 'Val' + str(rates['valid']) + 'Te' + str(
            1. - rates['train'] - rates['valid'])

    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
    preprocessing_batch = []
    early_preprocessing_batch=[]

    output_path += str_info

    producer = Producer(input_path, output_path,
                        norm = h_norm,
                        keep_ratio = keep_ratio,
                        padding = padding,
                        nb_cols = nb_cols, standardize = standardize,
                        preprocessing_data = preprocessing_data,
                        preprocessing_batch = preprocessing_batch,
                        early_preprocessing_batch = early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TST/"}, \
                                          ext_images=ext_origin)


if __name__ == '__main__':
    nb_cols = 2
    stride = 1
    padding = -1.
    norm = 32
    keep_ratio = True
    standardize = True
    shuff = False

    writing = 'neat'
    input_path = "/home/soullard/code/datasets/Hamelin_tmp/"
    output_path = input_path + "Hamelin_tmp_" + str(norm) + "_" + str(nb_cols)

    main(input_path, output_path, padding=padding, keep_ratio=keep_ratio, h_norm=norm,\
         stride=stride, nb_cols=nb_cols, standardize=standardize, writing=writing, shuff=shuff)
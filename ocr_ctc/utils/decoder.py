import keras.backend as K
import tensorflow as tf
import time


def default_decoding(list_pred, padding=-1):
    """ DEFAULT DECODING function 
    Return the prediction in list_pred without padding """

    decoded = []
    for seq in list_pred:
        out_seq = [elmt for elmt in list(seq) if elmt != padding]
        decoded.append(out_seq)

    return decoded

def decoding_str(pred, ref):
    """ DECODING function 

    Inputs:
        pred = list of int (from 0 to nb_labels)
        ref = list of reference code (e.g. characters)

    Output:
        a string containing elements of ref related to the positions given in pred
    """

    nb_ref = len(ref)
    outstr = ''
    for c in pred:
        if int(c) < nb_ref:
            outstr += ref[int(c)]

    return outstr

def decoding(pred, ref, data_len=-1):
    """ DECODING function 

    Inputs:
        pred = list of int (from 0 to nb_labels)
        ref = list of reference code (e.g. characters)

    Output:
        a list containing elements of ref related to the positions given in pred
    """

    nb_ref = len(ref)
    outlist = []
    for j in range(len(pred)):
        c = int(pred[j])
        if (c>=0 and c<nb_ref) and (j<data_len or data_len==-1):
            outlist.append(ref[c])

    return outlist

def decoding_with_reference(list_pred, list_ref):
    """ DECODING function 

    Inputs:
        pred = list of list of int (from 0 to nb_labels) = list of predictions
        ref = list of reference code (e.g. characters)

    Output:
        decoded = a list of string (one per element from list_pred)
                where each string contains elements of ref related to the positions given in pred
    """

    if len(list_ref) > 0:  # assert there is a reference, otherwise default decoding
        decoded = []
        for i in range(len(list_pred)):
            decoded.append(decoding(list_pred[i], list_ref))
    else:
        return default_decoding(list_pred)

    return decoded


def decodingLen_with_reference(list_pred, list_ref, data_len=[]):
    """ DECODING function 

    Inputs:
        pred = list of list of int (from 0 to nb_labels) = list of predictions
        ref = list of reference code (e.g. characters)

    Output:
        decoded = a list of string (one per element from list_pred)
                where each string contains elements of ref related to the positions given in pred
    """

    if len(data_len)==0:
        return decoding_with_reference(list_pred, list_ref)


    assert(len(list_pred) == len(data_len))

    if len(list_ref) > 0:  # assert there is a reference, otherwise default decoding
        decoded = []
        for i in range(len(list_pred)):
            decoded.append(decoding(list_pred[i], list_ref, data_len[i]))
    else:
        return default_decoding(list_pred)

    return decoded

def decode_batch(batch, x_len, label_array, greedy=True, beam_width=100, top_paths=1):

    batch_pred, batch_lik = K.ctc_decode(batch, tf.squeeze(x_len), greedy, beam_width, top_paths)
    pred_with_padding = [K.eval(elmt) for elmt in batch_pred]  # batch_pred is a list of Tensors, one per top_paths
    pred = [[[elmt for elmt in list(seq_pred) if elmt != -1] for seq_pred in out_pred] for out_pred in
            pred_with_padding]

    lik = K.eval(batch_lik)

    decode_pred = []
    for out_pred in pred:
        decode_pred.append(decoding_with_reference(out_pred, label_array))

    return decode_pred, lik


def fast_decode_batch(batch, x_len, greedy=True, beam_width=100, top_paths=1):
    """ Similar to decode_batch but without eval and decoding
    
    return tf.tensor batch_pred and batch_lik
    """

    batch_pred, batch_lik = K.ctc_decode(batch, tf.squeeze(x_len), greedy, beam_width, top_paths)

    return batch_pred, batch_lik
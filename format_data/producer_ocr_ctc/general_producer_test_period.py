import sys
sys.path.append('./')

import ocr_ctc
from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import random
import time
import re
os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE'

def main(input_path, dir_origin="", \
         dir_line_images='', str_info='', \
         dir_origin_images='../../../data/RIMES_paragraph/all_im/', \
         dir_baselines="", files_info_lab=None, charset=None, \
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, ext_im='.png',\
         window_structure='vertical', shuff=True, padding=-1, standardize=True, ext_origin='xml', \
         mean_stand=None, std_stand=None, no_new_label=False):



    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = os.path.join(input_path, "conv_"  + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride))
        if os.path.exists(output_path):
            print('Skipping, Already exists:',output_path)
            return 0

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []
        output_path = input_path + input_path.split('/')[-2] + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)


    if files_info_lab is None:
        files_info_lab = {"train": "train.txt", "valid": "valid.txt", "test": "test.txt"}

    print(files_info_lab)
    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding, label_array=charset,
                        nb_cols=nb_cols, standardize=standardize,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        early_preprocessing_batch=early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(splitter_label="",\
                                          sup_dir={"train": "images/",\
                                                   "valid": "images/",\
                                                   "test": ""}, \
                                                   #"test": "images/"}, \
                                          files_info_lab=files_info_lab, mean_stand=mean_stand, std_stand=std_stand,\
                                          ext_images="", removed_empty=removed_empty, no_new_label=no_new_label)

    # sup_dir = {"train": "out558_predict0_0.5/extracted_lines/", \
    #            "valid": "out558_predict0_0.5/extracted_lines/", \
    #            "test": "out558_predict0_0.5/extracted_lines/"}, \

def several_datasets(concerned_folder, concerned_file):


    nb_cols = 16 #32  # number of time frames that are considered at each time step
    stride = 2 #4
    padding = -1.
    norm = 32 #64  # height dimension
    keep_ratio = True  # resize the image by keeping the ratio
    conv = True  #  format data for a conv LSTM network
    conv_pad = True  #  do padding in the extremities of the images built for the conv LSTM network
    ImageFrombaseline = False  #  image is build according to the baseline (not available here)
    window_structure = 'vertical'  # parameter for ImageFromBaseline
    input_path = concerned_folder
    shuff = False  # mix the data before saved them
    str_info = 'coulisse_test_period_'+concerned_file.replace('.txt','') #'Art30866_' + elmt  # _30893_1' _test_ArtPoly0p35015
    files_info_lab = {"train": "no", \
                      "valid": "no", \
                      "test": concerned_file}
    mean_stand = 169.14015652234303 #None
    std_stand = 42.83750628205289 #None
    charset = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',\
                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',\
                '0','1','2','3','4','5','6','7','8','9','@','.',' ','/',',','(',')','-',':']
    print(len(charset))
    removed_empty = False
    no_new_label = True # do not add new label if there is some
    main(input_path, \
         h_norm=norm, nb_cols=nb_cols, stride=stride, str_info=str_info, charset=charset, \
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
         window_structure=window_structure, shuff=shuff, padding=padding, files_info_lab=files_info_lab, \
         mean_stand=mean_stand, std_stand=std_stand, removed_empty=removed_empty, no_new_label=no_new_label)



def recursive_looking(concerned_folder):
    for entry in os.scandir(concerned_folder):
        if os.path.isdir(entry.path):
            recursive_looking(entry.path)
        elif '.txt' in entry.name and re.findall(r'[\d]{8}',entry.name) != []:
            temp_info = entry.path.rsplit('/',1)
            da_folder, da_file = temp_info
            da_folder += '/'
            print(da_folder, da_file)
            several_datasets(da_folder, da_file)



if __name__ == '__main__':
    global_start_time = time.time()
    looking_folder = sys.argv[1]
    recursive_looking(looking_folder)
    global_end_time = time.time()
    print('global total time:', global_end_time - global_start_time)

from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.ImagesFromBaseline import ImagesFromBaseline
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import shutil
from lxml import etree
from numpy.random import shuffle

def get_transcription(input_xml):

    tree = etree.parse(input_xml)
    list_trans = []


    for line in tree.findall(".//TextLine"):

        transcript = line.find(".//Unicode")
        list_trans.append(transcript)

    return list_trans

def create_train_valid_test(input_path, dir_xml='../../../code/segmenteurFCN/xml/PlaIR/s0.6/',
                            dir_line_images='../../../data/PlaIR/line_segm0.6/', dict_dir=None):

    if not os.path.exists(input_path):
        os.mkdir(input_path)

    # Read transcriptions from xml


    if not dict_dir:
        list_transcript = []
        for xml_file in [elmt for elmt in os.listdir(dir_xml) if elmt[-3:] == 'xml']:
            transcript = get_transcription(dir_xml + xml_file)
            for k, trans in enumerate(transcript):
                list_transcript.append((xml_file[:-4] + '_' + str(k), trans))
        shuffle(list_transcript)
        nb_train = 45
        nb_val = 9
    else:
        list_transcript = {'train': [], 'valid': [], 'test': []}
        for elmt in dict_dir['train']:
            xml_file = elmt #+ '_p.xml'
            transcript = get_transcription(dir_xml + xml_file)
            for k, trans in enumerate(transcript):
                list_transcript['train'].append((xml_file[:-4] + '_' + str(k), trans))
        for elmt in dict_dir['valid']:
            xml_file = elmt #+ '_p.xml'
            transcript = get_transcription(dir_xml + xml_file)
            for k, trans in enumerate(transcript):
                list_transcript['valid'].append((xml_file[:-4] + '_' + str(k), trans))
        for elmt in dict_dir['test']:
            xml_file = elmt #+ '_p.xml'
            transcript = get_transcription(dir_xml + xml_file)
            for k, trans in enumerate(transcript):
                list_transcript['test'].append((xml_file[:-4] + '_' + str(k), trans))


    # create training / valid and test sets
    if not os.path.exists(input_path + '/TRAIN/'):
        os.mkdir(input_path + '/TRAIN/')
        os.mkdir(input_path + '/VAL/')
        os.mkdir(input_path + '/TST/')

    if not dict_dir:
        for i, (name_file, labeltranscript) in enumerate(list_transcript):

            label = '' if not labeltranscript or len(labeltranscript)==0 else labeltranscript

            if i<nb_train:
                shutil.copyfile(dir_line_images + name_file + '.jpeg', input_path + '/TRAIN/' + name_file + '.jpeg')
                if len(label)==0:
                    if os.path.exists(dir_line_images + name_file + 'txt'):
                        with open(dir_line_images + name_file + 'txt', "r") as my_file:
                            label = my_file.readlines()
                if i == 0:
                    with open(input_path + "/train.txt", "w") as train:
                        train.write(name_file + '.jpeg' + ' ' + label + '\n')
                else:
                    with open(input_path + "/train.txt", "a") as train:
                        train.write(name_file + '.jpeg' + ' ' + label + '\n')
            elif i < nb_train + nb_val:
                shutil.copyfile(dir_line_images + name_file + '.jpeg', input_path + '/VAL/' + name_file + '.jpeg')
                if i == nb_train:
                    with open(input_path + "/valid.txt", "w") as valid:
                        valid.write(name_file + '.jpeg' + ' ' + label + '\n')
                else:
                    with open(input_path + "/valid.txt", "a") as valid:
                        valid.write(name_file + '.jpeg' + ' ' + label + '\n')
            else:
                shutil.copyfile(dir_line_images + name_file + '.jpeg', input_path + '/TST/' + name_file + '.jpeg')
                if i== nb_train + nb_val:
                    with open(input_path + "/test.txt", "w") as test:
                        test.write(name_file + '.jpeg' + ' ' + label + '\n')
                else:
                    with open(input_path + "/test.txt", "a") as test:
                        test.write(name_file + '.jpeg' + ' ' + label + '\n')
    else:
        for i, (name_file, labeltranscript) in enumerate(list_transcript['train']):
            if not os.path.isfile(dir_line_images + name_file + '.jpeg'):
                continue
            label = '' if not labeltranscript or len(labeltranscript) == 0 else labeltranscript
            if len(label) == 0:
                if os.path.exists(dir_line_images + name_file + '.txt'):
                    with open(dir_line_images + name_file + '.txt', "r") as my_file:
                        label = my_file.readlines()
                        label = label[0]
            shutil.copyfile(dir_line_images + name_file + '.jpeg', input_path + '/TRAIN/' + name_file + '.jpeg')
            if i == 0:
                with open(input_path + "/train.txt", "w") as my_file:
                    my_file.write(name_file + '.jpeg' + ' ' + label + '\n')
            else:
                with open(input_path + "/train.txt", "a") as my_file:
                    my_file.write(name_file + '.jpeg' + ' ' + label + '\n')
        for i, (name_file, labeltranscript) in enumerate(list_transcript['valid']):
            if not os.path.isfile(dir_line_images + name_file + '.jpeg'):
                continue
            label = '' if not labeltranscript or len(labeltranscript) == 0 else labeltranscript
            shutil.copyfile(dir_line_images + name_file + '.jpeg', input_path + '/VAL/' + name_file + '.jpeg')
            if i == 0:
                with open(input_path + "/valid.txt", "w") as my_file:
                    my_file.write(name_file + '.jpeg' + ' ' + label + '\n')
            else:
                with open(input_path + "/valid.txt", "a") as my_file:
                    my_file.write(name_file + '.jpeg' + ' ' + label + '\n')
        for i, (name_file, labeltranscript) in enumerate(list_transcript['test']):
            if not os.path.isfile(dir_line_images + name_file + '.jpeg'):
                continue
            label = '' if not labeltranscript or len(labeltranscript) == 0 else labeltranscript
            shutil.copyfile(dir_line_images + name_file + '.jpeg', input_path + '/TST/' + name_file + '.jpeg')
            if i == 0:
                with open(input_path + "/test.txt", "w") as my_file:
                    my_file.write(name_file + '.jpeg' + ' ' + label + '\n')
            else:
                with open(input_path + "/test.txt", "a") as my_file:
                    my_file.write(name_file + '.jpeg' + ' ' + label + '\n')



def main(input_path, dir_origin='../../../code/segmenteurFCN/xml/PlaIR/s0.6/', dir_line_images='../../../data/PlaIR/line_segm0.6/',
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, shuff=False, ImageFrombaseline=False):
    #nb_cols = 1
    stride = 4
    padding = -1.
    norm = h_norm
    keep_ratio = True
    splitter_label = ""
    conv = True
    conv_pad = True
    ImageFrombaseline = ImageFrombaseline


    # if not os.path.exists(input_path):
    #     create_train_valid_test(input_path, dir_xml=dir_origin, dir_line_images=dir_line_images, dict_dir=dict_dir)
    # if not os.path.exists(input_path + "/TRAIN/"):
    #     if not os.path.exists(input_path + "/TST/"):
    #         os.mkdir(input_path + "/TST/")
    #     with open(input_path + 'test.txt', 'w') as ftest:
    #         for file_im in os.listdir(dir_line_images):
    #             ftest.write(file_im + ' \n')
    #             shutil.copy(dir_line_images + file_im, input_path + "/TST/" + file_im)


    input_path = input_path + '/'


    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        if ImageFrombaseline:
            #preprocessing_batch.append(ImagesFromBaseline(baselines, norm))
            norm = 0
        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_pad = "pad" if conv_pad else ""
        output_path = output_path = input_path + "PlaIRconv431_" + str(norm) + "_" + str(nb_cols) + "_" + str(stride) + "_" + str_pad

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        preprocessing_batch = []
        if ImageFrombaseline:
            #preprocessing_batch.append(ImagesFromBaseline(baselines, norm))
            norm = 0
        output_path = input_path + "PlaIR431_" + str(norm) + "_" + str(nb_cols)

    producer = Producer(input_path, output_path,
                        norm=norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=True,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        shuff=shuff
                        ).produce_dataset(splitter_label=splitter_label, sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "config_fcn/M431_Zoom1_Seuil0.55/extracted_lines/"},
                                          files_info_lab={"train": "train2iter_reduced_charset.txt", "valid": "valid.txt",
                                                          "test": "test_Mom431.txt"}, removed_empty=removed_empty)#produce_dataset(splitter_label=splitter_label, sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TEST/"},
                                          # files_info_lab={"train": "train2_reduced_charset_tmp.txt", "valid": "valid.txt",
                                          #                 "test": "test_tmp.txt"}, removed_empty=removed_empty)



if __name__ == '__main__':

    input_path = "../datasets/PlaIR/" #match_baselineResizedLines/ PlaIR_Mom558_0.8/" #PlaIRlab0.8/"
    dir_xml = '../../../../../code/segmenteurFCN/xml/PlaIR/s0.8/'
    dir_line_images = '../../../../../Documents/PlaIR/M-Schonbuch/images/MsMt-431/'
    h_norm = 64
    nb_cols = 16
    shuff = False
    removed_empty=False

    dict_dir = None#{'train': ['Mom_558_0010_0002_p.xml', 'Mom_558_0011_0003_p.xml'], 'valid': [], 'test': []}

    main(input_path, dir_origin=dir_xml, dir_line_images=dir_line_images, dict_dir=dict_dir, \
         h_norm=h_norm, removed_empty=removed_empty, nb_cols=nb_cols, shuff=shuff)


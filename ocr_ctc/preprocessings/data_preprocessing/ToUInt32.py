from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class ToUInt32(ImagePreprocessing):
    """
    Transforme le type de la séquence en uint32
    """
    def __init__(self, **kwargs):
        pass

    def preprocessing(self, image):
        return image.astype('uint32')

    def __str__(self):
        return "To Int32"
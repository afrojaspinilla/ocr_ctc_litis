import codecs
import getopt
import os
import sys
from random import shuffle

import numpy as np
from PIL import Image
from keras.preprocessing import sequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Standardize import Standardize
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose

from ocr_ctc.dataset_manager.new_batch.DatasetBatch import DatasetBatch

SIZE_RESIZE = 128


def load_txt(dir_file):
    f = codecs.open(dir_file, 'r', encoding='utf-8')  # open(dir_file,'r')
    data = ''
    for line in f.readlines():
        data = line.replace('\n', '')

    f.close()

    if not data:
        print("LOAD TXT : data " + dir_file + " is empty")
    return data


def produce_dataset(input_path, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=[], preprocessings_batch=[],
                    padding=-1., norm_height=100):
    train_files = []
    test_files = []
    valid_files = []
    label_dict = {}
    label_array = []

    cur_label = 0

    # read training labels, then valid and testing
    name_file = 'text_codage_syllabee_2_mgram_no_alpha.'
    list_labels_train = []
    with open(input_path + 'tokens/' + name_file + 'tr.txt', "r") as f_train:
        for line in f_train.readlines():
            data = line.replace('\n', '').replace('\r', '').split(' ')
            if len(data[-1])==0:
                data = data[:-1]
            else:
                print("end is not an empty term")
            if len(data)==0:
                print(line, "train is empty.")
                continue
            #print("labels:", data)
            list_labels_train.append(data)

            for i in data:
                if i not in label_dict:
                    label_dict[i] = cur_label
                    label_array.append(i)
                    cur_label += 1

    list_labels_valid = []
    with open(input_path + 'tokens/' + name_file + 'cv.txt', "r") as f_val:
        for line in f_val.readlines():
            data = line.replace('\n', '').replace('\r', '').split(' ')
            if len(data[-1])==0:
                data = data[:-1]
            if len(data)==0:
                print(line, "valid is empty.")
                continue
            list_labels_valid.append(data)

            for i in data:
                if i not in label_dict:
                    label_dict[i] = cur_label
                    label_array.append(i)
                    cur_label += 1

    list_labels_test = []
    with open(input_path + 'tokens/' + name_file + 'tst.txt', "r") as f_test:
        for line in f_test.readlines():
            data = line.replace('\n', '').replace('\r', '').split(' ')
            if len(data[-1])==0:
                data = data[:-1]
            if len(data)==0:
                print(line, "test is empty.")
                continue
            list_labels_test.append(data)

            for i in data:
                if i not in label_dict:
                    label_dict[i] = cur_label
                    label_array.append(i)
                    cur_label += 1


    it_train = 0
    it_valid = 0
    it_test = 0
    # Read files containing images
    for my_dir in os.listdir(input_path):
        new_dir = input_path + '/' + my_dir + '/'
        if 'TRAIN' in my_dir:
            list_dir = os.listdir(new_dir)
            list_dir = sorted(list_dir, key=lambda y: (int(y.split('.')[0].split('_')[0].split('-')[1]), int(y.split('.')[0].split('_')[1]), int(y.split('.')[0].split('_')[2])))
            for my_file in list_dir:
                if ("train" in my_file) and ('txt' not in my_file) and ('Gray.png' not in my_file):
                    name = my_file[:-4]
                    label_seq = list_labels_train[it_train] #load_txt(new_dir + name + ".gt.txt")
                    it_train += 1
                    train_files.append((new_dir + "/" + name + ".Gray.png", label_seq))
                    with open(input_path + 'TRAIN/' + name + '.gt.txt', "r") as f_label:
                        for line in f_label.readlines():
                            print(my_file, line)
                            print(list_labels_train[it_train-1])

        if 'TST' in my_dir:
            list_dir = os.listdir(new_dir)
            list_dir = sorted(list_dir, key=lambda y: (\
                int(y.split('.')[0].split('_')[0].split('-')[1]), int(y.split('.')[0].split('_')[1]),\
                int(y.split('.')[0].split('_')[2])))

            for my_file in list_dir:
                if ("eval" in my_file) and ('txt' not in my_file) and ('Gray.png' not in my_file):
                    name = my_file[:-4]
                    label_seq = list_labels_test[it_test] #load_txt(new_dir + name + ".gt.txt")
                    it_test += 1
                    test_files.append((new_dir + "/" + name + ".Gray.png", label_seq))

        if 'VAL' in my_dir:
            list_dir = os.listdir(new_dir)
            list_dir = sorted(list_dir, key=lambda y: (\
                int(y.split('.')[0].split('_')[0].split('-')[1]), int(y.split('.')[0].split('_')[1]),\
                int(y.split('.')[0].split('_')[2])))
            for my_file in list_dir:
                if ("train" in my_file) and ('txt' not in my_file) and ('Gray.png' not in my_file):
                    name = my_file[:-4]
                    label_seq = list_labels_valid[it_valid] #load_txt(new_dir + name + ".gt.txt")
                    it_valid += 1
                    valid_files.append((new_dir + "/" + name + ".Gray.png", label_seq))

    shuffle(train_files)
    shuffle(test_files)
    shuffle(valid_files)

    for i in range(len(train_files)):
        train_files[i] = (train_files[i][0], [label_dict[x] for x in train_files[i][1]])

    for i in range(len(test_files)):
        test_files[i] = (test_files[i][0], [label_dict[x] for x in test_files[i][1]])

    for i in range(len(valid_files)):
        valid_files[i] = (valid_files[i][0], [label_dict[x] for x in valid_files[i][1]])

    def load_img(it_data):
        output_path, label = train_files[it_data]
        image = Image.open(output_path)
        image = image.convert('L')
        width, height = image.size
        ratio = SIZE_RESIZE / height
        image = image.resize((int(width * ratio), SIZE_RESIZE), Image.ANTIALIAS)
        return np.array(image)

    def __prepare_set(file_list, index, size, padding, label_padding):
        x = []
        y = []
        x_length = []
        y_length = []
        try:
            for j in range(size):
                output_path, label = file_list[index + j]
                image = Image.open(output_path)
                image = image.convert('L')
                width, height = image.size
                ratio = norm_height / height
                image = image.resize((int(width * ratio), norm_height), Image.ANTIALIAS)
                image =  np.array(image)
                for preproc in preprocessings_data:
                    image, label = preproc(image, label)
                seq_len = image.shape[0]
                label_len = len(label)

                x.append(image)
                y.append(label)
                x_length.append(seq_len)
                y_length.append(label_len)
        except IndexError:
            print("Ignoring last batch...")

        for preproc_batch in preprocessings_batch:
            x, y, x_length, y_length, padding = preproc_batch(x, y, x_length, y_length, padding)

        x = sequence.pad_sequences(x, value=float(padding), dtype='float32', padding="post")
        y = sequence.pad_sequences(y, value=float(label_padding), dtype='float32', padding="post")

        return x, y, np.asarray(x_length), np.asarray(y_length)

    my_norm_std = Standardize(**{'func_load_img': load_img, 'nb_data': len(train_files)})
    mean, std = my_norm_std.get_mean_std()
    padding = my_norm_std.padding()
    preprocessing_data.append(Standardize(**{'mean': mean, 'std': std}))

    dataset = DatasetBatch(output_path)
    dataset.new_dataset(label_array=label_array, label_dict=label_dict, padding=padding)

    for i in range(0, len(train_files), train_size):
        dataset.save_train_batch(__prepare_set(train_files, i, train_size, padding, len(label_array)))

    for i in range(0, len(test_files), test_size):
        dataset.save_test_batch(__prepare_set(test_files, i, test_size, padding,len(label_array)))

    for i in range(0, len(valid_files), valid_size):
        dataset.save_valid_batch(__prepare_set(valid_files, i, valid_size, padding,len(label_array)))

    dataset.save()

if __name__ == '__main__':
    opts, _ = getopt.getopt(sys.argv[1:], '', ["input=", "out="])
    input_path = output_path = None

    input_path = "../../datasets/RIMES/"
    output_path = "../../datasets/RIMES/batch2grams3col" + str(SIZE_RESIZE)
    nb_col = 3
    padding = -1.
    train_size = 64
    test_size = 32
    valid_size = 32
    cropped = False

    for opt, arg in opts:
        if '--input' in opt:
            input_path = arg
        elif '--out' in opt:
            output_path = arg
        elif '--cropped' in opt:
            cropped = True

    if not input_path or not output_path:
        exit()

    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_col, padding=padding)]

    produce_dataset(input_path, output_path,
                            train_size, valid_size, test_size,
                            preprocessings_data=preprocessing_data, preprocessings_batch=[],
                            padding=padding, norm_height=SIZE_RESIZE)

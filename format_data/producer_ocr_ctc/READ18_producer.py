from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import random


def create_general_set(input_path, test_rate=0.1):

    my_dir = '/home/soullard/data/READ_ICFHR18/general_data/'
    all_files = {}
    for i,dir_num in enumerate(sorted(os.listdir(my_dir))):
        files_set = [fil for fil in os.listdir(my_dir + dir_num + '/' + os.listdir(my_dir + dir_num + '/')[0]) if fil[-3:]=='jpg']
        try:
            num_lines = [int(fl.split('_')[-1].split('.')[0].split('l')[-1]) if not 'line' in fl else int(fl.split('_')[-1].split('.')[0]) for fl in files_set]
        except:
            num_lines = [int(fl.split('_')[-1].split('.')[0][2:]) if 'r' in fl.split('_')[-1].split('.')[0] else int(fl.split('_')[-1].split('.')[0]) for fl in files_set]
        _, _, _, files_set = zip(*sorted(zip([int(fl.split('_')[0]) for fl in files_set], [int(fl.split('_')[1]) for fl in files_set], num_lines, files_set)))
        files_set = [elmt for elmt in files_set]
        all_files[dir_num] = files_set
        #random.shuffle(list(files_set))
        nb_test_files = max(int(len(files_set)*test_rate), 1)
        list_test = [files_set[k] for k in range(nb_test_files)]
        list_valid = [files_set[nb_test_files + k] for k in range(nb_test_files)]

        file_save = 'w' if i==0 else 'a'
        with open(input_path + 'testG.txt', file_save) as ftest:
            for k in range(nb_test_files):
                label_file = my_dir + dir_num + '/' + os.listdir(my_dir + dir_num + '/')[0] + '/' + files_set[k] + '.txt'
                with open(label_file, 'r') as labf:
                    label = labf.readlines()
                ftest.write(files_set[k] + ' ' + label[0] + '\n')

        with open(input_path + 'validG.txt', file_save) as ftest:
            for k in range(nb_test_files):
                label_file = my_dir + dir_num + '/' + os.listdir(my_dir + dir_num + '/')[0] + '/' + list_valid[k] + '.txt'
                with open(label_file, 'r') as labf:
                    label = labf.readlines()
                ftest.write(list_valid[k] + ' ' + label[0] + '\n')

        with open(input_path + 'trainG.txt', file_save) as ftrain:
            for k in range(len(all_files[dir_num])):
                if all_files[dir_num][k] not in list_test and all_files[dir_num][k] not in list_valid:
                    label_file = my_dir + dir_num + '/' + os.listdir(my_dir + dir_num + '/')[0] + '/' + all_files[dir_num][k] + '.txt'
                    with open(label_file, 'r') as labf:
                        label = labf.readlines()
                    ftrain.write(all_files[dir_num][k] + ' ' + label[0] + '\n')






def main(input_path, dir_origin="", \
         dir_line_images='', str_info='', \
         dir_origin_images='../../../data/RIMES_paragraph/all_im/', \
         dir_baselines="", files_info_lab=None, charset=None, \
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, ext_im='.png',\
         window_structure='vertical', shuff=True, padding=-1, standardize=True, ext_origin='xml', \
         mean_stand=None, std_stand=None):


    for key in files_info_lab:
        if not os.path.exists(input_path + files_info_lab[key]):
            if 'G' in files_info_lab[key]:
                if not os.path.exists(input_path + key + 'G.txt'):
                    create_general_set(input_path)



    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + input_path.split('/')[-2] + "conv"  + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []

        output_path = input_path + input_path.split('/')[-2] + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)


    if files_info_lab is None:
        files_info_lab = {"train": "train.txt", "valid": "valid.txt", "test": "test.txt"}


    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding, label_array=charset,
                        nb_cols=nb_cols, standardize=standardize,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        early_preprocessing_batch=early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(splitter_label="",\
                                          sup_dir={"train": "out431_predict0_0.5/extracted_lines/",\
                                                   "valid": "out431_predict0_0.5/extracted_lines/",\
                                                   "test": "out431_predict0_0.5/extracted_lines/"}, \
                                          files_info_lab=files_info_lab, mean_stand=mean_stand, std_stand=std_stand,\
                                          ext_images="", removed_empty=removed_empty)



def several_datasets():

    nb_cols = 64  # number of time frames that are considered at each time step
    stride = 4
    padding = -1.
    norm = 96  # height dimension
    keep_ratio = True  # resize the image by keeping the ratio
    conv = True  #  format data for a conv LSTM network
    conv_pad = True  #  do padding in the extremities of the images built for the conv LSTM network
    ImageFrombaseline = False  #  image is build according to the baseline (not available here)
    window_structure = 'vertical'  # parameter for ImageFromBaseline

    input_path = "../../../datasets/PlaIR/"

    shuff = False  # mix the data before saved them

    for elmt in ['0']:

        str_info = '_test431_predict0_0.5' #'Art30866_' + elmt  # _30893_1' _test_ArtPoly0p35015

        files_info_lab = {"train": "no", \
                          "valid": "no", \
                          "test": "test431-0_05.txt"}

        mean_stand = 210.1318798108303  # 209.55817933603757 #19.120696372144852# 209.55817933603757 # None = default, this is computed on the training set
        std_stand = 35.72464275175115  # 36.98662735037311 # 58.82913064998043 # 36.98662735037311 # None = default, this is computed on the training set

        charset = []
        with open('/'.join(input_path.split('/')[:-2]) + '/READ18/' + 'charset.txt') as f:
            for line in f.readlines():
                charset.append(line.rstrip('\n'))

        removed_empty = False

        main(input_path, \
             h_norm=norm, nb_cols=nb_cols, stride=stride, str_info=str_info, charset=charset, \
             keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
             window_structure=window_structure, shuff=shuff, padding=padding, files_info_lab=files_info_lab, \
             mean_stand=mean_stand, std_stand=std_stand, removed_empty=removed_empty)



if __name__ == '__main__':

    several_datasets()

    # nb_cols = 64 # number of time frames that are considered at each time step
    # stride = 4
    # padding = -1.
    # norm = 96 # height dimension
    # keep_ratio = True # resize the image by keeping the ratio
    # conv = True # format data for a conv LSTM network
    # conv_pad = True # do padding in the extremities of the images built for the conv LSTM network
    # ImageFrombaseline = False # image is build according to the baseline (not available here)
    # window_structure = 'vertical' # parameter for ImageFromBaseline
    #
    # input_path = "../datasets/READ18/"
    #
    # shuff=False # mix the data before saved them
    #
    # str_info = 'Art30882_16' #_30893_1' _test_ArtPoly0p35015
    #
    # files_info_lab = {"train": "trainArt30882_16only.txt", \
    #                   "valid": "valid30882_4only.txt", \
    #                   "test": "no"} # v
    #
    # mean_stand = 210.1318798108303 #209.55817933603757 #19.120696372144852# 209.55817933603757 # None = default, this is computed on the training set
    # std_stand = 35.72464275175115#36.98662735037311 # 58.82913064998043 # 36.98662735037311 # None = default, this is computed on the training set
    #
    # charset = []
    # with open(input_path + 'charset.txt') as f:
    #     for line in f.readlines():
    #         charset.append(line.rstrip('\n'))
    #
    # removed_empty = False
    #
    # main(input_path, \
    #      h_norm=norm, nb_cols=nb_cols, stride=stride, str_info=str_info, charset=charset,\
    #      keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
    #      window_structure=window_structure, shuff=shuff, padding=padding, files_info_lab=files_info_lab,\
    #      mean_stand=mean_stand, std_stand=std_stand, removed_empty=removed_empty)


from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class ToFloat64(ImagePreprocessing):
    """
    Transforme le type de la séquence en float64
    """
    def preprocessing(self, image):
        return image.astype('float64')

    def __init__(self, **kwargs):
        pass

    def __str__(self):
        return "To Float64"
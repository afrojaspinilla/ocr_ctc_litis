# encoding=utf-8
import editdistance
import itertools
import tensorflow as tf
from ocr_ctc.utils.decoder import fast_decode_batch
import operator
import numpy as np
from collections import defaultdict


def lowest_cost_action(ic, dc, sc, im, dm, sm, cost):
    """Given the following values, choose the action (insertion, deletion,
    or substitution), that results in the lowest cost (ties are broken using
    the 'match' score).  This is used within the dynamic programming algorithm.
    * ic - insertion cost
    * dc - deletion cost
    * sc - substitution cost
    * im - insertion match (score)
    * dm - deletion match (score)
    * sm - substitution match (score)
    """
    best_action = None
    best_match_count = -1
    min_cost = min(ic, dc, sc)
    if min_cost == sc and cost == 0:
        best_action = 'equal'
        best_match_count = sm
    elif min_cost == sc and cost == 1:
        best_action = 'replace'
        best_match_count = sm
    elif min_cost == ic and im > best_match_count:
        best_action = 'insert'
        best_match_count = im
    elif min_cost == dc and dm > best_match_count:
        best_action = 'delete'
        best_match_count = dm
    return best_action

def highest_match_action(ic, dc, sc, im, dm, sm, cost):
    """Given the following values, choose the action (insertion, deletion, or
    substitution), that results in the highest match score (ties are broken
    using the distance values).  This is used within the dynamic programming
    algorithm.
    * ic - insertion cost
    * dc - deletion cost
    * sc - substitution cost
    * im - insertion match (score)
    * dm - deletion match (score)
    * sm - substitution match (score)
    """
    # pylint: disable=unused-argument
    best_action = None
    lowest_cost = float("inf")
    max_match = max(im, dm, sm)
    if max_match == sm and cost == 0:
        best_action = 'equal'
        lowest_cost = sm
    elif max_match == sm and cost == 1:
        best_action = 'replace'
        lowest_cost = sm
    elif max_match == im and ic < lowest_cost:
        best_action = 'insert'
        lowest_cost = ic
    elif max_match == dm and dc < lowest_cost:
        best_action = 'delete'
        lowest_cost = dc
    return best_action


def compute_edit_distance(true_labeling, decoded_seq, num_data=-1, norm_ed = False):
    """ Compute the sum of the edit distance on dataset_manager
    Inputst:
        true_labeling = list of sequence labeling as the ground truth
        decoded_seq = list of predicted sequences of labels
        
    Outputs:
        sum_ed = sum of edit distance on the entire dataset_manager
        sum_norm_ed = sum of normalized edit distance (normalization is the len of the ground truth)
    """

    mean_norm_ed = 0.0
    mean_ed = 0.0

    if num_data == -1:
        num_data = len(decoded_seq)

    for j in range(num_data):
        edit_dist = editdistance.eval(decoded_seq[j], true_labeling[j])
        mean_ed += float(edit_dist)
        mean_norm_ed += float(edit_dist) / len(true_labeling[j]) if len(true_labeling[j])>0 else float(edit_dist)

    if norm_ed and num_data>0:
        mean_norm_ed = mean_norm_ed / num_data
        mean_ed = mean_ed / num_data

    return mean_ed, mean_norm_ed


def compute_normed_edit_distance(decoded_seq, true_labeling):
    """ Compute the sum of the edit distance on dataset_manager
    Inputst:
        true_labeling = list of sequence labeling as the ground truth
        decoded_seq = list of predicted sequences of labels

    Outputs:
        sum_ed = sum of edit distance on the entire dataset_manager
        sum_norm_ed = sum of normalized edit distance (normalization is the len of the ground truth)
    """

    mean_ed = 0.0

    mean_ed = editdistance.eval(decoded_seq, true_labeling)

    if len(true_labeling)==0 and len(decoded_seq)==0:
        return mean_ed
    else:
        mean_ed = mean_ed / np.max([len(true_labeling), len(decoded_seq)])

    return mean_ed


def compute_list_edit_distance(true_labeling, decoded_seq, num_data=-1, norm_ed=False):
    """ Compute the sum of the edit distance on dataset_manager
    Inputst:
        true_labeling = list of sequence labeling as the ground truth
        decoded_seq = list of predicted sequences of labels

    Outputs:
        sum_ed = sum of edit distance on the entire dataset_manager
        sum_norm_ed = sum of normalized edit distance (normalization is the len of the ground truth)
    """

    mean_norm_ed = 0.0
    mean_ed = 0.0
    list_ed = []

    if num_data == -1:
        num_data = len(decoded_seq)

    for j in range(num_data):
        edit_dist = editdistance.eval(decoded_seq[j], true_labeling[j])
        mean_ed += float(edit_dist)
        mean_norm_ed += float(edit_dist) / len(true_labeling[j]) if len(true_labeling[j])>0 else float(edit_dist)
        list_ed.append(float(edit_dist))

    if norm_ed and num_data>0:
        mean_norm_ed = mean_norm_ed / num_data
        mean_ed = mean_ed / num_data

    return mean_ed, mean_norm_ed, list_ed


def compute_character_error_rate(true_labeling, decoded_seq):
    """ Compute the character error rate from 2 string"""

    ch_gt = list(true_labeling)
    ch_dec = list(decoded_seq)

    edit_dist = editdistance.eval(ch_dec, ch_gt)

    if len(ch_gt) == 0:
        return edit_dist

    return float(edit_dist)/len(ch_gt)


def compute_cer_backpointer(true_labeling, decoded_seq):
    """ Compute the character error rate from 2 string"""

    ch_gt = list(true_labeling)
    ch_dec = list(decoded_seq)
    d, matches, opcodes = edit_distance_backpointer(ch_dec, ch_gt)
    edit_dist = d[-1][-1]

    seq_op = {'equal': 0, 'replace': 0, 'insert': 0, 'delete': 0}
    for elmt in opcodes:
        if elmt[0] in seq_op:
            seq_op[elmt[0]] += 1
        else:
            print(elmt[0], "not a standard analysis of edit distance operation.")

    if len(ch_gt) == 0:
        return edit_dist, seq_op

    return float(edit_dist)/len(ch_gt), seq_op



def compute_character_error_rate_from_lists(list_true_labeling, list_decoded_seq, lab_to_replace=None):
    """ Compute the character error rate from list of string
    
    lab_to_replace = list of tuple containing the sequences of labels that must be replaced by only one character 
    """

    nb_data = len(list_true_labeling)
    assert(len(list_decoded_seq)==nb_data)

    list_cer = []
    for k in range(nb_data):

        if isinstance(list_true_labeling[k], list):
            true_lab = ''.join(list_true_labeling[k])
        else:
            true_lab = list_true_labeling[k]
        if isinstance(list_decoded_seq[k], list):
            dec_seq = ''.join(list_decoded_seq[k])
        else:
            dec_seq = list_decoded_seq[k]

        for lr in lab_to_replace:
            true_lab=true_lab.replace(lr[0], lr[1])
            dec_seq=dec_seq.replace(lr[0], lr[1])

        cer = compute_character_error_rate(true_lab, dec_seq)
        list_cer.append(cer)

    return list_cer


def compute_cer_backpointer_from_lists(list_true_labeling, list_decoded_seq, lab_to_replace=None):
    """ Compute the character error rate from list of string

    lab_to_replace = list of tuple containing the sequences of labels that must be replaced by only one character 
    """

    nb_data = len(list_true_labeling)
    assert (len(list_decoded_seq) == nb_data)

    list_cer = []
    analysis_op = {'equal': 0, 'replace': 0, 'insert': 0, 'delete': 0}
    total_op = 0.
    for k in range(nb_data):

        if isinstance(list_true_labeling[k], list):
            true_lab = ''.join(list_true_labeling[k])
        else:
            true_lab = list_true_labeling[k]
        if isinstance(list_decoded_seq[k], list):
            dec_seq = ''.join(list_decoded_seq[k])
        else:
            dec_seq = list_decoded_seq[k]

        for lr in lab_to_replace:
            true_lab = true_lab.replace(lr[0], lr[1])
            dec_seq = dec_seq.replace(lr[0], lr[1])

        cer, seq_op = compute_cer_backpointer(true_lab, dec_seq)
        list_cer.append(cer)

        for key in analysis_op:
            val = seq_op[key]
            analysis_op[key] += val
            total_op += val

    if total_op != 0:
        for key in analysis_op:
            analysis_op[key] /= total_op
    return list_cer, analysis_op


def compute_word_error_rate(true_labeling, decoded_seq, norm=True):
    """ Compute the word error rate from 2 string"""

    w_gt = true_labeling.split(' ')
    w_dec = decoded_seq.split(' ')

    edit_dist = editdistance.eval(w_dec, w_gt)

    if len(w_gt) == 0:
        return edit_dist

    if norm == False:
        return float(edit_dist)

    return float(edit_dist) / len(w_gt)


def compute_word_error_rate_from_lists(list_true_labeling, list_decoded_seq, lab_to_replace=None):
    """ Compute the word error rate from list of string
    
    lab_to_replace = list of tuple containing the sequences of labels that must be replaced by only one character 
    """

    nb_data = len(list_true_labeling)
    assert (len(list_decoded_seq) == nb_data)

    list_wer = []
    for k in range(nb_data):

        if isinstance(list_true_labeling[k], list):
            true_lab = ''.join(list_true_labeling[k])
        else:
            true_lab = list_true_labeling[k]
        if isinstance(list_decoded_seq[k], list):
            dec_seq = ''.join(list_decoded_seq[k])
        else:
            dec_seq = list_decoded_seq[k]

        for lr in lab_to_replace:
            true_lab=true_lab.replace(lr[0], lr[1])
            dec_seq=dec_seq.replace(lr[0], lr[1])

        wer = compute_word_error_rate(true_lab, dec_seq)
        list_wer.append(wer)

    return list_wer


def tf_edit_distance(hypothesis, truth, norm=False):
    """ Edit distance using tensorflow 
    
    inputs are tf.Sparse_tensors """

    return tf.edit_distance(hypothesis, truth, normalize=norm, name='edit_distance')



def tf_set_edit_distance(predict_func, batch, batch_len, label_array, truth, norm=False, greedy=False, beam_width=100, top_paths=1):
    """ Edit distance using tensorflow  """

    pred_hypothesis = predict_func(batch, batch_size=batch.shape[0])
    hypothesis, _ = fast_decode_batch(pred_hypothesis, batch_len, label_array, greedy=greedy,
                                         beam_width=beam_width, top_paths=top_paths)

    return tf_edit_distance(hypothesis, truth, norm=norm)


def tb_log_edit_distance(tboard, epoch, mean_norm_ed=0.):

        summary = tf.Summary()
        summary_value = summary.value.add()
        summary_value.simple_value = mean_norm_ed
        summary_value.tag = "CER"
        tboard.writer.add_summary(summary, epoch)
        tboard.writer.flush()


def tb_log_print(tboard, epoch, val=0., tag="no tag..."):
    summary = tf.Summary()
    summary_value = summary.value.add()
    summary_value.simple_value = val
    summary_value.tag = tag
    tboard.writer.add_summary(summary, epoch)
    tboard.writer.flush()


def show_edit_distance(true_labeling, decoded_seq, num_to_show):
    """ """

    num_show = min(len(decoded_seq), num_to_show)
    mean_ed, mean_norm_ed = compute_edit_distance(true_labeling, decoded_seq, num_show)
    for j in range(num_show):
        print("\nDecoding and true labeling: ", decoded_seq[j], ";", true_labeling[j])

    print('\nOut of %d samples:  Mean edit distance: %.3f Mean normalized edit distance (CER): %0.3f'
      % (num_show, mean_ed, mean_norm_ed))



def compute_wer(true_labeling, decoded_seq, sep=' '):
    """ """

    change_sep = False

    if isinstance(true_labeling, list):
        if not sep == ' ': # if this is a specific term such as 'c0' or 's6' we replace it by ' ' for not confusion after concatenating
            for i in range(len(true_labeling)):
                if true_labeling[i] == sep:
                    true_labeling[i] = ' '
                    change_sep = True
        true_labeling = ''.join(true_labeling)

    if isinstance(decoded_seq, list):
        if not sep == ' ': # if this is a specific term such as 'c0' or 's6' we replace it by ' ' for not confusion after concatenating
            for i in range(len(decoded_seq)):
                if decoded_seq[i] == sep:
                    decoded_seq[i] = ' '
                    change_sep = True
        decoded_seq = ''.join(decoded_seq)

    if change_sep:
        sep=' '

    true_labeling=true_labeling.split(sep)
    decoded_seq=decoded_seq.split(sep)
    edit_dist = editdistance.eval(decoded_seq, true_labeling) / len(true_labeling)

    return edit_dist


def compute_wer_fromLists(true_labeling, decoded_seq, sep=' ', norm=True):
    """ """

    assert(len(true_labeling) == len(decoded_seq))
    list_wer = []

    for i in range(len(true_labeling)):
        list_wer.append(compute_wer(true_labeling[i], decoded_seq[i], sep))

    if norm and len(list_wer)>0:
        final_wer = sum(list_wer) /len(list_wer)
    else:
        final_wer = sum(list_wer)


    return final_wer


### CODE FOR COMPUTING EDIT DISTANCE ###

# Cost is basically: was there a match or not.
# The other numbers are cumulative costs and matches.




def editdist_cost_action(ic, dc, sc, im, dm, sm, cost):
    """Given the following values, choose the action (insertion, deletion,
    or substitution), that results in the lowest cost (ties are broken using
    the 'match' score).  This is used within the dynamic programming algorithm.
    * ic - insertion cost
    * dc - deletion cost
    * sc - substitution cost
    * im - insertion match (score)
    * dm - deletion match (score)
    * sm - substitution match (score)
    """
    best_action = None
    best_match_count = -1
    min_cost = min(ic, dc, sc)
    if min_cost == sc and cost == 0.:
        best_action = 'equal'
        best_match_count = sm
    elif min_cost == sc and cost > 0.:
        best_action = 'replace'
        best_match_count = sm
    elif min_cost == ic and im > best_match_count:
        best_action = 'insert'
        best_match_count = im
    elif min_cost == dc and dm > best_match_count:
        best_action = 'delete'
        best_match_count = dm
    return best_action



class SequenceMatcher:
    """Similar to the :py:mod:`difflib` :py:class:`~difflib.SequenceMatcher`, but uses Levenshtein/edit
    distance.
    """

    def __init__(self, a=None, b=None, test=operator.eq,
                 action_function=lowest_cost_action):
        """Initialize the object with sequences a and b.  Optionally, one can
        specify a test function that is used to compare sequence elements.
        This defaults to the built in ``eq`` operator (i.e. :py:func:`operator.eq`).
        """
        if a is None:
            a = []
        if b is None:
            b = []
        self.seq1 = a
        self.seq2 = b
        self._reset_object()
        self.action_function = action_function
        self.test = test
        self.dist = None
        self._matches = None
        self.opcodes = None

    def set_seqs(self, a, b):
        """Specify two alternative sequences -- reset any cached values."""
        self.set_seq1(a)
        self.set_seq2(b)
        self._reset_object()

    def _reset_object(self):
        """Clear out the cached values for distance, matches, and opcodes."""
        self.opcodes = None
        self.dist = None
        self._matches = None

    def set_seq1(self, a):
        """Specify a new sequence for sequence 1, resetting cached values."""
        self._reset_object()
        self.seq1 = a

    def set_seq2(self, b):
        """Specify a new sequence for sequence 2, resetting cached values."""
        self._reset_object()
        self.seq2 = b

    def find_longest_match(self, alo, ahi, blo, bhi):
        """Not implemented!"""
        raise NotImplementedError()

    def get_matching_blocks(self):
        """Similar to :py:meth:`get_opcodes`, but returns only the opcodes that are
        equal and returns them in a somewhat different format
        (i.e. ``(i, j, n)`` )."""
        opcodes = self.get_opcodes()
        match_opcodes = filter(lambda x: x[0] == 'equal', opcodes)
        return map(lambda opcode: [opcode[1], opcode[3], opcode[2] - opcode[1]],
                   match_opcodes)

    def get_opcodes(self):
        """Returns a list of opcodes.  Opcodes are the same as defined by
        :py:mod:`difflib`."""
        if not self.opcodes:
            d, m, opcodes = edit_distance_backpointer(self.seq1, self.seq2,
                                                      action_function=self.action_function,
                                                      test=self.test)
            if self.dist:
                assert d == self.dist
            if self._matches:
                assert m == self._matches
            self.dist = d
            self._matches = m
            self.opcodes = opcodes
        return self.opcodes

    def get_grouped_opcodes(self, n=None):
        """Not implemented!"""
        raise NotImplementedError()

    def ratio(self):
        """Ratio of matches to the average sequence length."""
        return 2.0 * self.matches() / (len(self.seq1) + len(self.seq2))

    def quick_ratio(self):
        """Same as :py:meth:`ratio`."""
        return self.ratio()

    def real_quick_ratio(self):
        """Same as :py:meth:`ratio`."""
        return self.ratio()

    def _compute_distance_fast(self):
        """Calls edit_distance, and asserts that if we already have values for
        matches and distance, that they match."""
        d, m = edit_distance(self.seq1, self.seq2,
                             action_function=self.action_function,
                             test=self.test)
        if self.dist:
            assert d == self.dist
        if self._matches:
            assert m == self._matches
        self.dist = d
        self._matches = m

    def distance(self):
        """Returns the edit distance of the two loaded sequences.  This should
        be a little faster than getting the same information from
        :py:meth:`get_opcodes`."""
        if not self.dist:
            self._compute_distance_fast()
        return self.dist

    def matches(self):
        """Returns the number of matches in the alignment of the two sequences.
        This should be a little faster than getting the same information from
        :py:meth:`get_opcodes`."""
        if not self._matches:
            self._compute_distance_fast()
        return self._matches

def edit_distance(seq1, seq2, action_function=lowest_cost_action, test=operator.eq):
    """Computes the edit distance between the two given sequences.
    This uses the relatively fast method that only constructs
    two columns of the 2d array for edits.  This function actually uses four columns
    because we track the number of matches too.
    """
    m = len(seq1)
    n = len(seq2)
    # Special, easy cases:
    if seq1 == seq2:
        return 0, n
    if m == 0:
        return n, 0
    if n == 0:
        return m, 0
    v0 = [0] * (n + 1)  # The two 'error' columns
    v1 = [0] * (n + 1)
    m0 = [0] * (n + 1)  # The two 'match' columns
    m1 = [0] * (n + 1)
    for i in range(1, n + 1):
        v0[i] = i
    for i in range(1, m + 1):
        v1[0] = i + 1
        for j in range(1, n + 1):
            cost = 0 if test(seq1[i - 1], seq2[j - 1]) else 1
            # The costs
            ins_cost = v1[j - 1] + 1
            del_cost = v0[j] + 1
            sub_cost = v0[j - 1] + cost
            # Match counts
            ins_match = m1[j - 1]
            del_match = m0[j]
            sub_match = m0[j - 1] + int(not cost)

            action = action_function(ins_cost, del_cost, sub_cost, ins_match,
                                     del_match, sub_match, cost)

            if action in ['equal', 'replace']:
                v1[j] = sub_cost
                m1[j] = sub_match
            elif action == 'insert':
                v1[j] = ins_cost
                m1[j] = ins_match
            elif action == 'delete':
                v1[j] = del_cost
                m1[j] = del_match
            else:
                raise Exception('Invalid dynamic programming option returned!')
                # Copy the columns over
        for i in range(0, n + 1):
            v0[i] = v1[i]
            m0[i] = m1[i]
    return v1[n], m1[n]

def edit_distance_backpointer(seq1, seq2, action_function=lowest_cost_action, test=operator.eq):
    """Similar to :py:func:`~edit_distance.edit_distance` except that this function keeps backpointers
    during the search.  This allows us to return the opcodes (i.e. the specific
    edits that were used to change from one string to another).  This function
    contructs the full 2d array (actually it contructs three of them: one
    for distances, one for matches, and one for backpointers)."""
    matches = 0
    # Create a 2d distance array
    m = len(seq1)
    n = len(seq2)
    # print("length 1 :", m, "length 2 :", n)
    # distances array:
    d = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # backpointer array:
    bp = [[None for x in range(n + 1)] for y in range(m + 1)]
    # matches array:
    matches = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # source prefixes can be transformed into empty string by
    # dropping all characters
    for i in range(1, m + 1):
        d[i][0] = i
        bp[i][0] = ['delete', i - 1, i, 0, 0]
    # target prefixes can be reached from empty source prefix by inserting
    # every characters
    for j in range(1, n + 1):
        d[0][j] = j
        bp[0][j] = ['insert', 0, 0, j - 1, j]
    # compute the edit distance...
    for i in range(1, m + 1):
        for j in range(1, n + 1):

            cost = 0 if test(seq1[i - 1], seq2[j - 1]) else 1
            # The costs of each action...
            ins_cost = d[i][j - 1] + 1  # insertion
            del_cost = d[i - 1][j] + 1  # deletion
            sub_cost = d[i - 1][j - 1] + cost  # substitution/match

            # The match scores of each action
            ins_match = matches[i][j - 1]
            del_match = matches[i - 1][j]
            sub_match = matches[i - 1][j - 1] + int(not cost)

            action = action_function(ins_cost, del_cost, sub_cost, ins_match,
                                     del_match, sub_match, cost)
            if action == 'equal':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['equal', i - 1, i, j - 1, j]
            elif action == 'replace':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['replace', i - 1, i, j - 1, j]
            elif action == 'insert':
                d[i][j] = ins_cost
                matches[i][j] = ins_match
                bp[i][j] = ['insert', i - 1, i - 1, j - 1, j]
            elif action == 'delete':
                d[i][j] = del_cost
                matches[i][j] = del_match
                bp[i][j] = ['delete', i - 1, i, j - 1, j - 1]
            else:
                raise Exception('Invalid dynamic programming action returned!')

    opcodes = get_opcodes_from_bp_table(bp)
    return d, matches, opcodes
    # return d[m][n], matches[m][n], opcodes


def edit_distance_backpointerScore(seq1, seq2, action_function=editdist_cost_action, test=compute_normed_edit_distance):
    """Similar to :py:func:`~edit_distance.edit_distance` except that this function keeps backpointers
    during the search.  This allows us to return the opcodes (i.e. the specific
    edits that were used to change from one string to another).  This function
    contructs the full 2d array (actually it contructs three of them: one
    for distances, one for matches, and one for backpointers)."""
    matches = 0
    # Create a 2d distance array
    m = len(seq1)
    n = len(seq2)
    # print("length 1 :", m, "length 2 :", n)
    # distances array:
    d = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # backpointer array:
    bp = [[None for x in range(n + 1)] for y in range(m + 1)]
    # matches array:
    matches = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # source prefixes can be transformed into empty string by
    # dropping all characters
    for i in range(1, m + 1):
        d[i][0] = i
        bp[i][0] = ['delete', i - 1, i, 0, 0]
    # target prefixes can be reached from empty source prefix by inserting
    # every characters
    for j in range(1, n + 1):
        d[0][j] = j
        bp[0][j] = ['insert', 0, 0, j - 1, j]
    # compute the edit distance...
    for i in range(1, m + 1):
        for j in range(1, n + 1):

            cost = test(seq1[i - 1], seq2[j - 1])
            # The costs of each action...
            ins_cost = d[i][j - 1] + 1  # insertion
            del_cost = d[i - 1][j] + 1  # deletion
            sub_cost = d[i - 1][j - 1] + cost  # substitution/match

            # The match scores of each action
            ins_match = matches[i][j - 1]
            del_match = matches[i - 1][j]
            sub_match = matches[i - 1][j - 1] + min(cost, 1) #int(not cost)

            action = action_function(ins_cost, del_cost, sub_cost, ins_match,
                                     del_match, sub_match, cost)
            if action == 'equal':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['equal', i - 1, i, j - 1, j]
            elif action == 'replace':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['replace', i - 1, i, j - 1, j]
            elif action == 'insert':
                d[i][j] = ins_cost
                matches[i][j] = ins_match
                bp[i][j] = ['insert', i - 1, i - 1, j - 1, j]
            elif action == 'delete':
                d[i][j] = del_cost
                matches[i][j] = del_match
                bp[i][j] = ['delete', i - 1, i, j - 1, j - 1]
            else:
                raise Exception('Invalid dynamic programming action returned!')

    opcodes = get_opcodes_from_bp_table(bp)
    return d, matches, opcodes
    # return d[m][n], matches[m][n], opcodes


def get_opcodes_from_bp_table(bp):
    """Given a 2d list structure, collect the opcodes from the best path."""
    x = len(bp) - 1
    y = len(bp[0]) - 1
    opcodes = []
    while x != 0 or y != 0:
        this_bp = bp[x][y]
        opcodes.append(this_bp)
        if this_bp[0] == 'equal' or this_bp[0] == 'replace':
            x = x - 1
            y = y - 1
        elif this_bp[0] == 'insert':
            y = y - 1
        elif this_bp[0] == 'delete':
            x = x - 1
    opcodes.reverse()
    return opcodes



#### COMPLEMENT ####

def seq_matching_edit_operations(seq1, seq2, op, out_str=False, sep=''):
    """ SEQUENCE MATCHING ACCORDING TO THE EDIT OPERATIONS

    Inputs:
        seq1 = seq2 = sequence of elements of the same type (e.g. characters, words)
        op = edit operations (insert, delete, equal, replace) to get seq1 from seq2. 

    Outputs:
        seq1 and seq2 which have been extended by '*' when there is an insertion 
        in order to have the same length.
    """

    out_seq1 = []
    out_seq2 = []
    idx_seq1 = idx_seq2 = 0
    unique_op = np.unique(op)

    if len(unique_op)==1 and 'equal' in unique_op:
        return seq1, seq2

    for i, elmt in enumerate(op):

        if elmt in ['equal', 'replace']:
            out_seq1.append(seq1[idx_seq1])
            idx_seq1 += 1
            out_seq2.append(seq2[idx_seq2])
            idx_seq2 += 1
        elif 'delete' in elmt:
            out_seq2.append('*')
            out_seq1.append(seq1[idx_seq1])
            idx_seq1 += 1
            # idx_seq2 += 1
        elif 'insert' in elmt:
            out_seq1.append('*')
            out_seq2.append(seq2[idx_seq2])
            # idx_seq1 += 1
            idx_seq2 += 1

    if out_str:
        out_seq1 = sep.join(out_seq1)
        out_seq2 = sep.join(out_seq2)

    return out_seq1, out_seq2


def edit_distance_backpointerWTN(seq1, seq2, action_function=lowest_cost_action, test=operator.eq):
    """Similar to :py:func:`~edit_distance.edit_distance` except that this function keeps backpointers
    during the search.  This allows us to return the opcodes (i.e. the specific
    edits that were used to change from one string to another).  This function
    contructs the full 2d array (actually it contructs three of them: one
    for distances, one for matches, and one for backpointers)."""
    matches = 0
    # Create a 2d distance array
    m = len(seq1)
    n = len(seq2)
    # print("length 1 :", m, "length 2 :", n)
    # distances array:
    d = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # backpointer array:
    bp = [[None for x in range(n + 1)] for y in range(m + 1)]
    # matches array:
    matches = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # source prefixes can be transformed into empty string by
    # dropping all characters
    for i in range(1, m + 1):
        d[i][0] = i
        bp[i][0] = ['delete', i - 1, i, 0, 0]
    # target prefixes can be reached from empty source prefix by inserting
    # every characters
    for j in range(1, n + 1):
        d[0][j] = j
        bp[0][j] = ['insert', 0, 0, j - 1, j]
    # compute the edit distance...
    for i in range(1, m + 1):
        for j in range(1, n + 1):

            cost = 0 if test(seq1[i - 1], seq2[j - 1]) else 1

            # The costs of each action...
            ins_cost = d[i][j - 1] + 1  # insertion
            del_cost = d[i - 1][j] + 1  # deletion
            sub_cost = d[i - 1][j - 1] + cost  # substitution/match

            # The match scores of each action
            ins_match = matches[i][j - 1]
            del_match = matches[i - 1][j]
            sub_match = matches[i - 1][j - 1] + int(not cost)

            action = action_function(ins_cost, del_cost, sub_cost, ins_match,
                                     del_match, sub_match, cost)
            if action == 'equal':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['equal', i - 1, i, j - 1, j]
            elif action == 'replace':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['replace', i - 1, i, j - 1, j]
            elif action == 'insert':
                d[i][j] = ins_cost
                matches[i][j] = ins_match
                bp[i][j] = ['insert', i - 1, i - 1, j - 1, j]
            elif action == 'delete':
                d[i][j] = del_cost
                matches[i][j] = del_match
                bp[i][j] = ['delete', i - 1, i, j - 1, j - 1]
            else:
                raise Exception('Invalid dynamic programming action returned!')

    opcodes = get_opcodes_from_bp_table(bp)
    return d, matches, opcodes
    # return d[m][n], matches[m][n], opcodes


def edit_distance_backpointerWTN_listSeq(list_seq1, seq2, action_function=lowest_cost_action, test=operator.eq):
    """Similar to :py:func:`~edit_distance.edit_distance` except that this function keeps backpointers
    during the search.  This allows us to return the opcodes (i.e. the specific
    edits that were used to change from one string to another).  This function
    contructs the full 2d array (actually it contructs three of them: one
    for distances, one for matches, and one for backpointers)."""
    matches = 0
    # Create a 2d distance array
    m = len(list_seq1[0])
    n = len(seq2)
    # print("length 1 :", m, "length 2 :", n)
    # distances array:
    d = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # backpointer array:
    bp = [[None for x in range(n + 1)] for y in range(m + 1)]
    # matches array:
    matches = [[0 for x in range(n + 1)] for y in range(m + 1)]
    # source prefixes can be transformed into empty string by
    # dropping all characters
    for i in range(1, m + 1):
        d[i][0] = i
        bp[i][0] = ['delete', i - 1, i, 0, 0]
    # target prefixes can be reached from empty source prefix by inserting
    # every characters
    for j in range(1, n + 1):
        d[0][j] = j
        bp[0][j] = ['insert', 0, 0, j - 1, j]
    # compute the edit distance...
    for i in range(1, m + 1):
        for j in range(1, n + 1):

            cost = 0 if 1 in [test(seq1[i - 1], seq2[j - 1]) for seq1 in list_seq1] else 1
            # The costs of each action...
            ins_cost = d[i][j - 1] + 1  # insertion
            del_cost = d[i - 1][j] + 1  # deletion
            sub_cost = d[i - 1][j - 1] + cost  # substitution/match

            # The match scores of each action
            ins_match = matches[i][j - 1]
            del_match = matches[i - 1][j]
            sub_match = matches[i - 1][j - 1] + int(not cost)

            action = action_function(ins_cost, del_cost, sub_cost, ins_match,
                                     del_match, sub_match, cost)
            if action == 'equal':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['equal', i - 1, i, j - 1, j]
            elif action == 'replace':
                d[i][j] = sub_cost
                matches[i][j] = sub_match
                bp[i][j] = ['replace', i - 1, i, j - 1, j]
            elif action == 'insert':
                d[i][j] = ins_cost
                matches[i][j] = ins_match
                bp[i][j] = ['insert', i - 1, i - 1, j - 1, j]
            elif action == 'delete':
                d[i][j] = del_cost
                matches[i][j] = del_match
                bp[i][j] = ['delete', i - 1, i, j - 1, j - 1]
            else:
                raise Exception('Invalid dynamic programming action returned!')

    opcodes = get_opcodes_from_bp_table(bp)
    return d, matches, opcodes
    # return d[m][n], matches[m][n], opcodes


def compute_prop_similar_label(seq1, seq2, list_forbidden=[]):
    """ Compute the proportion of similar labels 
    Sequences MUST HAVE THE SAME LENGTH """

    rate = 0.
    lg1 = len(seq1)
    assert (len(seq1) == len(seq2))

    for i in range(lg1):
        if seq1[i] == seq2[i] and seq1[i] not in list_forbidden:
            rate += 1

    rate /= lg1
    return rate

def label_error_proportion(seq1, seq2, norm=True):
    """ Compute the proportion of error labelling after getting a sequence matching
    Sequence matching provides sequences with the same length. 
    The rate of similar labels (the same label at the same time) is then compute."""

    # if isinstance(seq1, str):
    #     seq1 = list(seq1)
    # if isinstance(seq2, str):
    #     seq2 = list(seq2)

    _, _, op = edit_distance_backpointerWTN(seq1, seq2)
    sim_seq1, sim_seq2 = seq_matching_edit_operations(seq1, seq2, [elmt[0] for elmt in op])

    lep = compute_prop_similar_label(sim_seq1, sim_seq2, ['*']) / len(sim_seq1) if norm else compute_prop_similar_label(
        sim_seq1, sim_seq2, ['*'])
    return lep


def LEP_between_2sets(set1, set2, norm=True):
    """ Compute the label error proportion between each pair of sequences from set1 and set2.
    Return a np.matrix containing the proportions of good labels """

    lg1, lg2 = len(set1), len(set2)
    D = np.zeros((lg1, lg2))
    for i, seq1 in enumerate(set1):
        for j, seq2 in enumerate(set2):
            D[i, j] = label_error_proportion(seq1, seq2, norm)

    return D


def weigthed_edit_distance(set_seq1, set_seq2, D=[], D_no1=[], D_no2=[], dist='edit_distance', action_function=lowest_cost_action, test=operator.eq):
    """COMPUTE THE EDIT DISTANCE 
    set_seq1 and set_seq2 are two list of sequences of variable length 
    D = matrix m x n containing the distances / gap between elements in seq1 and seq2
    D_no1 = list of scores that elements in seq1 are not observed
    D_no2 = list of scores that elements in seq2 are not observed
    """

    m = len(set_seq1)
    n = len(set_seq2)

    # Cumulative cost matrix initialization
    C = np.zeros((m + 1, n + 1))

    if not D:
        if 'prop' in dist:
            D = 1 - LEP_between_2sets(set_seq1, set_seq2, norm=False)
        else:  # edit_distance
            D = np.zeros((m, n))
            for i in range(m):
                for j in range(n):
                    D[i, j] = edit_distance(set_seq1[i], set_seq2[j], lowest_cost_action, operator.eq)
    if not D_no1:
        if 'prop' in dist:
            D_no1 = [1 - min(D[i, :]) for i in range(m)]
        else:
            mean_val = np.mean(D)
            max_val = max(D)
            ratio = max_val / mean_val
            D_no1 = [ratio - mean_val / min(D[i, :]) for i in range(m)]

    if not D_no2:
        if 'prop' in dist:
            D_no2 = [1 - min(D[:, i]) for i in range(n)]
        else:
            D_no2 = []
            for i in range(n):
                val = max(D[:, i])  # mean(D[:,i])
                D_no2.append(val)

    C[0, 0] = 0
    for i in range(1, m + 1):
        C[i, 0] = C[i - 1, 0] + D_no1[i - 1]

    for j in range(1, n + 1):
        C[0, j] = C[0, j - 1] + D_no2[j - 1]

    # Edit distance computation
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            C[i, j] = min(C[i - 1, j - 1] + D[i - 1, j - 1], C[i - 1, j] + D_no1[i - 1], C[i, j - 1] + D_no2[j - 1])

    return C, D, D_no1, D_no2


def traceback_ED(C):
    """ traceback for edit distance 
    C = cumulative cost matrix coming from edit distance """

    m, n = C.shape
    m, n = m - 1, n - 1
    matches = [(m - 1, n - 1)]

    while (m > 0 and n > 0):
        tb = np.argmin((C[m - 1, n - 1], C[m - 1, n], C[m, n - 1]))

        if (tb == 0):
            m = m - 1
            n = n - 1
        elif (tb == 1):
            matches[0] = (m - 1, -1)
            m = m - 1
        elif (tb == 2):
            matches[0] = (-1, n - 1)
            n = n - 1

        matches.insert(0, (m - 1, n - 1))

    if m == 0:
        if n == 0:
            matches[0] = (-1, -1)
        else:
            matches[0] = (-1, n - 1)
            for i in range(n, 0, -1):
                matches.insert(0, (-1, i))
    else:
        matches[0] = (m - 1, -1)
        for i in range(m, 0, -1):
            matches.insert(0, (i, -1))

    return matches[1:]


def max_occurrences(seq):
    """ return the element in seq with the highest occurence
    Input 'seq' is a list of element """

    d_occ = get_occurrences(seq)

    return max(d_occ.items(), key=operator.itemgetter(1))

def weighted_max_occurrences(seq, weight):
    """ return the element in seq with the highest occurence
    Input 'seq' is a list of element """

    d_occ = weighted_get_occurrences(seq, weight)

    return max(d_occ.items(), key=operator.itemgetter(1))[0]



def weighted_vote_min_editDistance(seq, weight):
    """ return the element in seq with the lowest edit distance with the other
    Input 'seq' is a list of element """

    nb_seq = len(seq)
    list_edit = [0.for k in range(nb_seq)]

    for i in range(nb_seq):
        for j in range(nb_seq):
            edit_dist = editdistance.eval(seq[i], seq[j]) * weight[j]
            list_edit[i] += edit_dist

    sorted_edit_dist, sorted_index = zip(*sorted(zip(list_edit, [k for k in range(nb_seq)])))
    return seq[sorted_index[0]]



def get_occurrences(seq):
    """ return a dictionnary with the occurrence number of unique elements in seq 
    Input 'seq' is a list of element """

    d_occ = defaultdict(int)
    for item in seq:
        d_occ[item] += 1

    return d_occ

def weighted_get_occurrences(seq, weight):
    """ return a dictionnary with the occurrence number of unique elements in seq 
    Input 'seq' is a list of element """

    assert(len(seq)==len(weight))

    d_occ = defaultdict(int)
    for i,item in enumerate(seq):
        d_occ[item] += weight[i]

    return d_occ



def levenshtein(source, target):
    if len(source) < len(target):
        return levenshtein(target, source)

    # So now we have len(source) >= len(target).
    if len(target) == 0:
        return len(source)

    # We call tuple() to force strings to be used as sequences
    # ('c', 'a', 't', 's') - numpy uses them as values by default.
    source = np.array(tuple(source))
    target = np.array(tuple(target))

    # We use a dynamic programming algorithm, but with the
    # added optimization that we only need the last two rows
    # of the matrix.
    previous_row = np.arange(target.size + 1)
    for s in source:
        # Insertion (target grows longer than source):
        current_row = previous_row + 1

        # Substitution or matching:
        # Target and source items are aligned, and either
        # are different (cost of 1), or are the same (cost of 0).
        current_row[1:] = np.minimum(
            current_row[1:],
            np.add(previous_row[:-1], target != s))

        # Deletion (target grows shorter than source):
        current_row[1:] = np.minimum(
            current_row[1:],
            current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]


def levenshteinErrorSymGestion(mot, lexique, symChar):
    res = levenshteinError(mot, lexique)
    if res != mot:
        return res
    else:
        motNuS = mot.rstrip(symChar)
        suffixe = mot[-(len(mot) - len(motNuS)):]
        motNuP = mot.lstrip(symChar)
        prefixe = mot[:(len(mot) - len(motNuP))]
        motNu = motNuS.lstrip(symChar)
        res = levenshteinError(motNu, lexique)
        if res != mot:
            return prefixe + res + suffixe
        else:
            return mot


def levenshteinError(mot, lexique):
    dl = []
    bestDist = 100
    for motL in lexique:
        a, b, dist = needle(mot, motL)
        if dist < bestDist:
            dl = [motL]
            bestDist = dist
        elif dist == bestDist:
            dl.append(motL)
    if len(dl) == 1 and bestDist < 2:
        return dl[0]
    else:
        return mot
        # return dl[0]


def levenshteinCheckQuick(mot, lexique):
    for motL in lexique:
        a, b, dist = needle(mot, motL)
        if dist == 1:
            return False
    return True


def zeros(shape):
    retval = []
    for x in range(shape[0]):
        retval.append([])
        for y in range(shape[1]):
            retval[-1].append(0)
    return retval


match_award = 0
mismatch_penalty = 1
gap_penalty = 1  # both for opening and extanding


def match_score(alpha, beta):
    if alpha == beta:
        return match_award
    elif alpha == '-' or beta == '-':
        return gap_penalty
    else:
        return mismatch_penalty


def finalize(align1, align2):
    align1 = align1[::-1]  # reverse sequence 1
    align2 = align2[::-1]  # reverse sequence 2

    i, j = 0, 0

    # calcuate identity, score and aligned sequeces
    symbol = ''
    found = 0
    score = 0
    identity = 0
    for i in range(0, len(align1)):
        # if two AAs are the same, then output the letter
        if align1[i] == align2[i]:
            symbol = symbol + align1[i]
            identity = identity + 1
            score += match_score(align1[i], align2[i])

        # if they are not identical and none of them is gap
        elif align1[i] != align2[i] and align1[i] != '-' and align2[i] != '-':
            score += match_score(align1[i], align2[i])
            symbol += ' '
            found = 0

        # if one of them is a gap, output a space
        elif align1[i] == '-' or align2[i] == '-':
            symbol += ' '
            score += gap_penalty

    identity = float(identity) / len(align1) * 100

    print
    'Identity =', "%3.3f" % identity, 'percent'
    print
    'Score =', score
    # print align1
    # print symbol
    # print align2
    return align1, align2


def needle(seq1, seq2):
    m, n = len(seq1), len(seq2)  # length of two sequences

    # Generate DP table and traceback path pointer matrix
    score = zeros((m + 1, n + 1))  # the DP table

    # Calculate DP table
    for i in range(0, m + 1):
        score[i][0] = gap_penalty * i
    for j in range(0, n + 1):
        score[0][j] = gap_penalty * j
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            match = score[i - 1][j - 1] + match_score(seq1[i - 1], seq2[j - 1])
            delete = score[i - 1][j] + gap_penalty
            insert = score[i][j - 1] + gap_penalty
            score[i][j] = min(match, delete, insert)

    # Traceback and compute the alignment
    align1, align2 = '', ''
    i, j = m, n  # start from the bottom right cell
    while i > 0 and j > 0:  # end toching the top or the left edge
        score_current = score[i][j]
        score_diagonal = score[i - 1][j - 1]
        score_up = score[i][j - 1]
        score_left = score[i - 1][j]

        if score_current == score_diagonal + match_score(seq1[i - 1], seq2[j - 1]):
            align1 += seq1[i - 1]
            align2 += seq2[j - 1]
            i -= 1
            j -= 1
        elif score_current == score_left + gap_penalty:
            align1 += seq1[i - 1]
            align2 += '+'
            i -= 1
        elif score_current == score_up + gap_penalty:
            align1 += '+'
            align2 += seq2[j - 1]
            j -= 1

    # Finish tracing up to the top left cell
    while i > 0:
        align1 += seq1[i - 1]
        align2 += '+'
        i -= 1
    while j > 0:
        align1 += '+'
        align2 += seq2[j - 1]
        j -= 1

    align1 = align1[::-1]  # reverse sequence 1
    align2 = align2[::-1]  # reverse sequence 2
    return align1, align2, score[-1][-1]


def needle2(seq1, seq2):
    m, n = len(seq1), len(seq2)  # length of two sequences

    # Generate DP table and traceback path pointer matrix
    score = zeros((m + 1, n + 1))  # the DP table

    # Calculate DP table
    for i in range(0, m + 1):
        score[i][0] = gap_penalty * i
    for j in range(0, n + 1):
        score[0][j] = gap_penalty * j
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            match = score[i - 1][j - 1] + match_score(seq1[i - 1], seq2[j - 1])
            delete = score[i - 1][j] + gap_penalty
            insert = score[i][j - 1] + gap_penalty
            score[i][j] = min(match, delete, insert)

    # Traceback and compute the alignment
    align1, align2 = [], []
    i, j = m, n  # start from the bottom right cell
    while i > 0 and j > 0:  # end toching the top or the left edge
        score_current = score[i][j]
        score_diagonal = score[i - 1][j - 1]
        score_up = score[i][j - 1]
        score_left = score[i - 1][j]

        if score_current == score_diagonal + match_score(seq1[i - 1], seq2[j - 1]):
            align1.append(seq1[i - 1])
            align2.append(seq2[j - 1])
            i -= 1
            j -= 1
        elif score_current == score_left + gap_penalty:
            align1.append(seq1[i - 1])
            align2.append('+')
            i -= 1
        elif score_current == score_up + gap_penalty:
            align1.append('+')
            align2.append(seq2[j - 1])
            j -= 1

    # Finish tracing up to the top left cell
    while i > 0:
        align1.append(seq1[i - 1])
        align2.append('+')
        i -= 1
    while j > 0:
        align1.append('+')
        align2.append(seq2[j - 1])
        j -= 1

    align1 = align1[::-1]  # reverse sequence 1
    align2 = align2[::-1]  # reverse sequence 2
    return align1, align2, score[-1][-1]


def evaluateCERandWER(resList, targetList, symChar):
    segErr = 0
    cer = 0.0
    totalLength = 0.0
    wer = 0.0
    wer2 = 0.0
    nbWords = 0.0
    nbWords2 = 0.0
    sousSeg = 0.0
    surSeg = 0.0
    ler = 0.0

    for ite in range(len(resList)):
        res = resList[ite]
        targetLabel = targetList[ite]
        targetLabel = targetLabel.strip('_')
        res = res.strip('_')
        targetLabel = targetLabel.replace('___', '_')
        res = res.replace('___', '_')
        targetLabel = targetLabel.replace('__', '_')
        res = res.replace('__', '_')

        targetLabelu = targetLabel
        resu = res
        for symC in symChar:
            targetLabelu = targetLabelu.replace(symC, '_' + symC + '_')
            resu = resu.replace(symC, '_' + symC + '_')
        targetLabelu = targetLabelu.strip('_')
        resu = resu.strip('_')
        targetLabelu = targetLabelu.replace('___', '_')
        resu = resu.replace('___', '_')
        targetLabelu = targetLabelu.replace('__', '_')
        resu = resu.replace('__', '_')

        resListu = resu.split('_')
        targetListu = targetLabelu.split('_')
        a1, a2, dwer = needle2(resListu, targetListu)
        wer2 += dwer
        nbWords2 += len(targetListu)

        if res != targetLabel:
            a1, a2, cerTemp = needle(res, targetLabel)
            cer += cerTemp
        totalLength += len(targetLabel)

        for symC in symChar[1:]:
            targetLabel = targetLabel.replace(symC, '_')
            res = res.replace(symC, '_')
            targetLabel = targetLabel.lower()
            res = res.lower()
        targetLabel = targetLabel.replace('___', '_')
        res = res.replace('___', '_')
        targetLabel = targetLabel.replace('__', '_')
        res = res.replace('__', '_')
        targetLabel = targetLabel.strip('_')
        res = res.strip('_')

        targetSplit = targetLabel.split('_')
        resSplit = res.split('_')
        nbWords += len(targetSplit)

        a1, a2, dwer = needle2(resSplit, targetSplit)
        if dwer > 0:
            ler += 1
        wer += dwer
        segErr += abs(len(targetSplit) - len(resSplit))
        if len(targetSplit) > len(resSplit):
            sousSeg += 1
        if len(targetSplit) < len(resSplit):
            surSeg += 1

    print('CER : ' + str(cer / totalLength * 100) + ' WER : ' + str(wer / nbWords * 100) + ' WER2 : ' + str(
        wer2 / nbWords2 * 100) + ' segErr : ' + str(segErr / nbWords * 100))
    print('Sous seg : ' + str(sousSeg / float(ite) * 100.0) + ' Sur seg : ' + str(
        surSeg / float(len(resList)) * 100.0) + ' LER : ' + str(ler / float(len(resList)) * 100.0))

    return (cer / totalLength * 100), (wer2 / nbWords2 * 100)


def evaluateCERandWERonSet(resList, targetList, symChar):

    list_cer = []
    list_wer = []

    assert(len(resList)==len(targetList))

    for i in range(len(resList)):
        cer, wer = evaluateCERandWER(resList[i][0], targetList[i][0], symChar)
        list_cer.append(cer)
        list_wer.append(wer)

    return list_cer, list_wer


def analyze_transcription(list_pred, list_gt, computeCer=True, computeWer=True, computeSeqError=True):

    cer = 0.
    wer = 0.
    seq_error = 0.
    sum_char = 0.
    sum_words = 0.

    nb_data = len(list_pred)

    assert(nb_data<=len(list_gt))

    for i in range(nb_data):

        if computeCer and computeSeqError:
            #print(list_pred[i], list_gt[i])
            edit_dist = editdistance.eval(list_pred[i], list_gt[i])
            cer += edit_dist
            sum_char += len(list_gt[i])

            if computeSeqError and edit_dist==0:
                seq_error += 1

        if computeWer:
            if isinstance(list_pred[i], str):
                pred = list_pred[i].split(' ')
            else:
                pred = ''.join(list_pred[i]).split(' ')

            if isinstance(list_gt[i], str):
                gt = list_gt[i].split(' ')
            else:
                gt = ''.join(list_gt[i]).split(' ')

            edit_dist = editdistance.eval(pred, gt)
            #print(edit_dist, ':::', pred, '****', gt)
            wer += edit_dist #/ len(gt)
            sum_words += len(gt)

    if nb_data >0:
        cer = cer / sum_char * 100
        wer = wer / sum_words * 100
        seq_error = seq_error / nb_data * 100

    return (cer, wer, seq_error)



def main():
    """ -------- TEST ------------
    Read two files line-by-line and print edit distances between each pair
    of lines. Will terminate at the end of the shorter of the two files."""

    #    if len(sys.argv) != 3:
    #        print('Usage: {} <file1> <file2>'.format(sys.argv[0]))
    #        exit(-1)
    #    file1 = sys.argv[1]
    #    file2 = sys.argv[2]
    #
    #    with open(file1) as f1, open(file2) as f2:
    #        for line1, line2 in zip(f1, f2):
    #            print("Line 1: {}".format(line1.strip()))
    #            print("Line 2: {}".format(line2.strip()))
    #            dist, _, _ = edit_distance_backpointer(line1.split(), line2.split())
    #            print('Distance: {}'.format(dist))
    #            print('=' * 80)

    #    line1 = 'Hello guys, how are you?'
    #    line2 = 'Hi guys, how ware yolu?'
    list_line1 = ['abcd', 'abcd*', '*bzde', 'abcd', 'abcd']
    list_line2 = ['bzde', 'bcdef', 'bcdef', 'abc', 'abce']
    for i in range(len(list_line1)):
        line1 = list_line1[i]
        line2 = list_line2[i]
        print(list(line1))
        print(list(line2))
        d1, d2 = edit_distance(list(line1), list(line2))
        print('D1 / D2 ', d1, d2)
        dist, matches, opcodes = edit_distance_backpointer(list(line1), list(line2))
        print('Distance:\n {}'.format(np.matrix(dist)))
        print('Matching:\n {}'.format(np.matrix(matches)))
        print(opcodes)

if __name__ == "__main__":
    main()


# decode_truth = [['b','o','n','j','o','u','r',',','','t','o','t','o'], ['j','e','','v','a','i','s','','b','i','e','n']]
# decoded_batch = [['o','n','j','o','u','r',',','','t','a','t','a'], ['j','e','','v','o','i','s','','b','i','e','n','.']]
#
# ed = compute_list_edit_distance(decode_truth, decoded_batch, 2)
# print(ed)
# print(ed[2][0]/len(decode_truth[0]), ed[2][1]/len(decode_truth[1]))
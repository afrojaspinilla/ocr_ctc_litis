# encoding=utf-8
import getopt
import sys

import numpy as np
from PIL import Image
from keras.preprocessing import sequence
from random import shuffle

from ocr_ctc.dataset_manager import DatasetBatch
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
from ocr_ctc.preprocessings.data_preprocessing.Standardize import Standardize

from copy import deepcopy


def produce_dataset(data_dir, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=[], preprocessings_batch=[],
                    padding=-1., norm=64):
    """ TRAIN-B is used for the training and TRAIN-A for the valid and test """
    train_files = []
    test_files = []
    valid_files = []
    label_dict = {}
    label_array = []


    cur_label = 0

    sep = 'c0'

    gtdir = "trackB-GT-ite1Ascii.idf" # labeling of the ground truth


    # train set
    with open(data_dir + 'Train-B/' + gtdir, "r") as f_train: # encoding="ISO-8859-1"
        for line in f_train.readlines():

            if line is "\n":
                continue

            data = line.replace('\n', '').replace('\r', '').split(' ')
            data_file = data_dir + 'Train-B/img/' + data[0] + ".tif"
            label = data[1:]
            train_files.append((data_file, label))

            for i in label:
                if i not in label_dict:
                    label_dict[i] = cur_label
                    label_array.append(i)
                    cur_label += 1


    gtdir = "trackA-GroundTruthAscii.txt"
    all_label_dict = deepcopy(label_dict)
    all_label_array = deepcopy(label_array)

    # validation set
    with open(data_dir + 'Train-A/' + gtdir, "r") as f_valid:
        for line in f_valid.readlines():

            if line is "\n":
                continue

            data = line.replace('\n', '').replace('\r', '').split(' ')
            label = data[1:]
            data_file = data_dir + 'Train-A/img/' + data[0] + ".tif"
            valid_files.append((data_file, label))

            for lab in label:
                if lab not in all_label_dict:
                    print("new label:", lab)
                    all_label_dict[lab] = cur_label
                    all_label_array.append(lab)
                    cur_label += 1


    shuffle(train_files)
    #shuffle(test_files)
    shuffle(valid_files)

    dataset = DatasetBatch(output_path)

    def load_img_READ(it_data):

        output_path, label = train_files[it_data]
        image = Image.open(output_path)
        image = image.convert('L')
        width, _ = image.size
        image = image.resize((width, norm), Image.ANTIALIAS)
        image = np.array(image)

        return image

    my_norm_std = Standardize(**{'func_load_img': load_img_READ, 'nb_data': len(train_files)})
    mean, std = my_norm_std.get_mean_std()
    padding = my_norm_std.padding()
    preprocessing_data.append(Standardize(**{'mean': mean, 'std': std}))

    dataset.new_dataset(label_array=label_array, label_dict=label_dict, padding=padding, sep=sep, all_labels=all_label_array, all_label_dict=all_label_dict)


    def __prepare_set(file_list, index, size, padding, norm):
        x = []
        y = []
        x_length = []
        y_length = []
        try:
            for j in range(size):
                true_label = []
                output_path, label = file_list[index + j]
                for i in label:
                    true_label.append(all_label_dict[i])
                image = Image.open(output_path)
                image = image.convert('L')
                width, _ = image.size
                image = image.resize((width, norm), Image.ANTIALIAS)
                #image = np.array(image)

                # im = Image.open(file_and_dir)
                # (width, height) = im.size
                # imc = im.resize((width, self.get_dim_features()))
                (width, height) = image.size
                imc_map = np.array(list(image.getdata()))
                image = imc_map.reshape((height, width))

                for preproc in preprocessings_data:
                    image, true_label = preproc(image, true_label)
                seq_len = image.shape[0]
                label_len = len(true_label)

                x.append(image)
                y.append(true_label)
                x_length.append(seq_len)
                y_length.append(label_len)
        except IndexError:
            print("Ignoring last batch...")

        for preproc_batch in preprocessings_batch:
            x, y, x_length, y_length, padding = preproc_batch(x, y, x_length, y_length, padding)

        x = sequence.pad_sequences(x, value=float(padding), dtype='float32', padding="post")
        y = sequence.pad_sequences(y, value=float(len(label_array)), dtype='float32', padding="post")

        return x, y, np.asarray(x_length), np.asarray(y_length)


    print("save train...")
    for i in range(0, len(train_files), train_size):
        dataset.save_train_batch(__prepare_set(train_files, i, train_size, padding, norm))

    print("save valid...")
    for i in range(0, len(valid_files), valid_size):
        dataset.save_test_batch(__prepare_set(valid_files, i, test_size, padding, norm))

    print("save test...")
    for i in range(0, len(valid_files), valid_size):
        dataset.save_valid_batch(__prepare_set(valid_files, i, valid_size, padding, norm))

    dataset.save()


if __name__ == '__main__':

    opts, _ = getopt.getopt(sys.argv[1:], '', ["input=", "out=", "norm="])
    input_path = output_path = None
    input_path = "../../datasets/READ/" #os.path.join(os.path.dirname(__file__), "datasets/READ/")
    #os.path.join(os.path.dirname(__file__), "datasets/READ/batch/")
    nb_col = 3
    padding = -1.
    train_size = 64
    test_size = 64
    valid_size = 64
    norm = 128
    output_path = "../../datasets/READ/batch" + str(nb_col) + "col" + str(norm)

    for opt, arg in opts:
        if '--input' in opt:
            input_path = arg
        elif '--out' in opt:
            output_path = arg
        elif '--norm' in opt:
            norm = int(arg)


    if not input_path or not output_path:
        exit()

    # standardize later
    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_col, padding=padding)]

    produce_dataset(input_path, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=preprocessing_data, preprocessings_batch=[],
                    padding=padding, norm=norm)

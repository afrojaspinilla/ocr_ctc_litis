# coding: utf8
import numpy as np
from PIL import Image
import os
import sys
import random
#import preprocessings
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
from ocr_ctc.preprocessings.data_preprocessing.Standardize import Standardize
from keras.preprocessing import sequence
from ocr_ctc.dataset_manager.Dataset import Dataset
from ocr_ctc.dataset_manager.DatasetInfo import DatasetInfo
from collections import OrderedDict

class DatasetOnline(Dataset):
    """ NEW DATASET - DATASET BATCH

    Each data is saved alone
    Dataset batch are built when data are loaded (in generator)
    """

    def __init__(self, settings):
        # settings for the dataset
        self.settings = settings

        # Get the map of train labels
        if self.settings.TRAIN_LABELS != "":
            self.train_labels_map, self.train_list, self.train_size = self.set_list(self.settings.TRAIN_LABELS)

        # Get the map of valid labels
        if self.settings.VALID_LABELS != "":
            self.valid_labels_map, self.valid_list, self.valid_size = self.set_list(self.settings.VALID_LABELS)

        # Get the map of test labels
        if self.settings.TEST_LABELS != "":
            self.test_labels_map, self.test_list, self.test_size = self.set_list(self.settings.TEST_LABELS)


        self.charset_map = {}
        for k in range(len(self.settings.LABEL_LIST)):
            self.charset_map[self.settings.LABEL_LIST[k]] = k

        self.dataset_info = None

        self._train_data = []
        self._train_labels = []
        self._train_seqlen = []
        self._train_labellen = []

        self._test_data = []
        self._test_labels = []
        self._test_seqlen = []
        self._test_labellen = []

        self._valid_data = []
        self._valid_labels = []
        self._valid_seqlen = []
        self._valid_labellen = []

        self._train_files = []
        self._test_files = []
        self._valid_files = []

        self._loaded = False

    def set_list(self, path):
        labels_map = OrderedDict()
        labels = open(path, 'r', encoding='utf-8')
        for line in labels:
            labels_map[line.split(" ")[0].strip()] = " ".join(line.split(" ")[1:]).replace("\n","")
        llist = list(labels_map)
        size = len(llist)
        return labels_map, llist, size

    def load(self):
        raise NotImplementedError

    def save(self):
        raise NotImplementedError

    def get_generator(self, image_folder, labels_map, label_list, nb_batch=None, order="normal", continuous_id=None):
        preprocessing_batch = [To2DSequence(nb_cols=self.settings.NB_COLS, stride=self.settings.STRIDE)]
        if order == "shuffle":
            random.shuffle(label_list, random.random)
        iterator = iter(label_list)
        while True:
            x = []
            x_length = []
            y = []
            y_length = []

            while len(x) < self.settings.BATCH_SIZE:
                try:
                    file = next(iterator)
                except StopIteration:
                    # Reset iterator and draw an other data
                    if order == "shuffle":
                        random.shuffle(label_list, random.random)
                    iterator = iter(label_list)
                    file = next(iterator)

                image = np.asarray(self.pre_process_image(os.path.join(image_folder, file)))

                label = []
                for i in labels_map[file].strip():
                    label.append(self.charset_map[i])
                if len(image) > len(label):
                    x.append(image)
                    x_length.append(image.shape[0])
                    y.append(label)
                    y_length.append(len(label))


            for preproc_batch in preprocessing_batch:
                x, y, x_length, y_length, padding = preproc_batch(x, y, x_length, y_length, self.settings.PADDING)

                for idx_tmp in range(len(x)): # reverse images to get height x width for conv2D layers
                    if len(x[idx_tmp])>0:
                        x[idx_tmp] = np.swapaxes(x[idx_tmp], 1, 2)

            if len(x) > 0:
                x = sequence.pad_sequences(x, value=float(padding), dtype='float32', padding="post")
                y = sequence.pad_sequences(y, value=float(len(self.settings.LABEL_LIST)), dtype='float32', padding="post")
            yield [np.array(x),  np.array(y), np.array([[x] for x in x_length]), np.array([[x] for x in y_length])], np.zeros(y.shape)


    def get_train_generator(self, nb_batch=None, order="shuffle", continuous_id=None):
        return self.get_generator(self.settings.TRAIN_FOLDER, self.train_labels_map, self.train_list)

    def get_valid_generator(self, nb_batch=None, order="shuffle", continuous_id=None):
        return self.get_generator(self.settings.VALID_FOLDER, self.valid_labels_map, self.valid_list)

    def pre_process_image(self, path):
        image = Image.open(path)
        image = image.convert('L')
        # CENTER reduc
        preprocessing_data = [Transpose(), Standardize(mean=self.settings.MEAN, std=self.settings.STD)]

        width, height = image.size
        if self.settings.KEEP_RATIO:
            ratio = self.settings.NORM / height
            image = image.resize((int(width * ratio), self.settings.NORM), Image.ANTIALIAS)
        elif self.settings.NORM > 0:
            image = image.resize((width, self.settings.NORM), Image.ANTIALIAS)

        image = np.array(image)

        for preproc in preprocessing_data:
            image, _ = preproc(image, np.asarray([]))

        return image

    def get_test_generator(self):
        return self.get_generator(self.settings.TEST_FOLDER, self.test_labels_map, self.test_list)



    @property
    def nb_features(self):
        raise NotImplementedError

    @property
    def nbbatch_train(self):
        return len(self.train_list)

    @property
    def nbbatch_test(self):
        return len(self.test_list)

    @property
    def nbbatch_valid(self):
        return len(self.valid_list)

    @property
    def padding(self):
        raise NotImplementedError

    @property
    def label_array(self):
        raise NotImplementedError

    @property
    def label_dict(self):
        raise NotImplementedError

    @property
    def nb_labels(self):
        raise NotImplementedError

    @property
    def sep(self):
        raise NotImplementedError

    @property
    def all_labels(self):
        raise NotImplementedError

    @property
    def all_label_dict(self):
        raise NotImplementedError

    @property
    def window_size(self):
        raise NotImplementedError

    @property
    def nb_cols(self):
        raise NotImplementedError

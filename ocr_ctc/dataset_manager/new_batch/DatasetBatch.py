import pickle

import h5py
import numpy as np
import os
import sys
import random
from keras.preprocessing import sequence

from ocr_ctc.dataset_manager.Dataset import Dataset
from ocr_ctc.dataset_manager.DatasetInfo import DatasetInfo


class DatasetBatch(Dataset):
    """ NEW DATASET - DATASET BATCH

    Each data is saved alone
    Dataset batch are built when data are loaded (in generator)
    """

    def __init__(self, path, train_size=-1, valid_size=-1, test_size=-1):

        super().__init__(path, train_size, test_size, valid_size)
        self.current_files = None

    def new_dataset(self, label_array, label_dict, padding, sep=' ', all_labels=None, all_label_dict=None,
                    nb_features=None, window_size=None, nb_cols=None, train_files=[], valid_files=[], test_files=[]):

        if os.path.exists(self._path):
            print("PATH", self._path, "already exists. The dataset_manager can not be created.")
            sys.exit(0)
        else:
            os.mkdir(self._path)
            os.mkdir(self._path + "/TRAIN/")
            os.mkdir(self._path + "/TEST/")
            os.mkdir(self._path + "/VALID/")

        if not nb_features:
            nb_features = -1

        self.dataset_info = DatasetInfo(0, 0, 0, nb_features, padding, label_array, label_dict, sep=sep,
                                        all_labels=all_labels,
                                        all_label_dict=all_label_dict, window_size=window_size, nb_cols=nb_cols,
                                        train_files=train_files, valid_files=valid_files, test_files=test_files)

    def save_train_batch(self, batch):
        data, labels, seqlen, labellen = batch
        if len(data) > 0:  # make sure that inputs are not empty

            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[-1]

            for i in range(len(data)):
                self.dataset_info.nbbatch_train += 1
                h5f = h5py.File(self._path + "/TRAIN/" + str(self.dataset_info.nbbatch_train) + ".h5", 'w')
                h5f.create_dataset("data", data=np.asarray(data[i]))
                h5f.create_dataset("labels", data=np.asarray(labels[i]))
                h5f.create_dataset("seqlen", data=np.asarray([seqlen[i]]))
                h5f.create_dataset("labellen", data=np.asarray([labellen[i]]))
                h5f.close()
                self._train_files.append(self._path + "/TRAIN/" + str(self.dataset_info.nbbatch_train) + ".h5")

    def save_test_batch(self, batch):

        data, labels, seqlen, labellen = batch
        if len(data) > 0:

            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[-1]

            for i in range(len(data)):

                # warning length
                if seqlen[i] < 2 * labellen[i] + 1:
                    print("WARNING in DatasetBatch: sequence length is too short. Can have problems with CTC.")

                self.dataset_info.nbbatch_test += 1
                h5f = h5py.File(self._path + "/TEST/" + str(self.dataset_info.nbbatch_test) + ".h5", 'w')
                h5f.create_dataset("data", data=np.asarray(data[i]))
                h5f.create_dataset("labels", data=np.asarray(labels[i]))
                h5f.create_dataset("seqlen", data=np.asarray([seqlen[i]]))
                h5f.create_dataset("labellen", data=np.asarray([labellen[i]]))
                h5f.close()
                self._test_files.append(self._path + "/TEST/" + str(self.dataset_info.nbbatch_test) + ".h5")

    def save_valid_batch(self, batch):
        data, labels, seqlen, labellen = batch
        if len(data) > 0:

            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[-1]

            for i in range(len(data)):

                # warning length
                if seqlen[i] < 2 * labellen[i] + 1:
                    print("WARNING in DatasetBatch: sequence length is too short. Can have problems with CTC.")

                self.dataset_info.nbbatch_valid += 1
                h5f = h5py.File(self._path + "/VALID/" + str(self.dataset_info.nbbatch_valid) + ".h5", 'w')
                h5f.create_dataset("data", data=np.asarray(data[i]))
                h5f.create_dataset("labels", data=np.asarray(labels[i]))
                h5f.create_dataset("seqlen", data=np.asarray([seqlen[i]]))
                h5f.create_dataset("labellen", data=np.asarray([labellen[i]]))
                h5f.close()
                self._valid_files.append(self._path + "/VALID/" + str(self.dataset_info.nbbatch_valid) + ".h5")

    def save_custom_batch(self, batch, name_dir, index_batch):
        data, labels, seqlen, labellen = batch
        # self.dataset_info.nbbatch_valid += 1
        if len(data) > 0:
            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[2]

            for i in range(len(data)):

                # warning length
                if seqlen[i] < 2 * labellen[i] + 1:
                    print("WARNING in DatasetBatch: sequence length is too short. Can have problems with CTC.")

                h5f = h5py.File(self._path + "/" + name_dir + "/" + str(index_batch) + ".h5", 'w')
                h5f.create_dataset("data", data=np.asarray(data[i]))
                h5f.create_dataset("labels", data=np.asarray(labels[i]))
                h5f.create_dataset("seqlen", data=np.asarray([seqlen[i]]))
                h5f.create_dataset("labellen", data=np.asarray([labellen[i]]))
                h5f.close()
                self._valid_files.append(
                    self._path + "/" + name_dir + "/" + str(self.dataset_info.nbbatch_valid) + ".h5")

    def save(self):
        with open(self._path + "/dataset_manager.pickle", 'wb') as f:
            pickle.dump(self.dataset_info, f)

    def load(self):
        with open(self._path + "/dataset_manager.pickle", 'rb') as f:
            self.dataset_info = pickle.load(f)

        train_f = [(int(elmt.split('.')[0]), elmt) for elmt in os.listdir(self._path + "/TRAIN/")]
        for batch in sorted(train_f):
            self._train_files.append(self._path + "/TRAIN/" + batch[1])

        test_f = [(int(elmt.split('.')[0]), elmt) for elmt in os.listdir(self._path + "/TEST/")]
        for batch in sorted(test_f):
            self._test_files.append(self._path + "/TEST/" + batch[1])

        valid_f = [(int(elmt.split('.')[0]), elmt) for elmt in os.listdir(self._path + "/VALID/")]
        for batch in sorted(valid_f):
            self._valid_files.append(self._path + "/VALID/" + batch[1])

    @staticmethod
    def __sort_file_list(train_files):
        """ sort files according to the length of observation sequences from the shorter to the longer """

        lengths = []
        for file in train_files:
            h5f = h5py.File(file, 'r')
            mdata = h5f["data"][:]
            lengths.append(mdata.shape[0])

        files, _ = zip(*sorted(zip(train_files, lengths), key=lambda x: x[1]))
        return list(files)

    def get_train_generator(self, nb_batch=None, order="shuffle", continuous_id=None):#, rate_not_continuous=1.):
        """ generator on train data

        order = shuffle (by default: all data are shuffled). curriculum = from the shorter to the longuer sequence,
                otherwise on the given order
        nb_batch = number of batch that are seen. Always the same data. Be careful: it does not limit the number of batch per epoch.
        continuous_id = index of data that are seen at each epoch (without rand selection)
        # rate_not_continuous = rate of data that are not seen at each iteration that are considered.
        """
        if nb_batch is not None and isinstance(nb_batch, float):
            nb_batch = int(nb_batch)

        if continuous_id is not None and nb_batch is not None:
            id_not_cont = [idx for idx in range(len(self._train_files)) if idx not in continuous_id]
            random.shuffle(id_not_cont, random.random)
            tr_files = [self._train_files[idx] for idx in continuous_id] + [self._train_files[idx] for idx in id_not_cont]
            train_files = tr_files[:nb_batch * self.train_size] if nb_batch and self.train_size > 0 else tr_files

        elif nb_batch is not None:
            train_files = self._train_files[:nb_batch * self.train_size] if self.train_size > 0 else self._train_files

        else:
            train_files = self._train_files

        if order == "curriculum":
            train_files = self.__sort_file_list(train_files)

        while True:


            if order == "shuffle":
                random.shuffle(train_files, random.random)

            if self.train_size < 1:
                self.train_size = 1

            for files in zip(*[iter(train_files)] * self.train_size):

                self.current_files = []
                data = []
                labels = []
                seqlen = []
                labellen = []
                for file in files:
                    self.current_files.append(file)
                    h5f = h5py.File(file, 'r')
                    mdata = h5f["data"][:]
                    mlabels = h5f["labels"][:]
                    mseqlen = h5f["seqlen"][:]
                    mlabellen = h5f["labellen"][:]

                    data.append(mdata)
                    labels.append(mlabels)
                    seqlen.append(mseqlen)
                    labellen.append(mlabellen)

                    if self.train_size == 1:
                        self.current_files.append(file)
                        data.append(mdata)
                        labels.append(mlabels)
                        seqlen.append(mseqlen)
                        labellen.append(mlabellen)

                    h5f.close()

                data = sequence.pad_sequences(data, value=float(self.dataset_info.padding), dtype='float32',
                                              padding="post", truncating='post')

                if isinstance(labels, float):
                    labels = [labels]

                labels = sequence.pad_sequences(labels, value=float(len(self.dataset_info.label_array)),
                                                dtype='float32', padding="post")

                yield [np.asarray(data), np.asarray(labels), np.asarray(seqlen), np.asarray(labellen)], np.zeros(
                    data.shape[0])


    def get_train_generator_select_eq(self, nb_batch=None, order="shuffle", continuous_id=None, rate_notcontinous_per_epoch=None):  # , rate_not_continuous=1.):
        """ generator on train data with fixed iput data in continuous_id and a random selection in the pool of other data
        of size equal to the number of continuous id.

        order = shuffle (by default: all data are shuffled). curriculum = from the shorter to the longuer sequence,
                otherwise on the given order
        nb_batch = number of batch that are seen. Always the same data. Be careful: it does not limit the number of batch per epoch.
        continuous_id = index of data that are seen at each epoch (without rand selection)
        # rate_not_continuous = rate of data that are not seen at each iteration that are considered.
        """
        # if nb_batch is not None and isinstance(nb_batch, float):
        #     nb_batch = int(nb_batch)
        #
        # if continuous_id is not None and nb_batch is not None:
        #     id_not_cont = [idx for idx in range(len(self._train_files)) if idx not in continuous_id]
        #     random.shuffle(id_not_cont, random.random)
        #     tr_files = [self._train_files[idx] for idx in continuous_id] + [self._train_files[idx] for idx in id_not_cont]
        #     train_files = tr_files[:nb_batch * self.train_size] if nb_batch and self.train_size > 0 else tr_files
        #
        # elif nb_batch is not None:
        #     train_files = self._train_files[:nb_batch * self.train_size] if self.train_size > 0 else self._train_files
        #
        # else:
        #     train_files = self._train_files
        #
        # if order == "curriculum":
        #     train_files = self.__sort_file_list(train_files)
        #
        # tmp = 0
        it = 0

        while True:

            if nb_batch is not None and isinstance(nb_batch, float):
                nb_batch = int(nb_batch)

            if continuous_id is not None and nb_batch is not None:
                id_not_cont = [idx for idx in range(len(self._train_files)) if idx not in continuous_id]
                random.shuffle(id_not_cont, random.random)
                #nb_data_notcont = rate_notcontinous_per_epoch[it]*len(id_not_cont)
                tr_files = [self._train_files[idx] for idx in continuous_id] + [self._train_files[idx] for idx in
                                                                                id_not_cont]
                train_files = tr_files[:nb_batch * self.train_size] if nb_batch and self.train_size > 0 else tr_files

            elif nb_batch is not None:
                train_files = self._train_files[
                              :nb_batch * self.train_size] if self.train_size > 0 else self._train_files

            else:
                train_files = self._train_files

            if order == "curriculum":
                train_files = self.__sort_file_list(train_files)

            #tmp = 0
            it += 1
            print("ITER ", it)
            for k in range(len(continuous_id), nb_batch * self.train_size, 1):
                print(train_files[k])

            if order == "shuffle":
                random.shuffle(train_files, random.random)

            if self.train_size < 1:
                self.train_size = 1

            for files in zip(*[iter(train_files)] * self.train_size):

                #print("\nDo generator", tmp)
                #tmp += 1

                self.current_files = []
                data = []
                labels = []
                seqlen = []
                labellen = []
                for file in files:
                    self.current_files.append(file)
                    h5f = h5py.File(file, 'r')
                    mdata = h5f["data"][:]
                    mlabels = h5f["labels"][:]
                    mseqlen = h5f["seqlen"][:]
                    mlabellen = h5f["labellen"][:]

                    data.append(mdata)
                    labels.append(mlabels)
                    seqlen.append(mseqlen)
                    labellen.append(mlabellen)

                    if self.train_size == 1:
                        self.current_files.append(file)
                        data.append(mdata)
                        labels.append(mlabels)
                        seqlen.append(mseqlen)
                        labellen.append(mlabellen)

                    h5f.close()

                data = sequence.pad_sequences(data, value=float(self.dataset_info.padding), dtype='float32',
                                              padding="post", truncating='post')

                if isinstance(labels, float):
                    labels = [labels]

                labels = sequence.pad_sequences(labels, value=float(len(self.dataset_info.label_array)),
                                                dtype='float32', padding="post")

                yield [np.asarray(data), np.asarray(labels), np.asarray(seqlen), np.asarray(labellen)], np.zeros(
                    data.shape[0])

    def get_test_generator(self):
        while True:

            if self.test_size < 1:
                self.test_size = 1

            for files in zip(*[iter(self._test_files)] * self.test_size):
                self.current_files = []
                data = []
                labels = []
                seqlen = []
                labellen = []
                for file in files:
                    self.current_files.append(file)

                    h5f = h5py.File(file, 'r')
                    mdata = h5f["data"][:]
                    mlabels = h5f["labels"][:]
                    mseqlen = h5f["seqlen"][:]
                    mlabellen = h5f["labellen"][:]

                    data.append(mdata)
                    labels.append(mlabels)
                    seqlen.append(mseqlen)
                    labellen.append(mlabellen)

                    if self.test_size == 1:
                        self.current_files.append(file)
                        data.append(mdata)
                        labels.append(mlabels)
                        seqlen.append(mseqlen)
                        labellen.append(mlabellen)

                    h5f.close()

                data = sequence.pad_sequences(data, value=float(self.dataset_info.padding), dtype='float32',
                                              padding="post")

                if isinstance(labels, float):
                    labels = [labels]
                labels = sequence.pad_sequences(labels, value=float(len(self.dataset_info.label_array)),
                                                dtype='float32', padding="post")

                yield [np.asarray(data), np.asarray(labels), np.asarray(seqlen), np.asarray(labellen)], np.zeros(
                    data.shape[0])

    def get_valid_generator(self):
        while True:

            if self.valid_size < 1:
                self.valid_size = 1

            for files in zip(*[iter(self._valid_files)] * self.valid_size):

                self.current_files = []
                data = []
                labels = []
                seqlen = []
                labellen = []
                for file in files:
                    self.current_files.append(file)
                    h5f = h5py.File(file, 'r')
                    mdata = h5f["data"][:]
                    mlabels = h5f["labels"][:]
                    mseqlen = h5f["seqlen"][:]
                    mlabellen = h5f["labellen"][:]

                    data.append(mdata)
                    labels.append(mlabels)
                    seqlen.append(mseqlen)
                    labellen.append(mlabellen)

                    if self.valid_size == 1:
                        self.current_files.append(file)
                        data.append(mdata)
                        labels.append(mlabels)
                        seqlen.append(mseqlen)
                        labellen.append(mlabellen)

                    h5f.close()

                data = sequence.pad_sequences(data, value=float(self.dataset_info.padding), dtype='float32',
                                              padding="post")

                if isinstance(labels, float):
                    labels = [labels]
                labels = sequence.pad_sequences(labels, value=float(len(self.dataset_info.label_array)),
                                                dtype='float32', padding="post")

                yield [np.asarray(data), np.asarray(labels), np.asarray(seqlen), np.asarray(labellen)], np.zeros(
                    data.shape[0])

    @property
    def nb_features(self):
        return self.dataset_info.nb_features

    @property
    def nbbatch_train(self):
        return self.dataset_info.nbbatch_train

    @property
    def nbbatch_test(self):
        return self.dataset_info.nbbatch_test

    @property
    def nbbatch_valid(self):
        return self.dataset_info.nbbatch_valid

    @property
    def padding(self):
        return self.dataset_info.padding

    @property
    def label_array(self):
        return self.dataset_info.label_array

    @property
    def label_dict(self):
        return self.dataset_info.label_dict

    @property
    def nb_labels(self):
        return len(self.dataset_info.label_array)

    @property
    def sep(self):
        try:
            return self.dataset_info.sep
        except AttributeError:
            return ' '

    @property
    def all_labels(self):
        try:
            return self.dataset_info.all_labels if self.dataset_info.all_labels else self.label_array
        except AttributeError:
            return self.label_array

    @property
    def all_label_dict(self):
        try:
            return self.dataset_info.all_labels_dict if self.dataset_info.all_labels_dict else self.label_dict
        except AttributeError:
            return self.label_dict

    @property
    def window_size(self):
        try:
            return self.dataset_info.window_size
        except AttributeError:
            return None

    @property
    def nb_cols(self):
        try:
            return self.dataset_info.nb_cols
        except AttributeError:
            return None

    @property
    def train_files(self):
        try:
            return self.dataset_info.train_files
        except AttributeError:
            return []

    @property
    def valid_files(self):
        try:
            return self.dataset_info.valid_files
        except AttributeError:
            return []

    @property
    def test_files(self):
        try:
            return self.dataset_info.test_files
        except AttributeError:
            return []


# class DatasetBatchGenerator(GeneratorEnqueuer):
#
#
#     def __init__(self, path, train_size=-1, valid_size=-1, test_size=-1):
#
#         super().__init__(

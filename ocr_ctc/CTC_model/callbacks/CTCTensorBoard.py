# encoding=utf-8
from keras.callbacks import TensorBoard
from ocr_ctc.utils.utils_analysis import tb_log_print
import numpy as np


class CTCTensorBoard(TensorBoard):
    """
    Extension of the Tensorboard callback (Keras) for a CTCModel
    dataset_manager = dataset_manager structure in ctc_pipeline
    eval_valid / eval_train = boolean, True if one wants to visualize results on those sets
    test_steps = number of training epoch performed between two evaluations
    """

    def __init__(self, ctc_model, dataset, eval_valid=True, eval_train=False, eval_test=False,
                 log_dir='./logs', histogram_freq=0, write_graph=True,
                 write_images=True, embeddings_freq=0, embeddings_layer_names=None, batch_size=32,
                 embeddings_metadata=None, test_steps=1, nb_batch_train=-1, nb_batch_val=-1, nb_batch_test=-1,
                 max_epoch=100, metrics=['ler', 'ser']):
        super().__init__(log_dir, histogram_freq=histogram_freq, write_graph=write_graph, write_images=write_images,
                         embeddings_freq=embeddings_freq, embeddings_layer_names=embeddings_layer_names,
                         embeddings_metadata=embeddings_metadata, batch_size=batch_size)

        self.dataset = dataset
        self.set_model(ctc_model)
        self.ctc_model = ctc_model
        self.test_steps = test_steps
        self.max_epoch = max_epoch
        self.eval_train = eval_train
        self.eval_valid = eval_valid
        self.eval_test = eval_test
        self.metrics = metrics # metrics used in evaluate methods

        self.nb_batch_train = nb_batch_train if nb_batch_train != -1 else int(self.dataset.nbbatch_train/self.dataset.train_size)
        self.nb_batch_val = nb_batch_val if nb_batch_val != -1 else int(self.dataset.nbbatch_valid/self.dataset.valid_size)
        self.nb_batch_test = nb_batch_test if nb_batch_test != -1 else int(self.dataset.nbbatch_test /self.dataset.test_size)

    def on_epoch_end(self, epoch, logs=None):
        """ On EPOCH END 
        Evaluation on the valid, train and/or test sets after self.test_steps training epochs. 
        """

        #logs['val_loss'] = logs['loss']  # WARNING to fix a bug when there is no loss valid
        super().on_epoch_end(epoch, logs)

        # evaluation on the valid set
        if self.eval_valid:
            if (epoch % self.test_steps == 0) or (epoch == 1 or epoch == self.max_epoch):
                outmetrics = self.ctc_model.evaluate_generator(self.dataset.get_valid_generator(), self.nb_batch_val, \
                                                                  metrics=self.metrics)

                if 'ler' in self.metrics:
                    ler = np.mean(outmetrics[self.metrics.index('ler')])
                    print("LER valid", ler)
                    tb_log_print(self, epoch, val=float(ler), tag="Label Error Rate on valid")
                if 'ser' in self.metrics:
                    tb_log_print(self, epoch, val=float(outmetrics[self.metrics.index('ser')]), tag="Sequence Error Rate on valid")


        # evaluation on the training set
        if self.eval_train:
            if (epoch % self.test_steps == 0) or (epoch == 1 or epoch == self.max_epoch):
                outmetrics = self.ctc_model.evaluate_generator(self.dataset.get_train_generator(), self.nb_batch_train, \
                                                                  metrics=self.metrics)

                if 'ler' in self.metrics:
                    ler = np.mean(outmetrics[self.metrics.index('ler')])
                    tb_log_print(self, epoch, val=float(ler), tag="Label Error Rate on train")
                if 'ser' in self.metrics:
                    tb_log_print(self, epoch, val=float(outmetrics[self.metrics.index('ser')]), tag="Sequence Error Rate on train")


        # evaluation on the test set
        if self.eval_test:
            if (epoch % self.test_steps == 0) or (epoch == 1 or epoch == self.max_epoch):
                outmetrics = self.ctc_model.evaluate_generator(self.dataset.get_test_generator(), self.nb_batch_test, \
                                                                  metrics=self.metrics)

                if 'ler' in self.metrics:
                    ler = np.mean(outmetrics[self.metrics.index('ler')])
                    tb_log_print(self, epoch, val=float(ler), tag="Label Error Rate on test")
                if 'ser' in self.metrics:
                    tb_log_print(self, epoch, val=float(outmetrics[self.metrics.index('ser')]), tag="Sequence Error Rate on test")



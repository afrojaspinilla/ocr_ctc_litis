import os
from abc import abstractmethod


class Dataset:
    def __init__(self, path, train_size, test_size, valid_size):

        self.train_size = train_size
        self.test_size = test_size
        self.valid_size = valid_size

        self.dataset_info = None

        self._train_data = []
        self._train_labels = []
        self._train_seqlen = []
        self._train_labellen = []

        self._test_data = []
        self._test_labels = []
        self._test_seqlen = []
        self._test_labellen = []

        self._valid_data = []
        self._valid_labels = []
        self._valid_seqlen = []
        self._valid_labellen = []

        self._train_files = []
        self._test_files = []
        self._valid_files = []

        self._loaded = False

        self._path = path

    @abstractmethod
    def load(self):
        raise NotImplementedError

    @abstractmethod
    def save(self):
        raise NotImplementedError

    @abstractmethod
    def get_train_generator(self):
        raise NotImplementedError

    @abstractmethod
    def get_test_generator(self):
        raise NotImplementedError

    @abstractmethod
    def get_valid_generator(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def nb_features(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def nbbatch_train(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def nbbatch_test(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def nbbatch_valid(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def padding(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def label_array(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def label_dict(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def nb_labels(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def sep(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def all_labels(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def all_label_dict(self):
        raise NotImplementedError

    @property
    def window_size(self):
        raise NotImplementedError

    @property
    def nb_cols(self):
        raise NotImplementedError
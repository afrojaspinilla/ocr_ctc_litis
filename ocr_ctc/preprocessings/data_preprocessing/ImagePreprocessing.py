# encoding=utf-8
from abc import abstractmethod

from ocr_ctc.preprocessings.DataPreprocessing import DataPreprocessing


class ImagePreprocessing(DataPreprocessing):
    """
    Class wrapper pour DataPreprocessing
    
    Permet d'éviter de se trimballer les labels et de juste travailler sur les images
    """
    def __init__(self, **kwargs):
        raise NotImplementedError

    def __call__(self, image, label):
        return self.preprocessing(image), label

    @abstractmethod
    def preprocessing(self, image):
        raise NotImplementedError




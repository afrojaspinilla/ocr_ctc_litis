import sys

sys.path.append("./")

import ocr_ctc
from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import random
import time

os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
from tqdm import tqdm


def main(
    input_path,
    str_info="",
    files_info_lab=None,
    sup_dir=None,
    charset=None,
    h_norm=100,
    removed_empty=True,
    dict_dir=None,
    nb_cols=1,
    stride=1,
    keep_ratio=True,
    conv=False,
    conv_pad=False,
    ImageFrombaseline=False,
    ext_im=".png",
    window_structure="vertical",
    shuff=True,
    padding=-1,
    standardize=True,
    ext_origin="xml",
    mean_stand=None,
    std_stand=None,
    no_new_label=False,
):

    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        preprocessing_batch.append(
            To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad)
        )
        str_info += "_pad" if conv_pad else ""
        output_path = (
            input_path
            + input_path.split("/")[-2]
            + "_conv_"
            + str_info
            + str(h_norm)
            + "_"
            + str(nb_cols)
            + "_"
            + str(stride)
        )

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []

        output_path = (
            input_path
            + input_path.split("/")[-2]
            + str_info
            + str(h_norm)
            + "_"
            + str(nb_cols)
            + "_"
            + str(stride)
        )

    if files_info_lab is None:
        files_info_lab = {
            "train": "train.txt",
            "valid": "valid.txt",
            "test": "test.txt",
        }

    if sup_dir is None:
        sup_dir = {
            "train": "train/",
            "valid": "valid/",
            "test": "test/",
        }

    producer = Producer(
        input_path,
        output_path,
        norm=h_norm,
        keep_ratio=keep_ratio,
        padding=padding,
        label_array=charset,
        nb_cols=nb_cols,
        standardize=standardize,
        preprocessing_data=preprocessing_data,
        preprocessing_batch=preprocessing_batch,
        early_preprocessing_batch=early_preprocessing_batch,
        shuff=shuff,
    ).produce_dataset(
        splitter_label="",
        sup_dir=sup_dir,
        files_info_lab=files_info_lab,
        mean_stand=mean_stand,
        std_stand=std_stand,
        ext_images="",
        removed_empty=removed_empty,
        no_new_label=no_new_label,
    )

    # sup_dir = {"train": "out558_predict0_0.5/extracted_lines/", \
    #            "valid": "out558_predict0_0.5/extracted_lines/", \
    #            "test": "out558_predict0_0.5/extracted_lines/"}, \


def several_datasets(input_path):

    nb_cols = 16  # 32  # number of time frames that are considered at each time step
    stride = 2  # 4
    padding = -1.0
    norm = 32  # 64  # height dimension
    keep_ratio = True  # resize the image by keeping the ratio
    conv = True  #  format data for a conv LSTM network
    conv_pad = True  #  do padding in the extremities of the images built for the conv LSTM network
    ImageFrombaseline = (
        False  #  image is build according to the baseline (not available here)
    )
    window_structure = "vertical"  # parameter for ImageFromBaseline

    shuff = False  # mix the data before saved them

    str_info = "rimes_dataset_v1"

    files_info_lab = {
        "train": "train_labels.txt",
        "valid": "valid_labels.txt",
        "test": "test_labels.txt",
    }

    mean_stand = None  # 127.15439987266659 # 237.20395680296994  #210.09484374142548 #210.08713638936308  # 209.55817933603757 #19.120696372144852# 209.55817933603757 # None = default, this is computed on the training set
    std_stand = None  # 37.12547797780864# 45.972764583180705  #34.925641777103046 #34.275111586890084  # 36.98662735037311 # 58.82913064998043 # 36.98662735037311 # None = default, this is computed on the training set

    charset = [
        " ",
        "!",
        '"',
        "%",
        "'",
        "(",
        ")",
        "*",
        "+",
        ",",
        "-",
        ".",
        "/",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        ":",
        ";",
        "=",
        "?",
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z",
        "_",
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z",
        "{",
        "}",
        "¤",
        "°",
        "²",
        "À",
        "É",
        "à",
        "â",
        "ç",
        "è",
        "é",
        "ê",
        "ë",
        "î",
        "ô",
        "ù",
        "û",
        "œ",
        "€",
    ]

    removed_empty = False
    no_new_label = True  #  do not add new label if there is some

    # return
    main(
        input_path,
        h_norm=norm,
        nb_cols=nb_cols,
        stride=stride,
        str_info=str_info,
        charset=charset,
        keep_ratio=keep_ratio,
        conv=conv,
        conv_pad=conv_pad,
        ImageFrombaseline=ImageFrombaseline,
        window_structure=window_structure,
        shuff=shuff,
        padding=padding,
        files_info_lab=files_info_lab,
        mean_stand=mean_stand,
        std_stand=std_stand,
        removed_empty=removed_empty,
        no_new_label=no_new_label,
    )


if __name__ == "__main__":
    looking_folder = sys.argv[1]
    several_datasets(looking_folder)

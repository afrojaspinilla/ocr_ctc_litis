#!/bin/bash

if [ $# -ne 7 ]; then
	echo "Usage: test.sh OCR-probas";
	exit 1;
fi;

# Make test directory
proba_file=$1
page_folder=$2
page_name=$3
labes_file=$4
fst_file=$5
full_path=$6
output_log_file=$7
mkdir -p $full_path

one_step_script="new_utils/kaldi/exp/one_step_testP.sh"
time ./new_utils/kaldi/exp/one_step_testP.sh $1 $6 $3 $4 $5 > $7 2>&1;

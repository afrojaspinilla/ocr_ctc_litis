# encoding=utf-8
from math import sqrt

import numpy as np

from ocr_ctc.preprocessings.BatchPreprocessing import BatchPreprocessing
from ocr_ctc.preprocessings.DataPreprocessing import DataPreprocessing
from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class Standardize(ImagePreprocessing):
    """ Gaussian normalization on the entire dataset_manager and for all the pixels in the image
    
    Warning : a shuffle on the dataset_manager may affect the computations due to numerical approximations """


    def __init__(self, **kwargs):
        """ kwargs:
            just_centered = if True, inputs are only centered (default False = centered and reduced)
            mean = mean of the inputs (compute on image_set if None)
            std = standard deviation of the inputs (compute on image_set if None)
            data_generator = DataGenerator class (default None). Defines for initializing the mean and standard deviation
                            according to the data from DataGenerator.
        """
        func_load_img = kwargs.pop('func_load_img', None)
        nb_data = kwargs.pop('nb_data', None)
        func_args = kwargs.pop('func_args', None)

        if func_load_img and nb_data:
            self.computeMeanStdOnBatch(func_load_img, nb_data, func_args)
            self.mean, self.std = self.get_mean_std()

        else:
            self.mean = kwargs.pop('mean', None)
            self.std = kwargs.pop('std', None)

        self.nb_pixl = None
        self.just_centered = kwargs.pop('just_centered', False)


    def preprocessing(self, image):
        """ = data are centered (minus mean) and reduced (divided by the standard deviation) 

        Input: image_set = list of 2D array (one array per image)
        """

        # compute the mean on the image
        if not self.mean:
            print("there is no mean to standardize. Each image will be standardize based on the first image.")
            self.mean = np.sum(image)
            self.nb_pixl = np.prod(image.shape)

            if self.nb_pixl < 1:
                print("WARNING: images can't be normalized")
                return image
            self.mean /= self.nb_pixl


        # compute the standard deviation
        if not self.std:
            self.std = np.sum(np.square(image - self.mean))
            self.std = sqrt(self.std / self.nb_pixl)


        #  data normalization
        if self.just_centered:
            image -= self.mean
        else:
            image = (image - self.mean) / self.std

        return image


    def get_mean(self):
        return self.mean

    def get_std(self):
        return self.std

    def get_mean_std(self):
        return self.mean, self.std

    def set_mean(self, mean):
        self.mean = mean

    def set_std(self, std):
        self.std = std

    def set_mean_std(self, mean, std):
        self.mean = mean
        self.std = std


    def __computeSumMean__(self, image_dataset, batch_len):
        """ Compute the numerator """

        for i,image in enumerate(image_dataset):
            self.mean += np.sum(image[:batch_len[i],:])
            self.nb_pixl += np.prod([batch_len[i], image.shape[1]])

    def __computeSumMeanOneImg__(self, img, len_img=0):
        """ Compute the numerator """

        if len_img==0:
            self.mean += np.sum(img)
            self.nb_pixl += np.prod([img.shape[0], img.shape[1]])
        else:
            self.mean += np.sum(img[:len_img,:])
            self.nb_pixl += np.prod([len_img, img.shape[1]])


    def __computeMeanOneImg__(self, img, len_img=0):
        """ Compute the mean on one image """

        if len_img==0:
            mean = np.mean(img)
            nb_pixl = np.sum([img.shape[0], img.shape[1]])
        else:
            mean = np.mean(img[:len_img,:])
            nb_pixl = np.sum([len_img, img.shape[1]])

        return (mean, nb_pixl)


    def __computeSumStd__(self, image_dataset, batch_len):
        """ Compute the numerator """

        for i,image in enumerate(image_dataset):
            self.std = np.sum(np.square(image[:batch_len[i],:] - self.mean))

    def __computeSumStdOneImg__(self, img, len_img=0):
        """ Compute the numerator """

        if len_img==0:
            self.std = np.sum(np.square(img - self.mean))
        else:
            self.std = np.sum(np.square(img[:len_img,:] - self.mean))

    def __computeStdOneImg__(self, img, len_img=0):
        """ Compute the numerator """

        if len_img==0:
            std = np.std(img)
        else:
            std = np.std(img[:len_img,:])

        return std


    def computeMeanStdOnBatch(self, func_load_img, nb_data, func_args=None):

        self.mean = 0
        self.std = 0
        self.nb_pixl = 0

        list_mean = []
        list_weight = []
        list_std = []
        # compute mean, batch per batch
        #batch = next(data_generator.one_pass_next_train())[0][0]
        for n in range(nb_data-1, -1, -1):
            if func_args:
                img = func_load_img(n, func_args['file_list'], norm=func_args['norm'])
            else:
                img = func_load_img(n)
            (mean, nb_pixl) = self.__computeMeanOneImg__(img)
            list_mean.append(mean)
            list_weight.append(nb_pixl)

        #self.mean /= self.nb_pixl
        self.mean = np.average(list_mean, weights=list_weight)
        print("MEAN=", self.mean)

        # compute std, batch per batch
        for n in range(nb_data-1, -1, -1):
            if func_args:
                img = func_load_img(n, func_args['file_list'], norm=func_args['norm'])
            else:
                img = func_load_img(n)

            std = self.__computeStdOneImg__(img)
            list_std.append(std)


        #self.std = sqrt(self.std / self.nb_pixl)
        self.std = np.average(list_std, weights=list_weight)
        print("STD=", self.std)

    @staticmethod
    def padding():
        return 300

    def __str__(self):
        return "Centrage réduction, std:"+str(self.std)+", mean:"+str(self.mean)

from collections import OrderedDict
import difflib as df
import unidecode
import os
from new_utils.settings import now
import logging
logger = logging.getLogger(__name__)


#the name of the titles should be in lower_case and with _ instead of spaces
titles_in_year = OrderedDict()
titles_in_year[-99] = ['titles', '...']
titles_in_year[-1] = ['designation_des_valeurs', '...']#['sections', 'constant']
titles_in_year[0] = ['nombre_de_titres', '...']
titles_in_year[1] = ['valeur_nominale', '...']
titles_in_year[2] = ['designation_des_valeurs', '...']
titles_in_year[3] = ['cours_cotes_plus_haut_plus_bas_dernier', '...']
titles_in_year[4] = ['cours_precedents', '...']
titles_in_year[5] = ['coupons_date_du_dernier_paye', '...']
titles_in_year[6] = ['coupons_nos', '...']
titles_in_year[7] = ['coupons_montant_brut', '...']
titles_in_year[8] = ['dernier_revenu', '...']
titles_in_year[9] = ['observations', '...']

titles = [titles_in_year[key][0] for key in titles_in_year]

key_word_log = 'LOG'

def decode_kaldi_log(log_file_path, key_word_pred):
    print("->->->->->:",key_word_pred)
    res = []
    with open(log_file_path) as f_log:
        line_to_insert = []
        for line in f_log.readlines():
            if 'Decoder did not reach end-state, outputting partial traceback' in line:
                continue
            temp_pred = ''
            temp_log = ''
            if len(line_to_insert) == 3:
                res.append([line_to_insert[0],line_to_insert[1],line_to_insert[2]])
                line_to_insert = []
            if 'frames/sec' in line:
                break
            if key_word_pred in line:
                print('HERE1')
                print(key_word_pred, line)
                if line.index(key_word_pred) == 0:
                    print('HERE2')
                    print(line)
                    temp_pred = line.split(' ',1)[1].replace("\n",'').replace(' ','').replace('|',' ')
                    if temp_pred == '':
                        temp_pred = '.'
            if key_word_log in line:
                if line.index(key_word_log) == 0:
                    temp_log = line.split(' is')[1].split('over')[0].replace(' ','')
                    """temp_split = temp_log.split('over')
                    tempo_a = temp_split[0].replace(' ','')
                    tempo_b = temp_split[1].split('frames.')[0].replace(' ','')"""
                    temp_log = 10 ** (float(temp_log))
            if temp_pred != '':
                #print(line.split(' ')[0])
                #print(temp_pred)
                line_to_insert.append(line.split(' ')[0])
                line_to_insert.append(temp_pred)
            elif temp_log != '':
                extra = ''
                if temp_log > 0.80:
                    extra = '#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+#+'
                #print(temp_log, extra)
                line_to_insert.append("%.3f" % temp_log)
                #print('--------------')
    return res


def post_treat_proba(proba_pred, corresponding_fst):
    fst_name = corresponding_fst.split('/')[-1]
    if fst_name == 'cours_cotes_plus_haut_plus_bas_dernierTLG_lexicon.fst':
        for i in range(len(proba_pred)):
            tmp = proba_pred[i][2]
            tmp = tmp.replace('++','<').replace('££','>')
            for extra in ["Net a payer", "h.", "nul", "-", "hier", "cours nul"]:
                if extra in tmp:
                    if '<comment>' not in tmp:
                        tmp = "<comment>"+tmp+"</comment>"
            proba_pred[i][2] = tmp
    return proba_pred

def get_LM_prediction_from_proba_file(proba_file, column_number, column_title, key_word_pred, fst_folder="fst_folder"):
    """
    The probability file is going to be decoded using the language model of the
    column title, or column_number
    """
    column_title = column_title.strip()
    if column_title == '':
        try:
            column_title = titles_in_year[int(column_number)][0]
        except:
            return ''
    column_title = column_title.lower().strip().replace(' ','_')
    column_title = unidecode.unidecode(column_title)
    match = df.get_close_matches(column_title, titles, n=3, cutoff=0.6)
    if match == []:
        try:
            match = [titles_in_year[int(column_number)][0]]
        except:
            return []
    corresponding_fst = match[0]+"TLG_lexicon.fst"
    corresponding_fst = os.path.join('new_utils/thrax/',fst_folder,corresponding_fst)
    logger.info(now()+'using FST: '+str(corresponding_fst))
    if not os.path.exists(corresponding_fst):
        logger.error(now()+'the corresponding FST does not exist, returning []')
        print("!>>>> NO FILE:",corresponding_fst)
        return []

    print("using: ", corresponding_fst)
    page_folder = proba_file.rsplit('/',1)[0]
    page_name = proba_file.rsplit('/',1)[-1].split('.',1)[0]
    labes_file = "new_utils/kaldi/FST/fst/Labels.txt"
    new_decode_folder = os.path.join(page_folder,"kaldi_"+page_name)
    output_log_file = os.path.join(new_decode_folder, page_name+".log")

    commando = 'sh new_utils/kaldi/exp/testP.sh '+ \
                        proba_file+" "+ \
                        page_folder+" "+ \
                        page_name+" "+ \
                        labes_file+" "+ \
                        corresponding_fst+" "+ \
                        new_decode_folder+" "+ \
                        output_log_file
    logger.info(now()+commando)
    os.system(commando)
    logger.info(now()+'decoding and applying post treatment')
    proba_pred = decode_kaldi_log(output_log_file, key_word_pred)
    proba_pred = post_treat_proba(proba_pred, corresponding_fst)
    temp_avg_conf = sum([ float(confidence) for file_name, the_pred, confidence in proba_pred]) / len(proba_pred)
    if temp_avg_conf < 0.95:
        logger.error(now()+'average confidence under 0.95!')
    return proba_pred




if __name__ == "__main__":
    # x = get_LM_prediction_from_proba_file('testing/COTE-MARCHE_18991201_04/COTE-MARCHE_18991201_04_column-1_proba.txt',\
    #                                       1, 'VALEUR  nominale  VALEUR  nominale')

    x = decode_kaldi_log("temp_folder/COTE-MARCHE_18990607_04/kaldi_COTE-MARCHE_18990607_04_column-3_proba/COTE-MARCHE_18990607_04_column-3_proba.log")
    [print(y) for y in x]

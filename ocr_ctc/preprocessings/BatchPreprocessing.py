# encoding=utf-8
class BatchPreprocessing:
    """
        Class facilitant l'ajout de preprocessings

        Chaque BatchPreprocessing sera appelé sur chaque batch
        """
    def __init__(self, **kwargs):
        raise NotImplementedError

    def __call__(self, x, y, seq_len, label_len, padding):
        """
        Appel du preprocessing
        
        :param x: Batch de séquences
        :param y: Labels associés aux séquences
        :param seq_len: Longueurs des séquences
        :param label_len: Longueurs des labels
        :param padding: Valeur du padding
        :return: Tuple (new_x, new_y, new_seq_len, new_label_len, new_padding)
        """
        raise NotImplementedError

    def __str__(self):
        return "BatchPreprocessing"
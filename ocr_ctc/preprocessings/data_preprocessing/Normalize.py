import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class Normalize(ImagePreprocessing):
    """
    Normalise dans [-0.5;0.5] les séquences dont les valeurs sont dans [0;255] 
    """
    def __init__(self):
        self.reformatter = np.vectorize(lambda x: (x-128) / 255)

    def preprocessing(self, image):
        return self.reformatter(image)

    def __str__(self):
        return "Normalize (x-128) / 255"
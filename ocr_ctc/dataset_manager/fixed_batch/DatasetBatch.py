import pickle

import h5py
import numpy as np
import os
import sys
import random
from keras.preprocessing import sequence

from ocr_ctc.dataset_manager.Dataset import Dataset
from ocr_ctc.dataset_manager.DatasetInfo import DatasetInfo


class DatasetBatch(Dataset):
    """ FIXED DATASET - DATASET BATCH

        Data are saved in a batch. TO DO: If the batch_size is equal to 1, data are duplicated to avoid a bug when saving. 
        Dataset batch are built when data are loaded (in generator) 
        """


    def __init__(self, path, train_size=-1, valid_size=-1, test_size=-1):

        super().__init__(path, train_size, test_size, valid_size)

    def new_dataset(self, label_array, label_dict, padding, sep=' ', all_labels=None, all_label_dict=None, nb_features=None, window_size=None, nb_cols=None):

        if os.path.exists(self._path):
            print("PATH", self.path, "already exists. The dataset_manager can not be created.")
            sys.exit(0)
        else:
            os.mkdir(self._path)
            os.mkdir(self._path + "/TRAIN/")
            os.mkdir(self._path + "/TEST/")
            os.mkdir(self._path + "/VALID/")

        if not nb_features:
            nb_features = -1

        self.dataset_info = DatasetInfo(0, 0, 0, nb_features, padding, label_array, label_dict, sep=sep, all_labels=all_labels, \
                                        all_label_dict=all_label_dict, window_size=window_size, nb_cols=nb_cols)

    def save_train_batch(self, batch):
        data, labels, seqlen, labellen = batch
        if len(data)>0:
            self.dataset_info.nbbatch_train += 1

            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[2]

            h5f = h5py.File(self._path + "/TRAIN/" + str(self.dataset_info.nbbatch_train) + ".h5", 'w')
            h5f.create_dataset("data", data=np.asarray(data))
            h5f.create_dataset("labels", data=np.asarray(labels))
            h5f.create_dataset("seqlen", data=np.asarray(seqlen))
            h5f.create_dataset("labellen", data=np.asarray(labellen))
            h5f.close()

            self._train_files.append(self._path + "/TRAIN/" + str(self.dataset_info.nbbatch_train) + ".h5")

    def save_test_batch(self, batch):
        data, labels, seqlen, labellen = batch
        if len(data) > 0:
            self.dataset_info.nbbatch_test += 1

            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[2]

            h5f = h5py.File(self._path + "/TEST/" + str(self.dataset_info.nbbatch_test) + ".h5", 'w')
            h5f.create_dataset("data", data=np.asarray(data))
            h5f.create_dataset("labels", data=np.asarray(labels))
            h5f.create_dataset("seqlen", data=np.asarray(seqlen))
            h5f.create_dataset("labellen", data=np.asarray(labellen))
            h5f.close()

            self._test_files.append(self._path + "/TEST/" + str(self.dataset_info.nbbatch_test) + ".h5")

    def save_valid_batch(self, batch):
        data, labels, seqlen, labellen = batch
        if len(data) > 0:
            self.dataset_info.nbbatch_valid += 1

            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[2]

            h5f = h5py.File(self._path + "/VALID/" + str(self.dataset_info.nbbatch_valid) + ".h5", 'w')
            h5f.create_dataset("data", data=np.asarray(data))
            h5f.create_dataset("labels", data=np.asarray(labels))
            h5f.create_dataset("seqlen", data=np.asarray(seqlen))
            h5f.create_dataset("labellen", data=np.asarray(labellen))
            h5f.close()

            self._valid_files.append(self._path + "/VALID/" + str(self.dataset_info.nbbatch_valid) + ".h5")


    def save_custom_batch(self, batch, name_dir, index_batch):
        data, labels, seqlen, labellen = batch

        if len(data) > 0:
            if self.nb_features == -1:
                self.dataset_info.nb_features = data.shape[2]

            h5f = h5py.File(self._path + "/" + name_dir + "/" + str(index_batch) + ".h5", 'w')
            h5f.create_dataset("data", data=np.asarray(data))
            h5f.create_dataset("labels", data=np.asarray(labels))
            h5f.create_dataset("seqlen", data=np.asarray(seqlen))
            h5f.create_dataset("labellen", data=np.asarray(labellen))
            h5f.close()

            self._valid_files.append(self._path + "/" + name_dir + "/" + str(self.dataset_info.nbbatch_valid) + ".h5")


    def save(self):
        with open(self._path + "/dataset_manager.pickle", 'wb') as f:
            pickle.dump(self.dataset_info, f)

    def load(self):
        with open(self._path + "/dataset_manager.pickle", 'rb') as f:
            self.dataset_info = pickle.load(f)

        for batch in sorted(os.listdir(self._path + "/TRAIN/")):
            self._train_files.append(self._path + "/TRAIN/" + batch)

        for batch in sorted(os.listdir(self._path + "/TEST/")):
            self._test_files.append(self._path + "/TEST/" + batch)

        for batch in sorted(os.listdir(self._path + "/VALID/")):
            self._valid_files.append(self._path + "/VALID/" + batch)

    @staticmethod
    def __sort_file_list(train_files):

        lengths = []
        for file in train_files:
            h5f = h5py.File(file, 'r')
            mdata = h5f["data"][:]
            lengths.append(mdata.shape[1])

        files, _ = zip(*sorted(zip(train_files, lengths), key=lambda x: x[1]))
        return list(files)

    def get_train_generator(self, nb_batch=None, order="shuffle"):

        train_files = self._train_files[:nb_batch] if nb_batch else self._train_files

        if order == "curriculum":
            train_files = self.__sort_file_list(train_files)

        while True:
            if order == "shuffle":
                random.shuffle(train_files, random.random)
            for id_file in range(0,len(self._train_files), self.train_size):
                datas = []
                labels = []
                seqlens = []
                labellens = []
                for id_in_batch in range(self.train_size):
                    h5f = h5py.File(self._train_files[id_file + id_in_batch], 'r')
                    datas.append(np.asarray(h5f["data"][:]))
                    labels.append(np.asarray(h5f["labels"][:]))
                    seqlens.append(np.asarray(h5f["seqlen"][:]))
                    labellens.append(np.asarray(h5f["labellen"][:]))

                    if self.train_size == 1:
                        datas.append(np.asarray(h5f["data"][:]))
                        labels.append(np.asarray(h5f["labels"][:]))
                        seqlens.append(np.asarray(h5f["seqlen"][:]))
                        labellens.append(np.asarray(h5f["labellen"][:]))


                    h5f.close()

                data = sequence.pad_sequences(datas, value=float(self.dataset_info.padding), dtype='float32',
                                              padding="post", truncating='post')

                if isinstance(labels, float):
                    labels = [labels]

                labels = sequence.pad_sequences(labels, value=float(len(self.dataset_info.label_array)),
                                                dtype='float32', padding="post")
                yield [np.asarray(datas), np.asarray(labels), np.asarray(seqlens), np.asarray(labellens)], np.zeros(len(datas))

    def get_test_generator(self):
        while True:
            for id_file in range(0,len(self._test_files), self.test_size):
                datas = []
                labels = []
                seqlens = []
                labellens = []
                for id_in_batch in range(self.test_size):
                    h5f = h5py.File(self._test_files[id_file + id_in_batch], 'r')
                    datas.append(np.asarray(h5f["data"][:]))
                    labels.append(np.asarray(h5f["labels"][:]))
                    seqlens.append(np.asarray(h5f["seqlen"][:]))
                    labellens.append(np.asarray(h5f["labellen"][:]))

                    if self.train_size == 1:
                        datas.append(np.asarray(h5f["data"][:]))
                        labels.append(np.asarray(h5f["labels"][:]))
                        seqlens.append(np.asarray(h5f["seqlen"][:]))
                        labellens.append(np.asarray(h5f["labellen"][:]))


                    h5f.close()
                yield [np.asarray(datas), np.asarray(labels), np.asarray(seqlens), np.asarray(labellens)], np.zeros(
                    len(datas))

    def get_valid_generator(self):
        while True:
            for id_file in range(0,len(self._valid_files), self.valid_size):
                datas = []
                labels = []
                seqlens = []
                labellens = []
                for id_in_batch in range(self.valid_size):
                    h5f = h5py.File(self._valid_files[id_file + id_in_batch], 'r')
                    datas.append(np.asarray(h5f["data"][:]))
                    labels.append(np.asarray(h5f["labels"][:]))
                    seqlens.append(np.asarray(h5f["seqlen"][:]))
                    labellens.append(np.asarray(h5f["labellen"][:]))

                    if self.train_size == 1:
                        datas.append(np.asarray(h5f["data"][:]))
                        labels.append(np.asarray(h5f["labels"][:]))
                        seqlens.append(np.asarray(h5f["seqlen"][:]))
                        labellens.append(np.asarray(h5f["labellen"][:]))

                    h5f.close()
                yield [np.asarray(datas), np.asarray(labels), np.asarray(seqlens), np.asarray(labellens)], np.zeros(
                    len(datas))

    @property
    def nb_features(self):
        return self.dataset_info.nb_features

    @property
    def nbbatch_train(self):
        return self.dataset_info.nbbatch_train

    @property
    def nbbatch_test(self):
        return self.dataset_info.nbbatch_test

    @property
    def nbbatch_valid(self):
        return self.dataset_info.nbbatch_valid

    @property
    def padding(self):
        return self.dataset_info.padding

    @property
    def label_array(self):
        return self.dataset_info.label_array

    @property
    def label_dict(self):
        return self.dataset_info.label_dict

    @property
    def nb_labels(self):
        return len(self.dataset_info.label_array)

    @property
    def sep(self):
        try:
            return self.dataset_info.sep
        except AttributeError:
            return ' '

    @property
    def all_labels(self):
        try:
            return self.dataset_info.all_labels if self.dataset_info.all_labels else self.label_array
        except AttributeError:
            return self.label_array

    @property
    def all_label_dict(self):
        try:
            return self.dataset_info.all_labels_dict if self.dataset_info.all_labels_dict else self.label_dict
        except AttributeError:
            return self.label_dict

    @property
    def window_size(self):
        try:
            return self.dataset_info.window_size
        except AttributeError:
            return None


    @property
    def nb_cols(self):
        try:
            return self.dataset_info.nb_cols
        except AttributeError:
            return None
import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class NormalizeInterval(ImagePreprocessing):
    """
    Normalization in an interval [min_int, max_int]
    """
    def __init__(self, min_int=-1, max_int=1, min_val=0, max_val=249):
        """ min_int = min value of the final interval (after mapping)
        max_int = max value of the final interval 
        min_val = minimum value in input data
        max_val = max value in input data """
        self.min_int = min_int
        self.max_int = max_int
        self.min_val = min_val
        self.max_val = max_val

    def preprocessing(self, image, compute_min_max=False):

        if compute_min_max:
            min_val = np.min(image)
            max_val = np.max(image)
        else:
            min_val = self.min_val
            max_val = self.max_val


        return (self.max_int - self.min_int) * (image - min_val)/ (max_val - min_val) + self.min_int

    def __str__(self):
        return "Normalization in an interval"
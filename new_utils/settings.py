from collections import OrderedDict
from datetime import datetime
def now():
    return str(datetime.now())+' :: '

LABEL_LIST = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',\
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',\
            '0','1','2','3','4','5','6','7','8','9','@','.',' ','/',',','(',')','-',':']

NEW_LABELS_DICT = OrderedDict()
NEW_LABELS_DICT['A'] = ['À','Â']
NEW_LABELS_DICT['E'] = ['É','È','Ê','Ë']
NEW_LABELS_DICT['I'] = ['Î','Ï']
NEW_LABELS_DICT['O'] = ['Ô']
NEW_LABELS_DICT['U'] = ['Ù','Û','Ü']
NEW_LABELS_DICT['C'] = ['Ç']
NEW_LABELS_DICT['a'] = ['à','â']
NEW_LABELS_DICT['e'] = ['é','è','ê','ë']
NEW_LABELS_DICT['i'] = ['î','ï']
NEW_LABELS_DICT['o'] = ['ô']
NEW_LABELS_DICT['u'] = ['ù','û','ü']
NEW_LABELS_DICT['c'] = ['ç']
NEW_LABELS_DICT[':'] = [';']
NEW_LABELS_DICT['@'] = ['*','%',"'",'"','£','$','+','&']


LABEL_LIST = [' ', '"', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '£']
NEW_LABELS_DICT = OrderedDict()
NEW_LABELS_DICT['A'] = ['À','Â']
NEW_LABELS_DICT['E'] = ['É','È','Ê','Ë']
NEW_LABELS_DICT['I'] = ['Î','Ï']
NEW_LABELS_DICT['O'] = ['Ô']
NEW_LABELS_DICT['U'] = ['Ù','Û','Ü']
NEW_LABELS_DICT['C'] = ['Ç']
NEW_LABELS_DICT['a'] = ['à','â']
NEW_LABELS_DICT['e'] = ['é','è','ê','ë']
NEW_LABELS_DICT['i'] = ['î','ï']
NEW_LABELS_DICT['o'] = ['ô']
NEW_LABELS_DICT['u'] = ['ù','û','ü']
NEW_LABELS_DICT['c'] = ['ç']
LABEL_LIST = [' ', '"', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '~', '£']

FINAL_CHARSET = ['blank', '<sp>', '"', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '£', 'À', 'Â', 'É', 'È', 'Ê', 'Ë', 'Î', 'Ï', 'Ô', 'Ù', 'Û', 'Ü', 'Ç', 'à', 'â', 'é', 'è', 'ê', 'ë', 'î', 'ï', 'ô', 'ù', 'û', 'ü', 'ç']

TRAIN_LABELS = ""

VALID_LABELS = ""

TEST_LABELS = ""

BATCH_SIZE = 4
ORIGINAL_BATCH_SIZE = 4

TRAIN_FOLDER = ""

VALID_FOLDER = ""

TEST_FOLDER = ""

MEAN = 178.46429447857915
STD = 33.56414641258921

# MEAN = 173.7735004543969 #178.46429447857915 #169.14015652234303
# STD = 35.519464191115866 #33.56414641258921 #42.83750628205289

NORM = 32

KEEP_RATIO = True

NB_COLS = 16

STRIDE = 2

PADDING = -1

if __name__=="__main__":

    LABEL_LIST = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',\
                'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',\
                '0','1','2','3','4','5','6','7','8','9','@','.',' ','/',',','(',')','-',':']

    NEW_LABELS_DICT = OrderedDict()
    NEW_LABELS_DICT['A'] = ['À','Â']
    NEW_LABELS_DICT['E'] = ['É','È','Ê','Ë']
    NEW_LABELS_DICT['I'] = ['Î','Ï']
    NEW_LABELS_DICT['O'] = ['Ô']
    NEW_LABELS_DICT['U'] = ['Ù','Û','Ü']
    NEW_LABELS_DICT['C'] = ['Ç']
    NEW_LABELS_DICT['a'] = ['à','â']
    NEW_LABELS_DICT['e'] = ['é','è','ê','ë']
    NEW_LABELS_DICT['i'] = ['î','ï']
    NEW_LABELS_DICT['o'] = ['ô']
    NEW_LABELS_DICT['u'] = ['ù','û','ü']
    NEW_LABELS_DICT['c'] = ['ç']
    NEW_LABELS_DICT[':'] = [';']
    NEW_LABELS_DICT['@'] = ['*','%',"'",'"','£','$','+','&']

    LABEL_LIST = [' ', '"', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '£']
    NEW_LABELS_DICT = OrderedDict()
    NEW_LABELS_DICT['A'] = ['À','Â']
    NEW_LABELS_DICT['E'] = ['É','È','Ê','Ë']
    NEW_LABELS_DICT['I'] = ['Î','Ï']
    NEW_LABELS_DICT['O'] = ['Ô']
    NEW_LABELS_DICT['U'] = ['Ù','Û','Ü']
    NEW_LABELS_DICT['C'] = ['Ç']
    NEW_LABELS_DICT['a'] = ['à','â']
    NEW_LABELS_DICT['e'] = ['é','è','ê','ë']
    NEW_LABELS_DICT['i'] = ['î','ï']
    NEW_LABELS_DICT['o'] = ['ô']
    NEW_LABELS_DICT['u'] = ['ù','û','ü']
    NEW_LABELS_DICT['c'] = ['ç']

    final_labels = []

    for char in LABEL_LIST:
        final_labels.append(char)
    for key in NEW_LABELS_DICT:
        for char in NEW_LABELS_DICT[key]:
            final_labels.append(char)
    print(final_labels)
    with open('units.list','w') as f:
        for char in final_labels:
            if char == ' ':
                f.write("|\n")
            else:
                f.write(char+"\n")

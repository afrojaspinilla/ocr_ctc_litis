#!/usr/bin/env python

# Apache 2.0

import sys

fread = open(sys.argv[1], 'r')

print ('0 1 <epsilon> <epsilon>')
print ('1 1 <blk> <epsilon>')
print ('2 2 <blk> <epsilon>')
print ('2 0 <blk> <epsilon>')

nodeX = 3
for entry in fread.readlines():
    entry = entry.replace('\n','').strip()
    fields = entry.split(' ')
    phone = fields[0]
    if phone == '<epsilon>' or phone == '<blk>':
      continue

    if '#' in phone:
      print (str(0) + ' ' + str(0) + ' ' +  '<epsilon>' + ' ' + phone);
    else:
      print (str(1) + ' ' + str(nodeX) + ' ' +  phone + ' ' + phone);
      print (str(nodeX) + ' ' + str(nodeX) + ' ' +  phone + ' <epsilon>');
      print (str(nodeX) + ' ' + str(2) + ' ' + '<epsilon> <epsilon>');
    nodeX += 1
print ('0')
sys.exit()
fread.close()

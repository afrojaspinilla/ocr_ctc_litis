# encoding=utf-8
from __future__ import print_function

import os, sys

sys.path.append("./")
from keras.callbacks import ModelCheckpoint
from keras.layers import (
    TimeDistributed,
    Activation,
    Dense,
    Input,
    Bidirectional,
    Conv2D,
    Flatten,
    MaxPooling2D,
    GaussianNoise,
    Masking,
    LSTM,
)
from keras.optimizers import Adam, SGD, RMSprop

from ocr_ctc.CTC_model.CTCModel import CTCModel
from ocr_ctc.CTC_model.callbacks.CTCTensorBoard import CTCTensorBoard
from ocr_ctc.CTC_model.model_templates import *
from ocr_ctc.dataset_manager.new_batch.DatasetBatch import DatasetBatch
from ocr_ctc.CTC_model.loggers.HPLogger import HPLogger
from ocr_ctc.CTC_model.loggers.SummaryLogger import SummaryLogger
from ocr_ctc.CTC_model.weightnorm import AdamWithWeightnorm, data_based_init
from pathlib import Path
import shutil


def get_lattest_weights(log_path, initial_epoch=-1):
    if os.path.exists(log_path):
        fileNames = [
            fn
            for fn in os.listdir(os.path.join(os.path.dirname(__file__), log_path))
            if fn[-4:] == "hdf5"
        ]
        if len(fileNames) == 0:
            print("Warning: no weights to load")
        else:
            fileNames = sorted(
                fileNames,
                key=lambda y: int(y.rsplit(".")[-3].split("-")[0])
                if not "-inf" in y
                else int(y.rsplit(".")[-2].split("-")[0]),
            )  # int(y.rsplit('.')[-2]))
            if initial_epoch > -1:
                for elmt in fileNames:
                    ie = (
                        int(elmt.rsplit(".")[-3].split("-")[0])
                        if not "-inf" in elmt
                        else int(elmt.rsplit(".")[-2].split("-")[0])
                    )
                    if ie == initial_epoch:
                        print("Weights to load :", elmt)
                        return elmt

                print("Weights to load :", fileNames[-1])
                return fileNames[-1]
            else:
                print("Weights to load :", fileNames[-1])
                return fileNames[-1]
    else:
        print("Warning: no weights to load")

    return ""


def create_network(dataset, dropout):

    input_data = Input(
        name="input", shape=(None, dataset.nb_features, dataset.nb_cols, 1)
    )  #  window_size instead of nb_cols

    masking = Masking(mask_value=dataset.padding)(input_data)
    noise = GaussianNoise(0.01)(masking)

    conv0 = TimeDistributed(
        Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv0"
    )(noise)
    # pool0 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool0")(conv0)
    conv1 = TimeDistributed(
        Conv2D(32, (3, 3), padding="same", data_format="channels_last"), name="Conv1"
    )(conv0)
    pool1 = TimeDistributed(
        MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool1"
    )(conv1)
    conv2 = TimeDistributed(
        Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv2"
    )(pool1)
    # pool2 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool2")(conv2)
    conv3 = TimeDistributed(
        Conv2D(64, (3, 3), padding="same", data_format="channels_last"), name="Conv3"
    )(conv2)
    pool3 = TimeDistributed(
        MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool3"
    )(conv3)
    conv4 = TimeDistributed(
        Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv4"
    )(pool3)
    # pool4 = TimeDistributed(MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool4")(conv4)
    conv5 = TimeDistributed(
        Conv2D(128, (3, 3), padding="same", data_format="channels_last"), name="Conv5"
    )(conv4)
    pool5 = TimeDistributed(
        MaxPooling2D(pool_size=(2, 2), data_format="channels_last"), name="Pool5"
    )(conv5)
    conv6 = TimeDistributed(
        Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv6"
    )(pool5)
    # pool6 = TimeDistributed(MaxPooling2D(pool_size=(2, 1), data_format="channels_last"), name="Pool6")(conv6)
    conv7 = TimeDistributed(
        Conv2D(256, (3, 3), padding="same", data_format="channels_last"), name="Conv7"
    )(conv6)

    flatten = TimeDistributed(Flatten(), name="Flatten")(conv0)
    dense = TimeDistributed(Dense(256), name="Dense")(flatten)

    masking2 = Masking(mask_value=dataset.padding)(dense)

    lstm0 = Bidirectional(
        LSTM(256, return_sequences=True, dropout=dropout), name="LSTM0"
    )(masking2)
    lstm1 = Bidirectional(
        LSTM(256, return_sequences=True, dropout=dropout), name="LSTM1"
    )(lstm0)

    out = TimeDistributed(Dense(dataset.nb_labels + 1), name="DenseSoftmax")(dense)
    output = Activation("softmax", name="Softmax")(out)

    return CTCModel([input_data], [output], charset=dataset.all_labels)


def run(
    run_name,
    dataset,
    batch_size,
    dropout=0.0,
    lr=0.0001,
    momentum=0.9,
    epochs=100,
    defined_model=None,
    checkpoint_period=2,
    checkpoint=None,
    nb_batchs_train=-1,
    nb_batchs_valid=-1,
    sup_dir="",
    optimizer=Adam(lr=0.0001),
    beam_width_decode=100,
    greedy_decode=False,
    top_paths_decode=1,
    test_steps=2,
    initial_epoch=0,
    transfer_learning=None,
    init_archi=False,
    init_last_layer=False,
):

    log_path = "./" + sup_dir + "outputs/results/" + str(run_name)
    checkpoint_path = "./" + sup_dir + "outputs/checkpoints/" + str(run_name) + "/"
    if not os.path.exists("./" + sup_dir + "outputs"):
        os.mkdir("./" + sup_dir + "outputs")
    if not os.path.exists("./" + sup_dir + "outputs/results"):
        os.mkdir("./" + sup_dir + "outputs/results")
    if not os.path.exists("./" + sup_dir + "outputs/checkpoints"):
        os.mkdir("./" + sup_dir + "outputs/checkpoints")

    if (
        os.path.isdir(os.path.join(os.path.dirname(__file__), log_path))
        and len(os.listdir(os.path.join(os.path.dirname(__file__), checkpoint_path)))
        > 0
    ):
        checkpoint = True
    else:
        if not os.path.exists(log_path):
            os.mkdir(log_path)
        if not os.path.exists(checkpoint_path):
            os.mkdir(checkpoint_path)

            if transfer_learning is not None:
                dir_trf = "./" + sup_dir + "outputs/results/" + transfer_learning + "/"
                for file_trf in os.listdir(dir_trf):
                    if file_trf[-3:] in ["txt", "son", "pkl"]:
                        shutil.copy(dir_trf + file_trf, log_path + file_trf)
                dir_trf = (
                    "./" + sup_dir + "outputs/checkpoints/" + transfer_learning + "/"
                )
                file_trf = get_lattest_weights(dir_trf, initial_epoch=initial_epoch)
                shutil.copy(dir_trf + file_trf, checkpoint_path + file_trf)
                checkpoint = True

    my_file = Path(log_path + "/model_train.json")
    if checkpoint and my_file.is_file():

        if (
            init_archi
        ):  # initialize a new architecture in case of checkpoint (continue a training of  in case of transfer learning (useful for changing dropout value)
            if defined_model is not None:
                network = manage_model_template(defined_model, dataset, dropout)
            else:
                network = create_network(dataset, dropout)
        else:
            if init_last_layer:
                network = CTCModel(None, None, charset=dataset.all_labels)
            else:
                network = CTCModel(None, None)

        file_checkpoint = get_lattest_weights(
            checkpoint_path, initial_epoch=initial_epoch
        )  # "./checkpoints/" + log_path.split('/')[-1] + '/checkpoints/')
        initial_epoch = (
            int(file_checkpoint.rsplit(".")[-3].split("-")[0])
            if not "-inf" in file_checkpoint
            else int(file_checkpoint.rsplit(".")[-2].split("-")[0])
        )  # rsplit('.', 2)[-2])
        # network.model_train.load_weights(
        #     checkpoint_path + file_checkpoint)  # "./checkpoints/" + log_path.split('/')[-1] + '/checkpoints/' + file_checkpoint)
        # network.model_pred.set_weights(network.model_train.get_weights())
        # network.model_eval.set_weights(network.model_train.get_weights())

        network.load_model(
            log_path,
            optimizer,
            init_last_layer=init_last_layer,
            file_weights=checkpoint_path + file_checkpoint,
        )
        if network.charset is None:
            print("No charset has been saved.")
            network.charset = dataset.all_labels
            network.save_model(log_path)
        else:
            assert network.charset == dataset.all_labels
    else:
        if defined_model is not None:
            network = manage_model_template(defined_model, dataset, dropout)
        else:
            network = create_network(dataset, dropout)
        network.compile(optimizer)
        network.save_model(log_path)

    # Hyperparameters to log
    hyperparameters = {
        "Learning rate": lr,
        "Momentum": momentum,
        "Dropout": dropout,
        "Greedy decode": greedy_decode,
        "Beam decode width": beam_width_decode,
        "Top path decode": top_paths_decode,
        "Batch size": batch_size,
        "Nb of features per frames": dataset.nb_features,
        "Size of the windows": dataset.window_size,
    }

    # Logging
    print(network.get_model_train().summary())
    SummaryLogger(log_path, "summary_pred.txt", model=network.get_model_pred()).log()
    SummaryLogger(log_path, "summary_train.txt", model=network.get_model_train()).log()
    HPLogger(log_path, "hyperparameters.txt", parameters=hyperparameters).log()

    # Setting up the callbacks
    callbacks = [
        ModelCheckpoint(
            checkpoint_path + "weights.{epoch:02d}-{val_loss:.2f}.hdf5",
            period=checkpoint_period,
        ),
        CTCTensorBoard(
            network,
            dataset,
            test_steps=test_steps,
            log_dir=log_path,
            write_graph=False,
            batch_size=batch_size,
            eval_train=True,
            eval_test=True,
        )  # ,
        # ReduceLROnPlateau(factor=0.5, patience=3, verbose=1, epsilon=1e-3, cooldown=100)
    ]

    # load weights
    # if checkpoint:
    #     file_checkpoint = get_lattest_weights(
    #         checkpoint_path, initial_epoch=initial_epoch)  # "./checkpoints/" + log_path.split('/')[-1] + '/checkpoints/')
    #     initial_epoch = int(file_checkpoint.rsplit('.')[-3].split('-')[0]) if not '-inf' in file_checkpoint else int(file_checkpoint.rsplit('.')[-2].split('-')[0])  # rsplit('.', 2)[-2])
    #     network.model_train.load_weights(
    #         checkpoint_path + file_checkpoint)  # "./checkpoints/" + log_path.split('/')[-1] + '/checkpoints/' + file_checkpoint)
    #     network.model_pred.set_weights(network.model_train.get_weights())
    #     network.model_eval.set_weights(network.model_train.get_weights())

    nb_batchs_train = (
        int(dataset.nbbatch_train / batch_size)
        if nb_batchs_train == -1
        else nb_batchs_train
    )
    nb_batchs_valid = (
        int(dataset.nbbatch_valid / batch_size)
        if nb_batchs_valid == -1
        else nb_batchs_valid
    )
    if nb_batchs_valid == 0:
        nb_batchs_valid = None

    # Run
    network.fit_generator(
        generator=dataset.get_train_generator(),
        steps_per_epoch=nb_batchs_train,
        epochs=epochs,
        verbose=1,
        validation_data=dataset.get_valid_generator(),
        validation_steps=nb_batchs_valid,
        callbacks=callbacks,
        initial_epoch=initial_epoch,
    )


def main(
    batch_size=2,
    nb_batchs_train=-1,
    nb_batchs_valid=-1,
    setPath="",
    name="",
    defined_model="",
    initial_epoch=-1,
    transfer_learning=None,
    init_archi=True,
    learning_rate=0.0001,
):

    sup_dir = "ocr_ctc/"
    defined_model = string_to_model(defined_model)
    init_last_layer = False

    if not os.path.exists(setPath + "/dataset_manager.pickle"):
        print("data not found.")
        sys.exit(0)

    dataset = DatasetBatch(setPath, batch_size, batch_size, batch_size)
    dataset.load()

    lr = learning_rate
    # optimizer = Adam(lr=lr)
    optimizer = Adam(lr=0.0001)

    run(
        name,
        dataset,
        batch_size,
        dropout=0.1,
        epochs=200,
        lr=lr,
        nb_batchs_train=nb_batchs_train,
        initial_epoch=initial_epoch,
        nb_batchs_valid=nb_batchs_valid,
        sup_dir=sup_dir,
        transfer_learning=transfer_learning,
        defined_model=defined_model,
        optimizer=optimizer,
        init_archi=init_archi,
        init_last_layer=init_last_layer,
    )


if __name__ == "__main__":

    MAX_EXCEPTION = 10
    counter_exception = 0
    finished = False
    try_except = False

    if try_except:  # allow to continue after an exit (e.g. OOM)
        while finished == False:
            try:
                # K._LEARNING_PHASE = tf.constant(0)
                main()
                finished = True
            except:
                print(
                    "\nUnusual exit.. ("
                    + str(counter_exception)
                    + "/"
                    + str(MAX_EXCEPTION)
                    + ")"
                )
                finished = False

                counter_exception += 1
                if counter_exception > MAX_EXCEPTION:
                    sys.exit(0)
    else:
        main()

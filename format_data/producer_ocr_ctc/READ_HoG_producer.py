# encoding=utf-8
import getopt
import sys
import os

import numpy as np
from PIL import Image
from keras.preprocessing import sequence
from random import shuffle

from ocr_ctc.dataset_manager import DatasetBatch
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose

from copy import deepcopy
import cv2

def HoGFeatures(image, normF):#normF[0] = largeur colonne normF[1] = hauteur de normalisation
    largeurCol = normF[0] / 2
    imageMat = image
    m, n = imageMat.shape
    hog = cv2.HOGDescriptor((normF[0],normF[1]), (normF[0],16), (normF[0],8), (normF[0],8), 9)# Tradi : (8,64), (8,16), (8,8), (8,8), 9 # LAPA : ((8,32), (8,16), (8,8), (8,8), 9)
    for i in range(n):
        colImage = imageMat[:, int(max(i - largeurCol, 0)) : int(min(i + largeurCol, n))]
        if(i - largeurCol < 0):
            zeroPadding = np.ones((m, int(abs(i - largeurCol))), 'uint8') * 255
            colImage = np.concatenate((zeroPadding, colImage), axis=1)
        if(i + largeurCol > n):
            zeroPadding = np.ones((m, int(i + largeurCol - n)), 'uint8') * 255
            colImage = np.concatenate((colImage, zeroPadding), axis=1)
        tmp = hog.compute(colImage).T
        if i == 0:
            hogf = np.zeros((n, int(tmp.shape[1])),'f')
            hogf[i][:] = tmp
        else:
            hogf[i][:] = tmp
    return hogf.T

def produce_dataset(data_dir, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=[], preprocessings_batch=[],
                    padding=-1., norm=64, reduce_charset=False):
    """ TRAIN-B is used for the training and TRAIN-A for the valid and test """
    train_files = []
    test_files = []
    valid_files = []
    validA_files = []
    label_dict = {}
    label_array = []

    custom_size = valid_size

    cur_label = 0

    sep = 'c0'
    idx_train = 18000
    idx_val = 1000 # idx_train + idx_val


    if reduce_charset:
        # load a reduced charset
        file_char_reduced = data_dir + "/Train-A/read2017charsetReduceascii.txt"

        # Read the labels which will be considered in training
        with open(file_char_reduced) as f:
            for line in f.readlines():
                if line is "\n":
                    continue

                list_lab = line.split(' ')
                for lab in list_lab:
                    if lab not in label_dict:
                        label_dict[lab] = cur_label
                        label_array.append(lab)
                        cur_label += 1


    gtdir = "idf/trackB-GT-ite1Ascii.idf" # labeling of the ground truth


    # train set
    with open(data_dir + 'Train-B/' + gtdir, "r") as f_train: # encoding="ISO-8859-1"
        for line in f_train.readlines():

            if line is "\n":
                continue

            data = line.replace('\n', '').replace('\r', '').split(' ')
            data_file = data_dir + 'Train-B/img/' + data[0] + ".tif"
            label = data[1:]
            if label[-1] == '\n':
                label = label[:-1]
            index_file = int(data[0].split('_')[0])

            if reduce_charset and index_file<idx_train: # charset is reduced, we remove in training the labels that are not considered
                for i in range(len(label)-1, -1, -1):
                    lab = label[i]
                    if lab not in label_dict:
                        del label[i]
            elif not reduce_charset and index_file<idx_train: # we add labels that are not seen yet
                for i in label:
                    if i not in label_dict:
                        label_dict[i] = cur_label
                        label_array.append(i)
                        cur_label += 1

            if index_file < idx_train:
                train_files.append((data_file, label))


    all_label_dict = deepcopy(label_dict)
    all_label_array = deepcopy(label_array)

    # valid and test
    with open(data_dir + 'Train-B/' + gtdir, "r") as f_train:  # encoding="ISO-8859-1"
        for line in f_train.readlines():

            if line is "\n":
                continue

            data = line.replace('\n', '').replace('\r', '').split(' ')
            data_file = data_dir + 'Train-B/img/' + data[0] + ".tif"
            label = data[1:]
            if label[-1] == '\n':
                label = label[:-1]
            index_file = int(data[0].split('_')[0])
            # validation and test cases: labels are added in all_label objects
            if index_file >= idx_train:
                for lab in label:
                    if lab not in all_label_dict:
                        print("new label:", lab)
                        all_label_dict[lab] = cur_label
                        all_label_array.append(lab)
                        cur_label += 1

                if index_file < (idx_train + idx_val):
                    valid_files.append((data_file, label))
                else:
                    test_files.append((data_file, label))

            # if index_file < idx_train:
            #     train_files.append((data_file, label))
            # elif index_file < (idx_train + idx_val):
            #     valid_files.append((data_file, label))
            # else:
            #     test_files.append((data_file, label))


    gtdir = "idf/trackA-GroundTruthAscii.txt"
    # validation set
    with open(data_dir + 'Train-A/' + gtdir, "r") as f_valid:
        for line in f_valid.readlines():

            if line is "\n":
                continue

            data = line.replace('\n', '').replace('\r', '').split(' ')
            label = data[1:]
            data_file = data_dir + 'Train-A/img/' + data[0] + ".tif"
            validA_files.append((data_file, label))

            for lab in label:
                if lab not in all_label_dict:
                    print("new label:", lab)
                    all_label_dict[lab] = cur_label
                    all_label_array.append(lab)
                    cur_label += 1


    shuffle(train_files)
    shuffle(test_files)
    shuffle(valid_files)
    shuffle(validA_files)

    dataset = DatasetBatch(output_path)

    dataset.new_dataset(label_array=label_array, label_dict=label_dict, padding=padding, sep=sep, all_labels=all_label_array, all_label_dict=all_label_dict)

    try:
        os.mkdir(dataset._path + "/VALID_A/")
    except OSError:
        print("Attention le dossier existe deja!")
        pass

    def __prepare_set(file_list, index, size, padding, norm):
        x = []
        y = []
        x_length = []
        y_length = []
        try:
            for j in range(size):
                true_label = []
                output_path, label = file_list[index + j]
                for i in label:
                    true_label.append(all_label_dict[i])
                image = Image.open(output_path)
                image = image.convert('L')
                width, _ = image.size
                image = image.resize((width, norm), Image.ANTIALIAS)
                ##### METTRE HOG
                image = np.array(image)
                hogfeats = HoGFeatures(image, [8,64])
                image = hogfeats
                #(width, height) = image.size
                #imc_map = np.array(list(image.getdata()))
                #image = imc_map.reshape((height, width))

                for preproc in preprocessings_data:
                    image, true_label = preproc(image, true_label)
                seq_len = image.shape[0]
                label_len = len(true_label)

                x.append(image)
                y.append(true_label)
                x_length.append(seq_len)
                y_length.append(label_len)
        except IndexError:
            print("Ignoring last batch...")

        for preproc_batch in preprocessings_batch:
            x, y, x_length, y_length, padding = preproc_batch(x, y, x_length, y_length, padding)

        x = sequence.pad_sequences(x, value=float(padding), dtype='float32', padding="post")
        y = sequence.pad_sequences(y, value=float(len(label_array)), dtype='float32', padding="post")

        return x, y, np.asarray(x_length), np.asarray(y_length)


    print("save train...")
    for i in range(0, len(train_files), train_size):
        dataset.save_train_batch(__prepare_set(train_files, i, train_size, padding, norm))

    print("save valid...")
    for i in range(0, len(valid_files), valid_size):
        dataset.save_valid_batch(__prepare_set(valid_files, i, test_size, padding, norm))

    print("save test...")
    for i in range(0, len(test_files), test_size):
        dataset.save_test_batch(__prepare_set(test_files, i, valid_size, padding, norm))

    print("save valid 2...")
    for i in range(0, len(validA_files), custom_size):
        index_batch = i/custom_size
        dataset.save_custom_batch(__prepare_set(validA_files, i, valid_size, padding, norm), 'VALID_A', index_batch)

    dataset.save()


if __name__ == '__main__':

    opts, _ = getopt.getopt(sys.argv[1:], '', ["input=", "out=", "norm="])
    input_path = output_path = None
    input_path = "D:/Read2017/" #os.path.join(os.path.dirname(__file__), "datasets/READ/")
    #os.path.join(os.path.dirname(__file__), "datasets/READ/batch/")
    nb_col = 2
    padding = -1.
    train_size = 64
    test_size = 64
    valid_size = 64
    norm = 64
    reduce_charset = True
    if reduce_charset:
        str_reduce = 'reduce'
    else:
        str_reduce = ''
    output_path = "../../datasets/READ/batch" + str_reduce + "HOG" + str(nb_col) + "col" + str(norm)

    for opt, arg in opts:
        if '--input' in opt:
            input_path = arg
        elif '--out' in opt:
            output_path = arg
        elif '--norm' in opt:
            norm = int(arg)


    if not input_path or not output_path:
        exit()

    # standardize later
    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_col, padding=padding)]

    produce_dataset(input_path, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=preprocessing_data, preprocessings_batch=[],
                    padding=padding, norm=norm, reduce_charset=reduce_charset)
class DatasetInfo:
    def __init__(self,
                 nbbatch_train, nbbatch_test,
                 nbbatch_valid, nb_features, padding,
                 label_array, label_dict, sep=' ', all_labels=None, all_label_dict=None, window_size=None,
                 nb_cols=None, train_files=[], valid_files=[], test_files=[]):

        self.nbbatch_train = nbbatch_train
        self.nbbatch_test = nbbatch_test
        self.nbbatch_valid = nbbatch_valid

        self.nb_features = nb_features
        self.padding = padding
        self.label_array = label_array
        self.label_dict = label_dict
        self.sep = sep
        self.all_labels = all_labels
        self.all_label_dict = all_label_dict if all_label_dict else label_dict
        self.window_size = window_size
        self.train_files = train_files
        self.valid_files = valid_files
        self.test_files = test_files

        self.nb_cols = nb_cols

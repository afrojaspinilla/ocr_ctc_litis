from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.ImagesFromBaseline import ImagesFromBaseline
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequenceFromBaseline import To2DSequenceFromBaseline
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import shutil

def add_to_train(dir_data, dir_im_lines, input_path, sup_inf='1'):

    str_data = ''
    for file_data in os.listdir(dir_data):
        with open(dir_data +file_data, 'r') as fr:
            for line in fr.readlines():
                sline = line.split(' ')
                namef = sline[0].split('/')[-1]
                label = ' '.join(sline[3:])
                name_im = namef.split('.')[0] + '.jpeg'
                final_namef = sup_inf + name_im
                str_data += final_namef + ' ' + label
                shutil.copy(dir_im_lines + name_im, input_path + 'TRAIN/' + final_namef)

    shutil.copy(input_path + 'train_reduced_charset.txt', input_path + 'train' + sup_inf + '_reduced_charset.txt')
    with open(input_path + 'train' + sup_inf + '_reduced_charset.txt', 'a') as fw:
        fw.write(str_data)




def extract_baselines(txt_path, input_path, files_info_lab={"train": "train_reduced_charset.txt", "valid": "valid_reduced_charset.txt",
                                                          "test": "test_reduced_charset.txt"}):#, ns = {"ns": "http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15"}):
    """ """
    baselines = []
    with open(input_path + 'baselines.txt', "w") as file_baselines:
        for name_files in [files_info_lab["train"], files_info_lab["valid"], files_info_lab["test"]]:
            with open(input_path + name_files, "r") as my_file:
                for line in my_file.readlines():
                    if line is "\n":
                        continue

                    file_name = line.replace("\n", "").split(" ")[0]
                    page_name = '_'.join(file_name.split("_")[:-1]) + '.txt'
                    num_line = int(file_name.split("_")[-1].split('.')[0])

                    if "Mom" in file_name:
                        with open(txt_path[0] + page_name, "r") as txt_bl:
                            all_bl = txt_bl.readlines()

                        related_bl = all_bl[num_line].split(';')
                        baseline = [(float(elmt.split(',')[0]), float(elmt.split(',')[1])) for elmt in related_bl]
                        str_baseline = all_bl[num_line]
                    else:
                        baseline = []
                        str_baseline = ""

                    baselines.append(baseline)
                    file_baselines.write(file_name + str_baseline + '\n')
    
    return baselines


def main(input_path, dir_origin="", dir_line_images="",
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, \
         window_structure='vertical', shuff=True, padding=-1, splitter_label="", train_file=None):

    files_info_lab = {"train": "train_reduced_charset.txt", "valid": "valid_reduced_charset.txt",
                      "test": "test2iter_reduce_charset.txt"}
    if train_file:
        files_info_lab['train'] = train_file

    if ImageFrombaseline:
        shuff = False
        if os.path.exists(input_path + 'baselines.txt'):
            with open(input_path + 'baselines.txt', "r") as file_baselines:
                baselines = []
                for line in file_baselines.readlines():
                    if line is "\n":
                        continue
                    ls = line.split(" ")
                    for elmt in ls[1:]:
                        x, y = elmt.split(";")
                        baselines.append((float(x), float(y)))
        else:
            baselines = extract_baselines(dir_origin, input_path)

    str_info = ''
    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        if ImageFrombaseline:
            preprocessing_batch.append(To2DSequenceFromBaseline(baselines, h_norm, nb_cols=nb_cols, stride=stride, pad=conv_pad, win_structure=window_structure))
            h_norm = 0
            str_info += '_IFB' + window_structure
        else:
            preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + "READ-PLAIRconv_" + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride) + str_info

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        preprocessing_batch = []
        if ImageFrombaseline:
            preprocessing_batch.append(ImagesFromBaseline(baselines, h_norm))
            h_norm = 0
        output_path = input_path + "READ-PLAIR_" + str(h_norm) + "_" + str(nb_cols)

    producer = Producer(input_path, output_path,
                        norm=norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=True,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        shuff=shuff
                        ).produce_dataset(splitter_label=splitter_label, sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TEST/"},
                                          files_info_lab=files_info_lab, removed_empty=removed_empty)



if __name__ == '__main__':

    nb_cols = 16
    stride = 4
    padding = -1.
    norm = 64
    keep_ratio = True
    conv = True
    conv_pad = True
    ImageFrombaseline = False
    window_structure = 'horizontal'
    splitter_label = ""
    removed_empty=False

    input_path = "../../datasets/mixREADFCN-PlaIR/"
    txt_path = ["../../../../../code/segmenteurFCN/xml/PlaIR/Mom0.8/",""]
    dir_data = "../../../../../data/PlaIR/out_lines_matched_predictions1conv_0.8_baseline/"
    dir_im_line = "../../../../../data/PlaIR/lines_segm0.8Mom_v2/"

    sup_inf = '2'
    train_file = 'train' + sup_inf + 'iter_reduced_charset.txt'
    #add_to_train(dir_data, dir_im_line, input_path, sup_inf=sup_inf)

    shuff = False

    main(input_path, dir_origin=txt_path, dir_line_images='', h_norm=norm, nb_cols=nb_cols, stride=stride, \
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
         window_structure=window_structure, shuff=shuff, padding=padding, splitter_label=splitter_label,\
         train_file = train_file, removed_empty=removed_empty)


from PIL import Image
import tensorflow as tf
import numpy as np
import os

def read_coords_inXML(xml_file, line_ref):
    """ READ an xml as a txt 
    return coords of the given line reference (the index of the line one wants to get the bounding box)
    
    Return the a list of extrema points of the bounding box required with line_ref
    """

    count = 0
    coords = []
    with open(xml_file) as f:
        for line in f.readlines():
            if 'Coords' in line:
                if count == line_ref:
                    list_coords = line.split("\"")[1].split(' ')
                    for elmt in list_coords:
                        current_point = elmt.split(',')
                        coords.append(([int(pos) for pos in current_point]))
                    count += 1
                else:
                    count += 1

    bounds = []
    list_x = [cd[0] for cd in coords]
    list_y = [cd[1] for cd in coords]
    bounds.append(min(list_x))
    bounds.append(min(list_y))
    bounds.append(max(list_x))
    bounds.append(max(list_y))
    return bounds


def read_unicode_inXML(xml_file, line_ref):
    """ READ an xml as a txt 
    return the text of the given line reference
    
    list_unicode contains the text in xml_file at the 'line_ref'th line containing a Unicode. """

    list_unicode = []
    count = 0
    with open(xml_file) as f:
        for line in f.readlines():
            if 'Unicode' in line:
                if count == line_ref:
                    str_text = line.split("code>")[1].split("</")[0]
                    list_unicode.append(str_text)
                    count += 1
                else:
                    count += 1

    return list_unicode


def load_img_from_list_file(it_data, list_files, norm=-1):
    """ LOAD images from a list of path+name_file
        Resize image according to norm (if norm =-1, height is kept, otherwise this is norm)
        
        output = image as a np.array
    """

    output_path, label = list_files[it_data]
    image = Image.open(output_path)
    image = image.convert('L')
    width, _ = image.size
    if norm==-1:
        image = image.resize((width, norm), Image.ANTIALIAS)
    image = np.array(image)

    return image


def get_sparse_tensor(seq_labels, dtype=tf.int32):
    """Create a sparse representation of x.
    Args:
        seq_labels: a list of lists of type dtype where each element is a sequence (e.g. of labels)
    Returns:
        A tuple with (indices, values, shape)
    """
    indices = []
    values = []
    max_len = 0

    for n, seq in enumerate(seq_labels):
        if len(seq) > max_len:
            max_len = len(seq)
        for i in range(len(seq)):
            indices.append([n, i])
        # indices.extend(zip([n]*len(seq), range(len(seq)))) #
        values.extend(seq)  # every sequences are concatenate in a unique vector of values

    indices = np.asarray(indices, dtype=tf.int64)
    values = np.asarray(values, dtype=dtype)
    shape = np.asarray([len(seq_labels), max_len], dtype=tf.int64)  # asarray(indices).max(0)[1]+1], dtype=int64)

    return indices, values, shape


def save_transcription(name_file, list_trans, list_index=None, type_list='char'):
    """ save transcription (str in the list list_trans) in name_file """

    with open(name_file, 'w') as f:

        for i in range(len(list_trans)):
            if list_index:
                if 'word' in type_list:
                    f.write(str(list_index[i]) + ' ' + ' '.join(list_trans[i]) + '\n')
                else:
                    f.write(str(list_index[i]) + ' ' + ''.join(list_trans[i]) + '\n')
            else:
                if 'word' in type_list:
                    f.write(' '.join(list_trans[i]) + '\n')
                else:
                    f.write(''.join(list_trans[i]) + '\n')


def load_transcription(name_file):
    """ save transcription (str in the list list_trans) in name_file """

    list_index = []
    list_gt = []

    with open(name_file, 'r') as f:
        lines = f.readlines()

    for line in lines:
        line = line.replace('\n', '')
        split_line = line.split(' ')
        line_index = split_line[0]

        list_index.append(line_index)
        list_gt.append(' '.join(split_line[1:]))

    return list_gt, list_index


def replace_char_in_a_list_of_list(list_elmt, char_to_replace=' ', new_char='_'):
    """ """
    list_out = []
    for k in range(len(list_elmt)):
        list_out.append(new_char.join(list_elmt[k].split(char_to_replace)))

    return list_out


def replace_char_in_a_list_of_str(list_elmt, char_to_replace=' ', new_char='_'):
    """ """
    list_out = []
    for k in range(len(list_elmt)):
        list_out.append(list_elmt[k].replace(char_to_replace, new_char))

    return list_out

def join_elmt_in_a_list(list_elmt, char_join=' '):

    list_out = []
    for k in range(len(list_elmt)):
        list_out.append(char_join.join(list_elmt[k]))

    return list_out



def get_line_info_fromXML(input_xml, **kwargs):
    """ GET LINE INFO : extract information about bouding box coordinates and baselines from xml at PAGE format """

    from lxml import etree


    indiv_points = kwargs.get('indiv_points', False)
    ns = kwargs.get('ns', False)

    tree = etree.parse(input_xml)

    lines = []
    str_ns = "" if not ns else "ns:"

    for line in tree.findall(".//" + str_ns + "TextLine", namespaces=ns):

        if indiv_points:
            mbaseline = []
        else:
            mbaseline = line.find(".//" + str_ns + "Baseline", namespaces=ns)
            if mbaseline is None:
                lines.append(None)
                continue
            mbaseline = mbaseline.get("points")
            mbaseline = mbaseline.replace(",", " ").replace(";", " ").split(" ")
            mbaseline = list(map(lambda x: float(x), mbaseline))

        baseline = []

        for i in range(0, len(mbaseline) - 1, 2):
            baseline.append((mbaseline[i], mbaseline[i + 1]))

        if baseline == [] or baseline == ['']:
            baseline = None

        if indiv_points:
            coords = []
            mccord = line.find(".//" + str_ns + "Coords", namespaces=ns)
            for point in mccord.findall(".//" + str_ns + "Point", namespaces=ns):
                x, y = point.get("x"), point.get("y")
                coords.append(float(x))
                coords.append(float(y))
        else:
            coords = line.find(".//" + str_ns + "Coords", namespaces=ns).get("points")
            coords = coords.replace(",", " ").split(" ")
            coords = list(map(lambda x: float(x), coords))

        minx = minx2 = (100000, 0)
        maxx = maxx2 = (0, 0)

        miny = (0, 100000)
        maxy = (0, 0)
        #print(coords)
        for i in range(0, len(coords), 2):

            if coords[i] <= minx[0]:
                minx2 = minx
                minx = (coords[i], coords[i + 1])
            elif coords[i] <= minx2[0]:
                minx2 = (coords[i], coords[i + 1])

            if coords[i] >= maxx[0]:
                maxx2 = maxx
                maxx = (coords[i], coords[i + 1])
            elif coords[i] >= maxx2[0]:
                maxx2 = (coords[i], coords[i + 1])

            if coords[i+1] <= miny[1]:
                miny = (coords[i], coords[i + 1])

            if coords[i+1] >= maxy[1]:
                maxy = (coords[i], coords[i + 1])
        #print(minx, minx2, maxx, maxx2, miny, maxy)
        if minx2[1] > 0 and maxx2[1] > 0:

            if minx[1] > minx2[1]:

                left_edge_top_x = minx[0]
                left_edge_top_y = minx[1]
                left_edge_bottom_x = minx2[0]
                left_edge_bottom_y = minx2[1]
            else:
                left_edge_top_x = minx2[0]
                left_edge_top_y = minx2[1]
                left_edge_bottom_x = minx[0]
                left_edge_bottom_y = minx[1]

            if maxx[1] > maxx2[1]:
                right_edge_top_x = maxx[0]
                right_edge_top_y = maxx[1]
                right_edge_bottom_x = maxx2[0]
                right_edge_bottom_y = maxx2[1]

            else:
                right_edge_top_x = maxx2[0]
                right_edge_top_y = maxx2[1]
                right_edge_bottom_x = maxx[0]
                right_edge_bottom_y = maxx[1]

        else:
            left_edge_top_x = minx[0]
            left_edge_top_y = minx[1]
            left_edge_bottom_x = minx[0]
            left_edge_bottom_y = minx[1]

            right_edge_top_x = maxx[0]
            right_edge_top_y = maxx[1]
            right_edge_bottom_x = maxx[0]
            right_edge_bottom_y = maxx[1]

        lines.append({

            "baseline": baseline,
            "minx": minx[0],
            "maxx": maxx[0],
            "miny": miny[1],
            "maxy": maxy[1],

            "left_edge_top_x": left_edge_top_x,
            "left_edge_bottom_x": left_edge_bottom_x,
            "left_edge_top_y": left_edge_top_y,
            "left_edge_bottom_y": left_edge_bottom_y,

            "right_edge_top_x": right_edge_top_x,
            "right_edge_bottom_x": right_edge_bottom_x,
            "right_edge_top_y": right_edge_top_y,
            "right_edge_bottom_y": right_edge_bottom_y
        })

    return lines



def get_line_info_fromXMLasTXT(input_xml, splitter_pointsBaseline=";", splitter_pointsCoord=" ", splitter_points=",", **kwargs):
    """ GET LINE INFO : extract information about bouding box coordinates and baselines from xml at PAGE format """

    #indiv_points = kwargs.get('indiv_points', False)

    lines = []

    with open(input_xml, 'r') as f:
        all_lines = f.readlines()

    blines = []
    xcoord = []
    transcriptions = []

    for line in all_lines:
        if "<Baseline" in line:
            points = line.split("\"")[1]
            if len(points)==0:
                blines.append([])
            else:
                pline = []
                pts = points.split(splitter_pointsBaseline)
                for pt in pts:
                    pline.append((float(pt.split(splitter_points)[0]), float(pt.split(splitter_points)[1])))
                blines.append(pline)
        elif "<Coords" in line:
            points = line.split("\"")[1]
            if len(points) == 0:
                xcoord.append([])
            else:
                pline = []
                pts = points.split(splitter_pointsCoord)
                for pt in pts:
                    pline += [float(pt.split(splitter_points)[0]), float(pt.split(splitter_points)[1])]
                xcoord.append(pline)
        elif "<Unicode" in line:
            trans = line.split("<Unicode>")[-1].split("</Unicode>")
            if len(trans)>1:
                trans = trans[0]
            elif len(trans)==0:
                trans = ""
            transcriptions.append(trans)

    if len(xcoord)!=len(blines) and len(blines)!=0:
        print("Problem when reading the xml file")
    if len(xcoord)!=len(transcriptions) and len(transcriptions)!=0:
        print("Problem when reading the xml file")

    if len(transcriptions)==0:
        for i in range(len(xcoord)):
            transcriptions.append("")

    for i in range(len(xcoord)):

        baseline = []

        if blines[i] == [] or blines[i] == ['']:
            baseline = None
        else:
            baseline = blines[i]

        coords = xcoord[i]

        minx = minx2 = (100000, 0)
        maxx = maxx2 = (0, 0)

        miny = (0, 100000)
        maxy = (0, 0)
        #print(coords)
        for i in range(0, len(coords), 2):

            if coords[i] <= minx[0]:
                minx2 = minx
                minx = (coords[i], coords[i + 1])
            elif coords[i] <= minx2[0]:
                minx2 = (coords[i], coords[i + 1])

            if coords[i] >= maxx[0]:
                maxx2 = maxx
                maxx = (coords[i], coords[i + 1])
            elif coords[i] >= maxx2[0]:
                maxx2 = (coords[i], coords[i + 1])

            if coords[i+1] <= miny[1]:
                miny = (coords[i], coords[i + 1])

            if coords[i+1] >= maxy[1]:
                maxy = (coords[i], coords[i + 1])
        #print(minx, minx2, maxx, maxx2, miny, maxy)
        if minx2[1] > 0 and maxx2[1] > 0:

            if minx[1] > minx2[1]:

                left_edge_top_x = minx[0]
                left_edge_top_y = minx[1]
                left_edge_bottom_x = minx2[0]
                left_edge_bottom_y = minx2[1]
            else:
                left_edge_top_x = minx2[0]
                left_edge_top_y = minx2[1]
                left_edge_bottom_x = minx[0]
                left_edge_bottom_y = minx[1]

            if maxx[1] > maxx2[1]:
                right_edge_top_x = maxx[0]
                right_edge_top_y = maxx[1]
                right_edge_bottom_x = maxx2[0]
                right_edge_bottom_y = maxx2[1]

            else:
                right_edge_top_x = maxx2[0]
                right_edge_top_y = maxx2[1]
                right_edge_bottom_x = maxx[0]
                right_edge_bottom_y = maxx[1]

        else:
            left_edge_top_x = minx[0]
            left_edge_top_y = minx[1]
            left_edge_bottom_x = minx[0]
            left_edge_bottom_y = minx[1]

            right_edge_top_x = maxx[0]
            right_edge_top_y = maxx[1]
            right_edge_bottom_x = maxx[0]
            right_edge_bottom_y = maxx[1]

        lines.append({

            "baseline": baseline,
            "minx": minx[0],
            "maxx": maxx[0],
            "miny": miny[1],
            "maxy": maxy[1],

            "left_edge_top_x": left_edge_top_x,
            "left_edge_bottom_x": left_edge_bottom_x,
            "left_edge_top_y": left_edge_top_y,
            "left_edge_bottom_y": left_edge_bottom_y,

            "right_edge_top_x": right_edge_top_x,
            "right_edge_bottom_x": right_edge_bottom_x,
            "right_edge_top_y": right_edge_top_y,
            "right_edge_bottom_y": right_edge_bottom_y
        })

    return lines, transcriptions


def lineImageFromPage(page_image, line_info, epsilon_x=0.1, epsilon_y_under=0.5, epsilon_y_upper=0.3):

    min_x = min(line_info['left_edge_top_x'], line_info['left_edge_bottom_x'])
    max_x = max(line_info['right_edge_top_x'], line_info['right_edge_bottom_x'])
    min_y = min(line_info['left_edge_bottom_y'], line_info['right_edge_bottom_y'])
    max_y = max(line_info['left_edge_top_y'], line_info['right_edge_top_y'])

    vertical_len = max_y - min_y
    horizontal_len = max_x - min_x
    width, height = page_image.size
    bounding_box = (max(min_x-epsilon_x*horizontal_len,0), max(min_y-epsilon_y_under*vertical_len, 0),
                    min(max_x+epsilon_x*horizontal_len, width), min(max_y+epsilon_y_upper*vertical_len,height))
    line_image = page_image.crop(bounding_box)
    #line_image.show()

    return line_image



def get_lattest_weights(log_path):

    if os.path.exists(log_path):
        fileNames = [fn for fn in os.listdir(log_path) if fn[-4:] == 'hdf5']
        if len(fileNames)==0:
            print("Warning: no weights to load")
        else:
            fileNames = sorted(fileNames, key=lambda y: int(y.rsplit('.')[-3].split('-')[0]) if not '-inf' in y else int(y.rsplit('.')[-2].split('-')[0])) #int(y.rsplit('.')[-2]))
            return fileNames[-1]
    else:
        print("Warning: no weights to load")

    return ''


def get_weights(log_path, it_to_load):

    if os.path.exists(log_path):
        fileNames = [fn for fn in os.listdir(log_path) if fn[-4:] == 'hdf5'] # os.path.join(os.path.dirname(__file__),
        if len(fileNames)==0:
            print("Warning: no weights to load")
        else:
            not_seen = True
            i=0
            while not_seen and i<len(fileNames):
                try:
                    ref = int(fileNames[i].rsplit('.')[-3].split('-')[0]) if not '-inf' in fileNames[i] else int(fileNames[i].rsplit('.')[-2].split('-')[0]) #int(fileNames[i].rsplit('.')[-3].split('-')[0]) #int(fileNames[i].rsplit('.')[-2])
                    if ref == it_to_load:
                        not_seen = False
                    else:
                        i += 1
                except:
                    print(fileNames[i], ': number of file can not be read.')
                    i += 1
            if not_seen:
                print("weight not found. Get the lattest weight.")
                return get_lattest_weights(log_path)
            else:
                return fileNames[i]
    else:
        print("Warning: no weights to load")

    return ''


def order_weights(log_path, list_it=None):

    if os.path.exists(log_path):

        filesInfo = [(int(fn.rsplit('.')[-3].split('-')[0]), fn) if not '-inf.' in fn else (int(fn.rsplit('.')[-2].split('-')[0]), fn) for fn in
            os.listdir(log_path) if fn[-4:] == 'hdf5']

        idx_w, filesNames = zip(*sorted(filesInfo))
        max_it = int(np.max(idx_w))

        list_idx = []
        if list_it is not None:
            for k in list_it:
                if k in idx_w:
                    list_idx.append(idx_w.index(k))
                elif k<0 and (max_it + k + 1) in idx_w:
                    list_idx.append(idx_w.index(max_it + k + 1))
                else:
                    print("Training iteration {} is not in the list of weights that have been saved.".format(k))
        else:
            for k in idx_w:
                if k>=0:
                    list_idx.append(idx_w.index(k))
                else:
                    list_idx.append(idx_w.index(max_it + k + 1))
        filesNames = [filesNames[k] for k in list_idx]
        out_idx = [idx_w[k] for k in list_idx]
        return filesNames, out_idx
    else:
        print("Warning: no weights to load")

    return [], []


def get_index_from_ref(ref, name_files):

    list_idx = []
    for i,f in enumerate(name_files):
        if ref in f:
            list_idx.append(i)

    return list_idx
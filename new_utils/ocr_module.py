from ocr_ctc.dataset_manager.new_batch.DatasetHBDEX import DatasetForOCR
from ocr_ctc.main_evalRNN_without_h5 import save_pred, save_proba, save_proba_2, eval_net_generator
from new_utils.language_module import get_LM_prediction_from_proba_file
from new_utils.settings import now
import new_utils.settings as settings
import numpy as np
import os
from time import time
import logging
logger = logging.getLogger(__name__)

def greedy_decode(probas, charset):
    greed_ctc = []
    for k in probas:
        greed_ctc.append(np.argmax(k))
    last = 10000
    greed = []
    for k in greed_ctc:
        if k != last and k != 0:
            greed.append(k - 1)
        last = k
    ref = charset
    pred = greed
    nb_ref = len(ref)
    r = ""
    for j in range(len(pred)):
        c = int(pred[j])
        if (c>=0 and c<nb_ref):
            r += ref[c]
    return r

def get_prediction_from_proba_string(proba_string):
    output_pred = []
    for one_image in proba_string.split("]")[:-1]:
        one_image = one_image.strip("\n")
        splito = one_image.split("[",1)
        file_name = splito[0].strip("\n").strip()
        trails = splito[1].strip("\n").strip()
        rows = []
        if len(trails) == 0:
            continue
        for trail in trails.split("\n"):
            probas = trail.strip()
            temp = []
            for proba in probas.split(' '):
                temp.append(float(proba))
            rows.append(temp)
        rows = np.asarray(rows)
        out = greedy_decode(rows, settings.LABEL_LIST).replace('<sp>',' ')
        output_pred.append([file_name, out])
    return output_pred

def column_ocr(network, column_images_for_ocr, column_number, column_title, output_folder, page_name, fst_folder="fst_folder", use_grammar=False):
    """
    Main function for the OCR, it will take a list of images and then it will be
    treated to get a Probability_file which contanins the prediction. Then is
    decoded using the greedy decoder in order to get the Raw_OCR
    Then a language model will be used according to the name of the title of the
    column, or the number of the column
    """
    settings.BATCH_SIZE = settings.ORIGINAL_BATCH_SIZE
    print('column_images_for_ocr_len:',len(column_images_for_ocr))
    if len(column_images_for_ocr) == 1:
        column_images_for_ocr.append([column_images_for_ocr[0][0]+'extra',column_images_for_ocr[0][1]])
    if len(column_images_for_ocr) < settings.BATCH_SIZE:
        settings.BATCH_SIZE = len(column_images_for_ocr)
    logger.info(now()+'preparing the image generator for input')
    dataset = DatasetForOCR(settings, column_images_for_ocr)
    logger.info(now()+'performing eval_net_generator')
    out_eval = eval_net_generator(network, dataset.get_test_generator(), int(dataset.test_size / settings.BATCH_SIZE), get_pred=False, get_eval=None, \
                   get_prob=True, get_loss=False, file_to_eval=None, info_files=dataset.test_labels_map)
    proba_file = page_name+'_column-'+str(column_number)+'_proba.txt'
    out_file_proba = os.path.join(output_folder,page_name,proba_file)
    logger.info(now()+'saving the proba_file in the path: '+str(out_file_proba))
    str_out = save_proba_2(out_file_proba, out_eval['proba'], settings.LABEL_LIST, name_data=dataset.test_list, first_blank=True, duplicate_labels=settings.NEW_LABELS_DICT, save_whole_document=False, \
                   log_prob=True, save_format='kaldi', step_range=1, file_charset=None, eps=1e-100)
    logger.info(now()+'decoding RAW OUTPUT')
    output_pred = get_prediction_from_proba_string(str_out)
    # for name_img, predo in output_pred:
    #     if 'True' in name_img:
    #         print(predo)
    s_time = time()
    if use_grammar:
        logger.info(now()+'decoding LEXICAL OUTPUT')
        output_proba = get_LM_prediction_from_proba_file(out_file_proba, column_number, column_title, page_name, fst_folder)
    else:
        output_proba = []
    print("get_prediction_kaldi_time:",time()-s_time)

    for i in range(0, len(output_pred)):
        output_pred[i][1] = output_pred[i][1].replace('_','').replace('~','')

    for i in range(0, len(output_proba)):
        output_proba[i][1] = output_proba[i][1].replace('_','').replace('~','')

    print('========= RAW PRED ===========')
    print(len(output_pred))
    print('========= PROBA PRED ===========')
    print(len(output_proba))

    [print(x) for x in output_proba]

    return output_pred, output_proba

import numpy as np
from sklearn import preprocessing

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class Pad(ImagePreprocessing):
    """
    Revert the pixel values (px = 255-px)
    """

    def __init__(self, max_width=0):
        self.max_width = max_width

    def preprocessing(self, image):
        temp_h, temp_w = image.shape
        img = np.pad(image, [(0, 0), (0, self.max_width - temp_w)], mode='constant', constant_values=0)

        return img.astype('float32')

    def __str__(self):
        return "Revert the pixel values"
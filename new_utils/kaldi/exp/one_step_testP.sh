#!/bin/bash
#. path.sh

stage=0 # 0: all
	# 1: decoding

	echo "The number of arguments is: $#"
	a=${@}
	echo "The total length of all arguments is: ${#a}: "
	count=0
	for var in "$@"
	do
	    echo "The length of argument '$var' is: ${#var}"
	    (( count++ ))
	    (( accum += ${#var} ))
	done
	echo "The counted number of arguments is: $count"
	echo "The accumulated length of all arguments is: $accum"



if [ "$#" -ne 5 ]; then
	echo "Usage: one_step_testP.sh 1 2 3 4 5";
	exit 1;
fi;

# Retrieve script parameters
proba_file=$1
page_folder=$2
page_name=$3
labels_file=$4
fst_file=$5

echo $labels_file


OCR=$1  # test directory
suffix=$OCR #

# initial decoding parameters

init_acwt=0.1 # by gaving small confidence value for the optical model parameter we gave highest confidence value to the Grammar (lexicon verification) taking into account that grammar/language model scale parameter = 1/optical model scale parameter
#init_acwt=0.005
beam=4	# default 16 parameter for the beam search
# Locate useful directories

data_dir=../lines
models_dir=../FST
lm_dir=$models_dir/fst/


#echo =====================================================================
#echo "                Network Forwarding with $data_dir                  "
#echo =====================================================================

# Make test directory
dir=testF_$suffix/${OCR}

#mkdir -p $dir

# Decode
if [ $stage -le 1 ]; then

	\time decode-faster --beam=$beam --acoustic-scale=$init_acwt --word-symbol-table=$labels_file --allow-partial=true $fst_file ark:$proba_file ark,t:$page_folder/test_out.tra

fi;


# Decode output
if [ $stage -le 2 ]; then
	int2symp_script="new_utils/kaldi/exp/int2sym.pl"
	cat $page_folder/test_out.tra | $int2symp_script -f 2- $labels_file | sed "s/|/<sp>/g"  > $page_folder/test_out.chars
	cut -d" " -f1 $page_folder/test_out.chars > $page_folder/utts
	cut -d" " -f1 --complement  $page_folder/test_out.chars | sed -e "s/ //g" -e "s/<sp>/ /g" > $page_folder/words
	paste $page_folder/utts $page_folder/words > $page_folder/test_out.words
fi;

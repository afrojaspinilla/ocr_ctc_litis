# encoding=utf-8
import keras
import numpy as np
from misc.decoder import decoding_with_reference, decodingLen_with_reference

from utils.utils_analysis import show_edit_distance, tb_log_print


class CTCPrint(keras.callbacks.Callback):
    """
    Callback that print the model performance on the train, valid and/or test set.
    Compute Label Error Rate and Sequence Error Rate
    """

    def __init__(self, ctc_model, dataset, eval_valid=True, eval_train=False, eval_test=False, batch_size=32,\
                 num_display_words=6, test_steps=1, nb_batch_train=-1, nb_batch_val=-1, nb_batch_test=-1):
        self.dataset = dataset
        self.num_display_words = num_display_words
        self.ctc_model = ctc_model
        self.test_steps = test_steps
        self.eval_train = eval_train
        self.eval_valid = eval_valid
        self.eval_test = eval_test

        self.nb_batch_train = nb_batch_train if nb_batch_train != -1 else int(self.dataset.nbbatch_train / batch_size)
        self.nb_batch_val = nb_batch_val if nb_batch_val != -1 else int(self.dataset.nbbatch_valid / batch_size)
        self.nb_batch_test = nb_batch_test if nb_batch_test != -1 else int(self.dataset.nbbatch_test / batch_size)

    def on_epoch_end(self, epoch, logs={}):

        # evaluation on the valid set
        if self.eval_valid:
            if (epoch % self.test_steps == 0) or (epoch == 1 or epoch == self.max_epoch):
                ler_list, ser = self.ctc_model.evaluate_generator(self.dataset.get_valid_generator(), self.nb_batch_val)
                ler = np.mean(ler_list)

                print("epoch ", epoch, "valid CER = ", ler)
                print("epoch ", epoch, "valid SER = ", ser)

        # evaluation on the training set
        if self.eval_train:
            if (epoch % self.test_steps == 0) or (epoch == 1 or epoch == self.max_epoch):
                ler_list, ser = self.ctc_model.evaluate_generator(self.dataset.get_train_generator(), self.nb_batch_train)
                ler = np.mean(ler_list)

                print("epoch ", epoch, "train CER = ", ler)
                print("epoch ", epoch, "train SER = ", ser)

        # evaluation on the test set
        if self.eval_test:
            if (epoch % self.test_steps == 0) or (epoch == 1 or epoch == self.max_epoch):
                ler_list, ser = self.ctc_model.evaluate_generator(self.dataset.get_test_generator(), self.nb_batch_test)
                ler = np.mean(ler_list)

                print("epoch ", epoch, "test CER = ", ler)
                print("epoch ", epoch, "test SER = ", ser)
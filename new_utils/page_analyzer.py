from new_utils.ocr_module import column_ocr
from new_utils.settings import now
from operator import itemgetter
from scipy import stats
from scipy.signal import find_peaks
from PIL import Image, ImageFilter
import numpy as np
import math
import json
import os
import re
import random
random.seed(42)
import logging
logger = logging.getLogger(__name__)


def create_folder(path):
    """
    Creates a folder in the given path, if it exists, nothing is done
    """
    if not os.path.exists(path):
        os.mkdir(path)


def save_cropped_images_for_visualisation(list_of_cropped_images, colonne):
    output_dir = 'temp_folder/debug'
    create_folder(output_dir)
    output_dir = os.path.join(output_dir,str(colonne))
    create_folder(output_dir)
    for name, image in list_of_cropped_images:
        image.save(os.path.join(output_dir,name+'.png'))


def get_deskewing_angle(list_of_polylines):
    """
    Deskews the image from the lines in the json ir order to aline it
    returns the corrected image and the avg slope for later calculations
    """
    slopes = []
    for i in range(0,len(list_of_polylines)):
        item = list_of_polylines[i]
        polyline = item["polyline"].split(';')
        x = []
        y = []
        if len(polyline) < 3:
            continue
        for point in polyline:
            pointo = point.split(',')
            x.append(int(pointo[0]))
            y.append(int(pointo[1]))
        slope, _, _, _, _ = stats.linregress(x,y)
        slopes.append(math.degrees(math.atan(slope)))
    try:
        avg_slope = sum(slopes)/len(slopes)
    except ZeroDivisionError:
        avg_slope = 0
    return avg_slope


def rotated_polyline(polyline, avg_slope):
    """
    recalculates the polyline from the angle of slope of the page
    """
    rot_polyline = []
    origin_x = polyline[0][0]
    origin_y = polyline[0][1]
    for coord in polyline:
        rot_x = int(math.cos(-math.radians(avg_slope))*(coord[0]-origin_x) - math.sin(-math.radians(avg_slope))*(coord[1]-origin_y)) + origin_x
        rot_y = int(math.sin(-math.radians(avg_slope))*(coord[0]-origin_x) + math.cos(-math.radians(avg_slope))*(coord[1]-origin_y)) + origin_y
        rot_polyline.append((rot_x,rot_y))
    return rot_polyline


def add_slope_and_remake_polylines(all_columns, data_per_column):
    """
    Takes 2 aguments:
    data_per_column: A dict of key:ColumnNumber Value:ListOfItems
    all_columns: a dict containing the number of columns
    then it calculates the slope and makes the rotation of the polylines in
    order to have the less inclination possible
    """
    for key in all_columns:
        data_per_column[key]['slope'] = get_deskewing_angle(data_per_column[key]['lines'])
        current_slope = data_per_column[key]['slope']
        for i in range(0, len(data_per_column[key]['lines'])):
            item = data_per_column[key]['lines'][i]
            polyline = item['polyline']

            if len(polyline) < 3:
                logger.debug(now()+'polyline too short: '+str(item))
                continue
            polyline_ok = ()
            for point in item['polyline'].split(';'):
                pointo = point.split(',')
                polyline_ok = polyline_ok + ((int(pointo[0]),int(pointo[1])),)
            item['rotated_polyline'] = rotated_polyline(polyline_ok, current_slope)
            if 'list_polyline' not in item:
                item['rotated_list_polylines'] = [item['rotated_polyline']]
            elif item['list_polyline'] == [""]:
                item['rotated_list_polylines'] = [item['rotated_polyline']]
            else:
                rotated_list_polylines = []
                for temp_line in item['list_polyline']:
                    if len(temp_line) < 2:
                        continue
                    polyline_ok = ()
                    for point in temp_line.split(';'):
                        pointo = point.split(',')
                        if '' in pointo:
                            continue
                        polyline_ok = polyline_ok + ((int(pointo[0]),int(pointo[1])),)
                    polyline_ok = rotated_polyline(polyline_ok, current_slope)
                    rotated_list_polylines.append(polyline_ok)
                item['rotated_list_polylines'] = rotated_list_polylines
            data_per_column[key]['lines'][i] = item
    return data_per_column


def calculate_common_height(data_per_column):
    """
    It takes a dict of key:ColumnNumber Value:ListOfItems
    Then it searches the column with most items, and then it calculates the
    histogram of the separation between the polylineN and polylineN+1
    """
    # Getting the column with most items
    fullest_column = -1
    len_tmp = 0
    for key in data_per_column:
        len_col = len(data_per_column[key]['lines'])
        # print('key:',key,'len:',len_col)
        if len_col > len_tmp:
            len_tmp = len_col
            fullest_column = key

    # Calculating the height of the bounding box
    temp_y_list = []
    line_data_fullest_column = data_per_column[fullest_column]['lines']
    temp = {}
    for item in line_data_fullest_column:
        if 'polyline' in item:
            if item['polyline'].strip() != '':
                key = int(item['polyline'].split(';')[0].split(',')[1])
                temp[key] = item
    line_data_fullest_column = []
    for key in sorted(temp.keys()):
        line_data_fullest_column.append(temp[key])
    for item in line_data_fullest_column:
        if 'rotated_polyline' not in item:
            continue
        temp_y = (item['rotated_polyline'][0][1] + item['rotated_polyline'][-1][1])//2
        temp_y_list.append(temp_y)
    temp_y_list.sort()
    temp_heights = []
    for i in range(0, len(temp_y_list)-1):
        temp_heights.append(abs(temp_y_list[i]-temp_y_list[i+1]))
    intervals = np.arange(0,120,2)
    ocurrences, heights = np.histogram(temp_heights, bins=intervals)
    temp_ocurrence = 0
    chosen_height = 0
    for i in range(0,len(ocurrences)):
        if ocurrences[i] > temp_ocurrence:
            temp_ocurrence = ocurrences[i]
            chosen_height = heights[i]
        #print(heights[i],ocurrences[i])
    print("chosen_height:",chosen_height)
    return chosen_height


def make_name_imagette(line_item, page_name):
    """
    Creates the name of the imagette using the name of the page and the
    attributes of the item
    """
    res_name = page_name+"_"
    if 'polyline' in line_item:
        if line_item['polyline'].strip() != "":
            line_item['axisY'] = line_item['polyline'].split(';')[0].split(',')[1]
    characteristics = list(line_item.keys())
    characteristics.sort()
    for unwanted_characteristic in ['type','polyline','rotated_polyline','rotated_list_polylines','nomColonne','list_polylines','transcription', 'content', 'list_polyline', 'raw_ocr', 'lexical_ocr', 'truth']:
        if unwanted_characteristic in characteristics:
            characteristics.remove(unwanted_characteristic)
    for good_characteristic in characteristics:
        if isinstance(line_item[good_characteristic], str):
            temp = line_item[good_characteristic]
        else:
            temp = str(line_item[good_characteristic])
        res_name += good_characteristic+"-"+temp+"_"
    return res_name[:-1]


def adjust_area_height(area, max_image_height):
    """
    for adjusting the coordinates of the area to be extracted from the image to
    create the imagette. the area in question will be increased on both upper
    and lower boundary
    """
    current_height = area[3] - area[1]
    if current_height == max_image_height:
        return area
    height_diff = max_image_height - current_height
    lowerbound_plus = height_diff // 2
    upperbound_plus = height_diff - lowerbound_plus
    new_area = (area[0],\
                area[1]-upperbound_plus,\
                area[2],\
                area[3]+lowerbound_plus)
    return new_area


def find_pixel_value_between_text_and_background(image):
    temp_array = np.array(image.convert('L'))
    intervals = np.arange(20,240,1)
    ocurrences, pixels = np.histogram(temp_array, bins=intervals)
    pixels = pixels[:-1]
    ocurrences = ocurrences / max(ocurrences)
    # print(list(ocurrences))
    peaks = []
    threshold = 0.0001
    # print(list(ocurrences))
    while len(peaks) != 2 and threshold:
        threshold += 0.00001
        # print(threshold)
        peaks, properties = find_peaks(ocurrences, threshold=threshold, distance=20)
        # print(peaks)
    # pixel_value_between = sum(peaks)//len(peaks)
    pixel_value_between = peaks[-1]
    # print(pixel_value_between)
    # pixel_value_between = pixel_value_between - (pixel_value_between-peaks[0])//2
    return pixel_value_between


def dynamic_height(im, low_y, left_x, right_x, little_image_name, top_offset=60):
    """
    im: PIL image
    low_y: lower Y coord with a little offset
    left_x: lower Y coord with a little offset
    right_x: lower Y coord with a little offset
    top_offset: the height of the windows to analyse
    it checks each pixel row from down to up to find an empty espace where it
    is considered the top of the bounding box.
    """
    new_height = 10
    default_val = 20
    # im.save(os.path.join('testing_fold/headers',little_image_name+'_im.png'))
    crop_im = im.crop((left_x, low_y-top_offset, right_x, low_y))
    # crop_im.save(os.path.join('testing_fold/headers',little_image_name+'_crop.png'))
    # pixel_value_between = find_pixel_value_between_text_and_background(im)
    pixel_value_between = 255//2
    im_array = np.asarray(crop_im)
    height, width = im_array.shape
    list_of_how_many_black_pixels_per_row = []
    for i in reversed(range(0, height-default_val)):
        selected_area = im_array[i, :]
        temp = 1
        for x in selected_area:
            if x < pixel_value_between:
                temp += abs(x - pixel_value_between)**3
        # temp = sum(x < pixel_value_between for x in selected_area)
        list_of_how_many_black_pixels_per_row.append(temp)
    list_of_how_many_black_pixels_per_row = [(x / max(list_of_how_many_black_pixels_per_row)) for x in list_of_how_many_black_pixels_per_row]
    # if little_image_name == ':
    #     print(list_of_how_many_black_pixels_per_row)
    new_top = 0
    threshold_row_value_without_text = 0.05
    while new_top == 0 and threshold_row_value_without_text < 1:
        # print(threshold_row_value_without_text)
        threshold_row_value_without_text += 0.002
        for i in range(0, len(list_of_how_many_black_pixels_per_row)):
            # print(i)
            if list_of_how_many_black_pixels_per_row[i] < threshold_row_value_without_text:
                new_top = i
                break
    new_height = default_val + new_top
    return new_height

def get_list_of_cropped_images(column_lines_data, im, page_name, boundingbox_height_param, extra_thres=0.2, Y_order=True):
    """
    This function takes a list of items of a column, then it prepares the
    imagettes for the OCR. The extra_thres is a variable to change in case there
    is a offset with the top of the imagette
    """
    MINIMUM_HEIGHT = 25
    MINIMUM_HEIGHT_TITLES = 50
    column_images_for_ocr = []
    if boundingbox_height_param == 0:
        boundingbox_height_param = 40

    for item in column_lines_data:

        item["rotated_list_polylines"] = []
        line = item['polyline']
        polyline_ok = ()
        x = []
        y = []
        line = line.split(';')
        # line = [line[0],line[-1]]
        if len(line) < 2:
            continue
        for point in line:
            if len(point) < 4:
                continue
            pointo = point.split(',')
            polyline_ok = polyline_ok + ((int(pointo[0]),int(pointo[1])),)
            x.append(int(pointo[0]))
            y.append(int(pointo[1]))

        slope, _, _, _, _ = stats.linregress(x,y)
        # slope = (y[-1]-y[0])/(x[-1]-x[0])
        slope = math.degrees(math.atan(slope))

        temp_im = im.copy()
        # temp_im = temp_im.rotate(slope, center=(0,0))
        temp_im = temp_im.rotate(slope, center=(x[0],y[0]))

        if 'list_polyline' in item:
            if item['list_polyline'] != ['']:
                for line in item['list_polyline']:
                    if line == "":
                        continue
                    polyline_ok = ()
                    for point in line.split(';'):
                        pointo = point.split(',')
                        polyline_ok = polyline_ok + ((int(pointo[0]),int(pointo[1])),)
                    item["rotated_list_polylines"].append(rotated_polyline(polyline_ok, slope))
            else:
                item["rotated_list_polylines"] = [rotated_polyline(polyline_ok, slope)]
        else:
            item["rotated_list_polylines"] = [rotated_polyline(polyline_ok, slope)]

        little_image_name = make_name_imagette(item, page_name)
        areas_to_extract = []

        for line in item["rotated_list_polylines"]:
            if line == []:
                continue

            left_x = min(line, key=itemgetter(0))[0]
            right_x = max(line, key=itemgetter(0))[0]
            low_y =  max(line, key=itemgetter(1))[1]

            high_y = 0
            if 'title' in item:
                if item['title'] == "True":
                    high_y = dynamic_height(temp_im.copy(), low_y, left_x, right_x, little_image_name)
            if 'sectionTitle' in item:
                if item['sectionTitle'] == "True":
                    high_y = dynamic_height(temp_im.copy(), low_y, left_x, right_x, little_image_name)
            if str(item['column']) == "-1":
                high_y = dynamic_height(temp_im.copy(), low_y, left_x, right_x, little_image_name)

            if high_y != 0:
                boundingbox_height = high_y
                high_y = low_y - boundingbox_height - int(boundingbox_height*extra_thres*2)
                low_y += int(boundingbox_height*extra_thres*2)
            else:
                boundingbox_height = boundingbox_height_param
                high_y = low_y - boundingbox_height + int(boundingbox_height*extra_thres)
                low_y += int(boundingbox_height*extra_thres)

            left_x -= int(boundingbox_height*extra_thres)//2
            right_x += int(boundingbox_height*extra_thres)//2



            area = (left_x, high_y, right_x, low_y)
            areas_to_extract.append(area)
        if len(areas_to_extract) == 1:
            new_imagette = temp_im.crop(areas_to_extract[0])
            column_images_for_ocr.append([little_image_name, new_imagette])
        elif len(areas_to_extract) > 1:
            max_image_height = 0
            for area in areas_to_extract:
                image_height = area[3]-area[1]
                if image_height > max_image_height:
                    max_image_height = image_height
            for i in range(len(areas_to_extract)):
                areas_to_extract[i] = adjust_area_height(areas_to_extract[i],max_image_height)
            imagettes_to_join = [temp_im.crop(area) for area in areas_to_extract]
            widhts, heights = zip(*(i.size for i in imagettes_to_join))
            total_width = sum(widhts)
            max_height = max(heights)
            new_imagette = Image.new('RGB', (total_width, max_height))
            x_offset = 0
            for one_imagette in imagettes_to_join:
                new_imagette.paste(one_imagette, (x_offset,0))
                x_offset += one_imagette.size[0]
            column_images_for_ocr.append([little_image_name, new_imagette])

        # if "True" in little_image_name:
        #     # print(little_image_name)
        #     # print('areas_to_extract: ',len(areas_to_extract))
        #     new_imagette.save(os.path.join('testing_fold/headers',little_image_name+'.png'))


        del temp_im

    if Y_order:
        key_val = {}
        for file, ima in column_images_for_ocr:
            key = int(re.findall(r'axisY-\d+', file)[0].split('-')[1])
            key_val[key] = [file, ima]
        column_images_for_ocr = []
        keys = sorted(list(key_val.keys()))
        for key in keys:
            column_images_for_ocr.append(key_val[key])
        del key_val

    # save_imagettes(column_images_for_ocr, page_name, item['column'])
    return column_images_for_ocr


def apply_pretreatment(column_images_for_ocr):
    thresh = 120
    MEAN = 210
    for i in range(len(column_images_for_ocr)):
        extra = int(MEAN - np.mean(column_images_for_ocr[i][1]))
        # print(np.array(column_images_for_ocr[i][1]).min(), np.array(column_images_for_ocr[i][1]).max(),extra,np.mean(column_images_for_ocr[i][1]))
        fn = lambda x : x+extra if x < 255-extra else 255
        column_images_for_ocr[i][1] = column_images_for_ocr[i][1].convert('L').point(fn)
    return column_images_for_ocr



def save_imagettes(column_images_for_ocr, page_name, column_number):
        folder_create = os.path.join('temp_folder',page_name,str(column_number))
        if not os.path.exists(folder_create):
            os.mkdir(folder_create)
        for image_name, image in column_images_for_ocr:
            image.save(os.path.join(folder_create,image_name+'.png'))


def analyse_page(network, json_path, image_path, output_folder, json_output_folder, \
                 fst_folder='fst_folder', use_title_grammar=False, use_grammar=False):
    """
    Principal function that takes a NeuralNetwork, Json, Image.
    """

    # Creating a folder for the page's outputs
    page_name = json_path.rsplit('/',1)[-1].split('.',1)[0]
    create_folder(os.path.join(output_folder, page_name))

    # Making sure the the json is OK
    try:
        with open(json_path) as f:
            data = json.load(f)
        data['data'][1]
    except:
        logger.exception(now()+'bad json format or no information: '+json_path)

    # opening the original image, if not found, it will be downloaded automatically
    # if not os.path.exists(image_path):
    #     if not os.path.exists('temp_images'):
    #         os.mkdir('temp_images')
    #     image_path = get_one_page(json_path, 'temp_images')

    logger.info(now()+'opening image')
    im = Image.open(image_path)
    logger.info(now()+'image opened!')

    # Applying OCR and Grammar to titles
    logger.info(now()+'using OCR only on titles')
    titles = []
    for item in data['data']:
        if 'title' not in item:
            continue
        if item['title'] == 'True':
            titles.append(item)
    if len(titles) != 0:
        column_images_for_ocr = get_list_of_cropped_images(titles, im, page_name, 0, Y_order=False)
        if len(titles) != len(column_images_for_ocr):
            logger.error(now()+'number of prepared images not equal to number of titles in json')
        output_pred, output_proba = column_ocr(network, column_images_for_ocr, -99, 'titles', output_folder, page_name, fst_folder=fst_folder, use_grammar=use_title_grammar)
        if len(output_pred) != len(titles):
            logger.error(now()+'number of raw images not equal to number of titles in json')
        image_name_dict = {}
        for image_name, prediction in output_pred:
            image_name_dict[image_name] = {}
            image_name_dict[image_name]['raw'] = prediction
        if use_title_grammar:
            if len(output_proba) != len(titles):
                logger.error(now()+'number of lexical images not equal to number of titles in json')
            for image_name, proba_pred, confidence in output_proba:
                image_name_dict[image_name]['lex'] = proba_pred
                image_name_dict[image_name]['conf'] = confidence
        for item in data['data']:
            if 'rotated_list_polylines' in item:
                del item['rotated_list_polylines']
            if 'title' not in item:
                continue
            if item['title'] == 'True':
                little_image_name = make_name_imagette(item, page_name)
                item['raw_ocr'] = image_name_dict[little_image_name]['raw']
                if 'lex' in image_name_dict[little_image_name]:
                    item['lexical_ocr'] = image_name_dict[little_image_name]['lex']
                    item['confidence'] = image_name_dict[little_image_name]['conf']
    else:
        logger.info(now()+'no titles have been detected on the Json')


    # Grouping all the info of the JSON by columns
    logger.info(now()+'groupping the different items by column')
    all_columns = []
    data_per_column = {}
    for item in data["data"]:
        if not 'numTable' in item:
            continue
        if 'title' in item:
            if item['title'] == 'True':
                continue
        if int(item["column"]) not in all_columns:
            all_columns.append(int(item["column"]))
        if int(item['column']) not in data_per_column:
            data_per_column[int(item['column'])] = {}
            data_per_column[int(item['column'])]['lines'] = [item]
            data_per_column[int(item['column'])]['slope'] = ''
            data_per_column[int(item['column'])]['columnTitle'] = ''
        else:
            data_per_column[int(item['column'])]['lines'].append(item)
        if 'nomColonne' in item:
            data_per_column[int(item['column'])]['columnTitle'] += ' '+item['nomColonne']
            data_per_column[int(item['column'])]['columnTitle'] = data_per_column[int(item['column'])]['columnTitle'].strip()
    all_columns.sort()

    if all_columns == []:
        output_file = os.path.join(json_output_folder, page_name+".withOCR.json")
        logger.info(now()+'saving the new json in: '+str(output_file) )
        print('saving_file: ',output_file)
        with open(output_file, "w", encoding="utf-8") as out:
            json.dump(data, out, indent=1, ensure_ascii=False)
        return 0


    # Calculating the slope for each column and remaking the polylines
    logger.info(now()+'Calculating the slope for each column and remaking the polylines')
    data_per_column = add_slope_and_remake_polylines(all_columns, data_per_column)
    logger.info(now()+'done!')

    # calculating the height of the bounding box
    logger.info(now()+'calculating the general height of the bounding box:')
    boundingbox_height = calculate_common_height(data_per_column)
    logger.info(now()+'done! general height = '+str(boundingbox_height)+'px')

    # # Calculating the contrast value between the printed text and the paper
    # pixel_value_between = find_pixel_value_between_text_and_background(im)

    # preparing the data per column and using the ocr
    ocr_data_per_column = {}
    for column_key in all_columns:
        logger.info(now()+'-------------treating the column:'+str(column_key))
        # if column_key not in [3]:
        #     continue
        print("--> ANALYSING COLUMN:", column_key)
        ocr_data_per_column[column_key] = {}
        # temp_im = im.copy()
        # temp_im = temp_im.rotate(data_per_column[column_key]['slope'], center=(0,0))
        logger.info(now()+'creating the column images for the OCR')
        column_images_for_ocr = get_list_of_cropped_images(data_per_column[column_key]['lines'], im, page_name, boundingbox_height)
        logger.info(now()+'done! number of images = '+str(len(column_images_for_ocr)))
        # if len(column_images_for_ocr) > 20:
        #     column_images_for_ocr = random.sample(column_images_for_ocr, 20)
        column_images_for_ocr = apply_pretreatment(column_images_for_ocr)
        # save_cropped_images_for_visualisation(column_images_for_ocr, column_key)
        # del temp_im
        if len(column_images_for_ocr) != 0:
            try:
                output_pred, output_proba = column_ocr(network, column_images_for_ocr, column_key, data_per_column[column_key]['columnTitle'], output_folder, page_name, fst_folder=fst_folder, use_grammar=use_grammar)
            except ValueError:
                output_pred, output_proba = [], []
            if len(output_pred) != len(column_images_for_ocr):
                logger.error(now()+'number of raw images not equal to number of items in column; len_pred='+str(len(output_pred))+' ; len_images='+str(len(column_images_for_ocr)))
            for file_name, the_pred in output_pred:
                if file_name not in ocr_data_per_column[column_key]:
                    ocr_data_per_column[column_key][file_name] = {}
                ocr_data_per_column[column_key][file_name]['raw_ocr'] = the_pred
            if use_grammar:
                if len(output_proba) != len(column_images_for_ocr):
                    logger.error(now()+'number of lexical images not equal to number of items in column; len_pred='+str(len(output_proba))+' ; len_images='+str(len(column_images_for_ocr)))
                for file_name, the_pred, confidence in output_proba:
                    if file_name not in ocr_data_per_column[column_key]:
                        ocr_data_per_column[column_key][file_name] = {}
                    ocr_data_per_column[column_key][file_name]['lexical_ocr'] = the_pred
                    ocr_data_per_column[column_key][file_name]['confidence'] = confidence

    logger.info(now()+'injecting the OCR text into the json')
    for i in range(len(data['data'])):
        if 'rotated_list_polylines' in data['data'][i]:
            del data['data'][i]['rotated_list_polylines']
        if 'rotated_polyline' in data['data'][i]:
            del data['data'][i]['rotated_polyline']
        column_number = int(data['data'][i]['column'])
        little_image_name = make_name_imagette(data['data'][i], page_name)
        if column_number in ocr_data_per_column:
            if little_image_name in ocr_data_per_column[column_number]:
                for key in ocr_data_per_column[column_number][little_image_name].keys():
                    data['data'][i][key] = ocr_data_per_column[column_number][little_image_name][key]

    output_file = os.path.join(json_output_folder, page_name+".withOCR.json")
    logger.info(now()+'saving the new json in: '+str(output_file) )
    print('saving_file: ',output_file)
    with open(output_file, "w", encoding="utf-8") as out:
        json.dump(data, out, indent=1, ensure_ascii=False)

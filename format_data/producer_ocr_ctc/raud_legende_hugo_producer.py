import pprint

from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.batch_preprocessing.ImagesFromBaseline import ImagesFromBaseline
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequenceFromBaseline import To2DSequenceFromBaseline
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import glob
import shutil
from numpy.random import shuffle


def get_transcription(input_xml):
    list_trans = []
    from lxml import etree

    try:
        tree = etree.parse(input_xml)

        for line in tree.findall(".//TextLine"):
            transcript = line.find(".//Unicode")
            list_trans.append(transcript)
    except:
        print("Can not read xml, or there is no xml file.")

    return list_trans


def create_hugo_raw_base(basepath, origin_dir, font_list, deg_list=[1, 2]):
    if not os.path.exists(basepath):
        os.makedirs(basepath)

    with open(font_list, 'r') as file:
        fontlist = file.read().splitlines()
    print(basepath)
    print(origin_dir)
    print(font_list)
    for font in fontlist:
        print(font)
        for deg_nb in deg_list:
            print('--deg', deg_nb)
            suffix = "/12/300/deg" + str(deg_nb) + "/images/" + font + ".files"
            imgdir = os.path.join(origin_dir, font + suffix)
            filelist = os.listdir(imgdir)
            for file in filelist:
                basename = os.path.basename(file)
                shutil.copyfile(os.path.join(imgdir, basename),
                                os.path.join(basepath, font + ".deg" + str(deg_nb) + "." + basename))


def create_train_valid_test(input_path, dir_origin, ext_im='tiff'):
    print("create train valid test base :", dir_origin)

    if not os.path.exists(input_path):
        os.mkdir(input_path)

    if not os.path.exists(os.path.join(input_path, 'TRAIN')):
        os.mkdir(os.path.join(input_path, 'TRAIN'))
        os.mkdir(os.path.join(input_path, 'VAL'))
        os.mkdir(os.path.join(input_path, 'TST'))

    image_list = glob.glob(os.path.join(dir_origin, '*.' + ext_im))
    shuffle(image_list)

    train_size = len(image_list) // 10 * 7
    valid_size = len(image_list) // 10 * 2

    train_list = image_list[0:train_size]
    valid_list = image_list[train_size:train_size + valid_size]
    test_list = image_list[train_size + valid_size:]

    first_test = True
    first_val = True
    first_train = True

    for image in train_list:
        image = os.path.basename(image)
        shutil.copyfile(os.path.join(dir_origin, image), os.path.join(input_path, 'TRAIN', image))
        [prefix, _] = os.path.splitext(image)

        with open(os.path.join(dir_origin, prefix + ".txt"), 'r') as labelfile:
            label = labelfile.read().replace('\n', '')

        shutil.copyfile(os.path.join(dir_origin, prefix + '.txt'), os.path.join(input_path, 'TRAIN', prefix + '.txt'))
        if first_train:
            with open(os.path.join(input_path, 'train.txt'), "w") as file:
                file.write(image + ' ' + label + '\n')
            first_train = False
        else:
            with open(os.path.join(input_path, 'train.txt'), "a") as file:
                file.write(image + ' ' + label + '\n')

    for image in valid_list:
        image = os.path.basename(image)
        shutil.copyfile(os.path.join(dir_origin, image), os.path.join(input_path, 'VAL', image))
        [prefix, _] = os.path.splitext(image)

        with open(os.path.join(dir_origin, prefix + ".txt"), 'r') as labelfile:
            label = labelfile.read().replace('\n', '')

        shutil.copyfile(os.path.join(dir_origin, prefix + '.txt'), os.path.join(input_path, 'VAL', prefix + '.txt'))
        if first_val:
            with open(os.path.join(input_path, 'valid.txt'), "w") as file:
                file.write(image + ' ' + label + '\n')
            first_val = False
        else:
            with open(os.path.join(input_path, 'valid.txt'), "a") as file:
                file.write(image + ' ' + label + '\n')

    for image in test_list:
        image = os.path.basename(image)
        shutil.copyfile(os.path.join(dir_origin, image), os.path.join(input_path, 'TST', image))
        [prefix, _] = os.path.splitext(image)

        with open(os.path.join(dir_origin, prefix + ".txt"), 'r') as labelfile:
            label = labelfile.read().replace('\n', '')

        shutil.copyfile(os.path.join(dir_origin, prefix + '.txt'), os.path.join(input_path, 'TST', prefix + '.txt'))
        if first_test:
            with open(os.path.join(input_path, 'test.txt'), "w") as file:
                file.write(image + ' ' + label + '\n')
            first_test = False
        else:
            with open(os.path.join(input_path, 'test.txt'), "a") as file:
                file.write(image + ' ' + label + '\n')


def main(input_path, dir_origin="../../../../../code/segmenteurFCN/xml/allRIMES/",
         dir_line_images='../../../data/RIMES_paragraph/line_images0.5/',
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False,
         window_structure='vertical', shuff=True, padding=-1):
    if not os.path.exists(input_path):
        create_train_valid_test(input_path, dir_origin=dir_origin)

    str_sup = ''

    str_info = ''
    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []

        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + "HUGOconv_" + str_sup + str(h_norm) + "_" + str(nb_cols) + "_" + str(
            stride) + str_info
    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        preprocessing_batch = []

        output_path = input_path + "HUGO" + str_sup + str(h_norm) + "_" + str(nb_cols)

    print(output_path)
    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=True,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        shuff=shuff
                        ).produce_dataset(sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TST/"},
                                          ext_images="")


if __name__ == '__main__':
    nb_cols = 32
    stride = 1
    padding = -1.
    norm = 64
    keep_ratio = True
    conv = True
    conv_pad = True
    ImageFrombaseline = False
    window_structure = 'horizontal'

    txt_path = ""
    shuff = False

    input_path = '/media/swang/data/swang/LegendeMiniBaseH5/'
    origin_path = "/media/swang/data/swang/LegendeMiniBase/"
    # create_train_valid_test(input_path, origin_path)

    main(input_path, dir_origin=origin_path, dir_line_images='', h_norm=norm, nb_cols=nb_cols, stride=stride,
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline,
         window_structure=window_structure, shuff=shuff, padding=padding)

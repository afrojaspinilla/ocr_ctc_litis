import sys, os
sys.path.append('./')
sys.path.append('../')
from ocr_ctc.CTC_model.CTCModel import CTCModel
from ocr_ctc.CTC_model.callbacks.CTCTensorBoard import CTCTensorBoard
from ocr_ctc.dataset_manager.new_batch.DatasetBatch import DatasetBatch
from ocr_ctc.dataset_manager.new_batch.DatasetOnline import DatasetOnline
from pathlib import Path
from keras.optimizers import Adam
import numpy as np
from copy import deepcopy
import new_utils.settings as settings
from ocr_ctc.utils.decoder import decoding_with_reference
from ocr_ctc.utils.utils_analysis import compute_character_error_rate_from_lists, compute_word_error_rate_from_lists, \
    compute_cer_backpointer_from_lists
from ocr_ctc.utils.utils_data import order_weights
from time import time

#from ocr_ctc.CTC_model.model_templates import conv4

def create_network(dir_dataset_train, noise_stddev=0.01, beam_width_decode = 100, rnn_size=[100, 100, 100, 100],\
    greedy_decode = False, top_paths_decode = 1, optimizer=Adam(lr=0.0001), dropout=0.1):
    """ create a recurrent network
    (in case of it can not be loaded)
    Make sure that it is the right structure for the weights that will be loaded
    """
    from keras.layers import Masking, TimeDistributed, Activation, Dense, Input, Bidirectional, GRU, GaussianNoise, \
        SimpleRNN, Flatten, MaxPooling2D, Conv2D, LSTM

    dataset_train = DatasetBatch(dir_dataset_train)
    dataset_train.load()
    nb_labels = dataset_train.nb_labels
    charset = dataset_train.all_labels
    nb_features = dataset_train.nb_features * dataset_train.nb_cols if dataset_train.nb_cols else dataset_train.nb_features
    depth = len(rnn_size)

    input_data = Input(name='input', shape=[None, nb_features])
    masking = Masking(mask_value=dataset_train.padding, input_shape=(None, None))(input_data)
    layers = GaussianNoise(noise_stddev)(masking)
    # RECURRENT LAYERS
    layers = Bidirectional(LSTM(rnn_size[0], return_sequences=True, dropout=dropout))(layers)
    for i in range(1, depth):
        layers = Bidirectional(GRU(rnn_size[i], return_sequences=True, dropout=dropout))(layers)

    layers = TimeDistributed(Dense(nb_labels + 1, name="dense"))(layers)  # 102
    output = Activation('softmax', name='softmax')(layers)

    network = CTCModel([input_data], [output], greedy=greedy_decode, beam_width=beam_width_decode,
                       top_paths=top_paths_decode, charset=charset)
    network.compile(optimizer)


def load_model(log_path, file_weights, dir_data, dataset, rnn_size=[100, 100, 100, 100],\
                 dir_dataset_train=None, greedy_decode=False, top_paths_decode=1, beam_width_decode=100, \
                 optimizer=Adam(lr=0.0001), dropout=0.1, noise_stddev=0.01):
    """ Load model architecture and weights """

    my_file = Path(log_path + '/model_train.json')
    if my_file.is_file():
        network = CTCModel(None, None, greedy=greedy_decode, beam_width=beam_width_decode, top_paths=top_paths_decode)
        network.load_model(log_path, optimizer, file_weights=file_weights, change_parameters=True)


        if network.charset is None:
            print("No charset has been saved.")
            network.charset = dataset.all_labels
    else:  # create model
        print("create network")
        if dir_dataset_train is None:
            dir_dataset_train = dir_data

        network = create_network(dir_dataset_train, noise_stddev=noise_stddev, beam_width_decode=beam_width_decode,
                                 rnn_size=rnn_size, \
                                 greedy_decode=greedy_decode, top_paths_decode=top_paths_decode, optimizer=optimizer,
                                 dropout=dropout)

        network.model_train.load_weights(file_weights)
        network.model_pred.set_weights(network.model_train.get_weights())
        network.model_eval.set_weights(network.model_train.get_weights())
        network.model_init.set_weights(network.model_train.get_weights())

    return network


def eval_net_generator(network, generator, nb_batch, get_pred=True, get_eval=None, \
         get_prob=False, get_loss=False, file_to_eval=None, info_files=None):
    """ evaluate a dataset using a generator

    file_to_eval must be a tuple with the file name of the prediction and one another with the ground truth
    """

    final_out = {}
    # get loss
    if get_loss:
        loss, list_loss = network.get_loss_generator(generator, nb_batch)
        final_out['loss'] = (loss, list_loss)

    # get output probabilities
    if get_prob:
        s_time = time()
        probs = network.get_probas_generator(generator, nb_batch)
        final_out['proba'] = probs
        print('get_probas_generator_time:',time()-s_time)

    # get predictions
    if get_pred:
        s_time = time()
        prediction = network.predict_generator(generator, nb_batch, decode_func=decoding_with_reference)
        #print(prediction)
        final_out['pred'] = prediction
        print('predict_generator_time:',time()-s_time)

    # get metrics
    if get_eval is not None:

        metrics_for_ctcmodel = []
        if 'loss' in get_eval:
            metrics_for_ctcmodel.append('loss')
        if 'ler' in get_eval:
            metrics_for_ctcmodel.append('ler')
        if 'ser' in get_eval:
            metrics_for_ctcmodel.append('ser')

        if len(metrics_for_ctcmodel)>0:
            outmetrics_ctc = network.evaluate_generator(generator, nb_batch, metrics=metrics_for_ctcmodel)
        else:
            outmetrics_ctc = None

        final_out['eval'] = {}
        idx = 0
        if 'loss' in metrics_for_ctcmodel:
            final_out['eval']['loss'] = outmetrics_ctc[idx]
            idx += 1
        if 'ler' in metrics_for_ctcmodel:
            final_out['eval']['ler'] = outmetrics_ctc[idx]
            idx += 1
        if 'ser' in metrics_for_ctcmodel:
            final_out['eval']['ser'] = outmetrics_ctc[idx]

        if 'cer' in get_eval or 'wer' in get_eval:
            if file_to_eval is not None:
                try:
                    pred = []
                    names = []
                    with open(file_to_eval[0], 'r') as fev:
                        for line in fev.readlines():
                            spl_line = line.split(' ')
                            pred.append(' '.join(spl_line[1:]))
                            names.append(spl_line[0].split('/')[-1])
                    lab = []
                    if info_files is not None:
                        names_info = [infof[0].split('/')[-1] for infof in info_files]
                    with open(file_to_eval[1], 'r') as fev:
                        for line in fev.readlines():
                            spl_line = line.split(' ')
                            if info_files is not None:
                                if spl_line[0].split('/')[-1] in names_info:
                                    lab.append(' '.join(spl_line[1:]))
                                else:
                                    print(spl_line[0].split('/')[-1], "not found")
                            elif spl_line[0].split('/')[-1] in names:
                                lab.append(' '.join(spl_line[1:]))
                            else:
                                print(spl_line[0].split('/')[-1], "not found")
                except:
                    print("can not load predictions and ground truth.")
            elif get_pred:
                pred, lab = final_out['pred']
            else:
                pred, lab = network.predict_generator(generator, nb_batch, decode_func=decoding_with_reference)

            if 'cer_analysis' in get_eval:
                list_cer, analysis_op = compute_cer_backpointer_from_lists(lab, pred,
                                                                   lab_to_replace=[('<sp>', ' '), ('\n', '')])
                final_out['eval']['cer'] = list_cer
                final_out['eval']['cer_analysis'] = analysis_op
            if 'cer' in get_eval:
                list_cer = compute_character_error_rate_from_lists(lab, pred, lab_to_replace=[('<sp>', ' '), ('\n', '')])
                final_out['eval']['cer'] = list_cer
            if 'wer' in get_eval:
                list_wer = compute_word_error_rate_from_lists(lab, pred, lab_to_replace=[('<sp>', ' '),  ('\n', '')])
                final_out['eval']['wer'] = list_wer

    return final_out



def eval_one_exp(log_path, file_weights, dir_data, set_to_test=['test'], batch_size=2, \
                 get_pred=True, get_eval=None, get_prob=False, get_loss=False, rnn_size=[100, 100, 100, 100],\
                 dir_dataset_train=None, greedy_decode=False, top_paths_decode=1, beam_width_decode=100, \
                 optimizer=Adam(lr=0.0001), dropout=0.1, noise_stddev=0.01, verbose=False, file_to_eval=None):
    """ Evaluation of one experimental setting (i.e. one weight for one set of data)
     - saved in log_path
     - for the weights saved in the file file_weights
     - on data of the standard form (hdf5) saved in dir_data.
     - set_to_test = dataset one wants to evaluate (train', 'eval' and/or 'test' sets)
     - batch_size = batch size used for the generator
     - get_pred, get_eval, get_prob, get_loss: evaluation one wants to perform (prediction, metrics computation, probabilities, loss)
     - other parameters are not required if the model has been correctly saved.

     see eval_set_exp for more details.
     """

    print(log_path)
    print(file_weights)
    print(dir_data)
    print(file_to_eval)

    print("*******************************************************")

    # dataset = DatasetBatch(dir_data, train_size=batch_size, valid_size=batch_size, test_size=batch_size)
    # dataset.load()
    # print("dataset.test_files:",dataset.test_files)
    # print("dataset.nbbatch_test:",dataset.nbbatch_test)

    dataset = DatasetOnline()
    print(dataset.test_size)

    print("°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°")

    # load model architecture and weights
    network = load_model(log_path, file_weights, dir_data, dataset, rnn_size=rnn_size,\
                 dir_dataset_train=dir_dataset_train, greedy_decode=greedy_decode, top_paths_decode=top_paths_decode, beam_width_decode=beam_width_decode, \
                 optimizer=optimizer, dropout=dropout, noise_stddev=noise_stddev)

    out_expe = {}

    # evaluation on each specific dataset
    if 'test' in set_to_test:
        if len(dataset.test_labels_map)>0:
            out_eval = eval_net_generator(network, dataset.get_test_generator(), int(dataset.test_size / batch_size), get_pred=get_pred, get_eval=get_eval, \
                           get_prob=get_prob, get_loss=get_loss, file_to_eval=file_to_eval, info_files=dataset.test_labels_map)
            # out_eval = eval_net_generator(network, dataset.get_test_generator(), int(dataset.nbbatch_test / batch_size), get_pred=get_pred, get_eval=get_eval, \
            #                get_prob=get_prob, get_loss=get_loss, file_to_eval=file_to_eval, info_files=dataset.test_files)
            #out_expe['test'] = out_eval
            print('=======================')
            for i in range(len(out_eval['pred'][0])):
                print(dataset.test_list[i],out_eval['pred'][0][i])
            if get_pred:
                save_pred("123_test.txt", out_eval['pred'], name_data=dataset.test_list, print_eval=True, save_gt=False, step_range=1, save_list=False,\
                              list_to_replace=[('<sp>', ' ')])
        else:
            print("There is no test data")
        if verbose:
            print("Test set is evaluated.")

    if 'valid' in set_to_test:
        if len(dataset.valid_files) > 0:
            out_eval = eval_net_generator(network, dataset.get_valid_generator(), int(dataset.nbbatch_valid / batch_size), get_pred=get_pred, get_eval=get_eval, \
                           get_prob=get_prob, get_loss=get_loss, file_to_eval=file_to_eval, info_files=dataset.valid_files)
            out_expe['valid'] = out_eval
        else:
            print("There is no valid data")
        if verbose:
            print("Validation set is evaluated.")

    if 'train' in set_to_test:
        if len(dataset.train_files) > 0:
            out_eval = eval_net_generator(network, dataset.get_train_generator(), int(dataset.nbbatch_train / batch_size), get_pred=get_pred, get_eval=get_eval, \
                           get_prob=get_prob, get_loss=get_loss, file_to_eval=file_to_eval, info_files=dataset.train_files)
            out_expe['train'] = out_eval
        else:
            print("There is no train data")
        if verbose:
            print("Training set is evaluated.")

    return out_expe


def eval_whole_exp(log_path, dir_weights, dir_data, set_to_test=['test'], batch_size=2, list_of_it=None,\
                 get_pred=True, get_eval=None, get_prob=False, get_loss=False, rnn_size=[100, 100, 100, 100],\
                 dir_dataset_train=None, greedy_decode=False, top_paths_decode=1, beam_width_decode=100, \
                 optimizer=Adam(lr=0.0001), dropout=0.1, noise_stddev=0.01, verbose=False, file_to_eval=None):
    """ Evaluation for a all the weights that have been saved
    list_of_it = list of it one wants to load (None if all the weights)
    see eval_set_exp for more details.
    """

    # order weights in dir_weights
    order_w, out_idx = order_weights(dir_weights, list_of_it)

    # eval each weight
    all_expe = []
    for i,file_weights in enumerate(order_w):

        if verbose:
            print("training iteration: ", out_idx[i])
        if list_of_it[i]<0:
            list_of_it[i] = out_idx[i]

        out_expe = eval_one_exp(log_path, dir_weights + file_weights, dir_data, set_to_test=set_to_test, batch_size=batch_size,\
                     rnn_size=rnn_size, dir_dataset_train=dir_dataset_train, \
                     get_pred=get_pred, get_eval=get_eval, get_prob=get_prob, get_loss=get_loss, \
                     greedy_decode=greedy_decode, top_paths_decode=top_paths_decode, \
                     beam_width_decode = beam_width_decode, optimizer=optimizer, dropout=dropout, \
                     noise_stddev=noise_stddev, verbose=verbose, file_to_eval=file_to_eval)

        all_expe.append((out_idx[i], out_expe))

    return all_expe


def eval_set_exp(list_log_path, list_dir_weights, list_dir_data, set_to_test=['test'], batch_size=2, list_of_it=None,\
                 get_pred=True, get_eval=None, get_prob=False, get_loss=False, rnn_size=[100, 100, 100, 100],\
                 dir_dataset_train=None, greedy_decode=False, top_paths_decode=1, beam_width_decode=100, \
                 optimizer=Adam(lr=0.0001), dropout=0.1, noise_stddev=0.01, verbose=False, file_to_eval=None):
    """
    Evaluate a set of experiments

    assert that each list is of the same length:
    experiment of any index i is related to index i of each list (list_log_path, list_dir_weights, list_dir_data

     - list_log_path = list of directory where the model architectures are saved
     - list_dir_weights = list of directory where the model weights are saved
     - list_dir_data = list of directory where the input data that are analyzed are saved
     - set_to_test = dataset one wants to evaluate (train', 'eval' and/or 'test' sets)
     - batch_size = batch size used for the generator
     - get_pred = predictions: True or False
     - get_eval = evaluate metrics: None or a list of metrics: 'ler', 'cer', 'wer', 'ser', 'loss'
     - get_prob = probabilities: True or False
     - get_loss = loss value: True or False
     - list_of_it = list of it one wants to load (None if all the weights, -1 for the last one)

     - other parameters are not required if the model has been correctly saved, \
     otherwise it is used to define the network architecture related to the weights that will be loaded.
    """

    n_expe = len(list_log_path)
    assert(len(list_dir_weights) == n_expe)
    assert(len(list_dir_data) == n_expe)

    set_expe = []
    for i in range(n_expe):

        if verbose:
            print("expe ", i)

        log_path = list_log_path[i]
        dir_weights = list_dir_weights[i]
        dir_data = list_dir_data[i]

        all_expe = eval_whole_exp(log_path, dir_weights, dir_data, set_to_test=set_to_test, list_of_it=list_of_it, batch_size=batch_size, \
                       get_pred=get_pred, get_eval=get_eval, get_prob=get_prob, get_loss=get_loss, rnn_size=rnn_size, \
                       dir_dataset_train=dir_dataset_train, greedy_decode=greedy_decode, top_paths_decode=top_paths_decode, beam_width_decode=beam_width_decode, \
                       optimizer=optimizer, dropout=dropout, noise_stddev=noise_stddev, verbose=verbose, file_to_eval=file_to_eval)

        set_expe.append(all_expe)

    return set_expe


def save_pred(name_save, out_pred, name_data=[], print_eval=False, save_gt=False, step_range=1, save_list=False,\
              list_to_replace=[('<sp>', ' ')]):
    """ Save predictions from an evaluation """

    if len(out_pred[0])>len(name_data) and len(out_pred[0])!=len(name_data)*2:
        print("save predictions: ", len(out_pred[0]), len(name_data), "\nThe number of data name is not the one of the outputs.")
        name_data = [str(k) for k in range(len(out_pred[0]))]
    # else:
    #     # get the predicted lines in the right order
    #     if isinstance(name_data[0], tuple):
    #         list_sort = [('_'.join(name_data[idx_data][0].split('/')[-1].split('_')[:-1]),int(name_data[idx_data][0].split('/')[-1].split('_')[-1].split('.')[0])) for idx_data in range(len(name_data))]
    #         out_sort, names, preds, gts = zip(*sorted(zip(list_sort, name_data, out_pred[0], out_pred[1])))
    #     else:
    #         list_sort = [('_'.join(name_data[idx_data].split('_')[:-1]), int(name_data[idx_data].split('_')[-1].split('.')[0])) for idx_data in range(len(name_data))]
    #         out_sort, names, preds, gts = zip(*sorted(zip(list_sort, name_data, out_pred[0], out_pred[1])))

    str_out = ''
    for k in range(0, len(out_pred[0]), step_range):
        pred, gt = out_pred[0][k], out_pred[1][k]

        # check name data
        idx_data = int(k/step_range)
        if isinstance(name_data[idx_data], tuple):
            nf = name_data[idx_data][0].split('/')[-1]
        else:
            nf = name_data[idx_data]

        if save_list:
            str_out += nf
            for pr in pred:
                str_out += ' ' + pr
            str_out += '\n'
        else:
            if isinstance(pred, list):
                pred = ''.join(pred)
            for lr in list_to_replace:
                pred = pred.replace(lr[0], lr[1])
            str_out += nf + ' ' + pred + '\n'
        if save_gt:
            if save_list:
                str_out += nf
                for pr in gt:
                    str_out += ' ' + pr
                str_out += '\n'
            else:
                if isinstance(gt, list):
                    gt = ''.join(gt)
                for lr in list_to_replace:
                    gt = gt.replace(lr[0], lr[1])
                str_out += nf + ' ' + gt + '\n'

    if print_eval:
        print(str_out)

    with open(name_save, 'w') as fsave:
        fsave.write(str_out)


def save_loss(name_save, out_loss, name_data=[], print_eval=False, step_range=1):
    """ Save loss from an evaluation """

    if len(out_loss[-1])>len(name_data):
        print("save predictions: the number of data name is not the one of the outputs.")
        name_data = [str(k) for k in range(len(out_loss[-1]))]

    str_out = ''
    for k in range(0, len(out_loss[-1]), step_range):
        loss = out_loss[-1][k]
        nf = name_data[k]
        str_out += nf + ' ' + '%f' % (loss,) + '\n'

    str_out += 'L=' + '%f' % (float(out_loss[0])/step_range,) + '\n'

    if print_eval:
        print(str_out)

    with open(name_save, 'w') as fsave:
        fsave.write(str_out)


def save_eval(name_save, out_eval, print_eval=True):
    """ Save eval metrics from an evaluation """

    str_out = ''
    for key in out_eval:

        if isinstance(out_eval[key], dict):
            str_out += key + ' :'
            for keyeval in out_eval[key]:
                val = out_eval[key][keyeval]
                str_out += ' ' + keyeval + '%.4f' % (val,)
            str_out += '\n'
        elif isinstance(out_eval[key], list) or type(out_eval[key]).__module__ == np.__name__:
            val = np.mean(out_eval[key])
            str_out += key + ' ' + '%.4f' % (val,) + '\n'
        else:
            val = out_eval[key]
            str_out += key + ' ' + '%.4f' % (val,) + '\n'

        str_out += key + ' ' + '%.4f' % (val,)+ '\n'

    if print_eval:
        print(str_out)

    with open(name_save, 'w') as fsave:
        fsave.write(str_out)


def save_proba_2(name_save, out_probs, charset, first_blank=True, duplicate_labels=None, save_whole_document=False, \
               log_prob=True, name_data=[], save_format='kaldi', step_range=1, file_charset=None, eps=1e-100):
    """ Save proba from an evaluation

    - first_blank = the first element in the set of probability that will be saved will be the blank label
    - duplicate_label = None or dictionary where each key is a label (in charset) for which the lael will be duplicate,
                    each key contains a list of labels with the same proba (labels that are not in charset)
    - log_prob = save the log_probabilities if True
    """

    if not isinstance(charset, list):
        charset = list(charset)

    # Get the requested labeling order
    str_charset = ''
    if ' ' in charset:
        charset[charset.index(' ')] = '<sp>'
    charset_out = deepcopy(charset)
    if not 'blank' in charset:
        charset.append('blank')
        if first_blank:
            charset_out.insert(0, 'blank')
        else:
            charset_out.append('blank')
    elif first_blank:
        charset_out.remove('blank')
        charset_out.insert(0, 'blank')
    else:
        charset_out.remove('blank')
    if duplicate_labels is not None:
        for key in duplicate_labels:
            for l in duplicate_labels[key]:
                if l not in charset_out:
                    charset_out.append(l)
    for l in charset_out:
        str_charset += l + ' '
    str_charset = str_charset[:-1]
    #print(str_charset)
    # Get index in out_probs for each label
    all_duplicate = []
    if duplicate_labels is not None:
        for key in duplicate_labels:
            all_duplicate += [elmt for elmt in duplicate_labels[key]]
    idx_charset = []
    for ch in charset_out:
        if ch in all_duplicate:
            for key in duplicate_labels:
                if ch in duplicate_labels[key]:  # warning: make sure that a label is not in several key
                    idx_charset.append(charset.index(key))
        else:
            idx_charset.append(charset.index(ch))
    print(str_charset)
    if file_charset is not None:
        with open(file_charset, 'w') as fch:
            for ch in str_charset.split(' '):
                fch.write(ch + '\n')

    # Create a string with the probabilities (with the requested format)
    str_probs = ''
    nb_probs = len(out_probs) if step_range==1 else int(len(out_probs)/step_range)
    for idx_pr in range(nb_probs):
        pr_data = out_probs[idx_pr * step_range]
        T, D = pr_data.shape

        if save_format=='kaldi':
            if len(name_data) > 0:
                #str_probs += name_data[idx_pr][0].split('/')[-1] + ' [ ' + '\n' # THIS WAS OLD
                try:
                    str_probs += name_data[idx_pr].split('/')[-1] + ' [ ' + '\n' # THIS IS NEW
                except:
                    continue
            else:
                str_probs += str(idx_pr) + ' [ ' + '\n'

        last_index = -1
        for t in range(T):
            if log_prob:
                tempo = [np.log10(max(pr_data[t,d], eps)) for d in idx_charset]
                tempo = np.argmax(tempo)
                if tempo == last_index:
                    if tempo < len(charset):
                        if charset[tempo-1].isnumeric():
                            # print(charset[tempo-1])
                            continue
                    pass
                elif tempo != 0 and last_index != 0:
                    str_probs += '{:s}'.format(
                        ' '.join(['{:.6f}'.format(-9) if d != len(charset)-1 else '{:.6f}'.format(0) for d in idx_charset])) + '\n'
                    last_index = tempo
                else:
                    last_index = tempo
                str_probs += '{:s}'.format(
                    ' '.join(['{:.6f}'.format(np.log10(max(pr_data[t,d], eps))) for d in idx_charset])) + '\n'
            else:
                str_probs += '{:s}'.format(' '.join(['{:.6f}'.format(pr_data[t,d]) for d in idx_charset])) + '\n'

        if save_format == 'kaldi':
            str_probs = str_probs[:-1] + ' ]' + '\n'  # -1 to remove \n


    if save_whole_document:
        str_probs = str_probs[:-1] + ' ]' + '\n'

    #str_out = str_charset + '\n' + str_probs
    str_out = str_probs
    with open(name_save, 'w') as fsave:
        fsave.write(str_out)
    return str_out


def save_proba(name_save, out_probs, charset, first_blank=True, duplicate_labels=None, save_whole_document=False, \
               log_prob=True, name_data=[], save_format='kaldi', step_range=1, file_charset=None, eps=1e-100):
    """ Save proba from an evaluation

    - first_blank = the first element in the set of probability that will be saved will be the blank label
    - duplicate_label = None or dictionary where each key is a label (in charset) for which the lael will be duplicate,
                    each key contains a list of labels with the same proba (labels that are not in charset)
    - log_prob = save the log_probabilities if True
    """

    if not isinstance(charset, list):
        charset = list(charset)

    # Get the requested labeling order
    str_charset = ''
    if ' ' in charset:
        charset[charset.index(' ')] = '<sp>'
    charset_out = deepcopy(charset)
    if not 'blank' in charset:
        charset.append('blank')
        if first_blank:
            charset_out.insert(0, 'blank')
        else:
            charset_out.append('blank')
    elif first_blank:
        charset_out.remove('blank')
        charset_out.insert(0, 'blank')
    else:
        charset_out.remove('blank')
    if duplicate_labels is not None:
        for key in duplicate_labels:
            for l in duplicate_labels[key]:
                if l not in charset_out:
                    charset_out.append(l)
    for l in charset_out:
        str_charset += l + ' '
    str_charset = str_charset[:-1]
    #print(str_charset)
    # Get index in out_probs for each label
    all_duplicate = []
    if duplicate_labels is not None:
        for key in duplicate_labels:
            all_duplicate += [elmt for elmt in duplicate_labels[key]]
    idx_charset = []
    for ch in charset_out:
        if ch in all_duplicate:
            for key in duplicate_labels:
                if ch in duplicate_labels[key]:  # warning: make sure that a label is not in several key
                    idx_charset.append(charset.index(key))
        else:
            idx_charset.append(charset.index(ch))
    print(str_charset)
    if file_charset is not None:
        with open(file_charset, 'w') as fch:
            for ch in str_charset.split(' '):
                fch.write(ch + '\n')

    # Create a string with the probabilities (with the requested format)
    str_probs = ''
    nb_probs = len(out_probs) if step_range==1 else int(len(out_probs)/step_range)
    for idx_pr in range(nb_probs):
        pr_data = out_probs[idx_pr * step_range]
        T, D = pr_data.shape

        if save_format=='kaldi':
            if len(name_data) > 0:
                #str_probs += name_data[idx_pr][0].split('/')[-1] + ' [ ' + '\n' # THIS WAS OLD
                str_probs += name_data[idx_pr].split('/')[-1] + ' [ ' + '\n' # THIS IS NEW
            else:
                str_probs += str(idx_pr) + ' [ ' + '\n'

        last_index = -1
        for t in range(T):
            if log_prob:
                str_probs += '{:s}'.format(
                    ' '.join(['{:.6f}'.format(np.log10(max(pr_data[t,d], eps))) for d in idx_charset])) + '\n'
            else:
                str_probs += '{:s}'.format(' '.join(['{:.6f}'.format(pr_data[t,d]) for d in idx_charset])) + '\n'

        if save_format == 'kaldi':
            str_probs = str_probs[:-1] + ' ]' + '\n'  # -1 to remove \n


    if save_whole_document:
        str_probs = str_probs[:-1] + ' ]' + '\n'

    #str_out = str_charset + '\n' + str_probs
    str_out = str_probs
    with open(name_save, 'w') as fsave:
        fsave.write(str_out)
    return str_out



def plot_probas(probas, charset, save_dir=None):
    """ Save a plot containing the proba values according to the time frame """

    import matplotlib.pyplot as plt

    for k,prob_data in enumerate(probas[:4]):
        T = prob_data.shape[0]
        x = [i for i in range(T)]

        p = []
        for i in range(prob_data.shape[1]):
            p_data, = plt.plot(x, prob_data[:, i])
            p.append(p_data)
        plt.legend(p, charset)
        # p1 = plt.plot(x, prob_data[:, 0])
        # p2 = plt.plot(x, prob_data[:, 1])
        # plt.legend([p1, p2], ['1', '2'])#charset[0], charset[1]])
        plt.show()
        if save_dir is not None:
            plt.savefig(save_dir + 'plot_' + str(k) + '.png')



def save_set_expe(save_dir, set_expe, files_data=None, batch_size=2, name_expe=None, \
                  get_loss=False, get_prob=False, verbose=True,\
                  list_of_it=None, save_gt=False, global_save=False, \
                  log_path=None, dir_weights=None, dir_data=None, \
                  save_output=True, plot_output = False, duplicate_labels=None):

    if name_expe is not None:
        assert(len(set_expe)==len(name_expe))
    if get_prob:
        if log_path is None or dir_weights is None or dir_data is None or files_data is None:
            print("Can not save probabilities, there is an issue with at least one given structure.")
            return

    all_loss = [] # global structure to compare several experiments or iterations
    all_metrics = {} # global structure to compare several experiments or iterations
    if get_eval is not None:
        for key in get_eval:
            all_metrics[key] = []

    for num_exp, expe in enumerate(set_expe):

        if files_data is not None:
            dataset = DatasetBatch(files_data[num_exp], train_size=batch_size, valid_size=batch_size, test_size=batch_size)
            dataset.load()
        step_range = 2 if batch_size == 1 else 1 # as in case of a batch size of 1, data must be duplicated to avoid an issue in CTC tensorflow

        name_exp = name_expe[num_exp] if name_expe is not None else 'expe_' + str(num_exp)

        if get_prob:
            order_w, out_idx = order_weights(dir_weights[num_exp], list_of_it)
        if get_loss:
            all_loss.append([])
        if get_eval is not None:
            for key in all_metrics:
                all_metrics[key].append([])


        for it, exp_it in enumerate(expe):

            if list_of_it is not None:
                name_exp += '_' + str(list_of_it[it])
            else:
                name_exp += '_' + str(it)
            tr_it_expe = exp_it[0] # training iteration that has been evaluate

            for key_set in exp_it[1]:

                if files_data is not None:
                    if 'test' in key_set:
                        name_data = dataset.test_files
                    elif 'valid' in key_set:
                        name_data = dataset.valid_files
                    elif 'train' in key_set:
                        name_data = dataset.train_files
                    else:
                        name_data = []
                else:
                    name_data = []

                if verbose:
                    print(key_set)

                for key_eval in exp_it[1][key_set]:

                    file_exp = name_exp + '_' + key_set + '_' + key_eval + '.txt'

                    if not global_save:
                        if key_eval == 'proba':
                            charset = load_model(log_path[num_exp], order_w[it], dir_data[num_exp], dataset).charset
                            if plot_output:
                                plot_probas(exp_it[1][key_set][key_eval], charset, save_dir=save_dir)
                            if save_output:
                                save_proba(save_dir + file_exp, exp_it[1][key_set][key_eval], charset,\
                                           duplicate_labels=duplicate_labels, step_range=step_range, \
                                           name_data=name_data)
                        elif key_eval == 'pred':
                            save_pred(save_dir + file_exp, exp_it[1][key_set][key_eval], \
                                      name_data=name_data, print_eval=plot_output, save_gt=save_gt, step_range=step_range)
                        elif key_eval == 'loss':
                            save_loss(save_dir + file_exp, exp_it[1][key_set][key_eval], \
                                      name_data=name_data, print_eval=plot_output, step_range=step_range)
                            all_loss[-1].append(exp_it[1][key_set][key_eval][0])
                        elif key_eval == 'eval':
                            save_eval(save_dir + file_exp, exp_it[1][key_set][key_eval], print_eval=plot_output)
                            if 'loss' in get_eval:
                                all_metrics['loss'][-1].append(exp_it[1][key_set][key_eval][0][0])
                            # elif 'cer' in get_eval:
                            #     all_metrics['cer'][-1].append(np.mean(exp_it[1][key_set][key_eval][get_eval.index('cer')]))
                            # elif 'ser' in get_eval:
                            #     all_metrics['ser'][-1].append(np.mean(exp_it[1][key_set][key_eval][get_eval.index('cer')]))
                            # elif 'wer' in get_eval:
                            #     all_metrics['wer'][-1].append(np.mean(exp_it[1][key_set][key_eval][get_eval.index('cer')]))
                            # elif 'ler' in get_eval:
                            #     all_metrics['ler'][-1].append(np.mean(exp_it[1][key_set][key_eval][get_eval.index('cer')]))



def main_eval(log_path, dir_weights, dir_data, set_to_test=['test'], batch_size=2, save_dir=None, list_of_it=None,\
                 get_pred=True, get_eval=None, get_prob=False, get_loss=False, rnn_size=[100, 100, 100, 100],\
                save_gt=False, global_save=False, duplicate_labels=None,\
                 dir_dataset_train=None, greedy_decode=False, top_paths_decode=1, beam_width_decode=100, \
                 optimizer=Adam(lr=0.0001), dropout=0.1, noise_stddev=0.01, name_expe=None, verbose=False, \
              plot_output=False, save_output=True, file_to_eval=None):
    """
        Main function to evaluate a set of experiments and then saved the results

        assert that each list (log_path, dir_weights, dir_data) is of the same length:
        experiment of any index i is related to index i of each list (list_log_path, list_dir_weights, list_dir_data
        Note that they can not be a list but it can only refer to one experiment (so they are directory only)

         - list_log_path = list of directory where the model architectures are saved
         - list_dir_weights = list of directory where the model weights are saved
         - list_dir_data = list of directory where the input data that are analyzed are saved
         - set_to_test = dataset one wants to evaluate (train', 'eval' and/or 'test' sets)
         - batch_size = batch size used for the generator
         - get_pred = predictions: True or False
         - get_eval = evaluate metrics: None or a list of metrics: 'ler', 'cer', 'wer', 'ser', 'loss'
         - get_prob = probabilities: True or False
         - get_loss = loss value: True or False

         - save_dir = directory where the results will be saved (a directory or None if not saved)
         - list_of_it = list of it one wants to load (None if all the weights, -1 for the last one)
         - name_expe = name of expe (when outputs are saved) It is of length log_path if log_path is a directory, otherwise a string.

         - other parameters are not required if the model has been correctly saved, \
         otherwise it is used to define the network architecture related to the weights that will be loaded.
    """

    if not isinstance(log_path, list):
        log_path = [log_path]
    if not isinstance(dir_weights, list):
        dir_weights = [dir_weights]
    if not isinstance(dir_data, list):
        dir_data = [dir_data]

    set_expe = eval_set_exp(log_path, dir_weights, dir_data, set_to_test=set_to_test, batch_size=batch_size, list_of_it=list_of_it,\
                       get_pred=get_pred, get_eval=get_eval, get_prob=get_prob, get_loss=get_loss, rnn_size=rnn_size, \
                       dir_dataset_train=dir_dataset_train, greedy_decode=greedy_decode, top_paths_decode=top_paths_decode, beam_width_decode=beam_width_decode, \
                       optimizer=optimizer, dropout=dropout, noise_stddev=noise_stddev, verbose=verbose, file_to_eval=file_to_eval)


    if plot_output or save_output:
        save_set_expe(save_dir, set_expe, files_data=dir_data, batch_size=batch_size, name_expe=name_expe, \
                      get_loss=get_loss, get_prob=get_prob, verbose=verbose, duplicate_labels=duplicate_labels,\
                      list_of_it=list_of_it, save_gt=save_gt, global_save=global_save, \
                      log_path=log_path, dir_weights=dir_weights, dir_data=dir_data, \
                      plot_output=plot_output, save_output=save_output)


if __name__ == '__main__':
    start_time = time.time()
    the_folder_with_data = sys.argv[1]
    folder_name = the_folder_with_data.split('/')[-1]
    output_directory =  '/home/scr/person/invite/rojasand/ocr/text_recognizer/ocr_ctc/outputs/results/coulisse_h32_base_1899_1924_15000/' #expe_Andres_coulisse_1_curated
    #output_directory = '/home/scr/person/invite/rojasand/1924_february_page2/column_8_output'
    # NAME DIRECTORY RESULTS AND CHECKPOINTS
    name_model = 'coulisse_h32_base_1899_1924_15000/'  #'expe_Andres_coulisse_1_curated/'  #'expe_Andres/'#'PlaIR1158_it2' #RIMESconv7drop-256_pad64_32_4/'#'READdbG_conv11drop256_pad96_64_4/'#'READdbconv7drop256_64_32_4/'#RIMESconv_m2g1000_64_16_4_pad_256x2'#RIMESconv_m2g1000_64_16_4_pad_256x2/'
    sup_dir = 'ocr_ctc/'
    list_log_path = ['./' + sup_dir + 'outputs/results/' + name_model + '/']#, './' + sup_dir + 'outputs/results/' + name_model + '/']
    list_dir_weights = ['./' + sup_dir + 'outputs/checkpoints/' + name_model + '/']#, './' + sup_dir + 'outputs/checkpoints/' + name_model + '/']
    # list_log_path = ['./' + sup_dir + 'outputs/results/READdb_only1-35013_trfArtPolyA_conv7drop256_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdb_only1-35015_trfArtPolyA_conv7drop256_pad64_32_4/']
    # list_dir_weights = ['./' + sup_dir + 'outputs/checkpoints/READdb_only1-35013_trfArtPolyA_conv7drop256_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdb_only1-35015_trfArtPolyA_conv7drop256_pad64_32_4/']
    # list_log_path = ['./' + sup_dir + 'outputs/results/READdbArt30866_trf16only_conv7-drop_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdbArt30882_trf16only_conv7-drop_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdbArt30893_trf16only_conv7-drop_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdbArt35013_trf16only_conv7-drop_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdbArt35015_trf16only_conv7-drop_pad64_32_4/']
    # list_dir_weights = ['./' + sup_dir + 'outputs/checkpoints/READdbArt30866_trf16only_conv7-drop_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdbArt30882_trf16only_conv7-drop_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdbArt30893_trf16only_conv7-drop_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdbArt35013_trf16only_conv7-drop_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdbArt35015_trf16only_conv7-drop_pad64_32_4/']
    # list_log_path = ['./' + sup_dir + 'outputs/results/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/',
    #                   './' + sup_dir + 'outputs/results/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/']
    # list_dir_weights = ['./' + sup_dir + 'outputs/checkpoints/READdbGArtPolyA_trf_conv7drop256_pad64_32_4',
    # #                      './' + sup_dir + 'outputs/checkpoints/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/']
    # list_log_path = ['./' + sup_dir + 'outputs/results/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/',
    #                  './' + sup_dir + 'outputs/results/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/']
    # list_dir_weights = ['./' + sup_dir + 'outputs/checkpoints/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/',
    #                     './' + sup_dir + 'outputs/checkpoints/READdbGArtPolyA_trf_conv7drop256_pad64_32_4/']

    # NAME DATA
    #COTE-SYNDICAT_19240104_02-designation_des_valeursconvCOTE-SYNDICAT_19240104_02-designation_des_valeurs_pad64_32_4
    #preparation_data_test_ocrconv_coulisse_exp_1_test_curated_pad64_32_4
    list_dir_data = [the_folder_with_data] #['../datasets/PlaIR/PlaIRconv_test1158_pad64_32_4/']#,
                     #'../datasets/READ18/READconvG_pad64_32_4/', '../datasets/READ18/READconvG_pad64_32_4/', '../datasets/READ18/READconvG_pad64_32_4/', '../datasets/READ18/READconvG_pad64_32_4/']#, '../datasets/READ18/READconv_test_APp30882_pad64_32_4/', '../datasets/READ18/READconv_test_APp30893_pad64_32_4/']#, '../datasets/READ18/READconv_test_ArtPoly0p35015_pad64_32_4/']#, '../datasets/READ18/READconv_test_raw30882_pad64_32_4/']#, '../datasets/READ18/READconv_test_raw35015_pad64_32_4/'] # PlaIR/PlaIRconv431_64_16_4_pad/']#RIMES/RIMESconv_m2g1000_pad64_16_4/'] # RIMESconv_m2g1000_pad64_16_4
    # list_dir_data = ['../datasets/READ18/READconv_test_raw30866_pad64_32_4/',
    #                   '../datasets/READ18/READconv_test_raw30882_pad64_32_4/',
    #                   '../datasets/READ18/READconv_test_raw30893_pad64_32_4/', '../datasets/READ18/READconv_test_raw35013_pad64_32_4/',
    #                   '../datasets/READ18/READconv_test_raw35015_pad64_32_4/']  # , '../datasets/READ18/READconv_test_APp30882_pad64_32_4/', '../datasets/READ18/READconv_test_APp30893_pad64_32_4/']#, '../datasets/READ18/READconv_test_ArtPoly0p35015_pad64_32_4/']#, '../datasets/READ18/READconv_test_raw30882_pad64_32_4/']#, '../datasets/READ18/READconv_test_raw35015_pad64_32_4/'] # PlaIR/PlaIRconv431_64_16_4_pad/']#RIMES/RIMESconv_m2g1000_pad64_16_4/'] # RIMESconv_m2g1000_pad64_16_4

    # INFORMATION ABOUT EXPERIMENT - EVALUATION (that will be put in the evaluation file name).
    name_expe = ['epoch24_'+folder_name] #, '_test_30882-0conv7',
                 # '_test_30893-0conv7', '_test_35013-0conv7',
                 # '_test_35015-0conv7']#,
                 #'valid35015only4G']#, 'testspec_APp_30882Gx', 'testspec_APp_30893Gx']#, 'testspec_AP0p_35015only1']#, 'testspec30883']#, 'test30882']
    # OUTPUT DIRECTORY (by default this is in results)
    #name_expe = ['ERASE_THIS_TEST_'+entry.name] # ERASE THIS PART ----------------<-<-<-<-<-<-<-<->>>>>-------------------'(-è_ç_è-('"'(-è_çàç_è-( EERRAAASSSEEEE THISS
    save_dir = './' + sup_dir + 'outputs/results/' + name_model + '/'
    # DATASET TO EVALUATE
    set_to_test = ['test']
    # BATCH SIZE
    #batch_size = 64 # This was the value before = 1
    batch_size = int(sys.argv[2])
    batch_counter = 0
    for lentry in os.scandir(os.path.join(the_folder_with_data,'TEST')):
        batch_counter += 1
        if batch_counter > batch_size:
            break
    if batch_counter < batch_size:
        batch_size = batch_counter
    # LIST OF TRAINING ITERATIONS THAT WILL BE EVALUATED (default = -1 for the last one, None = all)
    list_of_it = [23] #[127] # -1 = last it, None = all

    # GET PREDICTIONS, EVALUATIONS, PROBAS or LOSS - WARNING: CER and WER are not performed in TF this can be time consuming.
    get_pred = True # True or False
    get_eval = None #['cer', 'wer']#, 'cer'] #['ler', 'cer', 'wer', 'ser'] # None or a list of metrics: 'ler', 'cer', 'wer', 'seqr', 'loss', 'cer_analysis'
    get_prob = True # True or False
    #if 'designation' in entry.name:
    #    get_prob = True
    get_loss = False # True or False

    file_to_eval = None#('/home/soullard/tmp/predREAD_wassim.txt', '/home/soullard/code/datasets/READ18/testG.txt')# default None, otherwise a tuple (predfile_name
    verbose = True
    save_gt = False # save ground truth (in some cases such as predictions)
    global_save = False # save one result for all the evaluation (False for evaluation of one iteration)
    plot_output = False # only for proba for the moment
    save_output = True # save the evaluation

    # default parameters regarding CTC: greedy = True, beam width = 100, top paths = 1
    greedy_decode = True
    beam_width_decode = 100
    top_paths_decode = 1
    # default parameter for proba
    duplicate_labels = None # labels for which proba are duplicated at save time

    # Parameters regarding the RNN architecture are not required if it has been saved properly
    #os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    main_eval(list_log_path, list_dir_weights, list_dir_data, set_to_test=set_to_test, batch_size=batch_size,\
              save_dir=save_dir, list_of_it=list_of_it, file_to_eval=file_to_eval,\
              get_pred=get_pred, get_eval=get_eval, get_prob=get_prob, get_loss=get_loss,\
              rnn_size=[100, 100, 100, 100], save_gt=save_gt, global_save=global_save,\
              dir_dataset_train=None, greedy_decode=greedy_decode, top_paths_decode=top_paths_decode, beam_width_decode=beam_width_decode, \
              optimizer=Adam(lr=0.0001), dropout=0.1, noise_stddev=0.01, name_expe=name_expe, verbose=verbose, \
              save_output=save_output, plot_output=plot_output, duplicate_labels=duplicate_labels)
    end_time = time.time()
    print("TIME TAKEN: ",end_time - start_time)

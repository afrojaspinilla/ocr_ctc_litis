from ocr_ctc.preprocessings.BatchPreprocessing import BatchPreprocessing

import numpy as np


class To2DSequence(BatchPreprocessing):

    def __init__(self, nb_cols=6, stride=1, pad=False):
        self.nb_cols = nb_cols if nb_cols>0 else 1
        self.stride = stride if stride>0 else 1
        self.pad = pad


    def __call__(self, x, y, seq_len, label_len, padding):
        x_ret = []
        seq_len_ret = []
        for doc in range(len(x)):

            pad = False
            sh = x[doc].shape

            index_start_windows = [idx_start_window for idx_start_window in range(0, sh[0], self.stride)]
            selected_start_windows = [start_window for start_window in index_start_windows if start_window <= sh[0] - self.nb_cols + 1]
            nb_windows = len(selected_start_windows)

            if len(selected_start_windows)==0:
                x_ret.append([])
                seq_len_ret.append(0)
                continue

            new_len = selected_start_windows[-1]

            if self.pad: # padding less than the half of the window
                pad_elmt = int((max([start_window for start_window in index_start_windows if start_window < sh[0] - self.nb_cols/2]) - new_len) / self.stride)
                total_len = nb_windows + 2*pad_elmt
            else:
                pad_elmt = 0
                total_len = nb_windows

            new_arr = np.zeros((total_len, self.nb_cols, sh[1], 1), "float32")
            index_window = 0

            # padding at the beginning of the sequence
            if pad_elmt > 0:
                for i in range(-pad_elmt, 0, self.stride):

                    for j in range(self.nb_cols):
                        if i + j >= 0:
                            vect_window = x[doc][i + j]
                        else:
                            vect_window = [padding for k in range(sh[1])]

                        new_arr[index_window][j] = np.reshape(vect_window, (sh[1], 1))

                    index_window += 1



            # build sequence of windows for complete sub-images (without padding)
            for i in range(0, new_len, self.stride):

                for j in range(self.nb_cols):
                    vect_window = x[doc][i + j]
                    new_arr[index_window][j] = np.reshape(vect_window, (sh[1], 1))
                    #new_arr[int(i / self.stride) + pad_elmt][j] = np.reshape(vect_window, (sh[1], 1))

                index_window += 1



            # padding at the end of the sequence
            if pad_elmt > 0:
                for i in range(pad_elmt):

                    for j in range(self.nb_cols):
                        if new_len + i + j < sh[0]:
                            vect_window = x[doc][new_len + i + j]
                        else:
                            vect_window = [padding for k in range(sh[1])]

                        new_arr[index_window][j] = np.reshape(vect_window, (sh[1], 1))

                    index_window += 1


            x_ret.append(new_arr)
            seq_len_ret.append(total_len)

        return x_ret, y, seq_len_ret, label_len, padding

    def __str__(self):
        return "To 2D Sequence"




# if __name__ == '__main__':
#
#     data =[np.asarray([[j/10 for i in range(3)] for j in range(6)]), np.asarray([[j/10 for i in range(3)] for j in range(8)])]
#
#     nb_cols= 20
#     stride = 2
#     conv_pad = True
#     preproc = To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad)
#     x= preproc(data, [0,0], [0,0], [0,0], 300)
#
#     print("ok")
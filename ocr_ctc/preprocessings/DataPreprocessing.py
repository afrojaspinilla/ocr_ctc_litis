# encoding=utf-8
class DataPreprocessing:
    """
    Class facilitant l'ajout de preprocessings
    
    Chaque DataPreprocessing sera appelé sur chaque 
    donnée, une à une
    """
    def __init__(self, **kwargs):
        raise NotImplementedError

    def __call__(self, image, label):
        """
        Appel du preprocessing
        
        :param image: Image d'entrée
        :param label: Label de l'image
        :return: Tuple (new_imge, new_label)
        """
        raise NotImplementedError

    def __str__(self):
        return "DataPreprocessing"


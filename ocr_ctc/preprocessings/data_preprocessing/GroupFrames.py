import numpy as np

from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class GroupFrames(ImagePreprocessing):
    """
    Regroupe les colonnes de pixels en frames de nb_cols
    """
    def __init__(self, **kwargs):
        self.nb_frames = kwargs.get("nb_col", 4)
        self.padding = kwargs.get("padding", -1.)

    def preprocessing(self, image):
        extra = (image.shape[0] % self.nb_frames)

        if extra is 0:
            return np.reshape(image, [int(image.shape[0] / self.nb_frames), image.shape[1] * self.nb_frames])

        reshaped = image[:-extra]
        reshaped = np.reshape(reshaped, [int(image.shape[0] / self.nb_frames), (image.shape[1] * self.nb_frames)])
        to_pad = image[-extra:]
        to_pad = np.pad(to_pad, ((0, self.nb_frames - extra),(0,0)), constant_values=self.padding, mode='constant')
        to_pad = np.reshape(to_pad, [1, image.shape[1] * self.nb_frames])

        return np.concatenate((reshaped,to_pad))

    def __str__(self):
        return "Frames grouped : "+str(self.nb_frames) + ", padding " + str(self.padding)




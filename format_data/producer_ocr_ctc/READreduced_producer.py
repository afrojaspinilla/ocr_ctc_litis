# encoding=utf-8
import getopt
import sys

import numpy as np
from PIL import Image
from keras.preprocessing import sequence
from random import shuffle


from ocr_ctc.dataset_manager import DatasetBatch
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Normalize import Normalize
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose


def produce_dataset(data_dir, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=[], preprocessings_batch=[],
                    padding=-1., norm=64):
    """ TRAIN-B is used for the training and TRAIN-A for the valid and test """
    train_files = []
    test_files = []
    valid_files = []
    label_dict = {}
    label_array = []

    cur_label = 0

    # load charset
    file_char_reduced = data_dir + "read2017charsetReduceascii.txt"

    # Read the labels which will be considered in training
    with open(file_char_reduced) as f:
        for line in f.readlines():
            if line is "\n":
                continue

            list_lab = line.split(' ')
            for lab in list_lab:
                if lab not in label_dict:
                    label_dict[lab] = cur_label
                    label_array.append(lab)
                    cur_label += 1


    gtdir = "trackB-GT-ite1Ascii.idf" # labeling of the ground truth

    # train set
    with open(data_dir + 'Train-B/' + gtdir, "r") as f_train: # encoding="ISO-8859-1"
        for line in f_train.readlines():

            if line is "\n":
                continue

            data = line.replace('\n', '').replace('\r', '').split(' ')
            ref_data = data[0].split('_')
            data_file = data_dir + 'Train-B/img/' + ref_data[0] + ".tif"
            label = data[1:]
            train_files.append((data_file, label))

            for i,lab in enumerate(label.reverse()):
                if lab not in label_dict:
                    del label[i]
                    # label_dict[i] = cur_label
                    # label_array.append(i)
                    # cur_label += 1


    gtdir = "trackA-GroundTruthAscii.txt"

    # validation set
    with open(data_dir + 'Train-A/' + gtdir, "r") as f_valid:
        for line in f_valid.readlines():
            data = line.replace('\n', '').replace('\r', '').split(' ')
            ref_data = data[0].split('_')
            label = data[1:]
            data_file = data_dir + 'Train-A/img/' + ref_data[0] + ".tif"
            valid_files.append((data_file, label))



    shuffle(train_files)
    #shuffle(test_files)
    shuffle(valid_files)

    dataset = DatasetBatch(output_path)
    dataset.new_dataset(label_array=label_array, label_dict=label_dict, padding=padding)

    def __prepare_set(file_list, index, size, padding, norm):
        x = []
        y = []
        x_length = []
        y_length = []
        try:
            for j in range(size):
                true_label = []
                output_path, label = file_list[index + j]
                for i in label:
                    true_label.append(label_dict[i])
                image = Image.open(output_path)
                image = image.convert('L')
                width, _ = image.size
                image = image.resize((width, norm), Image.ANTIALIAS)
                image = np.array(image)

                for preproc in preprocessings_data:
                    image, true_label = preproc(image, true_label)
                seq_len = image.shape[0]
                label_len = len(true_label)

                x.append(image)
                y.append(true_label)
                x_length.append(seq_len)
                y_length.append(label_len)
        except IndexError:
            print("Ignoring last batch...")

        for preproc_batch in preprocessings_batch:
            x, y, x_length, y_length, padding = preproc_batch(x, y, x_length, y_length, padding)

        x = sequence.pad_sequences(x, value=float(padding), dtype='float32', padding="post")
        y = sequence.pad_sequences(y, value=float(len(label_array)), dtype='float32', padding="post")

        return x, y, np.asarray(x_length), np.asarray(y_length)

    for i in range(0, len(train_files), train_size):
        dataset.save_train_batch(__prepare_set(train_files, i, train_size, padding, norm))

    for i in range(0, len(test_files), test_size):
        dataset.save_test_batch(__prepare_set(valid_files, i, test_size, padding, norm))

    for i in range(0, len(valid_files), valid_size):
        dataset.save_valid_batch(__prepare_set(valid_files, i, valid_size, padding, norm))

    dataset.save()


if __name__ == '__main__':
    opts, _ = getopt.getopt(sys.argv[1:], '', ["input=", "out=", "norm="])
    input_path = output_path = None
    input_path = "datasets/READ/"
    nb_col = 10
    padding = -1.
    train_size = 64
    test_size = 64
    valid_size = 64
    norm = 64

    for opt, arg in opts:
        if '--input' in opt:
            input_path = arg
        elif '--out' in opt:
            output_path = arg
        elif '--norm' in opt:
            norm = int(arg)


    if not input_path or not output_path:
        exit()

    preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_col, padding=padding), Normalize()]

    produce_dataset(input_path, output_path,
                    train_size, valid_size, test_size,
                    preprocessings_data=preprocessing_data, preprocessings_batch=[],
                    padding=padding, norm=norm)

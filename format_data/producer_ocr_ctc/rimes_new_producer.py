from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose


def main(input_path, dir_origin="", \
         dir_line_images='', str_info='', \
         dir_origin_images='../../../data/RIMES_paragraph/all_im/', \
         dir_baselines="", files_info_lab=None, charset=None, \
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, ext_im='.png',\
         window_structure='vertical', shuff=True, padding=-1, standardize=True, ext_origin='xml', \
         mean_stand=None, std_stand=None):

    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + "RIMESconv"  + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []

        output_path = input_path + "RIMES" + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)


    if files_info_lab is None:
        files_info_lab = {"train": "train.txt", "valid": "valid.txt", "test": "test.txt"}


    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding, label_array=charset,
                        nb_cols=nb_cols, standardize=standardize,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        early_preprocessing_batch=early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(splitter_label="",\
                                          sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TST/"}, \
                                          files_info_lab=files_info_lab, mean_stand=mean_stand, std_stand=std_stand, \
                                          ext_images=".Gray.png", removed_empty=removed_empty)


if __name__ == '__main__':

    nb_cols = 32 # number of time frames that are considered at each time step
    stride = 4
    padding = -1.
    norm = 128 # height dimension
    keep_ratio = True # resize the image by keeping the ratio
    conv = True # format data for a conv LSTM network
    conv_pad = True # do padding in the extremities of the images built for the conv LSTM network
    ImageFrombaseline = False # image is build according to the baseline (not available here)
    window_structure = 'vertical' # parameter for ImageFromBaseline
    load_charset = None # load a specific charset, WARNING do it for transfer learning

    input_path = "../datasets/RIMES/"

    shuff=False # mix the data before saved them

    str_info = ''

    files_info_lab = {"train": "train.txt", \
                      "valid": "valid.txt", \
                      "test": "test.txt"}

    mean_stand = None # None = default, this is computed on the training set
    std_stand = None

    if load_charset:
        charset = []
        with open(input_path + 'charset.txt') as f:
            for line in f.readlines():
                charset.append(line.rstrip('\n'))
    else:
        charset = None


    main(input_path, \
         h_norm=norm, nb_cols=nb_cols, stride=stride, str_info=str_info, charset=charset,\
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
         window_structure=window_structure, shuff=shuff, padding=padding, files_info_lab=files_info_lab,\
         mean_stand=mean_stand, std_stand=std_stand)


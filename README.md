# OCR-CTC LITIS version Docker

An OCR-CTC developped by LITIS laboratory, more specifically for the HBDEX and EURHISFIRM project

## Installation

Use docker to install and un the container.
Then mount the folder to the container to execute the code.

```bash

cd /path/to/this/folder

service docker start # start docker

# build the container with the "ocr_ctc" name
docker build -t ocr_ctc .

# check that the image container "ocr_ctc" exits
docker image ls

# run the container bash with the current directory mounted as /test
docker run -it -v $(pwd):/test -w /test ocr_ctc bash #

# run a test
python main.py -r hbdex_test_config.toml

# check the results in the json_output folder
ls json_output
```

## Usage

The `config_template.toml` is the file to be configured to treat the desired corpus (images and jsons)

Check `python main.py -h` for more help

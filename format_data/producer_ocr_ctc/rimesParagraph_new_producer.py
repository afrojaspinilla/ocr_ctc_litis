from ocr_ctc.dataset_manager.Producer import Producer
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequence import To2DSequence
from ocr_ctc.preprocessings.batch_preprocessing.ImagesFromBaseline import ImagesFromBaseline
from ocr_ctc.preprocessings.batch_preprocessing.To2DSequenceFromBaseline import To2DSequenceFromBaseline
from ocr_ctc.preprocessings.data_preprocessing.GroupFrames import GroupFrames
from ocr_ctc.preprocessings.data_preprocessing.Transpose import Transpose
import os
import shutil
from lxml import etree


def get_transcription_xml(input_xml):

    list_trans = []

    try:
        tree = etree.parse(input_xml)

        for line in tree.findall(".//TextLine"):
            transcript = line.find(".//Unicode")
            list_trans.append(transcript)
    except:
        print("Can not read xml, or there is no xml file.")

    return list_trans


def get_transcriptionMatch_txt(input_txt):

    list_trans = []
    list_num = []

    with open(input_txt, 'r', encoding='utf-8') as f:
        for line in f.readlines():
            sline = line.split(' ')
            list_num.append(int(sline[1]))
            lab = ' '.join(sline[3:])
            list_trans.append(lab)

    return list_trans, list_num


def create_train_valid_test(input_path, dir_origin="../../../../../code/segmenteurFCN/xml/allRIMES/", \
                            dir_line_images='../../../data/RIMES_paragraph/line_images0.5/', ext_im='.png', ext_origin='xml'):

    if not os.path.exists(input_path):
        os.mkdir(input_path)

    # Read transcriptions from xml
    list_transcript = []
    for my_file in [elmt for elmt in sorted(os.listdir(dir_origin)) if elmt[-3:] == ext_origin]:
        if ext_origin == 'xml':
            transcript = get_transcription_xml(dir_origin + my_file)
            for k, trans in enumerate(transcript):
                name_file = '.'.join(my_file.split('.')[:-1])
                list_transcript.append((name_file + '_' + str(k), trans))
        else:
            transcript, nums = get_transcriptionMatch_txt(dir_origin + my_file)
            for k, trans in enumerate(transcript):
                name_file = '.'.join(my_file.split('.')[:-1])
                list_transcript.append((name_file + '_' + str(nums[k]), trans))
    #shuffle(list_transcript)


    # create training / valid and test sets
    if not os.path.exists(input_path + '/TRAIN/'):
        os.mkdir(input_path + '/TRAIN/')
        os.mkdir(input_path + '/VAL/')
        os.mkdir(input_path + '/TST/')

    first_test = True
    first_val = True
    first_train = True

    count_val = 0
    nb_val = 1333

    for i, (name_file, labeltranscript) in enumerate(list_transcript):

        label = '' if not labeltranscript or len(labeltranscript)==0 else labeltranscript

        # line_im = Image.open(dir_line_images + name_file + ext_im)
        # line_im.show()
        # #viewer = subprocess.Popen(dir_line_images + name_file + ext_im)
        # print(label)
        # print('----')
        # #viewer.kill()
        # line_im.close()

        if 'eval' in name_file:
            shutil.copyfile(dir_line_images + name_file + ext_im, input_path + '/TST/' + name_file + ext_im)
            if first_test:
                with open(input_path + "/test.txt", "w") as test:
                    test.write(name_file + ' ' + label + '\n')
                first_test = False
            else:
                with open(input_path + "/test.txt", "a") as test:
                    test.write(name_file + ' ' + label + '\n')
        elif count_val<nb_val:
            count_val += 1
            shutil.copyfile(dir_line_images + name_file + ext_im, input_path + '/VAL/' + name_file + ext_im)
            if first_val:
                with open(input_path + "/valid.txt", "w") as valid:
                    valid.write(name_file + ' ' + label + '\n')
                first_val = False
            else:
                with open(input_path + "/valid.txt", "a") as valid:
                    valid.write(name_file + ' ' + label + '\n')
        else:
            shutil.copyfile(dir_line_images + name_file + ext_im, input_path + '/TRAIN/' + name_file + ext_im)
            if len(label) == 0:
                if os.path.exists(dir_line_images + name_file + 'txt'):
                    with open(dir_line_images + name_file + 'txt', "r") as my_file:
                        label = my_file.readlines()
            if first_train:
                with open(input_path + "/train.txt", "w") as train:
                    train.write(name_file + ' ' + label + '\n')
                first_train = False
            else:
                with open(input_path + "/train.txt", "a") as train:
                    train.write(name_file + ' ' + label + '\n')





def extract_baselines(txt_path, input_path, input_match='../../../data/RIMES_paragraph/line_images0.55matched_predictions0.55/'):#, ns = {"ns": "http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15"}):
    """ """

    baselines = []

    with open(input_path + 'baselines.txt', "w") as file_baselines:
        for name_files in ["train.txt", "valid.txt", "test.txt"]:
            with open(input_path + name_files, "r") as my_file:
                for tmp, line in enumerate(my_file.readlines()):
                    if line is "\n":
                        continue

                    file_name = line.replace("\n", "").split(" ")[0]
                    page_name = '_'.join(file_name.split("_")[:-1]) + '.txt'
                    num_line = int(file_name.split("_")[-1].split('.')[0])

                    # if input_match is not None:
                    #     with open(input_match + page_name, "r") as match_f:
                    #         match_lines = match_f.readlines()
                    #     stop = False
                    #     k=0
                    #     while k < len(match_lines) and not stop:
                    #         num_ref = int(match_lines[k].split(' ')[1]) # number of the baseline
                    #         if num_ref == num_line:
                    #             stop = True
                    #             new_num = int(match_lines[k].split(' ')[2])
                    #             if new_num != num_line:
                    #                 print(page_name, ": num line change from ", num_line, " to ", new_num)
                    #                 num_line2 = new_num
                    #         k += 1

                    with open(txt_path + page_name, "r") as txt_bl:
                        all_bl = txt_bl.readlines()

                    related_bl = all_bl[num_line].split(';')
                    baseline = [(float(elmt.split(',')[0]), float(elmt.split(',')[1])) for elmt in related_bl]
                    str_baseline = all_bl[num_line]
                    # try:
                    #     related_bl2 = all_bl[num_line2].split(';')
                    #     baseline2 = [(float(elmt.split(',')[0]), float(elmt.split(',')[1])) for elmt in related_bl]
                    # except:
                    #     print("can not match 2")
                    #     baseline2 = baseline
                    # page_im = page_name.split('.')[0] + '.png'
                    # page_im = Image.open('../../../data/RIMES_paragraph/all_im/' + page_im)
                    # drawtxt = ImageDraw.Draw(page_im)
                    # drawtxt.line(baseline, width=2, fill='blue')
                    # drawtxt = ImageDraw.Draw(page_im)
                    # drawtxt.line(baseline2, width=2, fill='red')
                    # page_im.show()
                    # label = ' '.join(line.replace("\n", "").split(" ")[1:])
                    # print(label)

                    file_name_out = page_name + '_' + str(num_line)
                    baselines.append(baseline)
                    ssplit = str_baseline.split(' ')
                    if ssplit[-1] == '\n':
                        file_baselines.write(file_name_out + ' ' + str_baseline)
                    else:
                        file_baselines.write(file_name_out + ' ' + str_baseline + '\n')


    return baselines


def getPageImages(input_path, dir_origin_images, ext_im='.png'):

    out_list_pages = []

    for name_files in ["train.txt", "valid.txt", "test.txt"]:
        with open(input_path + name_files, "r") as my_file:
            for line in my_file.readlines():
                if line is "\n":
                    continue

                file_name = '_'.join(line.replace("\n", "").split(" ")[0].split('_')[:-1]) + ext_im
                out_list_pages.append(dir_origin_images + file_name)

    return out_list_pages


def main(input_path, dir_origin="../../../../../code/segmenteurFCN/xml/allRIMES/", \
         dir_line_images='../../../data/RIMES_paragraph/line_images0.5/', \
         dir_origin_images='../../../data/RIMES_paragraph/all_im/', \
         dir_baselines="../../../code/segmenteurFCN/xml/allRIMES2/", \
         h_norm=100, removed_empty=True, dict_dir=None, nb_cols=1, stride=1,\
         keep_ratio=True, conv=False, conv_pad=False, ImageFrombaseline=False, \
         window_structure='vertical', shuff=True, padding=-1, standardize=True, ext_origin='xml'):

    if not os.path.exists(input_path):
        create_train_valid_test(input_path, dir_origin=dir_origin, dir_line_images=dir_line_images, ext_origin=ext_origin)


    if ImageFrombaseline:
        shuff = False
        if os.path.exists(input_path + 'baselines.txt'):
            with open(input_path + 'baselines.txt', "r") as file_baselines:
                baselines = []
                for line in file_baselines.readlines():
                    if line is "\n":
                        continue
                    bl = []
                    ls = line.split(" ")
                    for elmt in ls[1:]:
                        points = elmt.split(';')
                        for pts in points:
                            x, y = pts.split(",")
                            bl.append((float(x), float(y)))
                    baselines.append(bl)
        else:
            baselines = extract_baselines(dir_baselines, input_path)

    str_info = ''
    if conv:
        preprocessing_data = [Transpose()]
        preprocessing_batch = []
        early_preprocessing_batch = []
        if ImageFrombaseline:
            page_images = getPageImages(input_path, dir_origin_images)
            if window_structure == 'vertical':
                early_preprocessing_batch.append(
                    ImagesFromBaseline(baselines, page_images, height=h_norm, keep_ratio=keep_ratio))
                preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
            else:
                early_preprocessing_batch.append(To2DSequenceFromBaseline(baselines, page_images, height=h_norm, nb_cols=nb_cols, stride=stride, pad=conv_pad, win_structure=window_structure))
            #h_norm = 0
            #standardize=False
            str_info += 'IFB' + window_structure + '_'
        else:
            preprocessing_batch.append(To2DSequence(nb_cols=nb_cols, stride=stride, pad=conv_pad))
        str_info += "_pad" if conv_pad else ""
        output_path = input_path + "RIMESconv_"  + str_info + str(h_norm) + "_" + str(nb_cols) + "_" + str(stride)

    else:
        preprocessing_data = [Transpose(), GroupFrames(nb_col=nb_cols, padding=padding)]
        early_preprocessing_batch = []
        preprocessing_batch = []
        if ImageFrombaseline:
            page_images = getPageImages(input_path, dir_origin_images)
            early_preprocessing_batch.append(ImagesFromBaseline(baselines, page_images, height=h_norm, keep_ratio=keep_ratio))
            str_info += 'IFB_'

        output_path = input_path + "RIMES_" + str_info + str(h_norm) + "_" + str(nb_cols)

    producer = Producer(input_path, output_path,
                        norm=h_norm, keep_ratio=keep_ratio,
                        padding=padding,
                        nb_cols=nb_cols, standardize=standardize,
                        preprocessing_data=preprocessing_data,
                        preprocessing_batch=preprocessing_batch,
                        early_preprocessing_batch=early_preprocessing_batch,
                        shuff = shuff
                        ).produce_dataset(sup_dir={"train": "TRAIN/", "valid": "VAL/", "test": "TST/"}, \
                                          ext_images=".png", removed_empty=removed_empty)


if __name__ == '__main__':
    nb_cols = 4
    stride = 1
    padding = 255.
    norm = 64
    keep_ratio = True
    conv = False
    conv_pad = False
    ImageFrombaseline = True
    window_structure = 'vertical'

    input_path = "../../datasets/RIMES_paragraph/RIMES_paragraph3_0.55_lab/"
    txt_path = "../../../../../code/segmenteurFCN/xml/allRIMES2/"
    dir_origin_images = '../../../../../data/RIMES_paragraph/all_im/'
    dir_line_images = '../../../../../data/RIMES_paragraph/line_images3_0.55/'

    shuff=False

    main(input_path, dir_origin=txt_path, dir_line_images=dir_line_images, \
         dir_origin_images=dir_origin_images, h_norm=norm, nb_cols=nb_cols, stride=stride,\
         keep_ratio=keep_ratio, conv=conv, conv_pad=conv_pad, ImageFrombaseline=ImageFrombaseline, \
         window_structure=window_structure, shuff=shuff, padding=padding)





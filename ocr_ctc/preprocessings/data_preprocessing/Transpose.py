from ocr_ctc.preprocessings.data_preprocessing.ImagePreprocessing import ImagePreprocessing


class Transpose(ImagePreprocessing):
    """
    Transpose la séquence
    """
    def __init__(self, **kwargs):
        pass

    def preprocessing(self, image):
        return image.transpose()

    def __str__(self):
        return "Transpose"
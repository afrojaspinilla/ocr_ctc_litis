import os

os.environ["KERAS_BACKEND"] = "tensorflow"
from ocr_ctc.main_evalRNN_without_h5 import load_model
from new_utils.page_analyzer import analyse_page
from new_utils.settings import now
from keras.optimizers import Adam
from tqdm import tqdm
import shutil
import sys
import glob
import logging
import time


# Defininf the Logger
log_output_dir = "log_output"
if not os.path.exists(log_output_dir):
    os.makedirs(log_output_dir)
log_file_name = 'hbdex_ocr_log_file_'+time.strftime("%Yy%mm%dd-%Hh%Mm%Ss")+'_pid-'+str(os.getpid())+'.log'
log_file_path = os.path.join(log_output_dir, log_file_name)
logging.basicConfig(filename=log_file_path, level=logging.DEBUG)

temp_output_folder = "temp_folder/"
if not os.path.exists(temp_output_folder):
    os.makedirs(temp_output_folder)

def load_ocr(log_path, file_weights, dir_data, dataset):
    """
    Principal Function for loading the OCR, it returns a network ready for use
    """
    print("loading the ocr model...")
    logging.info(now()+'loading the ocr model...')
    network = load_model(
        log_path,
        file_weights,
        dir_data,
        dataset,
        rnn_size=[100, 100, 100, 100],
        dir_dataset_train=None,
        greedy_decode=False,
        top_paths_decode=1,
        beam_width_decode=100,
        optimizer=Adam(lr=0.0001),
        dropout=0.1,
        noise_stddev=0.01,
    )
    print("done!")
    logging.info(now()+'ocr model loading done!')
    return network


def prepare_jsons_and_images(folder_with_jsons, folder_with_images, images_format):
    """
    This function reads the folder declared in "folder_with_jsons" and tries to
    find the image with the same name in the images_folder
    """
    logging.info(now()+'preparing jsons with their corresponding images')
    res = []
    for entry in os.scandir(folder_with_jsons):
        if not ".json" in entry.name.lower():
            continue
        corresponding_image = get_image_file(
            entry.name, folder_with_images, images_format
        )
        if corresponding_image == "":
            logging.info(now()+'image not found for the json: '+entry.name)
            continue
        res.append([entry.path, corresponding_image])
    return res


def get_image_file(json_file, folder_with_images, images_format):
    """
    This function takes the json file name, and verifies inside the images_folder
    that the image exists, if not, nothing is returned
    """
    res = ""
    page_name = json_file.split(".")[0]
    image_path = os.path.join(folder_with_images, page_name + "." + images_format)
    if os.path.exists(image_path):
        res = image_path
    else:
        image_path = os.path.join(
            folder_with_images, page_name + "." + images_format.upper()
        )
        if os.path.exists(image_path):
            res = image_path
    return res


def main(network, jsons_and_images_list, json_output_folder, fst_folder='fst_folder', use_title_grammar=False, use_grammar=False):
    """
    The main function, it takes as arguments a loaded network and a list of
    [json_path, image_path] if the image doesnt exist, the function ananlyse_page
    will search the image in the FTP server.

    If there is a problem with a Json, the function will automatically ignore it
    and continue with the next json, and the bad json will be copied to a
    directory called 'bad_jsons'

    During the treatment of a Json, many temporal files will be created in the
    temp_folder, so after a json is finished, any temporal data will be deleted
    for that json

    Important!!! If used for first time, dont ignore exception or you wont see
    which modules are missing to install
    """
    for json_path, image_path in tqdm(jsons_and_images_list):
        page_name = json_path.rsplit("/", 1)[-1].split(".", 1)[0]
        print("-> -> -> DOING: ", page_name)
        output_file = os.path.join(json_output_folder, page_name + ".withOCR.json")

        logging.info("<--ignore\n\n")
        if os.path.exists(output_file):
            logging.error(now()+'output json already exists: '+str(output_file))
            continue
        logging.info(now()+'Starting treatment for: '+json_path)
        logging.info(now()+'using the image: '+ image_path)
        start_time = time.time()
        try:
            analyse_page(
                network, json_path, image_path, temp_output_folder, json_output_folder,\
                fst_folder=fst_folder, use_title_grammar=use_title_grammar, use_grammar=use_grammar
            )
        except Exception as e:
            logging.error(e, exc_info=True)
        logging.info(now()+'********* image analysed, seconds taken: '+str(time.time()-start_time))

        # try:
        #     analyse_page(network, json_path, image_path, temp_output_folder, json_output_folder)
        # except Exception as ex:
        #     print("\n\n\n")
        #     print(ex)
        #     print("\n\n\n")
        #     shutil.copy(json_path, 'bad_jsons/')
        page_name = json_path.rsplit("/", 1)[-1].split(".", 1)[0]
        temp_page_folder = os.path.join(temp_output_folder, page_name)
        if os.path.exists(temp_page_folder):
            logging.info(now()+'deleting the temp_folder of the image: '+str(temp_page_folder))
            shutil.rmtree(temp_page_folder, ignore_errors=True)


def delete_all_in_folder(the_folder):
    """
    This function deletes everything inside the given directory
    """
    for entry in os.scandir(the_folder):
        f = entry.path
        if os.path.isfile(f):
            os.remove(f)
        elif os.path.isdir(f):
            shutil.rmtree(f, ignore_errors=True)


def run_all(
    folder_with_jsons,
    folder_with_images,
    json_output_folder,
    images_format,
    log_path,
    file_weights,
    fst_folder='fst_folder',
    use_title_grammar=False,
    use_grammar=False,
    dir_data="",
    dataset="",
):
    try:
        network = load_ocr(log_path, file_weights, dir_data, dataset)
        jsons_and_images_list = prepare_jsons_and_images(
            folder_with_jsons, folder_with_images, images_format
        )
        main(network, jsons_and_images_list, json_output_folder, fst_folder, use_title_grammar, use_grammar)
    except Exception as e:
        logging.error(e, exc_info=True)
